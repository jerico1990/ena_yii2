var map;
require([
	"esri/views/MapView",
	"esri/Map",
	"esri/layers/FeatureLayer",
	"esri/widgets/Expand"
], function(MapView, Map, FeatureLayer, Expand) {
	const listNode = document.getElementById("list_graphics");
	const seasonsNodes = document.querySelectorAll(`.season-item`);
	const seasonsElement = document.getElementById("seasons-filter");
	let layer, view; //map, 
	let selectedSeason = null;
	let floodLayerView;
	let graphics = null;
	let nodeclic=0;
	// functions
	var xcod;
	//var local = document.location.href;
	/* var getStringL = local.split('?ruc=')[1];
	if (getStringL == "local") {
		var getStringL = '<?php json_encode($ruc) ?>';
	} */
	
	var getStringL = document.getElementById("ruc").value;
	console.log(getStringL);
	xcod = getStringL.substring(0,11);
	//xnew = getStringL.substring(16,18);
	//const filterBySeason = function (event) {
	const filterBySeason = function () {
		view.center = [-79, -8.2];  
		view.zoom = 7;  
		//selectedSeason = event.target.getAttribute("data-season");
		//selectedSeason = "20131589086";//"20132292109";
		selectedSeason=xcod;
		floodLayerView.filter = {
		//where: "Season = '" + selectedSeason + "'"
			where: "RUC = '" + selectedSeason + "'"
		};
		updateList();
		//view.goTo(layerView.geometry.extent.expand(2));  //Agregado
	};
	const updateList = function () {
		if (!graphics) {
		return;
		}
		const fragment = document.createDocumentFragment();
		graphics.forEach(function(result, index) {
		const attributes = result.attributes;
		//if (!selectedSeason || attributes.SEASON ===  selectedSeason) {
		if (!selectedSeason || attributes.RUC ===  selectedSeason) {
			//const name = attributes.IssueDate;
			const name = attributes.GEOCODIGO_FDO;
			// Create the list
			const li = document.createElement("li");
			li.classList.add("panel-result");
			li.tabIndex = 0;
			li.setAttribute("data-result-id", index);
			li.textContent = name;
			fragment.appendChild(li);
		}
		});
		// Empty the current list
		listNode.innerHTML = "";
		listNode.appendChild(fragment);
		//view.goTo(tt.geometry.extent.expand(2));
	};

	const popupTemplate = {
		// autocasts as new PopupTemplate()
		//title: "{NAME} in {COUNTY}",
		title: "{RAZON_SOC}",
		content: [
		{
			type: "fields",
			fieldInfos: [
			{
				fieldName: "RUC",
				label: "RUC",
				format: {
				places: 0//,
				//digitSeparator: true
				}
			},
			{
				fieldName: "GEOCODIGO_FDO",
				label: "GEOCODIGO"//,
				//format: {
				//  places: 0
				//}
			},
			{
				fieldName: "AREA_HA_Z17",
				label: "Area (Ha.)",
				format: {
				places: 2,
				digitSeparator: true
				}
			},
			{
				fieldName: "NRO_FDO",
				label: "Nro Fundo",
				format: {
				places: 0//,
				//digitSeparator: true
				}
			}
			]
		}
		]
	};		
	renderer = {
		//label: value.name,
		type: "simple", // autocasts as new SimpleRenderer()
		symbol: {
			type: "simple-fill",
			style: "none",
			outline: {
				cap: "round",
				join: "round",
				color: [255, 255, 0, 1],
				width: 1,
			},
		},
	};
	layer = new FeatureLayer({
		url: "https://winlmprap09.midagri.gob.pe/winlmprap15/rest/services/L13_Piloto/L13_MuestraMarcoLista/MapServer/2",
		outFields: ["RUC", "GEOCODIGO_FDO"], // used by queryFeatures
		popupTemplate: popupTemplate,
		renderer: renderer
	});

	map = new Map({
		basemap: "hybrid",
		layers: [layer]
	});
	view = new MapView({
		map: map,
		container: "viewDiv",
		center: [-79, -8.2],
		zoom: 7
	});
// click event handler for seasons choices
//seasonsElement.addEventListener("click", filterBySeason);

	view.whenLayerView(layer).then(function(layerView) {
		/*
		filter
		*/
		floodLayerView = layerView;
		filterBySeason();
		// set up UI items
		//seasonsElement.style.visibility = "visible";
		const seasonsExpand = new Expand({
		view: view,
		content: seasonsElement,
		expandIconClass: "esri-icon-filter",
		group: "top-left",
		expanded: true
		});
		//clear the filters when user closes the expand widget
		seasonsExpand.watch("expanded", function() {
		if (!seasonsExpand.expanded) {
			//floodLayerView.filter = null;
		}
		});
		//view.ui.add(seasonsExpand, "top-left");
		view.ui.add("titleDiv", "bottom-left");

		/*
		query      
		*/
		layerView.watch("updating", function(value) {
		if (!value) {
			if (nodeclic===0){
			// wait for the layer view to finish updating
			// query all the features available for drawing.
			layerView
			.queryFeatures({
				geometry: view.extent,
				returnGeometry: true,
				orderByFields: ["GEOCODIGO_FDO"]
			})
			.then(function (results) {
				graphics = results.features;
				updateList();
			})
			.catch(function(error) {
				console.error("query failed: ", error);
			});
			}else{
			nodeclic=0;
			}
		}
		});
	});
/*

query

*/
// listen to click event on list items

	listNode.addEventListener("click", onListClickHandler);
	function onListClickHandler(event) {
		nodeclic=1;
		const target = event.target;
		const resultId = target.getAttribute("data-result-id");
		// get the graphic corresponding to the clicked item
		const result =
		resultId && graphics && graphics[parseInt(resultId, 7)];
		if (result) {
		// open the popup at the centroid of polygon
		// and set the popup's features which will populate popup content and title.
		view
			.goTo(result.geometry.extent.expand(2))
			.then(function() {
			view.popup.open({
				features: [result],
				location: result.geometry.centroid
			});
			})
			.catch(function(error) {
			if (error.name != "AbortError") {
				console.error(error);
			}
			});
		}
	};
//seasonsElement.addEventListener("click", filterBySeason);
//seasonsElement.addEventListener("load", filterBySeason);
//seasonsElement.click();
});