<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\CapituloXInfraestructuras;


class CapituloxController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        echo 'no permitido';
        return;
    }
    
    public function actionCreateCapituloXInfraestructuras($id_encuesta = null,$geocodigo_fdo=null){
        $this->layout='privado';
        //$cap_ix = (new \yii\db\Query())->select('ID_CAP_IX')->from('ENA_TM_DET_CAP_X')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
        // if($cap_iv_mod_a['ID_CAP_IV_A']==null){
            
        // }

        $request = Yii::$app->request;
        $model = new CapituloXInfraestructuras;
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $connection = \Yii::$app->db;
                $transaction = $connection->beginTransaction();
                try {
                    $seq = (new \yii\db\Query())->select('ENA_TM_DET_CAP_X_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();
                    $commandCapX = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_X (
                        ID_CAP_X,
                        ID_ENCUESTA,
                        ID_INFRAESTRUCTURA,
                        NUM_AREA_INFRAESTRUCTURA,
                        TIP_ESTADO,
                        TXT_OTRO
                        ) values (
                        :ID_CAP_X,
                        :ID_ENCUESTA,
                        :ID_INFRAESTRUCTURA,
                        :NUM_AREA_INFRAESTRUCTURA,
                        :TIP_ESTADO,
                        :TXT_OTRO
                        )');
                    $commandCapX->bindParam(':ID_CAP_X',$seq['contador']);
                    $commandCapX->bindParam(':ID_ENCUESTA',$id_encuesta);
                    $commandCapX->bindParam(':ID_INFRAESTRUCTURA',$model->cap10_p1001);
                    $commandCapX->bindParam(':NUM_AREA_INFRAESTRUCTURA',$model->cap10_p1002);
                    $commandCapX->bindParam(':TIP_ESTADO',$model->cap10_p1003);
                    $commandCapX->bindParam(':TXT_OTRO',$model->cap10_p1001_888_especifique);
                    $commandCapX->execute();

                    $transaction->commit();
                    return ['success'=>true,'id_cap_x'=>$seq['contador']];
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    throw $e;
                }
            }
        }
        return $this->render('create-capitulo-x-infraestructuras',['geocodigo_fdo'=>$geocodigo_fdo]);
    }

    public function actionUpdateCapituloXInfraestructuras($id_cap_x = null,$geocodigo_fdo=null){
        $this->layout='privado';
        $cap_x = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_X')->where('ID_CAP_X=:ID_CAP_X',[':ID_CAP_X'=>$id_cap_x])->one();


        $request = Yii::$app->request;
        $model = new CapituloXInfraestructuras;
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $connection = \Yii::$app->db;
                $transaction = $connection->beginTransaction();
                try {
                    
                    $commandCapX = $connection->createCommand('UPDATE ENA_TM_DET_CAP_X SET
                        ID_INFRAESTRUCTURA=:ID_INFRAESTRUCTURA,
                        NUM_AREA_INFRAESTRUCTURA=:NUM_AREA_INFRAESTRUCTURA,
                        TIP_ESTADO=:TIP_ESTADO,
                        TXT_OTRO=:TXT_OTRO
                        WHERE ID_CAP_X=:ID_CAP_X');
                    $commandCapX->bindParam(':ID_CAP_X',$id_cap_x);
                    $commandCapX->bindParam(':ID_INFRAESTRUCTURA',$model->cap10_p1001);
                    $commandCapX->bindParam(':NUM_AREA_INFRAESTRUCTURA',$model->cap10_p1002);
                    $commandCapX->bindParam(':TIP_ESTADO',$model->cap10_p1003);
                    $commandCapX->bindParam(':TXT_OTRO',$model->cap10_p1001_888_especifique);
                    $commandCapX->execute();

                    $transaction->commit();
                    return ['success'=>true];
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    throw $e;
                }
            }
        }
        return $this->render('update-capitulo-x-infraestructuras',['cap_x'=>$cap_x,'geocodigo_fdo'=>$geocodigo_fdo]);
    }

    public function actionViewCapituloXInfraestructuras($id_cap_x = null){
        $this->layout='privado';
        $cap_x = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_X')->where('ID_CAP_X=:ID_CAP_X',[':ID_CAP_X'=>$id_cap_x])->one();

        return $this->render('view-capitulo-x-infraestructuras',['cap_x'=>$cap_x]);
    }

    public function actionGetListaInfraestructuras(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $infraestructuras = (new \yii\db\Query())
                ->select('*')
                ->from('ENA_TG_INFRAESTRUCTURA')
                ->orderBy('TXT_DESCRIPCION ASC')
                ->all();
            return ['success'=>true,'infraestructuras'=>$infraestructuras];
        }
    }

    public function actionEliminarCapx(){
        $this->layout='privado';
        if($_POST){
            $id_cap_x = $_POST['id_cap_x'];

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {
                

                $commandCapX = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_X WHERE ID_CAP_X=:ID_CAP_X');
                $commandCapX->bindParam(':ID_CAP_X',$id_cap_x);
                $commandCapX->execute();
                 
                $transaction->commit();
                return ['success'=>true];
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
        }
    }
}
