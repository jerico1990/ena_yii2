<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;


class CentroPobladoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        $this->layout='privado';
        echo 'retorno';
        return;
    }


    public function actionGetCentrosPoblados(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $cod_ubigeo = $_POST['cod_ubigeo'];

            $centros_poblados = (new \yii\db\Query())
                ->select('COD_CCPP,TXT_CCPP')
                ->from('ENA_TG_CENTRO_POBLADO')
                ->where('COD_UBIGEO=:COD_UBIGEO',[':COD_UBIGEO'=>$cod_ubigeo])
                ->groupBy('COD_CCPP,TXT_CCPP')
                ->orderBy('TXT_CCPP asc')
                ->all();
            return ['success'=>true,'centros_poblados'=>$centros_poblados];
        }
    }

}
