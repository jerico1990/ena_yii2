<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;


class UnidadMedidaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        $this->layout='privado';
        echo 'retorno';
        return;
    }


    public function actionGetListaUnidadesMedidasAreas(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $unidades_medidas = (new \yii\db\Query())
                ->select('ID_UM_AREA ID_UM,TXT_DESCRIPCION')
                ->from('ENA_TG_UM_AREA')
                ->orderBy('TXT_DESCRIPCION ASC')
                ->all();
            return ['success'=>true,'unidades_medidas'=>$unidades_medidas];
        }
    }

    public function actionGetListaUnidadesMedidasPesos(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $unidades_medidas = (new \yii\db\Query())
                ->select('ID_UM_KG ID_UM,TXT_DESCRIPCION')
                ->from('ENA_TG_UM_KG')
                ->orderBy('TXT_DESCRIPCION ASC')
                ->all();
            return ['success'=>true,'unidades_medidas'=>$unidades_medidas];
        }
    }

    public function actionGetListaUnidadesMedidasPecuario(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $unidades_medidas = (new \yii\db\Query())
                ->select('COD_UM_PECUARIA ID_UM,TXT_DESCRIPCION')
                ->from('ENA_TG_UM_PECUARIA')
                ->orderBy('TXT_DESCRIPCION ASC')
                ->all();
            return ['success'=>true,'unidades_medidas'=>$unidades_medidas];
        }
    }

}
