<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\CapituloIXMaquinarias;


class CapituloixController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        echo 'no permitido';
        return;
    }
    
    public function actionCreateCapituloIxMaquinarias($id_encuesta = null,$geocodigo_fdo=null){
        $this->layout='privado';
        $cap_ix = (new \yii\db\Query())->select('ID_CAP_IX')->from('ENA_TM_CAP_IX')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
        // if($cap_iv_mod_a['ID_CAP_IV_A']==null){
            
        // }

        $request = Yii::$app->request;
        $model = new CapituloIXMaquinarias;
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $connection = \Yii::$app->db;
                $transaction = $connection->beginTransaction();
                try {
                    $seq = (new \yii\db\Query())->select('ENA_TM_DET_CAP_IX_MAQUINARIA_EQUIPO_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();
                    $commandCapIx = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IX_MAQUINARIA_EQUIPO (
                        ID_MAQUINARIA_EQUIPO,
                        ID_CAP_IX,
                        ID_TIPO_MAQUINARIA_EQUIPO,
                        TXT_OTRO_MAQUINARIA,
                        NUM_POTENCIA,
                        ID_TENENCIA,
                        TXT_OTRO_TENENCIA,
                        NUM_VALOR_S_H,
                        NUM_VALOR_S_HA,
                        NUM_ANTIGUEDAD,
                        TIP_ESTADO
                        ) values (
                        :ID_MAQUINARIA_EQUIPO,
                        :ID_CAP_IX,
                        :ID_TIPO_MAQUINARIA_EQUIPO,
                        :TXT_OTRO_MAQUINARIA,
                        :NUM_POTENCIA,
                        :ID_TENENCIA,
                        :TXT_OTRO_TENENCIA,
                        :NUM_VALOR_S_H,
                        :NUM_VALOR_S_HA,
                        :NUM_ANTIGUEDAD,
                        :TIP_ESTADO
                        )');
                    $commandCapIx->bindParam(':ID_MAQUINARIA_EQUIPO',$seq['contador']);
                    $commandCapIx->bindParam(':ID_CAP_IX',$cap_ix['ID_CAP_IX']);
                    $commandCapIx->bindParam(':ID_TIPO_MAQUINARIA_EQUIPO',$model->cap9_p902);
                    $commandCapIx->bindParam(':TXT_OTRO_MAQUINARIA',$model->cap9_p902_888_889_especifique);
                    $commandCapIx->bindParam(':NUM_POTENCIA',$model->cap9_p903);
                    $commandCapIx->bindParam(':ID_TENENCIA',$model->cap9_p904);
                    $commandCapIx->bindParam(':TXT_OTRO_TENENCIA',$model->cap9_p904_5_especifique);
                    $commandCapIx->bindParam(':NUM_VALOR_S_H',$model->cap9_p905_a);
                    $commandCapIx->bindParam(':NUM_VALOR_S_HA',$model->cap9_p905_b);
                    $commandCapIx->bindParam(':NUM_ANTIGUEDAD',$model->cap9_p906);
                    $commandCapIx->bindParam(':TIP_ESTADO',$model->cap9_p907);
                    $commandCapIx->execute();

                    $transaction->commit();
                    return ['success'=>true,'id_maquinaria_equipo'=>$seq['contador']];
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    throw $e;
                }
            }
        }
        return $this->render('create-capitulo-ix-maquinarias',['geocodigo_fdo'=>$geocodigo_fdo]);
    }


    public function actionUpdateCapituloIxMaquinarias($id_maquinaria_equipo = null,$geocodigo_fdo=null){
        $this->layout='privado';
        $cap_ix_maquinaria = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_IX_MAQUINARIA_EQUIPO')->where('ID_MAQUINARIA_EQUIPO=:ID_MAQUINARIA_EQUIPO',[':ID_MAQUINARIA_EQUIPO'=>$id_maquinaria_equipo])->one();

        // $cap_iv_mod_a = (new \yii\db\Query())->select('ID_CAP_IV_A')->from('ENA_TM_CAP_IV_A')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
        // if($cap_iv_mod_a['ID_CAP_IV_A']==null){
            
        // }

        $request = Yii::$app->request;
        $model = new CapituloIXMaquinarias;
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $connection = \Yii::$app->db;
                $transaction = $connection->beginTransaction();
                try {
                    
                    $commandCapIx = $connection->createCommand('UPDATE ENA_TM_DET_CAP_IX_MAQUINARIA_EQUIPO SET
                        ID_TIPO_MAQUINARIA_EQUIPO=:ID_TIPO_MAQUINARIA_EQUIPO,
                        TXT_OTRO_MAQUINARIA=:TXT_OTRO_MAQUINARIA,
                        NUM_POTENCIA=:NUM_POTENCIA,
                        ID_TENENCIA=:ID_TENENCIA,
                        TXT_OTRO_TENENCIA=:TXT_OTRO_TENENCIA,
                        NUM_VALOR_S_H=:NUM_VALOR_S_H,
                        NUM_VALOR_S_HA=:NUM_VALOR_S_HA,
                        NUM_ANTIGUEDAD=:NUM_ANTIGUEDAD,
                        TIP_ESTADO=:TIP_ESTADO
                        WHERE ID_MAQUINARIA_EQUIPO=:ID_MAQUINARIA_EQUIPO');
                    $commandCapIx->bindParam(':ID_MAQUINARIA_EQUIPO',$id_maquinaria_equipo);
                    $commandCapIx->bindParam(':ID_TIPO_MAQUINARIA_EQUIPO',$model->cap9_p902);
                    $commandCapIx->bindParam(':TXT_OTRO_MAQUINARIA',$model->cap9_p902_888_889_especifique);
                    $commandCapIx->bindParam(':NUM_POTENCIA',$model->cap9_p903);
                    $commandCapIx->bindParam(':ID_TENENCIA',$model->cap9_p904);
                    $commandCapIx->bindParam(':TXT_OTRO_TENENCIA',$model->cap9_p904_5_especifique);
                    $commandCapIx->bindParam(':NUM_VALOR_S_H',$model->cap9_p905_a);
                    $commandCapIx->bindParam(':NUM_VALOR_S_HA',$model->cap9_p905_b);
                    $commandCapIx->bindParam(':NUM_ANTIGUEDAD',$model->cap9_p906);
                    $commandCapIx->bindParam(':TIP_ESTADO',$model->cap9_p907);
                    $commandCapIx->execute();

                    $transaction->commit();
                    return ['success'=>true];
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    throw $e;
                }
            }
        }
        return $this->render('update-capitulo-ix-maquinarias',['cap_ix_maquinaria'=>$cap_ix_maquinaria,'geocodigo_fdo'=>$geocodigo_fdo]);
    }

    public function actionViewCapituloIxMaquinarias($id_maquinaria_equipo = null){
        $this->layout='privado';
        $cap_ix_maquinaria = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_IX_MAQUINARIA_EQUIPO')->where('ID_MAQUINARIA_EQUIPO=:ID_MAQUINARIA_EQUIPO',[':ID_MAQUINARIA_EQUIPO'=>$id_maquinaria_equipo])->one();

        return $this->render('view-capitulo-ix-maquinarias',['cap_ix_maquinaria'=>$cap_ix_maquinaria]);
    }

    
    public function actionGetListaMaquinarias(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $maquinarias = (new \yii\db\Query())
                ->select('*')
                ->from('ENA_TG_MAQ_EQUIP')
                ->orderBy('TXT_DESCRIPCION ASC')
                ->all();
            return ['success'=>true,'maquinarias'=>$maquinarias];
        }
    }

    public function actionEliminarMaquinariaEquipo(){
        $this->layout='privado';
        if($_POST){
            $id_maquinaria_equipo = $_POST['id_maquinaria_equipo'];

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {
                

                $commandCapIx = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_IX_MAQUINARIA_EQUIPO WHERE ID_MAQUINARIA_EQUIPO=:ID_MAQUINARIA_EQUIPO');
                $commandCapIx->bindParam(':ID_MAQUINARIA_EQUIPO',$id_maquinaria_equipo);
                $commandCapIx->execute();
                 
                $transaction->commit();
                return ['success'=>true];
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
        }
    }
}
