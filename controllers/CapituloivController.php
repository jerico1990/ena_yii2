<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\CapituloIVModuloA;
use app\models\CapituloIVModuloB;
use app\models\CapituloIVModuloC;

class CapituloivController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        echo 'no permitido';
        return;
    }

    public function actionCreateCapituloIvModuloA($id_encuesta = null,$geocodigo_fdo=null){
        $this->layout='privado';

        $request = Yii::$app->request;
        $model = new CapituloIVModuloA;
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $connection = \Yii::$app->db;
                $transaction = $connection->beginTransaction();
                try {
                    
                    $seq = (new \yii\db\Query())->select('ENA_TM_CAP_IV_A_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();
                    $commandCapIvModA = $connection->createCommand('INSERT INTO ENA_TM_CAP_IV_A (
                        ID_CAP_IV_A,
                        ID_ENCUESTA,
                        NUM_LOTE,
                        ID_TIEMPO_CULTIVO,
                        ID_CULTIVO,
                        TXT_MES_SIEMBRA,
                        TXT_ANIO_SIEMBRA,
                        ID_CULTIVO_SOLO_ASOCIADO,
                        NUM_AREA_SEMBRADA,
                        ID_UM_SEMBRADA,
                        NUM_EQ_SEMBRADA,
                        NUM_PLANTONES,
                        ID_FINALIDAD_PLANTACION,
                        TXT_MES_INICIO_COSECHA,
                        TXT_ANIO_INICIO_COSECHA,
                        TXT_MES_FINAL_COSECHA,
                        TXT_ANIO_FINAL_COSECHA,
                        FLG_COSECHA_ENE,
                        FLG_COSECHA_FEB,
                        FLG_COSECHA_MAR,
                        FLG_COSECHA_ABR,
                        FLG_COSECHA_MAY,
                        FLG_COSECHA_JUN,
                        FLG_COSECHA_JUL,
                        FLG_COSECHA_AGO,
                        FLG_COSECHA_SET,
                        FLG_COSECHA_OCT,
                        FLG_COSECHA_NOV,
                        FLG_COSECHA_DIC,
                        NUM_AREA_COSECHADA,
                        ID_UM_COSECHADA,
                        NUM_EQ_COSECHADA,
                        NUM_PORC_AREA_PERDIDA,
                        NUM_PORC_AREA_AFECTADA,
                        ID_MODO_COSECHA,
                        NUM_CANTIDAD_PRODUCCION,
                        ID_UM_PRODUCCION,
                        NUM_EQ_PRODUCCION,
                        ID_ESPECIFICACION_PRODUCTO,
                        NUM_PRODUCCION_CULTIVO,
                        NUM_RENDIMIENTO,
                        FLG_PRODUCCION_CALIDAD,
                        NUM_CANTIDAD_CALIDAD,
                        TXT_OTRO_CULTIVO
                        ) values (
                        :ID_CAP_IV_A,
                        :ID_ENCUESTA,
                        :NUM_LOTE,
                        :ID_TIEMPO_CULTIVO,
                        :ID_CULTIVO,
                        :TXT_MES_SIEMBRA,
                        :TXT_ANIO_SIEMBRA,
                        :ID_CULTIVO_SOLO_ASOCIADO,
                        :NUM_AREA_SEMBRADA,
                        :ID_UM_SEMBRADA,
                        :NUM_EQ_SEMBRADA,
                        :NUM_PLANTONES,
                        :ID_FINALIDAD_PLANTACION,
                        :TXT_MES_INICIO_COSECHA,
                        :TXT_ANIO_INICIO_COSECHA,
                        :TXT_MES_FINAL_COSECHA,
                        :TXT_ANIO_FINAL_COSECHA,
                        :FLG_COSECHA_ENE,
                        :FLG_COSECHA_FEB,
                        :FLG_COSECHA_MAR,
                        :FLG_COSECHA_ABR,
                        :FLG_COSECHA_MAY,
                        :FLG_COSECHA_JUN,
                        :FLG_COSECHA_JUL,
                        :FLG_COSECHA_AGO,
                        :FLG_COSECHA_SET,
                        :FLG_COSECHA_OCT,
                        :FLG_COSECHA_NOV,
                        :FLG_COSECHA_DIC,
                        :NUM_AREA_COSECHADA,
                        :ID_UM_COSECHADA,
                        :NUM_EQ_COSECHADA,
                        :NUM_PORC_AREA_PERDIDA,
                        :NUM_PORC_AREA_AFECTADA,
                        :ID_MODO_COSECHA,
                        :NUM_CANTIDAD_PRODUCCION,
                        :ID_UM_PRODUCCION,
                        :NUM_EQ_PRODUCCION,
                        :ID_ESPECIFICACION_PRODUCTO,
                        :NUM_PRODUCCION_CULTIVO,
                        :NUM_RENDIMIENTO,
                        :FLG_PRODUCCION_CALIDAD,
                        :NUM_CANTIDAD_CALIDAD,
                        :TXT_OTRO_CULTIVO
                        )');
                    $commandCapIvModA->bindParam(':ID_CAP_IV_A',$seq['contador']);
                    $commandCapIvModA->bindParam(':ID_ENCUESTA',$id_encuesta);
                    $commandCapIvModA->bindParam(':NUM_LOTE',$model->cap4_moda_p401_a);
                    $commandCapIvModA->bindParam(':ID_TIEMPO_CULTIVO',$model->cap4_moda_p401);
                    $commandCapIvModA->bindParam(':ID_CULTIVO',$model->cap4_moda_p402);
                    $commandCapIvModA->bindParam(':TXT_OTRO_CULTIVO',$model->cap4_moda_p402_9999_especifique);

                    
                    $commandCapIvModA->bindParam(':TXT_MES_SIEMBRA',$model->cap4_moda_p403_mes);
                    $commandCapIvModA->bindParam(':TXT_ANIO_SIEMBRA',$model->cap4_moda_p403_anio);
                    $commandCapIvModA->bindParam(':ID_CULTIVO_SOLO_ASOCIADO',$model->cap4_moda_p404);
                    $commandCapIvModA->bindParam(':NUM_AREA_SEMBRADA',$model->cap4_moda_p405_a);
                    $commandCapIvModA->bindParam(':ID_UM_SEMBRADA',$model->cap4_moda_p405_b);
                    $commandCapIvModA->bindParam(':NUM_EQ_SEMBRADA',$model->cap4_moda_p405_c);
                    $commandCapIvModA->bindParam(':NUM_PLANTONES',$model->cap4_moda_p406);
                    $commandCapIvModA->bindParam(':ID_FINALIDAD_PLANTACION',$model->cap4_moda_p409);
                    $commandCapIvModA->bindParam(':TXT_MES_INICIO_COSECHA',$model->cap4_moda_p410_a_mes);
                    $commandCapIvModA->bindParam(':TXT_ANIO_INICIO_COSECHA',$model->cap4_moda_p410_a_anio);
                    $commandCapIvModA->bindParam(':TXT_MES_FINAL_COSECHA',$model->cap4_moda_p410_b_mes);
                    $commandCapIvModA->bindParam(':TXT_ANIO_FINAL_COSECHA',$model->cap4_moda_p410_b_anio);
                    $commandCapIvModA->bindParam(':FLG_COSECHA_ENE',$model->cap4_moda_p411_1);
                    $commandCapIvModA->bindParam(':FLG_COSECHA_FEB',$model->cap4_moda_p411_2);
                    $commandCapIvModA->bindParam(':FLG_COSECHA_MAR',$model->cap4_moda_p411_3);
                    $commandCapIvModA->bindParam(':FLG_COSECHA_ABR',$model->cap4_moda_p411_4);
                    $commandCapIvModA->bindParam(':FLG_COSECHA_MAY',$model->cap4_moda_p411_5);
                    $commandCapIvModA->bindParam(':FLG_COSECHA_JUN',$model->cap4_moda_p411_6);
                    $commandCapIvModA->bindParam(':FLG_COSECHA_JUL',$model->cap4_moda_p411_7);
                    $commandCapIvModA->bindParam(':FLG_COSECHA_AGO',$model->cap4_moda_p411_8);
                    $commandCapIvModA->bindParam(':FLG_COSECHA_SET',$model->cap4_moda_p411_9);
                    $commandCapIvModA->bindParam(':FLG_COSECHA_OCT',$model->cap4_moda_p411_10);
                    $commandCapIvModA->bindParam(':FLG_COSECHA_NOV',$model->cap4_moda_p411_11);
                    $commandCapIvModA->bindParam(':FLG_COSECHA_DIC',$model->cap4_moda_p411_12);
                    $commandCapIvModA->bindParam(':NUM_AREA_COSECHADA',$model->cap4_moda_p412_a);
                    $commandCapIvModA->bindParam(':ID_UM_COSECHADA',$model->cap4_moda_p412_b);
                    $commandCapIvModA->bindParam(':NUM_EQ_COSECHADA',$model->cap4_moda_p412_c);
                    $commandCapIvModA->bindParam(':NUM_PORC_AREA_PERDIDA',$model->cap4_moda_p413);
                    $commandCapIvModA->bindParam(':NUM_PORC_AREA_AFECTADA',$model->cap4_moda_p414);
                    $commandCapIvModA->bindParam(':ID_MODO_COSECHA',$model->cap4_moda_p418);
                    $commandCapIvModA->bindParam(':NUM_CANTIDAD_PRODUCCION',$model->cap4_moda_p419_a);
                    $commandCapIvModA->bindParam(':ID_UM_PRODUCCION',$model->cap4_moda_p419_b);
                    $commandCapIvModA->bindParam(':NUM_EQ_PRODUCCION',$model->cap4_moda_p419_c);
                    $commandCapIvModA->bindParam(':ID_ESPECIFICACION_PRODUCTO',$model->cap4_moda_p419_d);
                    $commandCapIvModA->bindParam(':NUM_PRODUCCION_CULTIVO',$model->cap4_moda_p419_e);
                    $commandCapIvModA->bindParam(':NUM_RENDIMIENTO',$model->cap4_moda_p420);
                    $commandCapIvModA->bindParam(':FLG_PRODUCCION_CALIDAD',$model->cap4_moda_p421);
                    $commandCapIvModA->bindParam(':NUM_CANTIDAD_CALIDAD',$model->cap4_moda_p422);
                    $commandCapIvModA->execute();

                    if($model->cap4_moda_p407){
                        $commandCapIvModAP407Delete = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_IV_A_SEM_PLANT WHERE ID_CAP_IV_A=:ID_CAP_IV_A');
                        $commandCapIvModAP407Delete->bindParam(':ID_CAP_IV_A',$seq['contador']);
                        $commandCapIvModAP407Delete->execute();

                        $cap4_moda_p407 = explode(",",$model->cap4_moda_p407);
                        foreach($cap4_moda_p407 as $p407){
                            if($p407=="10"){
                                $commandCapIvModAP407 = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_A_SEM_PLANT (ID_CAP_IV_A,ID_SEMILLAS_PLANTONES,TXT_OTRO) values (:ID_CAP_IV_A,:ID_SEMILLAS_PLANTONES,:TXT_OTRO)');
                                $commandCapIvModAP407->bindParam(':ID_CAP_IV_A',$seq['contador']);
                                $commandCapIvModAP407->bindParam(':ID_SEMILLAS_PLANTONES',$p407);
                                $commandCapIvModAP407->bindParam(':TXT_OTRO',$model->cap4_moda_p407_10_especifique);
                                $commandCapIvModAP407->execute();
                            }else{
                                $commandCapIvModAP407 = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_A_SEM_PLANT (ID_CAP_IV_A,ID_SEMILLAS_PLANTONES) values (:ID_CAP_IV_A,:ID_SEMILLAS_PLANTONES)');
                                $commandCapIvModAP407->bindParam(':ID_CAP_IV_A',$seq['contador']);
                                $commandCapIvModAP407->bindParam(':ID_SEMILLAS_PLANTONES',$p407);
                                $commandCapIvModAP407->execute();
                            }
                        }
                    }

                    if($model->cap4_moda_p408){
                        $commandCapIvModAp408Delete = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_IV_A_SIST_RIEGO WHERE ID_CAP_IV_A=:ID_CAP_IV_A');
                        $commandCapIvModAp408Delete->bindParam(':ID_CAP_IV_A',$seq['contador']);
                        $commandCapIvModAp408Delete->execute();

                        $cap4_moda_p408 = explode(",",$model->cap4_moda_p408);
                        foreach($cap4_moda_p408 as $p408){
                            if($p408=="9"){
                                $commandCapIvModAp408 = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_A_SIST_RIEGO (ID_CAP_IV_A,ID_SISTEMA_RIEGO,TXT_OTRO) values (:ID_CAP_IV_A,:ID_SISTEMA_RIEGO,:TXT_OTRO)');
                                $commandCapIvModAp408->bindParam(':ID_CAP_IV_A',$seq['contador']);
                                $commandCapIvModAp408->bindParam(':ID_SISTEMA_RIEGO',$p408);
                                $commandCapIvModAp408->bindParam(':TXT_OTRO',$model->cap4_moda_p408_9_especifique);
                                $commandCapIvModAp408->execute();
                            }else{
                                $commandCapIvModAp408 = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_A_SIST_RIEGO (ID_CAP_IV_A,ID_SISTEMA_RIEGO) values (:ID_CAP_IV_A,:ID_SISTEMA_RIEGO)');
                                $commandCapIvModAp408->bindParam(':ID_CAP_IV_A',$seq['contador']);
                                $commandCapIvModAp408->bindParam(':ID_SISTEMA_RIEGO',$p408);
                                $commandCapIvModAp408->execute();
                            }
                        }
                    }

                    if($model->cap4_moda_p415){
                        $commandCapIvModAp415Delete = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_IV_A_FEN_NAT WHERE ID_CAP_IV_A=:ID_CAP_IV_A');
                        $commandCapIvModAp415Delete->bindParam(':ID_CAP_IV_A',$seq['contador']);
                        $commandCapIvModAp415Delete->execute();

                        $cap4_moda_p415 = explode(",",$model->cap4_moda_p415);
                        foreach($cap4_moda_p415 as $p415){
                            $commandCapIvModAp415 = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_A_FEN_NAT (ID_CAP_IV_A,ID_FENOMENO_NATURAL) values (:ID_CAP_IV_A,:ID_FENOMENO_NATURAL)');
                            $commandCapIvModAp415->bindParam(':ID_CAP_IV_A',$seq['contador']);
                            $commandCapIvModAp415->bindParam(':ID_FENOMENO_NATURAL',$p415);
                            $commandCapIvModAp415->execute();
                        }
                    }

                    if($model->cap4_moda_p416){
                        $commandCapIvModAp416Delete = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_IV_A_FACTORES_ECONOMICOS WHERE ID_CAP_IV_A=:ID_CAP_IV_A');
                        $commandCapIvModAp416Delete->bindParam(':ID_CAP_IV_A',$seq['contador']);
                        $commandCapIvModAp416Delete->execute();

                        $cap4_moda_p416 = explode(",",$model->cap4_moda_p416);
                        foreach($cap4_moda_p416 as $p416){
                            $commandCapIvModAp416 = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_A_FACTORES_ECONOMICOS (ID_CAP_IV_A,ID_FACTORES_ECONOMICOS) values (:ID_CAP_IV_A,:ID_FACTORES_ECONOMICOS)');
                            $commandCapIvModAp416->bindParam(':ID_CAP_IV_A',$seq['contador']);
                            $commandCapIvModAp416->bindParam(':ID_FACTORES_ECONOMICOS',$p416);
                            $commandCapIvModAp416->execute();
                        }
                    }

                    if($model->cap4_moda_p417){
                        $commandCapIvModAp417Delete = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_IV_A_ACT_MEJORAMIENTO WHERE ID_CAP_IV_A=:ID_CAP_IV_A');
                        $commandCapIvModAp417Delete->bindParam(':ID_CAP_IV_A',$seq['contador']);
                        $commandCapIvModAp417Delete->execute();

                        $cap4_moda_p417 = explode(",",$model->cap4_moda_p417);
                        foreach($cap4_moda_p417 as $p417){
                            $commandCapIvModAp417 = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_A_ACT_MEJORAMIENTO (ID_CAP_IV_A,ID_ACT_MEJORAMIENTO) values (:ID_CAP_IV_A,:ID_ACT_MEJORAMIENTO)');
                            $commandCapIvModAp417->bindParam(':ID_CAP_IV_A',$seq['contador']);
                            $commandCapIvModAp417->bindParam(':ID_ACT_MEJORAMIENTO',$p417);
                            $commandCapIvModAp417->execute();
                        }
                    }


                    if($model->cap4_moda_p427){
                        $commandCapIvModAp427Delete = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_IV_A_MOTIVO_SIEMBRA WHERE ID_CAP_IV_A=:ID_CAP_IV_A');
                        $commandCapIvModAp427Delete->bindParam(':ID_CAP_IV_A',$seq['contador']);
                        $commandCapIvModAp427Delete->execute();

                        $cap4_moda_p427 = explode(",",$model->cap4_moda_p427);
                        foreach($cap4_moda_p427 as $p427){
                            if($p427=="10"){
                                $commandCapIvModAp427 = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_A_MOTIVO_SIEMBRA (ID_CAP_IV_A,ID_MOTIVO_SIEMBRA,TXT_OTRO) values (:ID_CAP_IV_A,:ID_MOTIVO_SIEMBRA,:TXT_OTRO)');
                                $commandCapIvModAp427->bindParam(':ID_CAP_IV_A',$seq['contador']);
                                $commandCapIvModAp427->bindParam(':ID_MOTIVO_SIEMBRA',$p427);
                                $commandCapIvModAp427->bindParam(':TXT_OTRO',$model->cap4_moda_p427_10_especifique);
                                $commandCapIvModAp427->execute();
                            }else{
                                $commandCapIvModAp427 = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_A_MOTIVO_SIEMBRA (ID_CAP_IV_A,ID_MOTIVO_SIEMBRA) values (:ID_CAP_IV_A,:ID_MOTIVO_SIEMBRA)');
                                $commandCapIvModAp427->bindParam(':ID_CAP_IV_A',$seq['contador']);
                                $commandCapIvModAp427->bindParam(':ID_MOTIVO_SIEMBRA',$p427);
                                $commandCapIvModAp427->execute();
                            }
                        }
                    }
                    $transaction->commit();
                    return ['success'=>true,'id_cap_iv_a'=>$seq['contador']];
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    throw $e;
                }
            }
        }
        return $this->render('create-capitulo-iv-modulo-a',['geocodigo_fdo'=>$geocodigo_fdo]);
    }

    public function actionUpdateCapituloIvModuloA($id_cap_iv_a = null,$geocodigo_fdo=null){
        $this->layout='privado';
        // $cap_iv_mod_a = (new \yii\db\Query())->select('ID_CAP_IV_A')->from('ENA_TM_CAP_IV_A')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
        // if($cap_iv_mod_a['ID_CAP_IV_A']==null){
            
        // }

        $request = Yii::$app->request;
        $model = new CapituloIVModuloA;
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $connection = \Yii::$app->db;
                $transaction = $connection->beginTransaction();
                try {
                    
                    $commandCapIvModA = $connection->createCommand('UPDATE ENA_TM_CAP_IV_A SET
                        NUM_LOTE=:NUM_LOTE,
                        ID_TIEMPO_CULTIVO=:ID_TIEMPO_CULTIVO,
                        ID_CULTIVO=:ID_CULTIVO,
                        TXT_MES_SIEMBRA=:TXT_MES_SIEMBRA,
                        TXT_ANIO_SIEMBRA=:TXT_ANIO_SIEMBRA,
                        ID_CULTIVO_SOLO_ASOCIADO=:ID_CULTIVO_SOLO_ASOCIADO,
                        NUM_AREA_SEMBRADA=:NUM_AREA_SEMBRADA,
                        ID_UM_SEMBRADA=:ID_UM_SEMBRADA,
                        NUM_EQ_SEMBRADA=:NUM_EQ_SEMBRADA,
                        NUM_PLANTONES=:NUM_PLANTONES,
                        ID_FINALIDAD_PLANTACION=:ID_FINALIDAD_PLANTACION,
                        TXT_MES_INICIO_COSECHA=:TXT_MES_INICIO_COSECHA,
                        TXT_ANIO_INICIO_COSECHA=:TXT_ANIO_INICIO_COSECHA,
                        TXT_MES_FINAL_COSECHA=:TXT_MES_FINAL_COSECHA,
                        TXT_ANIO_FINAL_COSECHA=:TXT_ANIO_FINAL_COSECHA,
                        FLG_COSECHA_ENE=:FLG_COSECHA_ENE,
                        FLG_COSECHA_FEB=:FLG_COSECHA_FEB,
                        FLG_COSECHA_MAR=:FLG_COSECHA_MAR,
                        FLG_COSECHA_ABR=:FLG_COSECHA_ABR,
                        FLG_COSECHA_MAY=:FLG_COSECHA_MAY,
                        FLG_COSECHA_JUN=:FLG_COSECHA_JUN,
                        FLG_COSECHA_JUL=:FLG_COSECHA_JUL,
                        FLG_COSECHA_AGO=:FLG_COSECHA_AGO,
                        FLG_COSECHA_SET=:FLG_COSECHA_SET,
                        FLG_COSECHA_OCT=:FLG_COSECHA_OCT,
                        FLG_COSECHA_NOV=:FLG_COSECHA_NOV,
                        FLG_COSECHA_DIC=:FLG_COSECHA_DIC,
                        NUM_AREA_COSECHADA=:NUM_AREA_COSECHADA,
                        ID_UM_COSECHADA=:ID_UM_COSECHADA,
                        NUM_EQ_COSECHADA=:NUM_EQ_COSECHADA,
                        NUM_PORC_AREA_PERDIDA=:NUM_PORC_AREA_PERDIDA,
                        NUM_PORC_AREA_AFECTADA=:NUM_PORC_AREA_AFECTADA,
                        ID_MODO_COSECHA=:ID_MODO_COSECHA,
                        NUM_CANTIDAD_PRODUCCION=:NUM_CANTIDAD_PRODUCCION,
                        ID_UM_PRODUCCION=:ID_UM_PRODUCCION,
                        NUM_EQ_PRODUCCION=:NUM_EQ_PRODUCCION,
                        ID_ESPECIFICACION_PRODUCTO=:ID_ESPECIFICACION_PRODUCTO,
                        NUM_PRODUCCION_CULTIVO=:NUM_PRODUCCION_CULTIVO,
                        NUM_RENDIMIENTO=:NUM_RENDIMIENTO,
                        FLG_PRODUCCION_CALIDAD=:FLG_PRODUCCION_CALIDAD,
                        NUM_CANTIDAD_CALIDAD=:NUM_CANTIDAD_CALIDAD,
                        TXT_OTRO_CULTIVO=:TXT_OTRO_CULTIVO
                        WHERE ID_CAP_IV_A=:ID_CAP_IV_A');
                    $commandCapIvModA->bindParam(':ID_CAP_IV_A',$id_cap_iv_a);
                    $commandCapIvModA->bindParam(':NUM_LOTE',$model->cap4_moda_p401_a);
                    $commandCapIvModA->bindParam(':ID_TIEMPO_CULTIVO',$model->cap4_moda_p401);
                    $commandCapIvModA->bindParam(':ID_CULTIVO',$model->cap4_moda_p402);
                    $commandCapIvModA->bindParam(':TXT_OTRO_CULTIVO',$model->cap4_moda_p402_9999_especifique);
                    $commandCapIvModA->bindParam(':TXT_MES_SIEMBRA',$model->cap4_moda_p403_mes);
                    $commandCapIvModA->bindParam(':TXT_ANIO_SIEMBRA',$model->cap4_moda_p403_anio);
                    $commandCapIvModA->bindParam(':ID_CULTIVO_SOLO_ASOCIADO',$model->cap4_moda_p404);
                    $commandCapIvModA->bindParam(':NUM_AREA_SEMBRADA',$model->cap4_moda_p405_a);
                    $commandCapIvModA->bindParam(':ID_UM_SEMBRADA',$model->cap4_moda_p405_b);
                    $commandCapIvModA->bindParam(':NUM_EQ_SEMBRADA',$model->cap4_moda_p405_c);
                    $commandCapIvModA->bindParam(':NUM_PLANTONES',$model->cap4_moda_p406);
                    $commandCapIvModA->bindParam(':ID_FINALIDAD_PLANTACION',$model->cap4_moda_p409);
                    $commandCapIvModA->bindParam(':TXT_MES_INICIO_COSECHA',$model->cap4_moda_p410_a_mes);
                    $commandCapIvModA->bindParam(':TXT_ANIO_INICIO_COSECHA',$model->cap4_moda_p410_a_anio);
                    $commandCapIvModA->bindParam(':TXT_MES_FINAL_COSECHA',$model->cap4_moda_p410_b_mes);
                    $commandCapIvModA->bindParam(':TXT_ANIO_FINAL_COSECHA',$model->cap4_moda_p410_b_anio);
                    $commandCapIvModA->bindParam(':FLG_COSECHA_ENE',$model->cap4_moda_p411_1);
                    $commandCapIvModA->bindParam(':FLG_COSECHA_FEB',$model->cap4_moda_p411_2);
                    $commandCapIvModA->bindParam(':FLG_COSECHA_MAR',$model->cap4_moda_p411_3);
                    $commandCapIvModA->bindParam(':FLG_COSECHA_ABR',$model->cap4_moda_p411_4);
                    $commandCapIvModA->bindParam(':FLG_COSECHA_MAY',$model->cap4_moda_p411_5);
                    $commandCapIvModA->bindParam(':FLG_COSECHA_JUN',$model->cap4_moda_p411_6);
                    $commandCapIvModA->bindParam(':FLG_COSECHA_JUL',$model->cap4_moda_p411_7);
                    $commandCapIvModA->bindParam(':FLG_COSECHA_AGO',$model->cap4_moda_p411_8);
                    $commandCapIvModA->bindParam(':FLG_COSECHA_SET',$model->cap4_moda_p411_9);
                    $commandCapIvModA->bindParam(':FLG_COSECHA_OCT',$model->cap4_moda_p411_10);
                    $commandCapIvModA->bindParam(':FLG_COSECHA_NOV',$model->cap4_moda_p411_11);
                    $commandCapIvModA->bindParam(':FLG_COSECHA_DIC',$model->cap4_moda_p411_12);
                    $commandCapIvModA->bindParam(':NUM_AREA_COSECHADA',$model->cap4_moda_p412_a);
                    $commandCapIvModA->bindParam(':ID_UM_COSECHADA',$model->cap4_moda_p412_b);
                    $commandCapIvModA->bindParam(':NUM_EQ_COSECHADA',$model->cap4_moda_p412_c);
                    $commandCapIvModA->bindParam(':NUM_PORC_AREA_PERDIDA',$model->cap4_moda_p413);
                    $commandCapIvModA->bindParam(':NUM_PORC_AREA_AFECTADA',$model->cap4_moda_p414);
                    $commandCapIvModA->bindParam(':ID_MODO_COSECHA',$model->cap4_moda_p418);
                    $commandCapIvModA->bindParam(':NUM_CANTIDAD_PRODUCCION',$model->cap4_moda_p419_a);
                    $commandCapIvModA->bindParam(':ID_UM_PRODUCCION',$model->cap4_moda_p419_b);
                    $commandCapIvModA->bindParam(':NUM_EQ_PRODUCCION',$model->cap4_moda_p419_c);
                    $commandCapIvModA->bindParam(':ID_ESPECIFICACION_PRODUCTO',$model->cap4_moda_p419_d);
                    $commandCapIvModA->bindParam(':NUM_PRODUCCION_CULTIVO',$model->cap4_moda_p419_e);
                    $commandCapIvModA->bindParam(':NUM_RENDIMIENTO',$model->cap4_moda_p420);
                    $commandCapIvModA->bindParam(':FLG_PRODUCCION_CALIDAD',$model->cap4_moda_p421);
                    $commandCapIvModA->bindParam(':NUM_CANTIDAD_CALIDAD',$model->cap4_moda_p422);
                    $commandCapIvModA->execute();

                    if($model->cap4_moda_p407){
                        $commandCapIvModAP407Delete = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_IV_A_SEM_PLANT WHERE ID_CAP_IV_A=:ID_CAP_IV_A');
                        $commandCapIvModAP407Delete->bindParam(':ID_CAP_IV_A',$id_cap_iv_a);
                        $commandCapIvModAP407Delete->execute();

                        $cap4_moda_p407 = explode(",",$model->cap4_moda_p407);
                        foreach($cap4_moda_p407 as $p407){
                            if($p407=="10"){
                                $commandCapIvModAP407 = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_A_SEM_PLANT (ID_CAP_IV_A,ID_SEMILLAS_PLANTONES,TXT_OTRO) values (:ID_CAP_IV_A,:ID_SEMILLAS_PLANTONES,:TXT_OTRO)');
                                $commandCapIvModAP407->bindParam(':ID_CAP_IV_A',$id_cap_iv_a);
                                $commandCapIvModAP407->bindParam(':ID_SEMILLAS_PLANTONES',$p407);
                                $commandCapIvModAP407->bindParam(':TXT_OTRO',$model->cap4_moda_p407_10_especifique);
                                $commandCapIvModAP407->execute();
                            }else{
                                $commandCapIvModAP407 = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_A_SEM_PLANT (ID_CAP_IV_A,ID_SEMILLAS_PLANTONES) values (:ID_CAP_IV_A,:ID_SEMILLAS_PLANTONES)');
                                $commandCapIvModAP407->bindParam(':ID_CAP_IV_A',$id_cap_iv_a);
                                $commandCapIvModAP407->bindParam(':ID_SEMILLAS_PLANTONES',$p407);
                                $commandCapIvModAP407->execute();
                            }
                        }
                    }

                    if($model->cap4_moda_p408){
                        $commandCapIvModAp408Delete = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_IV_A_SIST_RIEGO WHERE ID_CAP_IV_A=:ID_CAP_IV_A');
                        $commandCapIvModAp408Delete->bindParam(':ID_CAP_IV_A',$id_cap_iv_a);
                        $commandCapIvModAp408Delete->execute();

                        $cap4_moda_p408 = explode(",",$model->cap4_moda_p408);
                        foreach($cap4_moda_p408 as $p408){
                            if($p408=="9"){
                                $commandCapIvModAp408 = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_A_SIST_RIEGO (ID_CAP_IV_A,ID_SISTEMA_RIEGO,TXT_OTRO) values (:ID_CAP_IV_A,:ID_SISTEMA_RIEGO,:TXT_OTRO)');
                                $commandCapIvModAp408->bindParam(':ID_CAP_IV_A',$id_cap_iv_a);
                                $commandCapIvModAp408->bindParam(':ID_SISTEMA_RIEGO',$p408);
                                $commandCapIvModAp408->bindParam(':TXT_OTRO',$model->cap4_moda_p408_9_especifique);
                                $commandCapIvModAp408->execute();
                            }else{
                                $commandCapIvModAp408 = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_A_SIST_RIEGO (ID_CAP_IV_A,ID_SISTEMA_RIEGO) values (:ID_CAP_IV_A,:ID_SISTEMA_RIEGO)');
                                $commandCapIvModAp408->bindParam(':ID_CAP_IV_A',$id_cap_iv_a);
                                $commandCapIvModAp408->bindParam(':ID_SISTEMA_RIEGO',$p408);
                                $commandCapIvModAp408->execute();
                            }
                        }
                    }

                    if($model->cap4_moda_p415){
                        $commandCapIvModAp415Delete = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_IV_A_FEN_NAT WHERE ID_CAP_IV_A=:ID_CAP_IV_A');
                        $commandCapIvModAp415Delete->bindParam(':ID_CAP_IV_A',$id_cap_iv_a);
                        $commandCapIvModAp415Delete->execute();

                        $cap4_moda_p415 = explode(",",$model->cap4_moda_p415);
                        foreach($cap4_moda_p415 as $p415){
                            $commandCapIvModAp415 = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_A_FEN_NAT (ID_CAP_IV_A,ID_FENOMENO_NATURAL) values (:ID_CAP_IV_A,:ID_FENOMENO_NATURAL)');
                            $commandCapIvModAp415->bindParam(':ID_CAP_IV_A',$id_cap_iv_a);
                            $commandCapIvModAp415->bindParam(':ID_FENOMENO_NATURAL',$p415);
                            $commandCapIvModAp415->execute();
                        }
                    }

                    if($model->cap4_moda_p416){
                        $commandCapIvModAp416Delete = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_IV_A_FACTORES_ECONOMICOS WHERE ID_CAP_IV_A=:ID_CAP_IV_A');
                        $commandCapIvModAp416Delete->bindParam(':ID_CAP_IV_A',$id_cap_iv_a);
                        $commandCapIvModAp416Delete->execute();

                        $cap4_moda_p416 = explode(",",$model->cap4_moda_p416);
                        foreach($cap4_moda_p416 as $p416){
                            $commandCapIvModAp416 = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_A_FACTORES_ECONOMICOS (ID_CAP_IV_A,ID_FACTORES_ECONOMICOS) values (:ID_CAP_IV_A,:ID_FACTORES_ECONOMICOS)');
                            $commandCapIvModAp416->bindParam(':ID_CAP_IV_A',$id_cap_iv_a);
                            $commandCapIvModAp416->bindParam(':ID_FACTORES_ECONOMICOS',$p416);
                            $commandCapIvModAp416->execute();
                        }
                    }

                    if($model->cap4_moda_p417){
                        $commandCapIvModAp417Delete = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_IV_A_ACT_MEJORAMIENTO WHERE ID_CAP_IV_A=:ID_CAP_IV_A');
                        $commandCapIvModAp417Delete->bindParam(':ID_CAP_IV_A',$id_cap_iv_a);
                        $commandCapIvModAp417Delete->execute();

                        $cap4_moda_p417 = explode(",",$model->cap4_moda_p417);
                        foreach($cap4_moda_p417 as $p417){
                            $commandCapIvModAp417 = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_A_ACT_MEJORAMIENTO (ID_CAP_IV_A,ID_ACT_MEJORAMIENTO) values (:ID_CAP_IV_A,:ID_ACT_MEJORAMIENTO)');
                            $commandCapIvModAp417->bindParam(':ID_CAP_IV_A',$id_cap_iv_a);
                            $commandCapIvModAp417->bindParam(':ID_ACT_MEJORAMIENTO',$p417);
                            $commandCapIvModAp417->execute();
                        }
                    }

                    if($model->cap4_moda_p427){
                        $commandCapIvModAp427Delete = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_IV_A_MOTIVO_SIEMBRA WHERE ID_CAP_IV_A=:ID_CAP_IV_A');
                        $commandCapIvModAp427Delete->bindParam(':ID_CAP_IV_A',$id_cap_iv_a);
                        $commandCapIvModAp427Delete->execute();

                        $cap4_moda_p427 = explode(",",$model->cap4_moda_p427);
                        foreach($cap4_moda_p427 as $p427){
                            if($p427=="10"){
                                $commandCapIvModAp427 = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_A_MOTIVO_SIEMBRA (ID_CAP_IV_A,ID_MOTIVO_SIEMBRA,TXT_OTRO) values (:ID_CAP_IV_A,:ID_MOTIVO_SIEMBRA,:TXT_OTRO)');
                                $commandCapIvModAp427->bindParam(':ID_CAP_IV_A',$id_cap_iv_a);
                                $commandCapIvModAp427->bindParam(':ID_MOTIVO_SIEMBRA',$p427);
                                $commandCapIvModAp427->bindParam(':TXT_OTRO',$model->cap4_moda_p427_10_especifique);
                                $commandCapIvModAp427->execute();
                            }else{
                                $commandCapIvModAp427 = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_A_MOTIVO_SIEMBRA (ID_CAP_IV_A,ID_MOTIVO_SIEMBRA) values (:ID_CAP_IV_A,:ID_MOTIVO_SIEMBRA)');
                                $commandCapIvModAp427->bindParam(':ID_CAP_IV_A',$id_cap_iv_a);
                                $commandCapIvModAp427->bindParam(':ID_MOTIVO_SIEMBRA',$p427);
                                $commandCapIvModAp427->execute();
                            }
                        }
                    }
                    $transaction->commit();
                    return ['success'=>true];
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    throw $e;
                }
            }
        }
        return $this->render('update-capitulo-iv-modulo-a',['id_cap_iv_a'=>$id_cap_iv_a,'geocodigo_fdo'=>$geocodigo_fdo]);
    }

    public function actionViewCapituloIvModuloA($id_cap_iv_a = null){
        $this->layout='privado';

        return $this->render('view-capitulo-iv-modulo-a',['id_cap_iv_a'=>$id_cap_iv_a]);
    }

    public function actionCreateCalidad($id_cap_iv_a=null){
        $this->layout = 'vacio';
        $request = Yii::$app->request;

        $model = new CapituloIVModuloA();
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $connection = \Yii::$app->db;
                $transaction = $connection->beginTransaction();
                try {
                    $seqCalidad = (new \yii\db\Query())->select('ENA_TM_DET_CAP_IV_A_CALIDAD_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();
                    $commandCapIvModAp423 = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_A_CALIDAD (ID_CAP_IV_A_CALIDAD,ID_CAP_IV_A,TXT_NOMB_CALIDAD,NUM_CANTIDAD_COSECHADA,ID_UM_CALIDAD,NUM_EQ_CALIDAD,NUM_CANTIDAD_VENTA,ID_UM_VENTA,ID_ESPECIFICACION_CULTIVO,NUM_PRECIO,MONEDA) values (:ID_CAP_IV_A_CALIDAD,:ID_CAP_IV_A,:TXT_NOMB_CALIDAD,:NUM_CANTIDAD_COSECHADA,:ID_UM_CALIDAD,:NUM_EQ_CALIDAD,:NUM_CANTIDAD_VENTA,:ID_UM_VENTA,:ID_ESPECIFICACION_CULTIVO,:NUM_PRECIO,:MONEDA)');
                    
                    $commandCapIvModAp423->bindParam(':ID_CAP_IV_A_CALIDAD',$seqCalidad['contador']);
                    $commandCapIvModAp423->bindParam(':ID_CAP_IV_A',$id_cap_iv_a);
                    $commandCapIvModAp423->bindParam(':TXT_NOMB_CALIDAD',$model->cap4_moda_p423_a);
                    $commandCapIvModAp423->bindParam(':NUM_CANTIDAD_COSECHADA',$model->cap4_moda_p423_b);
                    $commandCapIvModAp423->bindParam(':ID_UM_CALIDAD',$model->cap4_moda_p423_c);
                    $commandCapIvModAp423->bindParam(':NUM_EQ_CALIDAD',$model->cap4_moda_p423_d);
                    $commandCapIvModAp423->bindParam(':NUM_CANTIDAD_VENTA',$model->cap4_moda_p425_a);
                    $commandCapIvModAp423->bindParam(':ID_UM_VENTA',$model->cap4_moda_p425_b);
                    $commandCapIvModAp423->bindParam(':ID_ESPECIFICACION_CULTIVO',$model->cap4_moda_p425_c);
                    $commandCapIvModAp423->bindParam(':NUM_PRECIO',$model->cap4_moda_p425_d);
                    $commandCapIvModAp423->bindParam(':MONEDA',$model->cap4_moda_p425_d_moneda);
                    $commandCapIvModAp423->execute();

                    if($model->cap4_moda_p424){
                        $commandCapIvModAp424Delete = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_IV_A_DEST_PROD WHERE ID_CAP_IV_A_CALIDAD=:ID_CAP_IV_A_CALIDAD');
                        $commandCapIvModAp424Delete->bindParam(':ID_CAP_IV_A_CALIDAD',$seqCalidad['contador']);
                        $commandCapIvModAp424Delete->execute();

                        $cap4_moda_p424 = explode(",",$model->cap4_moda_p424);
                        foreach($cap4_moda_p424 as $p424){
                            if($p424==13){
                                $commandCapIvModAp424 = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_A_DEST_PROD (ID_CAP_IV_A_CALIDAD,ID_DESTINO_PRODUCCION,TXT_OTRO) values (:ID_CAP_IV_A_CALIDAD,:ID_DESTINO_PRODUCCION,:TXT_OTRO)');
                                $commandCapIvModAp424->bindParam(':ID_CAP_IV_A_CALIDAD',$seqCalidad['contador']);
                                $commandCapIvModAp424->bindParam(':ID_DESTINO_PRODUCCION',$p424);
                                $commandCapIvModAp424->bindParam(':TXT_OTRO',$model->cap4_moda_p424_13_especifique);
                                $commandCapIvModAp424->execute();
                            }else{
                                $commandCapIvModAp424 = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_A_DEST_PROD (ID_CAP_IV_A_CALIDAD,ID_DESTINO_PRODUCCION) values (:ID_CAP_IV_A_CALIDAD,:ID_DESTINO_PRODUCCION)');
                                $commandCapIvModAp424->bindParam(':ID_CAP_IV_A_CALIDAD',$seqCalidad['contador']);
                                $commandCapIvModAp424->bindParam(':ID_DESTINO_PRODUCCION',$p424);
                                $commandCapIvModAp424->execute();
                            }
                            
                        }
                    }

                    if($model->cap4_moda_p426_a){
                        $commandCapIvModAp426aDelete = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_IV_A_VENT_PROD WHERE ID_CAP_IV_A_CALIDAD=:ID_CAP_IV_A_CALIDAD');
                        $commandCapIvModAp426aDelete->bindParam(':ID_CAP_IV_A_CALIDAD',$seqCalidad['contador']);
                        $commandCapIvModAp426aDelete->execute();

                        $cap4_moda_p426_a = explode(",",$model->cap4_moda_p426_a);
                        foreach($cap4_moda_p426_a as $p426_a){
                            if($p426_a==6){
                                $commandCapIvModAp426a = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_A_VENT_PROD (ID_CAP_IV_A_CALIDAD,ID_VENTA_PRODUCCION,TXT_OTRO) values (:ID_CAP_IV_A_CALIDAD,:ID_VENTA_PRODUCCION,:TXT_OTRO)');
                                $commandCapIvModAp426a->bindParam(':ID_CAP_IV_A_CALIDAD',$seqCalidad['contador']);
                                $commandCapIvModAp426a->bindParam(':ID_VENTA_PRODUCCION',$p426_a);
                                $commandCapIvModAp426a->bindParam(':TXT_OTRO',$model->cap4_moda_p426_a_6_especifique);
                                $commandCapIvModAp426a->execute();
                            }else{
                                $commandCapIvModAp426a = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_A_VENT_PROD (ID_CAP_IV_A_CALIDAD,ID_VENTA_PRODUCCION) values (:ID_CAP_IV_A_CALIDAD,:ID_VENTA_PRODUCCION)');
                                $commandCapIvModAp426a->bindParam(':ID_CAP_IV_A_CALIDAD',$seqCalidad['contador']);
                                $commandCapIvModAp426a->bindParam(':ID_VENTA_PRODUCCION',$p426_a);
                                $commandCapIvModAp426a->execute();
                            }
                        }
                    }

                    if($model->cap4_moda_p426_b){
                        $commandCapIvModAp426bDelete = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_IV_A_PUNTO_VENTA WHERE ID_CAP_IV_A_CALIDAD=:ID_CAP_IV_A_CALIDAD');
                        $commandCapIvModAp426bDelete->bindParam(':ID_CAP_IV_A_CALIDAD',$seqCalidad['contador']);
                        $commandCapIvModAp426bDelete->execute();

                        $cap4_moda_p426_b = explode(",",$model->cap4_moda_p426_b);
                        foreach($cap4_moda_p426_b as $p426_b){
                            if($p426_b==9){
                                $commandCapIvModAp426b = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_A_PUNTO_VENTA (ID_CAP_IV_A_CALIDAD,ID_PUNTO_VENTA,TXT_OTRO) values (:ID_CAP_IV_A_CALIDAD,:ID_PUNTO_VENTA,:TXT_OTRO)');
                                $commandCapIvModAp426b->bindParam(':ID_CAP_IV_A_CALIDAD',$seqCalidad['contador']);
                                $commandCapIvModAp426b->bindParam(':ID_PUNTO_VENTA',$p426_b);
                                $commandCapIvModAp426b->bindParam(':TXT_OTRO',$model->cap4_moda_p426_b_9_especifique);
                                $commandCapIvModAp426b->execute();
                            }else{
                                $commandCapIvModAp426b = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_A_PUNTO_VENTA (ID_CAP_IV_A_CALIDAD,ID_PUNTO_VENTA) values (:ID_CAP_IV_A_CALIDAD,:ID_PUNTO_VENTA)');
                                $commandCapIvModAp426b->bindParam(':ID_CAP_IV_A_CALIDAD',$seqCalidad['contador']);
                                $commandCapIvModAp426b->bindParam(':ID_PUNTO_VENTA',$p426_b);
                                $commandCapIvModAp426b->execute();
                            }
                            
                        }
                    }
                    
                    $transaction->commit();
                    return ['success'=>true];
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    throw $e;
                }
            }
            return $this->render('create-calidad');
        }
    }

    public function actionUpdateCalidad($id_calidad=null){
        $this->layout = 'vacio';
        $request = Yii::$app->request;

        $model = new CapituloIVModuloA();
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $connection = \Yii::$app->db;
                $transaction = $connection->beginTransaction();
                try {
                    //$seqCalidad = (new \yii\db\Query())->select('ENA_TM_DET_CAP_IV_A_CALIDAD_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();
                    //$capIvModACalidad = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_IV_A_CALIDAD')->where('ID_CAP_IV_A_CALIDAD=:ID_CAP_IV_A_CALIDAD',[':ID_ENCUESTA'=>$id_calidad])->one();


                    $commandCapIvModAp423 = $connection->createCommand('UPDATE ENA_TM_DET_CAP_IV_A_CALIDAD SET TXT_NOMB_CALIDAD=:TXT_NOMB_CALIDAD,NUM_CANTIDAD_COSECHADA=:NUM_CANTIDAD_COSECHADA,ID_UM_CALIDAD=:ID_UM_CALIDAD,NUM_EQ_CALIDAD=:NUM_EQ_CALIDAD,NUM_CANTIDAD_VENTA=:NUM_CANTIDAD_VENTA,ID_UM_VENTA=:ID_UM_VENTA,ID_ESPECIFICACION_CULTIVO=:ID_ESPECIFICACION_CULTIVO,NUM_PRECIO=:NUM_PRECIO,MONEDA=:MONEDA WHERE ID_CAP_IV_A_CALIDAD=:ID_CAP_IV_A_CALIDAD ');
                    
                    $commandCapIvModAp423->bindParam(':ID_CAP_IV_A_CALIDAD',$id_calidad);
                    $commandCapIvModAp423->bindParam(':TXT_NOMB_CALIDAD',$model->cap4_moda_p423_a);
                    $commandCapIvModAp423->bindParam(':NUM_CANTIDAD_COSECHADA',$model->cap4_moda_p423_b);
                    $commandCapIvModAp423->bindParam(':ID_UM_CALIDAD',$model->cap4_moda_p423_c);
                    $commandCapIvModAp423->bindParam(':NUM_EQ_CALIDAD',$model->cap4_moda_p423_d);
                    $commandCapIvModAp423->bindParam(':NUM_CANTIDAD_VENTA',$model->cap4_moda_p425_a);
                    $commandCapIvModAp423->bindParam(':ID_UM_VENTA',$model->cap4_moda_p425_b);
                    $commandCapIvModAp423->bindParam(':ID_ESPECIFICACION_CULTIVO',$model->cap4_moda_p425_c);
                    $commandCapIvModAp423->bindParam(':NUM_PRECIO',$model->cap4_moda_p425_d);
                    $commandCapIvModAp423->bindParam(':MONEDA',$model->cap4_moda_p425_d_moneda);
                    $commandCapIvModAp423->execute();

                    if($model->cap4_moda_p424){
                        $commandCapIvModAp424Delete = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_IV_A_DEST_PROD WHERE ID_CAP_IV_A_CALIDAD=:ID_CAP_IV_A_CALIDAD');
                        $commandCapIvModAp424Delete->bindParam(':ID_CAP_IV_A_CALIDAD',$id_calidad);
                        $commandCapIvModAp424Delete->execute();

                        $cap4_moda_p424 = explode(",",$model->cap4_moda_p424);
                        foreach($cap4_moda_p424 as $p424){
                            if($p424==13){
                                $commandCapIvModAp424 = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_A_DEST_PROD (ID_CAP_IV_A_CALIDAD,ID_DESTINO_PRODUCCION,TXT_OTRO) values (:ID_CAP_IV_A_CALIDAD,:ID_DESTINO_PRODUCCION,:TXT_OTRO)');
                                $commandCapIvModAp424->bindParam(':ID_CAP_IV_A_CALIDAD',$id_calidad);
                                $commandCapIvModAp424->bindParam(':ID_DESTINO_PRODUCCION',$p424);
                                $commandCapIvModAp424->bindParam(':TXT_OTRO',$model->cap4_moda_p424_13_especifique);
                                $commandCapIvModAp424->execute();
                            }else{
                                $commandCapIvModAp424 = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_A_DEST_PROD (ID_CAP_IV_A_CALIDAD,ID_DESTINO_PRODUCCION) values (:ID_CAP_IV_A_CALIDAD,:ID_DESTINO_PRODUCCION)');
                                $commandCapIvModAp424->bindParam(':ID_CAP_IV_A_CALIDAD',$id_calidad);
                                $commandCapIvModAp424->bindParam(':ID_DESTINO_PRODUCCION',$p424);
                                $commandCapIvModAp424->execute();
                            }
                            
                        }
                    }

                    if($model->cap4_moda_p426_a){
                        $commandCapIvModAp426aDelete = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_IV_A_VENT_PROD WHERE ID_CAP_IV_A_CALIDAD=:ID_CAP_IV_A_CALIDAD');
                        $commandCapIvModAp426aDelete->bindParam(':ID_CAP_IV_A_CALIDAD',$id_calidad);
                        $commandCapIvModAp426aDelete->execute();

                        $cap4_moda_p426_a = explode(",",$model->cap4_moda_p426_a);
                        foreach($cap4_moda_p426_a as $p426_a){
                            if($p426_a==6){
                                $commandCapIvModAp426a = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_A_VENT_PROD (ID_CAP_IV_A_CALIDAD,ID_VENTA_PRODUCCION,TXT_OTRO) values (:ID_CAP_IV_A_CALIDAD,:ID_VENTA_PRODUCCION,:TXT_OTRO)');
                                $commandCapIvModAp426a->bindParam(':ID_CAP_IV_A_CALIDAD',$id_calidad);
                                $commandCapIvModAp426a->bindParam(':ID_VENTA_PRODUCCION',$p426_a);
                                $commandCapIvModAp426a->bindParam(':TXT_OTRO',$model->cap4_moda_p426_a_6_especifique);
                                $commandCapIvModAp426a->execute();
                            }else{
                                $commandCapIvModAp426a = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_A_VENT_PROD (ID_CAP_IV_A_CALIDAD,ID_VENTA_PRODUCCION) values (:ID_CAP_IV_A_CALIDAD,:ID_VENTA_PRODUCCION)');
                                $commandCapIvModAp426a->bindParam(':ID_CAP_IV_A_CALIDAD',$id_calidad);
                                $commandCapIvModAp426a->bindParam(':ID_VENTA_PRODUCCION',$p426_a);
                                $commandCapIvModAp426a->execute();
                            }
                        }
                    }

                    if($model->cap4_moda_p426_b){
                        $commandCapIvModAp426bDelete = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_IV_A_PUNTO_VENTA WHERE ID_CAP_IV_A_CALIDAD=:ID_CAP_IV_A_CALIDAD');
                        $commandCapIvModAp426bDelete->bindParam(':ID_CAP_IV_A_CALIDAD',$id_calidad);
                        $commandCapIvModAp426bDelete->execute();

                        $cap4_moda_p426_b = explode(",",$model->cap4_moda_p426_b);
                        foreach($cap4_moda_p426_b as $p426_b){
                            if($p426_b==9){
                                $commandCapIvModAp426b = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_A_PUNTO_VENTA (ID_CAP_IV_A_CALIDAD,ID_PUNTO_VENTA,TXT_OTRO) values (:ID_CAP_IV_A_CALIDAD,:ID_PUNTO_VENTA,:TXT_OTRO)');
                                $commandCapIvModAp426b->bindParam(':ID_CAP_IV_A_CALIDAD',$id_calidad);
                                $commandCapIvModAp426b->bindParam(':ID_PUNTO_VENTA',$p426_b);
                                $commandCapIvModAp426b->bindParam(':TXT_OTRO',$model->cap4_moda_p426_b_9_especifique);
                                $commandCapIvModAp426b->execute();
                            }else{
                                $commandCapIvModAp426b = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_A_PUNTO_VENTA (ID_CAP_IV_A_CALIDAD,ID_PUNTO_VENTA) values (:ID_CAP_IV_A_CALIDAD,:ID_PUNTO_VENTA)');
                                $commandCapIvModAp426b->bindParam(':ID_CAP_IV_A_CALIDAD',$id_calidad);
                                $commandCapIvModAp426b->bindParam(':ID_PUNTO_VENTA',$p426_b);
                                $commandCapIvModAp426b->execute();
                            }
                            
                        }
                    }
                    
                    $transaction->commit();
                    return ['success'=>true];
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    throw $e;
                }
            }
            return $this->render('update-calidad');
        }
    }

    public function actionViewCalidad($id_calidad=null){
        $this->layout = 'vacio';
        $request = Yii::$app->request;

        return $this->render('view-calidad');
    }

    public function actionCreateCapituloIvModuloB($id_encuesta = null,$geocodigo_fdo=null){
        $this->layout='privado';
        
        //var_dump("a");die;
        $request = Yii::$app->request;
        $model = new CapituloIVModuloB;
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $connection = \Yii::$app->db;
                $transaction = $connection->beginTransaction();
                try {
                    //$seq = (new \yii\db\Query())->select('ENA_TM_DET_CAP_IV_B_ARB_FRUT_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();
                    $commandCapIvModuloB= $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_B_ARB_FRUT (
                        ID_ARBOLES_FRUTALES,
                        ID_ENCUESTA,
                        NUM_ORDEN,
                        NUM_PLANTAS,
                        NUM_PLANTAS_PROD,
                        FLG_ENE,
                        FLG_FEB,
                        FLG_MAR,
                        FLG_ABR,
                        FLG_MAY,
                        FLG_JUN,
                        FLG_JUL,
                        FLG_AGO,
                        FLG_SET,
                        FLG_OCT,
                        FLG_NOV,
                        FLG_DIC
                        ) values (
                        :ID_ARBOLES_FRUTALES,
                        :ID_ENCUESTA,
                        :NUM_ORDEN,
                        :NUM_PLANTAS,
                        :NUM_PLANTAS_PROD,
                        :FLG_ENE,
                        :FLG_FEB,
                        :FLG_MAR,
                        :FLG_ABR,
                        :FLG_MAY,
                        :FLG_JUN,
                        :FLG_JUL,
                        :FLG_AGO,
                        :FLG_SET,
                        :FLG_OCT,
                        :FLG_NOV,
                        :FLG_DIC
                        )');
                    $commandCapIvModuloB->bindParam(':ID_ENCUESTA',$id_encuesta);
                    $commandCapIvModuloB->bindParam(':NUM_ORDEN',$model->cap4_modb_p428_a);
                    $commandCapIvModuloB->bindParam(':ID_ARBOLES_FRUTALES',$model->cap4_modb_p428_b);
                    $commandCapIvModuloB->bindParam(':NUM_PLANTAS',$model->cap4_modb_p429);
                    $commandCapIvModuloB->bindParam(':NUM_PLANTAS_PROD',$model->cap4_modb_p430);
                    $commandCapIvModuloB->bindParam(':FLG_ENE',$model->cap4_modb_p431_1);
                    $commandCapIvModuloB->bindParam(':FLG_FEB',$model->cap4_modb_p431_2);
                    $commandCapIvModuloB->bindParam(':FLG_MAR',$model->cap4_modb_p431_3);
                    $commandCapIvModuloB->bindParam(':FLG_ABR',$model->cap4_modb_p431_4);
                    $commandCapIvModuloB->bindParam(':FLG_MAY',$model->cap4_modb_p431_5);
                    $commandCapIvModuloB->bindParam(':FLG_JUN',$model->cap4_modb_p431_6);
                    $commandCapIvModuloB->bindParam(':FLG_JUL',$model->cap4_modb_p431_7);
                    $commandCapIvModuloB->bindParam(':FLG_AGO',$model->cap4_modb_p431_8);
                    $commandCapIvModuloB->bindParam(':FLG_SET',$model->cap4_modb_p431_9);
                    $commandCapIvModuloB->bindParam(':FLG_OCT',$model->cap4_modb_p431_10);
                    $commandCapIvModuloB->bindParam(':FLG_NOV',$model->cap4_modb_p431_11);
                    $commandCapIvModuloB->bindParam(':FLG_DIC',$model->cap4_modb_p431_12);
                    $commandCapIvModuloB->execute();

                    $transaction->commit();
                    return ['success'=>true,'id_encuesta'=>$id_encuesta,'num_orden'=>$model->cap4_modb_p428_a];
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    throw $e;
                }
            }
        }
        return $this->render('create-capitulo-iv-modulo-b',['geocodigo_fdo'=>$geocodigo_fdo]);


    }

    public function actionUpdateCapituloIvModuloB($num_orden=null,$id_encuesta=null,$geocodigo_fdo=null){
        $this->layout='privado';
        $cap_iv_modulo_b = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_IV_B_ARB_FRUT')->where('ID_ENCUESTA=:ID_ENCUESTA and NUM_ORDEN=:NUM_ORDEN',[':ID_ENCUESTA'=>$id_encuesta,':NUM_ORDEN'=>$num_orden])->one();
        //var_dump($cap_iv_modulo_b);die;
        $request = Yii::$app->request;
        $model = new CapituloIVModuloB;
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $connection = \Yii::$app->db;
                $transaction = $connection->beginTransaction();
                try {
                    $commandCapIvmodb = $connection->createCommand('UPDATE ENA_TM_DET_CAP_IV_B_ARB_FRUT SET
                        NUM_PLANTAS=:NUM_PLANTAS,
                        NUM_PLANTAS_PROD=:NUM_PLANTAS_PROD,
                        ID_ARBOLES_FRUTALES=:ID_ARBOLES_FRUTALES,
                        FLG_ENE=:FLG_ENE,
                        FLG_FEB=:FLG_FEB,
                        FLG_MAR=:FLG_MAR,
                        FLG_ABR=:FLG_ABR,
                        FLG_MAY=:FLG_MAY,
                        FLG_JUN=:FLG_JUN,
                        FLG_JUL=:FLG_JUL,
                        FLG_AGO=:FLG_AGO,
                        FLG_SET=:FLG_SET,
                        FLG_OCT=:FLG_OCT,
                        FLG_NOV=:FLG_NOV,
                        FLG_DIC=:FLG_DIC
                        WHERE NUM_ORDEN=:NUM_ORDEN');
                    $commandCapIvmodb->bindParam(':NUM_ORDEN',$model->cap4_modb_p428_a);
                    $commandCapIvmodb->bindParam(':ID_ARBOLES_FRUTALES',$model->cap4_modb_p428_b);
                    $commandCapIvmodb->bindParam(':NUM_PLANTAS',$model->cap4_modb_p429);
                    $commandCapIvmodb->bindParam(':NUM_PLANTAS_PROD',$model->cap4_modb_p430);
                    
                    $commandCapIvmodb->bindParam(':FLG_ENE',$model->cap4_modb_p431_1);
                    $commandCapIvmodb->bindParam(':FLG_FEB',$model->cap4_modb_p431_2);
                    $commandCapIvmodb->bindParam(':FLG_MAR',$model->cap4_modb_p431_3);
                    $commandCapIvmodb->bindParam(':FLG_ABR',$model->cap4_modb_p431_4);
                    $commandCapIvmodb->bindParam(':FLG_MAY',$model->cap4_modb_p431_5);
                    $commandCapIvmodb->bindParam(':FLG_JUN',$model->cap4_modb_p431_6);
                    $commandCapIvmodb->bindParam(':FLG_JUL',$model->cap4_modb_p431_7);
                    $commandCapIvmodb->bindParam(':FLG_AGO',$model->cap4_modb_p431_8);
                    $commandCapIvmodb->bindParam(':FLG_SET',$model->cap4_modb_p431_9);
                    $commandCapIvmodb->bindParam(':FLG_OCT',$model->cap4_modb_p431_10);
                    $commandCapIvmodb->bindParam(':FLG_NOV',$model->cap4_modb_p431_11);
                    $commandCapIvmodb->bindParam(':FLG_DIC',$model->cap4_modb_p431_12);
                    $commandCapIvmodb->execute();
                    $transaction->commit();
                    return ['success'=>true];
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    throw $e;
                }
            }
        }
        return $this->render('update-capitulo-iv-modulo-b',['cap_iv_modulo_b'=>$cap_iv_modulo_b,'geocodigo_fdo'=>$geocodigo_fdo]);
    }

    public function actionViewCapituloIvModuloB($num_orden=null,$id_encuesta=null){
        $this->layout='privado';
        $cap_iv_modulo_b = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_IV_B_ARB_FRUT')->where('ID_ENCUESTA=:ID_ENCUESTA and NUM_ORDEN=:NUM_ORDEN',[':ID_ENCUESTA'=>$id_encuesta,':NUM_ORDEN'=>$num_orden])->one();
        
        return $this->render('view-capitulo-iv-modulo-b',['cap_iv_modulo_b'=>$cap_iv_modulo_b]);
    }

    public function actionCreateCapituloIvModuloC($id_encuesta = null,$geocodigo_fdo=null){
        $this->layout='privado';
        
        $request = Yii::$app->request;
        $model = new CapituloIVModuloC;
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $connection = \Yii::$app->db;
                $transaction = $connection->beginTransaction();
                try {
                    //$seq = (new \yii\db\Query())->select('ENA_TM_DET_CAP_IV_B_ARB_FRUT_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();
                    $commandCapIvModuloB= $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_C_SEMILLEROS (
                        ID_SEMILLEROS,
                        ID_ENCUESTA,
                        NUM_ORDEN_SEMILLERO,
                        NUM_CANT_PLANTONES,
                        ID_UM_PLANTONES,
                        NUM_EQ_PLANTONES,
                        FLG_VENDIO_PLANTONES,
                        NUM_CANT_PLANTONES_VENDIDOS,
                        ID_UM_PLANT_VENDIDOS,
                        NUM_EQ_PLANTONES_VENDIDOS,
                        NUM_PLANTONES_VENDIDOS,
                        NUM_SOLES_KG,
                        NUM_SOLES_PLANTON
                        ) values (
                        :ID_SEMILLEROS,
                        :ID_ENCUESTA,
                        :NUM_ORDEN_SEMILLERO,
                        :NUM_CANT_PLANTONES,
                        :ID_UM_PLANTONES,
                        :NUM_EQ_PLANTONES,
                        :FLG_VENDIO_PLANTONES,
                        :NUM_CANT_PLANTONES_VENDIDOS,
                        :ID_UM_PLANT_VENDIDOS,
                        :NUM_EQ_PLANTONES_VENDIDOS,
                        :NUM_PLANTONES_VENDIDOS,
                        :NUM_SOLES_KG,
                        :NUM_SOLES_PLANTON
                        )');
                    $commandCapIvModuloB->bindParam(':ID_SEMILLEROS',$model->cap4_modc_p432_b);
                    $commandCapIvModuloB->bindParam(':ID_ENCUESTA',$id_encuesta);
                    $commandCapIvModuloB->bindParam(':NUM_ORDEN_SEMILLERO',$model->cap4_modc_p432_a);
                    $commandCapIvModuloB->bindParam(':NUM_CANT_PLANTONES',$model->cap4_modc_p433_a);
                    $commandCapIvModuloB->bindParam(':ID_UM_PLANTONES',$model->cap4_modc_p433_b);
                    $commandCapIvModuloB->bindParam(':NUM_EQ_PLANTONES',$model->cap4_modc_p433_c);
                    $commandCapIvModuloB->bindParam(':FLG_VENDIO_PLANTONES',$model->cap4_modc_p434);
                    $commandCapIvModuloB->bindParam(':NUM_CANT_PLANTONES_VENDIDOS',$model->cap4_modc_p435_a);
                    $commandCapIvModuloB->bindParam(':ID_UM_PLANT_VENDIDOS',$model->cap4_modc_p435_b);
                    $commandCapIvModuloB->bindParam(':NUM_EQ_PLANTONES_VENDIDOS',$model->cap4_modc_p435_c);
                    $commandCapIvModuloB->bindParam(':NUM_PLANTONES_VENDIDOS',$model->cap4_modc_p435_d);
                    $commandCapIvModuloB->bindParam(':NUM_SOLES_KG',$model->cap4_modc_p436_a);
                    $commandCapIvModuloB->bindParam(':NUM_SOLES_PLANTON',$model->cap4_modc_p436_b);
                    $commandCapIvModuloB->execute();

                    $transaction->commit();
                    return ['success'=>true,'num_orden_semillero'=>$model->cap4_modc_p432_a,'id_encuesta'=>$id_encuesta];
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    throw $e;
                }
            }
        }
        return $this->render('create-capitulo-iv-modulo-c',['geocodigo_fdo'=>$geocodigo_fdo]);


    }

    public function actionUpdateCapituloIvModuloC($num_orden_semillero=null,$id_encuesta=null,$geocodigo_fdo=null){
        $this->layout='privado';
        $cap_iv_modulo_c = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_IV_C_SEMILLEROS')->where('ID_ENCUESTA=:ID_ENCUESTA and NUM_ORDEN_SEMILLERO=:NUM_ORDEN_SEMILLERO',[':ID_ENCUESTA'=>$id_encuesta,':NUM_ORDEN_SEMILLERO'=>$num_orden_semillero])->one();
        //var_dump($cap_iv_modulo_b);die;
        $request = Yii::$app->request;
        $model = new CapituloIVModuloC;
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $connection = \Yii::$app->db;
                $transaction = $connection->beginTransaction();
                try {
                    $commandCapIvModuloB = $connection->createCommand('UPDATE ENA_TM_DET_CAP_IV_C_SEMILLEROS SET
                        ID_SEMILLEROS=:ID_SEMILLEROS,
                        NUM_CANT_PLANTONES=:NUM_CANT_PLANTONES,
                        ID_UM_PLANTONES=:ID_UM_PLANTONES,
                        NUM_EQ_PLANTONES=:NUM_EQ_PLANTONES,
                        FLG_VENDIO_PLANTONES=:FLG_VENDIO_PLANTONES,
                        NUM_CANT_PLANTONES_VENDIDOS=:NUM_CANT_PLANTONES_VENDIDOS,
                        ID_UM_PLANT_VENDIDOS=:ID_UM_PLANT_VENDIDOS,
                        NUM_EQ_PLANTONES_VENDIDOS=:NUM_EQ_PLANTONES_VENDIDOS,
                        NUM_PLANTONES_VENDIDOS=:NUM_PLANTONES_VENDIDOS,
                        NUM_SOLES_KG=:NUM_SOLES_KG,
                        NUM_SOLES_PLANTON=:NUM_SOLES_PLANTON
                        WHERE NUM_ORDEN_SEMILLERO=:NUM_ORDEN_SEMILLERO AND ID_ENCUESTA=:ID_ENCUESTA');
                    $commandCapIvModuloB->bindParam(':ID_SEMILLEROS',$model->cap4_modc_p432_b);
                    $commandCapIvModuloB->bindParam(':ID_ENCUESTA',$id_encuesta);
                    $commandCapIvModuloB->bindParam(':NUM_ORDEN_SEMILLERO',$model->cap4_modc_p432_a);
                    $commandCapIvModuloB->bindParam(':NUM_CANT_PLANTONES',$model->cap4_modc_p433_a);
                    $commandCapIvModuloB->bindParam(':ID_UM_PLANTONES',$model->cap4_modc_p433_b);
                    $commandCapIvModuloB->bindParam(':NUM_EQ_PLANTONES',$model->cap4_modc_p433_c);
                    $commandCapIvModuloB->bindParam(':FLG_VENDIO_PLANTONES',$model->cap4_modc_p434);
                    $commandCapIvModuloB->bindParam(':NUM_CANT_PLANTONES_VENDIDOS',$model->cap4_modc_p435_a);
                    $commandCapIvModuloB->bindParam(':ID_UM_PLANT_VENDIDOS',$model->cap4_modc_p435_b);
                    $commandCapIvModuloB->bindParam(':NUM_EQ_PLANTONES_VENDIDOS',$model->cap4_modc_p435_c);
                    $commandCapIvModuloB->bindParam(':NUM_PLANTONES_VENDIDOS',$model->cap4_modc_p435_d);
                    $commandCapIvModuloB->bindParam(':NUM_SOLES_KG',$model->cap4_modc_p436_a);
                    $commandCapIvModuloB->bindParam(':NUM_SOLES_PLANTON',$model->cap4_modc_p436_b);
                    
                    $commandCapIvModuloB->execute();
                    $transaction->commit();
                    return ['success'=>true];
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    throw $e;
                }
            }
        }
        return $this->render('update-capitulo-iv-modulo-c',['cap_iv_modulo_c'=>$cap_iv_modulo_c,'geocodigo_fdo'=>$geocodigo_fdo]);
    }

    public function actionViewCapituloIvModuloC($num_orden_semillero=null,$id_encuesta=null){
        $this->layout='privado';
        $cap_iv_modulo_c = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_IV_C_SEMILLEROS')->where('ID_ENCUESTA=:ID_ENCUESTA and NUM_ORDEN_SEMILLERO=:NUM_ORDEN_SEMILLERO',[':ID_ENCUESTA'=>$id_encuesta,':NUM_ORDEN_SEMILLERO'=>$num_orden_semillero])->one();
        
        return $this->render('view-capitulo-iv-modulo-c',['cap_iv_modulo_c'=>$cap_iv_modulo_c]);
    }

    public function actionGetModuloA(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $id_cap_iv_a = $_POST['id_cap_iv_a'];
            $capIVModAP407 = null;
            $capIVModAP408 = null;
            $capIVModAP415 = null;
            $capIVModAP416 = null;
            $capIVModAP417 = null;
            $capIVModAP427 = null;
            $capIVModACalidad = null;
            
            $capIVModA = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_IV_A')->where('ID_CAP_IV_A=:ID_CAP_IV_A',[':ID_CAP_IV_A'=>$id_cap_iv_a])->one();
            if($capIVModA){
                $capIVModAP407 = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_IV_A_SEM_PLANT')->where('ID_CAP_IV_A=:ID_CAP_IV_A',[':ID_CAP_IV_A'=>$capIVModA['ID_CAP_IV_A']])->all();
                $capIVModAP408 = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_IV_A_SIST_RIEGO')->where('ID_CAP_IV_A=:ID_CAP_IV_A',[':ID_CAP_IV_A'=>$capIVModA['ID_CAP_IV_A']])->all();
                $capIVModAP415 = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_IV_A_FEN_NAT')->where('ID_CAP_IV_A=:ID_CAP_IV_A',[':ID_CAP_IV_A'=>$capIVModA['ID_CAP_IV_A']])->all();
                $capIVModAP416 = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_IV_A_FACTORES_ECONOMICOS')->where('ID_CAP_IV_A=:ID_CAP_IV_A',[':ID_CAP_IV_A'=>$capIVModA['ID_CAP_IV_A']])->all();
                $capIVModAP417 = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_IV_A_ACT_MEJORAMIENTO')->where('ID_CAP_IV_A=:ID_CAP_IV_A',[':ID_CAP_IV_A'=>$capIVModA['ID_CAP_IV_A']])->all();
                $capIVModAP427 = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_IV_A_MOTIVO_SIEMBRA')->where('ID_CAP_IV_A=:ID_CAP_IV_A',[':ID_CAP_IV_A'=>$capIVModA['ID_CAP_IV_A']])->all();
    
                $capIVModACalidad = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_IV_A_CALIDAD')->where('ID_CAP_IV_A=:ID_CAP_IV_A',[':ID_CAP_IV_A'=>$capIVModA['ID_CAP_IV_A']])->all();
            }

            
            


            return [
                'success'=>true,
                'cabecera'=>$capIVModA,
                'detalle_p407'=>$capIVModAP407,
                'detalle_p408'=>$capIVModAP408,
                'detalle_p415'=>$capIVModAP415,
                'detalle_p416'=>$capIVModAP416,
                'detalle_p417'=>$capIVModAP417,
                'detalle_p427'=>$capIVModAP427,
                'detalle_calidad'=>$capIVModACalidad
            ];
        }
    }

    public function actionGetCalidad(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $id_calidad = $_POST['id_calidad'];

            $capIVModACalidad = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_IV_A_CALIDAD')->where('ID_CAP_IV_A_CALIDAD=:ID_CAP_IV_A_CALIDAD',[':ID_CAP_IV_A_CALIDAD'=>$id_calidad])->one();
            $capIVModAP424 = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_IV_A_DEST_PROD')->where('ID_CAP_IV_A_CALIDAD=:ID_CAP_IV_A_CALIDAD',[':ID_CAP_IV_A_CALIDAD'=>$id_calidad])->all();
            $capIVModAP426A = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_IV_A_VENT_PROD')->where('ID_CAP_IV_A_CALIDAD=:ID_CAP_IV_A_CALIDAD',[':ID_CAP_IV_A_CALIDAD'=>$id_calidad])->all();
            $capIVModAP426B = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_IV_A_PUNTO_VENTA')->where('ID_CAP_IV_A_CALIDAD=:ID_CAP_IV_A_CALIDAD',[':ID_CAP_IV_A_CALIDAD'=>$id_calidad])->all();
            
            
            return [
                'success'=>true,
                'cabecera'=>$capIVModACalidad,
                'detalle_p424'=>$capIVModAP424,
                'detalle_p426_a'=>$capIVModAP426A,
                'detalle_p426_b'=>$capIVModAP426B,

            ];
        }
    }

    public function actionGetListaCalidad(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $id_cap_iv_a = $_POST['id_cap_iv_a'];

            $capIVModACalidad = (new \yii\db\Query())
            ->select('ENA_TM_DET_CAP_IV_A_CALIDAD.*,ENA_TG_UM_KG.TXT_DESCRIPCION TXT_UNIDAD_MEDIDA,ENA_TG_ESPECIFICACION.TXT_ESPECIFICACION')->from('ENA_TM_DET_CAP_IV_A_CALIDAD')
            ->leftJoin('ENA_TG_UM_KG','ENA_TG_UM_KG.ID_UM_KG=ENA_TM_DET_CAP_IV_A_CALIDAD.ID_UM_VENTA')
            ->leftJoin('ENA_TG_ESPECIFICACION','ENA_TG_ESPECIFICACION.ID_ESPECIFICACION=ENA_TM_DET_CAP_IV_A_CALIDAD.ID_ESPECIFICACION_CULTIVO')
            ->where('ENA_TM_DET_CAP_IV_A_CALIDAD.ID_CAP_IV_A=:ID_CAP_IV_A',[':ID_CAP_IV_A'=>$id_cap_iv_a])->all();
           
            
            return [
                'success'=>true,
                'cabecera'=>$capIVModACalidad
            ];
        }
    }

    public function actionEliminarModuloA(){
        $this->layout='privado';
        if($_POST){
            $id_cap_iv_a = $_POST['id_cap_iv_a'];

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {
                $cap_iv_modulo_a = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_IV_A')->where('ID_CAP_IV_A=:ID_CAP_IV_A',[':ID_CAP_IV_A'=>$id_cap_iv_a])->one();
                $calidades = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_IV_A_CALIDAD')->where('ID_CAP_IV_A=:ID_CAP_IV_A',[':ID_CAP_IV_A'=>$id_cap_iv_a])->all();
                
                $commandCapIvModAP407Delete = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_IV_A_SEM_PLANT WHERE ID_CAP_IV_A=:ID_CAP_IV_A');
                $commandCapIvModAP407Delete->bindParam(':ID_CAP_IV_A',$id_cap_iv_a);
                $commandCapIvModAP407Delete->execute();

                $commandCapIvModAp408Delete = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_IV_A_SIST_RIEGO WHERE ID_CAP_IV_A=:ID_CAP_IV_A');
                $commandCapIvModAp408Delete->bindParam(':ID_CAP_IV_A',$id_cap_iv_a);
                $commandCapIvModAp408Delete->execute();

                $commandCapIvModAp415Delete = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_IV_A_FEN_NAT WHERE ID_CAP_IV_A=:ID_CAP_IV_A');
                $commandCapIvModAp415Delete->bindParam(':ID_CAP_IV_A',$id_cap_iv_a);
                $commandCapIvModAp415Delete->execute();


                $commandCapIvModAp416Delete = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_IV_A_FACTORES_ECONOMICOS WHERE ID_CAP_IV_A=:ID_CAP_IV_A');
                $commandCapIvModAp416Delete->bindParam(':ID_CAP_IV_A',$id_cap_iv_a);
                $commandCapIvModAp416Delete->execute();

                $commandCapIvModAp417Delete = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_IV_A_ACT_MEJORAMIENTO WHERE ID_CAP_IV_A=:ID_CAP_IV_A');
                $commandCapIvModAp417Delete->bindParam(':ID_CAP_IV_A',$id_cap_iv_a);
                $commandCapIvModAp417Delete->execute();


                $commandCapIvModAp427Delete = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_IV_A_MOTIVO_SIEMBRA WHERE ID_CAP_IV_A=:ID_CAP_IV_A');
                $commandCapIvModAp427Delete->bindParam(':ID_CAP_IV_A',$id_cap_iv_a);
                $commandCapIvModAp427Delete->execute();

                

                foreach($calidades as $calidad){
                    $commandCapIvModAp424Delete = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_IV_A_DEST_PROD WHERE ID_CAP_IV_A_CALIDAD=:ID_CAP_IV_A_CALIDAD');
                    $commandCapIvModAp424Delete->bindParam(':ID_CAP_IV_A_CALIDAD',$calidad['ID_CAP_IV_A_CALIDAD']);
                    $commandCapIvModAp424Delete->execute();

                    $commandCapIvModAp426aDelete = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_IV_A_VENT_PROD WHERE ID_CAP_IV_A_CALIDAD=:ID_CAP_IV_A_CALIDAD');
                    $commandCapIvModAp426aDelete->bindParam(':ID_CAP_IV_A_CALIDAD',$calidad['ID_CAP_IV_A_CALIDAD']);
                    $commandCapIvModAp426aDelete->execute();

                    $commandCapIvModAp426bDelete = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_IV_A_PUNTO_VENTA WHERE ID_CAP_IV_A_CALIDAD=:ID_CAP_IV_A_CALIDAD');
                    $commandCapIvModAp426bDelete->bindParam(':ID_CAP_IV_A_CALIDAD',$calidad['ID_CAP_IV_A_CALIDAD']);
                    $commandCapIvModAp426bDelete->execute();
                }

                $commandCapIvModAp423Delete = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_IV_A_CALIDAD WHERE ID_CAP_IV_A=:ID_CAP_IV_A');
                $commandCapIvModAp423Delete->bindParam(':ID_CAP_IV_A',$id_cap_iv_a);
                $commandCapIvModAp423Delete->execute();

                $commandCapIvModA = $connection->createCommand('DELETE FROM ENA_TM_CAP_IV_A WHERE ID_CAP_IV_A=:ID_CAP_IV_A');
                $commandCapIvModA->bindParam(':ID_CAP_IV_A',$id_cap_iv_a);
                $commandCapIvModA->execute();

                 
                $transaction->commit();
                return ['success'=>true];
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
        }
    }

    public function actionEliminarModuloB(){
        $this->layout='privado';
        if($_POST){
            $num_orden = $_POST['num_orden'];
            $id_encuesta = $_POST['id_encuesta'];

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {
                

                $commandCapIvmodb = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_IV_B_ARB_FRUT WHERE NUM_ORDEN=:NUM_ORDEN and ID_ENCUESTA=:ID_ENCUESTA');
                $commandCapIvmodb->bindParam(':NUM_ORDEN',$num_orden);
                $commandCapIvmodb->bindParam(':ID_ENCUESTA',$id_encuesta);
                $commandCapIvmodb->execute();
                 
                $transaction->commit();
                return ['success'=>true];
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
        }
    }

    public function actionEliminarModuloC(){
        $this->layout='privado';
        if($_POST){
            $num_orden_semillero = $_POST['num_orden_semillero'];
            $id_encuesta = $_POST['id_encuesta'];

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {
                

                $commandCapIvmodc = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_IV_C_SEMILLEROS WHERE NUM_ORDEN_SEMILLERO=:NUM_ORDEN_SEMILLERO and ID_ENCUESTA=:ID_ENCUESTA');
                $commandCapIvmodc->bindParam(':NUM_ORDEN_SEMILLERO',$num_orden_semillero);
                $commandCapIvmodc->bindParam(':ID_ENCUESTA',$id_encuesta);
                $commandCapIvmodc->execute();
                 
                $transaction->commit();
                return ['success'=>true];
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
        }
    }

    public function actionEliminarCalidad(){
        $this->layout='privado';
        if($_POST){
            $id_calidad = $_POST['id_calidad'];

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {

                
                
                $commandCapIvModAp424Delete = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_IV_A_DEST_PROD WHERE ID_CAP_IV_A_CALIDAD=:ID_CAP_IV_A_CALIDAD');
                $commandCapIvModAp424Delete->bindParam(':ID_CAP_IV_A_CALIDAD',$id_calidad);
                $commandCapIvModAp424Delete->execute();

                $commandCapIvModAp426aDelete = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_IV_A_VENT_PROD WHERE ID_CAP_IV_A_CALIDAD=:ID_CAP_IV_A_CALIDAD');
                $commandCapIvModAp426aDelete->bindParam(':ID_CAP_IV_A_CALIDAD',$id_calidad);
                $commandCapIvModAp426aDelete->execute();

                $commandCapIvModAp426bDelete = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_IV_A_PUNTO_VENTA WHERE ID_CAP_IV_A_CALIDAD=:ID_CAP_IV_A_CALIDAD');
                $commandCapIvModAp426bDelete->bindParam(':ID_CAP_IV_A_CALIDAD',$id_calidad);
                $commandCapIvModAp426bDelete->execute();

                $commandCapIvModAp423Delete = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_IV_A_CALIDAD WHERE ID_CAP_IV_A_CALIDAD=:ID_CAP_IV_A_CALIDAD');
                $commandCapIvModAp423Delete->bindParam(':ID_CAP_IV_A_CALIDAD',$id_calidad);
                $commandCapIvModAp423Delete->execute();

                $transaction->commit();
                return ['success'=>true];
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
        }
    }
}
