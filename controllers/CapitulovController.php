<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\CapituloVModuloB;
use app\models\CapituloVModuloG;
use app\models\CapituloVModuloH;



class CapitulovController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        echo 'no permitido';
        return;
    }

    public function actionCapituloVModuloB($id_encuesta = null,$id_especie_categoria=null,$geocodigo_fdo=null){
        $this->layout='privado';
        $cap_v_mod_b = (new \yii\db\Query())->select('ID_CAP_V_B')->from('ENA_TM_CAP_V_B')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
        if($cap_v_mod_b){
            $request = Yii::$app->request;
            $model = new CapituloVModuloB;
            if($request->isAjax){
                if ($model->load($request->post())) {
                    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    $connection = \Yii::$app->db;
                    $transaction = $connection->beginTransaction();
                    try {
                        $cap_v_mod_b_det = (new \yii\db\Query())->select('ID_DET_CAP_V_B')->from('ENA_TM_DET_CAP_V_B')->where('ID_CAP_V_B=:ID_CAP_V_B and ID_ESPECIE_CATEGORIA=:ID_ESPECIE_CATEGORIA',[':ID_CAP_V_B'=>$cap_v_mod_b['ID_CAP_V_B'],':ID_ESPECIE_CATEGORIA'=>$id_especie_categoria])->one();
                        if(!$cap_v_mod_b_det){
                            $seq = (new \yii\db\Query())->select('ENA_TM_DET_CAP_V_B_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();
                            $commandCapVModB = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_V_B (
                                ID_DET_CAP_V_B,
                                ID_CAP_V_B,
                                ID_ESPECIE_CATEGORIA,
                                NUM_CANTIDAD_ENERO,
                                NUM_CANTIDAD_NACIERON,
                                NUM_CANTIDAD_COMPRO,
                                NUM_STOCK_DONACION,
                                NUM_CANTIDAD_VENDIO,
                                NUM_PRECIO_VENDIO,
                                NUM_CANTIDAD_PIE,
                                NUM_PESO_PROMEDIO_PIE,
                                NUM_PRECIO_PIE,
                                NUM_CANTIDAD_CONSUMO_HOGAR,
                                NUM_CANTIDAD_TRUEQUE,
                                NUM_CANTIDAD_MURIERON,
                                NUM_CANTIDAD_DERIVADO,
                                NUM_CANTIDAD_DONACION,
                                NUM_CANTIDAD_DIC_2020,
                                NUM_CANTIDAD_DIC_2021
                                ) values (
                                :ID_DET_CAP_V_B,
                                :ID_CAP_V_B,
                                :ID_ESPECIE_CATEGORIA,
                                :NUM_CANTIDAD_ENERO,
                                :NUM_CANTIDAD_NACIERON,
                                :NUM_CANTIDAD_COMPRO,
                                :NUM_STOCK_DONACION,
                                :NUM_CANTIDAD_VENDIO,
                                :NUM_PRECIO_VENDIO,
                                :NUM_CANTIDAD_PIE,
                                :NUM_PESO_PROMEDIO_PIE,
                                :NUM_PRECIO_PIE,
                                :NUM_CANTIDAD_CONSUMO_HOGAR,
                                :NUM_CANTIDAD_TRUEQUE,
                                :NUM_CANTIDAD_MURIERON,
                                :NUM_CANTIDAD_DERIVADO,
                                :NUM_CANTIDAD_DONACION,
                                :NUM_CANTIDAD_DIC_2020,
                                :NUM_CANTIDAD_DIC_2021
                                )');
                            $commandCapVModB->bindParam(':ID_DET_CAP_V_B',$seq['contador']);
                            $commandCapVModB->bindParam(':ID_CAP_V_B',$cap_v_mod_b['ID_CAP_V_B']);
                            $commandCapVModB->bindParam(':ID_ESPECIE_CATEGORIA',$id_especie_categoria);
                            $commandCapVModB->bindParam(':NUM_CANTIDAD_ENERO',$model->cap5_modb_p503);
                            $commandCapVModB->bindParam(':NUM_CANTIDAD_NACIERON',$model->cap5_modb_p504_a);
                            $commandCapVModB->bindParam(':NUM_CANTIDAD_COMPRO',$model->cap5_modb_p504_b);
                            $commandCapVModB->bindParam(':NUM_STOCK_DONACION',$model->cap5_modb_p504_c);
                            $commandCapVModB->bindParam(':NUM_CANTIDAD_VENDIO',$model->cap5_modb_p504_d_1);
                            $commandCapVModB->bindParam(':NUM_PRECIO_VENDIO',$model->cap5_modb_p504_d_2);
                            $commandCapVModB->bindParam(':NUM_CANTIDAD_PIE',$model->cap5_modb_p504_e_1);
                            $commandCapVModB->bindParam(':NUM_PESO_PROMEDIO_PIE',$model->cap5_modb_p504_e_2);
                            $commandCapVModB->bindParam(':NUM_PRECIO_PIE',$model->cap5_modb_p504_e_3);
                            $commandCapVModB->bindParam(':NUM_CANTIDAD_CONSUMO_HOGAR',$model->cap5_modb_p504_f);
                            $commandCapVModB->bindParam(':NUM_CANTIDAD_TRUEQUE',$model->cap5_modb_p504_g);
                            $commandCapVModB->bindParam(':NUM_CANTIDAD_MURIERON',$model->cap5_modb_p504_h);
                            $commandCapVModB->bindParam(':NUM_CANTIDAD_DERIVADO',$model->cap5_modb_p504_i);
                            $commandCapVModB->bindParam(':NUM_CANTIDAD_DONACION',$model->cap5_modb_p504_j);
                            $commandCapVModB->bindParam(':NUM_CANTIDAD_DIC_2020',$model->cap5_modb_p506);
                            $commandCapVModB->bindParam(':NUM_CANTIDAD_DIC_2021',$model->cap5_modb_p508);
                            $commandCapVModB->execute();
                            $cap_v_mod_b_det = (new \yii\db\Query())->select('ID_DET_CAP_V_B')->from('ENA_TM_DET_CAP_V_B')->where('ID_CAP_V_B=:ID_CAP_V_B and ID_ESPECIE_CATEGORIA=:ID_ESPECIE_CATEGORIA',[':ID_CAP_V_B'=>$cap_v_mod_b['ID_CAP_V_B'],':ID_ESPECIE_CATEGORIA'=>$id_especie_categoria])->one();
                        }else{
                            $commandCapVModB = $connection->createCommand('UPDATE ENA_TM_DET_CAP_V_B 
                                SET
                                NUM_CANTIDAD_ENERO=:NUM_CANTIDAD_ENERO,
                                NUM_CANTIDAD_NACIERON=:NUM_CANTIDAD_NACIERON,
                                NUM_CANTIDAD_COMPRO=:NUM_CANTIDAD_COMPRO,
                                NUM_STOCK_DONACION=:NUM_STOCK_DONACION,
                                NUM_CANTIDAD_VENDIO=:NUM_CANTIDAD_VENDIO,
                                NUM_PRECIO_VENDIO=:NUM_PRECIO_VENDIO,
                                NUM_CANTIDAD_PIE=:NUM_CANTIDAD_PIE,
                                NUM_PESO_PROMEDIO_PIE=:NUM_PESO_PROMEDIO_PIE,
                                NUM_PRECIO_PIE=:NUM_PRECIO_PIE,
                                NUM_CANTIDAD_CONSUMO_HOGAR=:NUM_CANTIDAD_CONSUMO_HOGAR,
                                NUM_CANTIDAD_TRUEQUE=:NUM_CANTIDAD_TRUEQUE,
                                NUM_CANTIDAD_MURIERON=:NUM_CANTIDAD_MURIERON,
                                NUM_CANTIDAD_DERIVADO=:NUM_CANTIDAD_DERIVADO,
                                NUM_CANTIDAD_DONACION=:NUM_CANTIDAD_DONACION,
                                NUM_CANTIDAD_DIC_2020=:NUM_CANTIDAD_DIC_2020,
                                NUM_CANTIDAD_DIC_2021=:NUM_CANTIDAD_DIC_2021
                                where ID_DET_CAP_V_B=:ID_DET_CAP_V_B AND ID_ESPECIE_CATEGORIA=:ID_ESPECIE_CATEGORIA
                                ');
                            $commandCapVModB->bindParam(':ID_DET_CAP_V_B',$cap_v_mod_b_det['ID_DET_CAP_V_B']);
                            $commandCapVModB->bindParam(':ID_ESPECIE_CATEGORIA',$id_especie_categoria);

                            $commandCapVModB->bindParam(':NUM_CANTIDAD_ENERO',$model->cap5_modb_p503);
                            $commandCapVModB->bindParam(':NUM_CANTIDAD_NACIERON',$model->cap5_modb_p504_a);
                            $commandCapVModB->bindParam(':NUM_CANTIDAD_COMPRO',$model->cap5_modb_p504_b);
                            $commandCapVModB->bindParam(':NUM_STOCK_DONACION',$model->cap5_modb_p504_c);
                            $commandCapVModB->bindParam(':NUM_CANTIDAD_VENDIO',$model->cap5_modb_p504_d_1);
                            $commandCapVModB->bindParam(':NUM_PRECIO_VENDIO',$model->cap5_modb_p504_d_2);
                            $commandCapVModB->bindParam(':NUM_CANTIDAD_PIE',$model->cap5_modb_p504_e_1);
                            $commandCapVModB->bindParam(':NUM_PESO_PROMEDIO_PIE',$model->cap5_modb_p504_e_2);
                            $commandCapVModB->bindParam(':NUM_PRECIO_PIE',$model->cap5_modb_p504_e_3);
                            $commandCapVModB->bindParam(':NUM_CANTIDAD_CONSUMO_HOGAR',$model->cap5_modb_p504_f);
                            $commandCapVModB->bindParam(':NUM_CANTIDAD_TRUEQUE',$model->cap5_modb_p504_g);
                            $commandCapVModB->bindParam(':NUM_CANTIDAD_MURIERON',$model->cap5_modb_p504_h);
                            $commandCapVModB->bindParam(':NUM_CANTIDAD_DERIVADO',$model->cap5_modb_p504_i);
                            $commandCapVModB->bindParam(':NUM_CANTIDAD_DONACION',$model->cap5_modb_p504_j);
                            $commandCapVModB->bindParam(':NUM_CANTIDAD_DIC_2020',$model->cap5_modb_p506);
                            $commandCapVModB->bindParam(':NUM_CANTIDAD_DIC_2021',$model->cap5_modb_p508);
                            $commandCapVModB->execute();
                        }

                        /* LIMPIANDO DETALLE  P505 */
                        $commandCap05ModBP505 = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_V_B_VENTA WHERE ID_DET_CAP_V_B=:ID_DET_CAP_V_B');
                        $commandCap05ModBP505->bindParam(':ID_DET_CAP_V_B',$cap_v_mod_b_det['ID_DET_CAP_V_B']);
                        $commandCap05ModBP505->execute();

                        if($model->cap5_modb_p505){
                            $cap5_modb_p505 = explode(",",$model->cap5_modb_p505);
                            foreach($cap5_modb_p505 as $p505){
                                if($p505 == 11){
                                    $commandCap05P505Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_V_B_VENTA (ID_DET_CAP_V_B,ID_LUGAR_VENTA,TXT_OTRO) values (:ID_DET_CAP_V_B,:ID_LUGAR_VENTA,:TXT_OTRO)');
                                    $commandCap05P505Det->bindParam(':ID_DET_CAP_V_B',$cap_v_mod_b_det['ID_DET_CAP_V_B']);
                                    $commandCap05P505Det->bindParam(':ID_LUGAR_VENTA',$p505);
                                    $commandCap05P505Det->bindParam(':TXT_OTRO',$model->cap5_modb_p505_11_especifique);
                                    $commandCap05P505Det->execute();
                                }else{
                                    $commandCap05P505Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_V_B_VENTA (ID_DET_CAP_V_B,ID_LUGAR_VENTA) values (:ID_DET_CAP_V_B,:ID_LUGAR_VENTA)');
                                    $commandCap05P505Det->bindParam(':ID_DET_CAP_V_B',$cap_v_mod_b_det['ID_DET_CAP_V_B']);
                                    $commandCap05P505Det->bindParam(':ID_LUGAR_VENTA',$p505);
                                    $commandCap05P505Det->execute();
                                }
                                
                            }
                        }

                        /* LIMPIANDO DETALLE  P507 */
                        $commandCap05ModBP507 = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_V_B_MUERTE WHERE ID_DET_CAP_V_B=:ID_DET_CAP_V_B');
                        $commandCap05ModBP507->bindParam(':ID_DET_CAP_V_B',$cap_v_mod_b_det['ID_DET_CAP_V_B']);
                        $commandCap05ModBP507->execute();

                        if($model->cap5_modb_p507){
                            $cap5_modb_p507 = explode(",",$model->cap5_modb_p507);
                            foreach($cap5_modb_p507 as $p507){
                                if($p507 == 10){
                                    $commandCap05P507Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_V_B_MUERTE (ID_DET_CAP_V_B,ID_CAUSA_MUERTE,TXT_OTRO) values (:ID_DET_CAP_V_B,:ID_CAUSA_MUERTE,:TXT_OTRO)');
                                    $commandCap05P507Det->bindParam(':ID_DET_CAP_V_B',$cap_v_mod_b_det['ID_DET_CAP_V_B']);
                                    $commandCap05P507Det->bindParam(':ID_CAUSA_MUERTE',$p507);
                                    $commandCap05P507Det->bindParam(':TXT_OTRO',$model->cap5_modb_p507_10_especifique);
                                    $commandCap05P507Det->execute();
                                }else{
                                    $commandCap05P507Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_V_B_MUERTE (ID_DET_CAP_V_B,ID_CAUSA_MUERTE) values (:ID_DET_CAP_V_B,:ID_CAUSA_MUERTE)');
                                    $commandCap05P507Det->bindParam(':ID_DET_CAP_V_B',$cap_v_mod_b_det['ID_DET_CAP_V_B']);
                                    $commandCap05P507Det->bindParam(':ID_CAUSA_MUERTE',$p507);
                                    $commandCap05P507Det->execute();
                                }
                                
                            }
                        }
                        

                        $transaction->commit();
                        return ['success'=>true];
                    } catch (\Exception $e) {
                        $transaction->rollBack();
                        throw $e;
                    } catch (\Throwable $e) {
                        $transaction->rollBack();
                        throw $e;
                    }
                }
            }
            
            return $this->render('capitulo-v-modulo-b',['id_encuesta'=>$id_encuesta,'id_especie_categoria'=>$id_especie_categoria,'geocodigo_fdo'=>$geocodigo_fdo]);
        }
        
    }

    public function actionViewCapituloVModuloB($id_encuesta = null,$id_especie_categoria=null){
        $this->layout='privado';
        $cap_v_mod_b = (new \yii\db\Query())->select('ID_CAP_V_B')->from('ENA_TM_CAP_V_B')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
        if($cap_v_mod_b['ID_CAP_V_B']!=null){
            return $this->render('view-capitulo-v-modulo-b',['id_encuesta'=>$id_encuesta,'id_especie_categoria'=>$id_especie_categoria]);
        }
        
    }

    public function actionGetModuloB(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $id_encuesta = $_POST['id_encuesta'];
            $id_especie_categoria = $_POST['id_especie_categoria'];
            $capVModBDet = null;
            $capVModBDetP505 = null;
            $capVModBDetP507 = null;

            $capVModB = (new \yii\db\Query())->select('ID_CAP_V_B')->from('ENA_TM_CAP_V_B')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
            if($capVModB){
                $capVModBDet = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_V_B')->where('ID_CAP_V_B=:ID_CAP_V_B AND ID_ESPECIE_CATEGORIA=:ID_ESPECIE_CATEGORIA',[':ID_CAP_V_B'=>$capVModB['ID_CAP_V_B'],'ID_ESPECIE_CATEGORIA'=>$id_especie_categoria])->one();
                if($capVModBDet){
                    $capVModBDetP505 = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_V_B_VENTA')->where('ID_DET_CAP_V_B=:ID_DET_CAP_V_B',[':ID_DET_CAP_V_B'=>$capVModBDet['ID_DET_CAP_V_B']])->all();
                    $capVModBDetP507 = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_V_B_MUERTE')->where('ID_DET_CAP_V_B=:ID_DET_CAP_V_B',[':ID_DET_CAP_V_B'=>$capVModBDet['ID_DET_CAP_V_B']])->all();
                }
                
            }
            

            return [
                'success'=>true,
                'cabecera'=>$capVModBDet,
                'detalle_p505'=>$capVModBDetP505,
                'detalle_p507'=>$capVModBDetP507
            ];
        }
    }

    public function actionCapituloVModuloG1($id_encuesta = null,$id_avicola=null,$geocodigo_fdo=null){
        $this->layout='privado';
        
            $request = Yii::$app->request;
            $model = new CapituloVModuloG;
            if($request->isAjax){
                if ($model->load($request->post())) {
                    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    $connection = \Yii::$app->db;
                    $transaction = $connection->beginTransaction();
                    try {
                        $cap_v_mod_g = (new \yii\db\Query())->select('ID_CAP_V_G_CARNE')->from('ENA_TM_CAP_V_G_CARNE')->where('ID_ENCUESTA=:ID_ENCUESTA AND ID_AVICOLA=:ID_AVICOLA',[':ID_ENCUESTA'=>$id_encuesta,':ID_AVICOLA'=>$id_avicola])->one();
                        
                        if(!$cap_v_mod_g){
                            $seq = (new \yii\db\Query())->select('ENA_TM_CAP_V_G_CARNE_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();
                            $commandCapVModG = $connection->createCommand('INSERT INTO ENA_TM_CAP_V_G_CARNE (
                                ID_CAP_V_G_CARNE,
                                ID_ENCUESTA,
                                ID_AVICOLA,
                                NUM_CANTIDAD_ENERO,
                                NUM_CANTIDAD_DIC,
                                NUM_CANTIDAD_JUL,
                                NUM_CANTIDAD_COBB,
                                NUM_CANTIDAD_ROSS,
                                NUM_CANTIDAD_HYBRID,
                                NUM_CANTIDAD_NICHOLAS,
                                NUM_CANTIDAD_HIBRONICHOLA,
                                NUM_CANTIDAD_OTRA,
                                NUM_HUEVO_FERTIL,
                                NUM_HUEVO_INCUBABLE,
                                NUM_SACA_STOCK,
                                NUM_SACA_EDAD,
                                NUM_SACA_VOLUMEN,
                                NUM_SACA_PESO_PIE,
                                NUM_SACA_PRECIO_PROD,
                                NUM_MORTALIDAD_LEVANTE,
                                NUM_MORTALIDAD_PRODUCCION,
                                NUM_MORTALIDAD_TOTAL
                                ) values (
                                :ID_CAP_V_G_CARNE,
                                :ID_ENCUESTA,
                                :ID_AVICOLA,
                                :NUM_CANTIDAD_ENERO,
                                :NUM_CANTIDAD_DIC,
                                :NUM_CANTIDAD_JUL,
                                :NUM_CANTIDAD_COBB,
                                :NUM_CANTIDAD_ROSS,
                                :NUM_CANTIDAD_HYBRID,
                                :NUM_CANTIDAD_NICHOLAS,
                                :NUM_CANTIDAD_HIBRONICHOLA,
                                :NUM_CANTIDAD_OTRA,
                                :NUM_HUEVO_FERTIL,
                                :NUM_HUEVO_INCUBABLE,
                                :NUM_SACA_STOCK,
                                :NUM_SACA_EDAD,
                                :NUM_SACA_VOLUMEN,
                                :NUM_SACA_PESO_PIE,
                                :NUM_SACA_PRECIO_PROD,
                                :NUM_MORTALIDAD_LEVANTE,
                                :NUM_MORTALIDAD_PRODUCCION,
                                :NUM_MORTALIDAD_TOTAL
                                )');
                            $commandCapVModG->bindParam(':ID_CAP_V_G_CARNE',$seq['contador']);
                            $commandCapVModG->bindParam(':ID_ENCUESTA',$id_encuesta);
                            $commandCapVModG->bindParam(':ID_AVICOLA',$id_avicola);
                            $commandCapVModG->bindParam(':NUM_CANTIDAD_ENERO',$model->cap5_modg_p510);
                            $commandCapVModG->bindParam(':NUM_CANTIDAD_DIC',$model->cap5_modg_p511);
                            $commandCapVModG->bindParam(':NUM_CANTIDAD_JUL',$model->cap5_modg_p512);
                            $commandCapVModG->bindParam(':NUM_CANTIDAD_COBB',$model->cap5_modg_p513_a);
                            $commandCapVModG->bindParam(':NUM_CANTIDAD_ROSS',$model->cap5_modg_p513_b);
                            $commandCapVModG->bindParam(':NUM_CANTIDAD_HYBRID',$model->cap5_modg_p513_c);
                            $commandCapVModG->bindParam(':NUM_CANTIDAD_NICHOLAS',$model->cap5_modg_p513_d);
                            $commandCapVModG->bindParam(':NUM_CANTIDAD_HIBRONICHOLA',$model->cap5_modg_p513_e);
                            $commandCapVModG->bindParam(':NUM_CANTIDAD_OTRA',$model->cap5_modg_p513_f);
                            $commandCapVModG->bindParam(':NUM_HUEVO_FERTIL',$model->cap5_modg_p514_a);
                            $commandCapVModG->bindParam(':NUM_HUEVO_INCUBABLE',$model->cap5_modg_p514_b);
                            $commandCapVModG->bindParam(':NUM_SACA_STOCK',$model->cap5_modg_p515_a_1);
                            $commandCapVModG->bindParam(':NUM_SACA_EDAD',$model->cap5_modg_p515_a_2);
                            $commandCapVModG->bindParam(':NUM_SACA_VOLUMEN',$model->cap5_modg_p515_a_3);
                            $commandCapVModG->bindParam(':NUM_SACA_PESO_PIE',$model->cap5_modg_p515_a_4);
                            $commandCapVModG->bindParam(':NUM_SACA_PRECIO_PROD',$model->cap5_modg_p515_a_5);
                            $commandCapVModG->bindParam(':NUM_MORTALIDAD_LEVANTE',$model->cap5_modg_p515_b_1);
                            $commandCapVModG->bindParam(':NUM_MORTALIDAD_PRODUCCION',$model->cap5_modg_p515_b_2);
                            $commandCapVModG->bindParam(':NUM_MORTALIDAD_TOTAL',$model->cap5_modg_p515_b_3);
                            $commandCapVModG->execute();
                            $cap_v_mod_g = (new \yii\db\Query())->select('ID_CAP_V_G_CARNE')->from('ENA_TM_CAP_V_G_CARNE')->where('ID_ENCUESTA=:ID_ENCUESTA AND ID_AVICOLA=:ID_AVICOLA',[':ID_ENCUESTA'=>$id_encuesta,':ID_AVICOLA'=>$id_avicola])->one();
                        }else{
                            $commandCapVModG = $connection->createCommand('UPDATE ENA_TM_CAP_V_G_CARNE 
                                SET
                                NUM_CANTIDAD_ENERO=:NUM_CANTIDAD_ENERO,
                                NUM_CANTIDAD_DIC=:NUM_CANTIDAD_DIC,
                                NUM_CANTIDAD_JUL=:NUM_CANTIDAD_JUL,
                                NUM_CANTIDAD_COBB=:NUM_CANTIDAD_COBB,
                                NUM_CANTIDAD_ROSS=:NUM_CANTIDAD_ROSS,
                                NUM_CANTIDAD_HYBRID=:NUM_CANTIDAD_HYBRID,
                                NUM_CANTIDAD_NICHOLAS=:NUM_CANTIDAD_NICHOLAS,
                                NUM_CANTIDAD_HIBRONICHOLA=:NUM_CANTIDAD_HIBRONICHOLA,
                                NUM_CANTIDAD_OTRA=:NUM_CANTIDAD_OTRA,
                                NUM_HUEVO_FERTIL=:NUM_HUEVO_FERTIL,
                                NUM_HUEVO_INCUBABLE=:NUM_HUEVO_INCUBABLE,
                                NUM_SACA_STOCK=:NUM_SACA_STOCK,
                                NUM_SACA_EDAD=:NUM_SACA_EDAD,
                                NUM_SACA_VOLUMEN=:NUM_SACA_VOLUMEN,
                                NUM_SACA_PESO_PIE=:NUM_SACA_PESO_PIE,
                                NUM_SACA_PRECIO_PROD=:NUM_SACA_PRECIO_PROD,
                                NUM_MORTALIDAD_LEVANTE=:NUM_MORTALIDAD_LEVANTE,
                                NUM_MORTALIDAD_PRODUCCION=:NUM_MORTALIDAD_PRODUCCION,
                                NUM_MORTALIDAD_TOTAL=:NUM_MORTALIDAD_TOTAL
                                where ID_ENCUESTA=:ID_ENCUESTA AND ID_AVICOLA=:ID_AVICOLA
                                ');
                            $commandCapVModG->bindParam(':ID_ENCUESTA',$id_encuesta);
                            $commandCapVModG->bindParam(':ID_AVICOLA',$id_avicola);
                            $commandCapVModG->bindParam(':NUM_CANTIDAD_ENERO',$model->cap5_modg_p510);
                            $commandCapVModG->bindParam(':NUM_CANTIDAD_DIC',$model->cap5_modg_p511);
                            $commandCapVModG->bindParam(':NUM_CANTIDAD_JUL',$model->cap5_modg_p512);
                            $commandCapVModG->bindParam(':NUM_CANTIDAD_COBB',$model->cap5_modg_p513_a);
                            $commandCapVModG->bindParam(':NUM_CANTIDAD_ROSS',$model->cap5_modg_p513_b);
                            $commandCapVModG->bindParam(':NUM_CANTIDAD_HYBRID',$model->cap5_modg_p513_c);
                            $commandCapVModG->bindParam(':NUM_CANTIDAD_NICHOLAS',$model->cap5_modg_p513_d);
                            $commandCapVModG->bindParam(':NUM_CANTIDAD_HIBRONICHOLA',$model->cap5_modg_p513_e);
                            $commandCapVModG->bindParam(':NUM_CANTIDAD_OTRA',$model->cap5_modg_p513_f);
                            $commandCapVModG->bindParam(':NUM_HUEVO_FERTIL',$model->cap5_modg_p514_a);
                            $commandCapVModG->bindParam(':NUM_HUEVO_INCUBABLE',$model->cap5_modg_p514_b);
                            $commandCapVModG->bindParam(':NUM_SACA_STOCK',$model->cap5_modg_p515_a_1);
                            $commandCapVModG->bindParam(':NUM_SACA_EDAD',$model->cap5_modg_p515_a_2);
                            $commandCapVModG->bindParam(':NUM_SACA_VOLUMEN',$model->cap5_modg_p515_a_3);
                            $commandCapVModG->bindParam(':NUM_SACA_PESO_PIE',$model->cap5_modg_p515_a_4);
                            $commandCapVModG->bindParam(':NUM_SACA_PRECIO_PROD',$model->cap5_modg_p515_a_5);
                            $commandCapVModG->bindParam(':NUM_MORTALIDAD_LEVANTE',$model->cap5_modg_p515_b_1);
                            $commandCapVModG->bindParam(':NUM_MORTALIDAD_PRODUCCION',$model->cap5_modg_p515_b_2);
                            $commandCapVModG->bindParam(':NUM_MORTALIDAD_TOTAL',$model->cap5_modg_p515_b_3);
                            $commandCapVModG->execute();
                        }


                        $transaction->commit();
                        return ['success'=>true];
                    } catch (\Exception $e) {
                        $transaction->rollBack();
                        throw $e;
                    } catch (\Throwable $e) {
                        $transaction->rollBack();
                        throw $e;
                    }
                }
            }
            
            return $this->render('capitulo-v-modulo-g1',['id_encuesta'=>$id_encuesta,'id_avicola'=>$id_avicola,'geocodigo_fdo'=>$geocodigo_fdo]);
        
    }

    public function actionViewCapituloVModuloG1($id_encuesta = null,$id_avicola=null){
        $this->layout='privado';
        return $this->render('view-capitulo-v-modulo-g1',['id_encuesta'=>$id_encuesta,'id_avicola'=>$id_avicola]);
    }

    public function actionGetModuloG1(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $id_encuesta = $_POST['id_encuesta'];
            $id_avicola = $_POST['id_avicola'];
            $capVModG = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_V_G_CARNE')->where('ID_ENCUESTA=:ID_ENCUESTA AND ID_AVICOLA=:ID_AVICOLA',[':ID_ENCUESTA'=>$id_encuesta,':ID_AVICOLA'=>$id_avicola])->one();

            return [
                'success'=>true,
                'cabecera'=>$capVModG
            ];
        }
    }

    public function actionCapituloVModuloG2($id_encuesta = null,$id_avicola=null,$geocodigo_fdo=null){
        $this->layout='privado';
        
            $request = Yii::$app->request;
            $model = new CapituloVModuloG;
            if($request->isAjax){
                if ($model->load($request->post())) {
                    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    $connection = \Yii::$app->db;
                    $transaction = $connection->beginTransaction();
                    try {
                        $cap_v_mod_g = (new \yii\db\Query())->select('ID_CAP_V_G_POSTURA')->from('ENA_TM_CAP_V_G_POSTURA')->where('ID_ENCUESTA=:ID_ENCUESTA AND ID_AVICOLA=:ID_AVICOLA',[':ID_ENCUESTA'=>$id_encuesta,':ID_AVICOLA'=>$id_avicola])->one();
                        
                        if(!$cap_v_mod_g){
                            $seq = (new \yii\db\Query())->select('ENA_TM_CAP_V_G_POSTURA_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();
                            $commandCapVModG = $connection->createCommand('INSERT INTO ENA_TM_CAP_V_G_POSTURA (
                                ID_CAP_V_G_POSTURA,
                                ID_ENCUESTA,
                                ID_AVICOLA,
                                NUM_CANTIDAD_ENERO,
                                NUM_CANTIDAD_DIC,
                                NUM_CANTIDAD_JUL,
                                NUM_CANTIDAD_HYLINE,
                                NUM_CANTIDAD_ISA,
                                NUM_CANTIDAD_LOHMAN,
                                NUM_CANTIDAD_BABCOK,
                                NUM_CANTIDAD_BOVANS,
                                NUM_CANTIDAD_HISEX,
                                NUM_CANTIDAD_HYN,
                                NUM_CANTIDAD_TETRA,
                                NUM_CANTIDAD_OTRA,
                                NUM_HUEVO_FERTIL,
                                NUM_HUEVO_INCUBABLE,
                                NUM_HUEVO_COMERCIAL,
                                NUM_VOLUMEN_HUEVO_COMERCIAL,
                                NUM_SACA_STOCK,
                                NUM_SACA_EDAD,
                                NUM_SACA_VOLUMEN,
                                NUM_SACA_PESO_PIE,
                                NUM_SACA_PRECIO_PROD,
                                NUM_MORTALIDAD_TOTAL
                                ) values (
                                :ID_CAP_V_G_POSTURA,
                                :ID_ENCUESTA,
                                :ID_AVICOLA,
                                :NUM_CANTIDAD_ENERO,
                                :NUM_CANTIDAD_DIC,
                                :NUM_CANTIDAD_JUL,
                                :NUM_CANTIDAD_HYLINE,
                                :NUM_CANTIDAD_ISA,
                                :NUM_CANTIDAD_LOHMAN,
                                :NUM_CANTIDAD_BABCOK,
                                :NUM_CANTIDAD_BOVANS,
                                :NUM_CANTIDAD_HISEX,
                                :NUM_CANTIDAD_HYN,
                                :NUM_CANTIDAD_TETRA,
                                :NUM_CANTIDAD_OTRA,
                                :NUM_HUEVO_FERTIL,
                                :NUM_HUEVO_INCUBABLE,
                                :NUM_HUEVO_COMERCIAL,
                                :NUM_VOLUMEN_HUEVO_COMERCIAL,
                                :NUM_SACA_STOCK,
                                :NUM_SACA_EDAD,
                                :NUM_SACA_VOLUMEN,
                                :NUM_SACA_PESO_PIE,
                                :NUM_SACA_PRECIO_PROD,
                                :NUM_MORTALIDAD_TOTAL
                                )');
                            $commandCapVModG->bindParam(':ID_CAP_V_G_POSTURA',$seq['contador']);
                            $commandCapVModG->bindParam(':ID_ENCUESTA',$id_encuesta);
                            $commandCapVModG->bindParam(':ID_AVICOLA',$id_avicola);
                            $commandCapVModG->bindParam(':NUM_CANTIDAD_ENERO',$model->cap5_modg_p517);
                            $commandCapVModG->bindParam(':NUM_CANTIDAD_DIC',$model->cap5_modg_p518);
                            $commandCapVModG->bindParam(':NUM_CANTIDAD_JUL',$model->cap5_modg_p519);
                            $commandCapVModG->bindParam(':NUM_CANTIDAD_HYLINE',$model->cap5_modg_p520_a);
                            $commandCapVModG->bindParam(':NUM_CANTIDAD_ISA',$model->cap5_modg_p520_b);
                            $commandCapVModG->bindParam(':NUM_CANTIDAD_LOHMAN',$model->cap5_modg_p520_c);
                            $commandCapVModG->bindParam(':NUM_CANTIDAD_BABCOK',$model->cap5_modg_p520_d);
                            $commandCapVModG->bindParam(':NUM_CANTIDAD_BOVANS',$model->cap5_modg_p520_e);
                            $commandCapVModG->bindParam(':NUM_CANTIDAD_HISEX',$model->cap5_modg_p520_f);
                            $commandCapVModG->bindParam(':NUM_CANTIDAD_HYN',$model->cap5_modg_p520_g);
                            $commandCapVModG->bindParam(':NUM_CANTIDAD_TETRA',$model->cap5_modg_p520_h);
                            $commandCapVModG->bindParam(':NUM_CANTIDAD_OTRA',$model->cap5_modg_p520_i);
                            $commandCapVModG->bindParam(':NUM_HUEVO_FERTIL',$model->cap5_modg_p521_a);
                            $commandCapVModG->bindParam(':NUM_HUEVO_INCUBABLE',$model->cap5_modg_p521_b);
                            $commandCapVModG->bindParam(':NUM_HUEVO_COMERCIAL',$model->cap5_modg_p521_c);
                            $commandCapVModG->bindParam(':NUM_VOLUMEN_HUEVO_COMERCIAL',$model->cap5_modg_p521_d);
                            $commandCapVModG->bindParam(':NUM_SACA_STOCK',$model->cap5_modg_p522_a_1);
                            $commandCapVModG->bindParam(':NUM_SACA_EDAD',$model->cap5_modg_p522_a_2);
                            $commandCapVModG->bindParam(':NUM_SACA_VOLUMEN',$model->cap5_modg_p522_a_3);
                            $commandCapVModG->bindParam(':NUM_SACA_PESO_PIE',$model->cap5_modg_p522_a_4);
                            $commandCapVModG->bindParam(':NUM_SACA_PRECIO_PROD',$model->cap5_modg_p522_a_5);
                            $commandCapVModG->bindParam(':NUM_MORTALIDAD_TOTAL',$model->cap5_modg_p522_b);
                            $commandCapVModG->execute();
                            $cap_v_mod_g = (new \yii\db\Query())->select('ID_CAP_V_G_POSTURA')->from('ENA_TM_CAP_V_G_POSTURA')->where('ID_ENCUESTA=:ID_ENCUESTA AND ID_AVICOLA=:ID_AVICOLA',[':ID_ENCUESTA'=>$id_encuesta,':ID_AVICOLA'=>$id_avicola])->one();
                        }else{
                            $commandCapVModG = $connection->createCommand('UPDATE ENA_TM_CAP_V_G_POSTURA 
                                SET
                                NUM_CANTIDAD_ENERO=:NUM_CANTIDAD_ENERO,
                                NUM_CANTIDAD_DIC=:NUM_CANTIDAD_DIC,
                                NUM_CANTIDAD_JUL=:NUM_CANTIDAD_JUL,
                                NUM_CANTIDAD_HYLINE=:NUM_CANTIDAD_HYLINE,
                                NUM_CANTIDAD_ISA=:NUM_CANTIDAD_ISA,
                                NUM_CANTIDAD_LOHMAN=:NUM_CANTIDAD_LOHMAN,
                                NUM_CANTIDAD_BABCOK=:NUM_CANTIDAD_BABCOK,
                                NUM_CANTIDAD_BOVANS=:NUM_CANTIDAD_BOVANS,
                                NUM_CANTIDAD_HISEX=:NUM_CANTIDAD_HISEX,
                                NUM_CANTIDAD_HYN=:NUM_CANTIDAD_HYN,
                                NUM_CANTIDAD_TETRA=:NUM_CANTIDAD_TETRA,
                                NUM_CANTIDAD_OTRA=:NUM_CANTIDAD_OTRA,
                                NUM_HUEVO_FERTIL=:NUM_HUEVO_FERTIL,
                                NUM_HUEVO_INCUBABLE=:NUM_HUEVO_INCUBABLE,
                                NUM_HUEVO_COMERCIAL=:NUM_HUEVO_COMERCIAL,
                                NUM_VOLUMEN_HUEVO_COMERCIAL=:NUM_VOLUMEN_HUEVO_COMERCIAL,
                                NUM_SACA_STOCK=:NUM_SACA_STOCK,
                                NUM_SACA_EDAD=:NUM_SACA_EDAD,
                                NUM_SACA_VOLUMEN=:NUM_SACA_VOLUMEN,
                                NUM_SACA_PESO_PIE=:NUM_SACA_PESO_PIE,
                                NUM_SACA_PRECIO_PROD=:NUM_SACA_PRECIO_PROD,
                                NUM_MORTALIDAD_TOTAL=:NUM_MORTALIDAD_TOTAL
                                where ID_ENCUESTA=:ID_ENCUESTA AND ID_AVICOLA=:ID_AVICOLA
                                ');
                            $commandCapVModG->bindParam(':ID_ENCUESTA',$id_encuesta);
                            $commandCapVModG->bindParam(':ID_AVICOLA',$id_avicola);
                            $commandCapVModG->bindParam(':NUM_CANTIDAD_ENERO',$model->cap5_modg_p517);
                            $commandCapVModG->bindParam(':NUM_CANTIDAD_DIC',$model->cap5_modg_p518);
                            $commandCapVModG->bindParam(':NUM_CANTIDAD_JUL',$model->cap5_modg_p519);
                            $commandCapVModG->bindParam(':NUM_CANTIDAD_HYLINE',$model->cap5_modg_p520_a);
                            $commandCapVModG->bindParam(':NUM_CANTIDAD_ISA',$model->cap5_modg_p520_b);
                            $commandCapVModG->bindParam(':NUM_CANTIDAD_LOHMAN',$model->cap5_modg_p520_c);
                            $commandCapVModG->bindParam(':NUM_CANTIDAD_BABCOK',$model->cap5_modg_p520_d);
                            $commandCapVModG->bindParam(':NUM_CANTIDAD_BOVANS',$model->cap5_modg_p520_e);
                            $commandCapVModG->bindParam(':NUM_CANTIDAD_HISEX',$model->cap5_modg_p520_f);
                            $commandCapVModG->bindParam(':NUM_CANTIDAD_HYN',$model->cap5_modg_p520_g);
                            $commandCapVModG->bindParam(':NUM_CANTIDAD_TETRA',$model->cap5_modg_p520_h);
                            $commandCapVModG->bindParam(':NUM_CANTIDAD_OTRA',$model->cap5_modg_p520_i);
                            $commandCapVModG->bindParam(':NUM_HUEVO_FERTIL',$model->cap5_modg_p521_a);
                            $commandCapVModG->bindParam(':NUM_HUEVO_INCUBABLE',$model->cap5_modg_p521_b);
                            $commandCapVModG->bindParam(':NUM_HUEVO_COMERCIAL',$model->cap5_modg_p521_c);
                            $commandCapVModG->bindParam(':NUM_VOLUMEN_HUEVO_COMERCIAL',$model->cap5_modg_p521_d);
                            $commandCapVModG->bindParam(':NUM_SACA_STOCK',$model->cap5_modg_p522_a_1);
                            $commandCapVModG->bindParam(':NUM_SACA_EDAD',$model->cap5_modg_p522_a_2);
                            $commandCapVModG->bindParam(':NUM_SACA_VOLUMEN',$model->cap5_modg_p522_a_3);
                            $commandCapVModG->bindParam(':NUM_SACA_PESO_PIE',$model->cap5_modg_p522_a_4);
                            $commandCapVModG->bindParam(':NUM_SACA_PRECIO_PROD',$model->cap5_modg_p522_a_5);
                            $commandCapVModG->bindParam(':NUM_MORTALIDAD_TOTAL',$model->cap5_modg_p522_b);
                            $commandCapVModG->execute();
                        }


                        $transaction->commit();
                        return ['success'=>true];
                    } catch (\Exception $e) {
                        $transaction->rollBack();
                        throw $e;
                    } catch (\Throwable $e) {
                        $transaction->rollBack();
                        throw $e;
                    }
                }
            }
            
            return $this->render('capitulo-v-modulo-g2',['id_encuesta'=>$id_encuesta,'id_avicola'=>$id_avicola,'geocodigo_fdo'=>$geocodigo_fdo]);
        
    }

    public function actionViewCapituloVModuloG2($id_encuesta = null,$id_avicola=null){
        $this->layout='privado';
        return $this->render('view-capitulo-v-modulo-g2',['id_encuesta'=>$id_encuesta,'id_avicola'=>$id_avicola]);
        
    }

    public function actionGetModuloG2(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $id_encuesta = $_POST['id_encuesta'];
            $id_avicola = $_POST['id_avicola'];
            $capVModG = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_V_G_POSTURA')->where('ID_ENCUESTA=:ID_ENCUESTA AND ID_AVICOLA=:ID_AVICOLA',[':ID_ENCUESTA'=>$id_encuesta,':ID_AVICOLA'=>$id_avicola])->one();
            return [
                'success'=>true,
                'cabecera'=>$capVModG
            ];
        }
    }

    public function actionCapituloVModuloH($id_encuesta = null,$id_producto=null,$geocodigo_fdo=null){
        $this->layout='privado';
        $cap_v_mod_h = (new \yii\db\Query())->select('ID_CAP_V_H')->from('ENA_TM_CAP_V_H')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
        if($cap_v_mod_h['ID_CAP_V_H']!=null){
            $request = Yii::$app->request;
            $model = new CapituloVModuloH;
            if($request->isAjax){
                if ($model->load($request->post())) {
                    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    $connection = \Yii::$app->db;
                    $transaction = $connection->beginTransaction();
                    try {
                        $cap_v_mod_det_h = (new \yii\db\Query())->select('ID_DET_CAP_V_H,ID_CAP_V_H')->from('ENA_TM_DET_CAP_V_H_PECUARIO')->where('ID_CAP_V_H=:ID_CAP_V_H AND ID_PRODUCTO=:ID_PRODUCTO',[':ID_CAP_V_H'=>$cap_v_mod_h['ID_CAP_V_H'],':ID_PRODUCTO'=>$id_producto])->one();
                        
                        if(!$cap_v_mod_det_h){
                            $seq = (new \yii\db\Query())->select('ENA_TM_DET_CAP_V_H_PECUARIO_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();
                            $commandCapVModH = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_V_H_PECUARIO (
                                ID_DET_CAP_V_H,
                                ID_CAP_V_H,
                                ID_PRODUCTO,
                                NUM_CANTIDAD_PRODUCCION,
                                ID_UM_PRODUCCION,
                                NUM_EQ_PRODUCCION,
                                NUM_CANTIDAD_PRODUCCION_TOTAL,
                                NUM_CANTIDAD_ANIMALES,
                                NUM_RENDIMIENTO,
                                NUM_PRODUCCION_VENTA,
                                ID_UM_VENTA,
                                NUM_PRECIO_VENTA,
                                NUM_PRODUCCION_CONSUMO,
                                ID_UM_CONSUMO,
                                NUM_EQ_CONSUMO,
                                NUM_PRODUCCION_DERIVADOS,
                                ID_UM_DERIVADOS,
                                NUM_EQ_DERIVADOS
                                ) values (
                                :ID_DET_CAP_V_H,
                                :ID_CAP_V_H,
                                :ID_PRODUCTO,
                                :NUM_CANTIDAD_PRODUCCION,
                                :ID_UM_PRODUCCION,
                                :NUM_EQ_PRODUCCION,
                                :NUM_CANTIDAD_PRODUCCION_TOTAL,
                                :NUM_CANTIDAD_ANIMALES,
                                :NUM_RENDIMIENTO,
                                :NUM_PRODUCCION_VENTA,
                                :ID_UM_VENTA,
                                :NUM_PRECIO_VENTA,
                                :NUM_PRODUCCION_CONSUMO,
                                :ID_UM_CONSUMO,
                                :NUM_EQ_CONSUMO,
                                :NUM_PRODUCCION_DERIVADOS,
                                :ID_UM_DERIVADOS,
                                :NUM_EQ_DERIVADOS
                                )');
                            $commandCapVModH->bindParam(':ID_DET_CAP_V_H',$seq['contador']);
                            $commandCapVModH->bindParam(':ID_CAP_V_H',$cap_v_mod_h['ID_CAP_V_H']);
                            $commandCapVModH->bindParam(':ID_PRODUCTO',$id_producto);
                            $commandCapVModH->bindParam(':NUM_CANTIDAD_PRODUCCION',$model->cap5_modh_p525_a);
                            $commandCapVModH->bindParam(':ID_UM_PRODUCCION',$model->cap5_modh_p525_b);
                            $commandCapVModH->bindParam(':NUM_EQ_PRODUCCION',$model->cap5_modh_p525_c);
                            $commandCapVModH->bindParam(':NUM_CANTIDAD_PRODUCCION_TOTAL',$model->cap5_modh_p525_d);
                            $commandCapVModH->bindParam(':NUM_CANTIDAD_ANIMALES',$model->cap5_modh_p526);
                            $commandCapVModH->bindParam(':NUM_RENDIMIENTO',$model->cap5_modh_p526_a);
                            $commandCapVModH->bindParam(':NUM_PRODUCCION_VENTA',$model->cap5_modh_p527_a);
                            $commandCapVModH->bindParam(':ID_UM_VENTA',$model->cap5_modh_p527_b);
                            $commandCapVModH->bindParam(':NUM_PRECIO_VENTA',$model->cap5_modh_p527_c);
                            $commandCapVModH->bindParam(':NUM_PRODUCCION_CONSUMO',$model->cap5_modh_p528_a);
                            $commandCapVModH->bindParam(':ID_UM_CONSUMO',$model->cap5_modh_p528_b);
                            $commandCapVModH->bindParam(':NUM_EQ_CONSUMO',$model->cap5_modh_p528_c);
                            $commandCapVModH->bindParam(':NUM_PRODUCCION_DERIVADOS',$model->cap5_modh_p529_a);
                            $commandCapVModH->bindParam(':ID_UM_DERIVADOS',$model->cap5_modh_p529_b);
                            $commandCapVModH->bindParam(':NUM_EQ_DERIVADOS',$model->cap5_modh_p529_c);
                            $commandCapVModH->execute();
                            //$cap_v_mod_det_h = (new \yii\db\Query())->select('ID_DET_CAP_V_H')->from('ENA_TM_DET_CAP_V_H_PECUARIO')->where('ID_CAP_V_H=:ID_CAP_V_H AND ID_PRODUCTO=:ID_PRODUCTO',[':ID_CAP_V_H'=>$cap_v_mod_h['ID_CAP_V_H'],':ID_PRODUCTO'=>$id_producto])->one();
                        }else{
                            $commandCapVModH = $connection->createCommand('UPDATE ENA_TM_DET_CAP_V_H_PECUARIO 
                                SET
                                NUM_CANTIDAD_PRODUCCION=:NUM_CANTIDAD_PRODUCCION,
                                ID_UM_PRODUCCION=:ID_UM_PRODUCCION,
                                NUM_EQ_PRODUCCION=:NUM_EQ_PRODUCCION,
                                NUM_CANTIDAD_PRODUCCION_TOTAL=:NUM_CANTIDAD_PRODUCCION_TOTAL,
                                NUM_CANTIDAD_ANIMALES=:NUM_CANTIDAD_ANIMALES,
                                NUM_RENDIMIENTO=:NUM_RENDIMIENTO,
                                NUM_PRODUCCION_VENTA=:NUM_PRODUCCION_VENTA,
                                ID_UM_VENTA=:ID_UM_VENTA,
                                NUM_PRECIO_VENTA=:NUM_PRECIO_VENTA,
                                NUM_PRODUCCION_CONSUMO=:NUM_PRODUCCION_CONSUMO,
                                ID_UM_CONSUMO=:ID_UM_CONSUMO,
                                NUM_EQ_CONSUMO=:NUM_EQ_CONSUMO,
                                NUM_PRODUCCION_DERIVADOS=:NUM_PRODUCCION_DERIVADOS,
                                ID_UM_DERIVADOS=:ID_UM_DERIVADOS,
                                NUM_EQ_DERIVADOS=:NUM_EQ_DERIVADOS
                                where ID_CAP_V_H=:ID_CAP_V_H AND ID_PRODUCTO=:ID_PRODUCTO
                                ');
                            $commandCapVModH->bindParam(':ID_CAP_V_H',$cap_v_mod_det_h['ID_CAP_V_H']);
                            $commandCapVModH->bindParam(':ID_PRODUCTO',$id_producto);
                            $commandCapVModH->bindParam(':NUM_CANTIDAD_PRODUCCION',$model->cap5_modh_p525_a);
                            $commandCapVModH->bindParam(':ID_UM_PRODUCCION',$model->cap5_modh_p525_b);
                            $commandCapVModH->bindParam(':NUM_EQ_PRODUCCION',$model->cap5_modh_p525_c);
                            $commandCapVModH->bindParam(':NUM_CANTIDAD_PRODUCCION_TOTAL',$model->cap5_modh_p525_d);
                            $commandCapVModH->bindParam(':NUM_CANTIDAD_ANIMALES',$model->cap5_modh_p526);
                            $commandCapVModH->bindParam(':NUM_RENDIMIENTO',$model->cap5_modh_p526_a);
                            $commandCapVModH->bindParam(':NUM_PRODUCCION_VENTA',$model->cap5_modh_p527_a);
                            $commandCapVModH->bindParam(':ID_UM_VENTA',$model->cap5_modh_p527_b);
                            $commandCapVModH->bindParam(':NUM_PRECIO_VENTA',$model->cap5_modh_p527_c);
                            $commandCapVModH->bindParam(':NUM_PRODUCCION_CONSUMO',$model->cap5_modh_p528_a);
                            $commandCapVModH->bindParam(':ID_UM_CONSUMO',$model->cap5_modh_p528_b);
                            $commandCapVModH->bindParam(':NUM_EQ_CONSUMO',$model->cap5_modh_p528_c);
                            $commandCapVModH->bindParam(':NUM_PRODUCCION_DERIVADOS',$model->cap5_modh_p529_a);
                            $commandCapVModH->bindParam(':ID_UM_DERIVADOS',$model->cap5_modh_p529_b);
                            $commandCapVModH->bindParam(':NUM_EQ_DERIVADOS',$model->cap5_modh_p529_c);
                            $commandCapVModH->execute();
                        }


                        $transaction->commit();
                        return ['success'=>true];
                    } catch (\Exception $e) {
                        $transaction->rollBack();
                        throw $e;
                    } catch (\Throwable $e) {
                        $transaction->rollBack();
                        throw $e;
                    }
                }
            }
            
            return $this->render('capitulo-v-modulo-h',['id_encuesta'=>$id_encuesta,'id_producto'=>$id_producto,'geocodigo_fdo'=>$geocodigo_fdo]);
        }
        
    }


    public function actionViewCapituloVModuloH($id_encuesta = null,$id_producto=null){
        $this->layout='privado';
        $cap_v_mod_h = (new \yii\db\Query())->select('ID_CAP_V_H')->from('ENA_TM_CAP_V_H')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
        if($cap_v_mod_h['ID_CAP_V_H']!=null){
            
            return $this->render('view-capitulo-v-modulo-h',['id_encuesta'=>$id_encuesta,'id_producto'=>$id_producto]);
        }
        
    }

    public function actionGetModuloH(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $id_encuesta = $_POST['id_encuesta'];
            $id_producto = $_POST['id_producto'];
            $capVModDetH = null;
            $capVModH = (new \yii\db\Query())->select('ID_CAP_V_H')->from('ENA_TM_CAP_V_H')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
            if($capVModH){
                $capVModDetH = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_V_H_PECUARIO')->where('ID_CAP_V_H=:ID_CAP_V_H AND ID_PRODUCTO=:ID_PRODUCTO',[':ID_CAP_V_H'=>$capVModH['ID_CAP_V_H'],':ID_PRODUCTO'=>$id_producto])->one();
            }
            
            
            return [
                'success'=>true,
                'cabecera'=>$capVModDetH
            ];
        }
    }


}
