<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Parcela;


class CultivoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        echo 'no permitido';
        return;
    }


    public function actionGetListaCultivos(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $cultivos = (new \yii\db\Query())
                ->select('*')
                ->from('ENA_TG_CULTIVO')
                ->orderBy('TXT_CULTIVO ASC')
                ->all();
                
            return ['success'=>true,'cultivos'=>$cultivos];
        }
    }

    public function actionGetListaCultivoEspecificaciones(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $id_cultivo = $_POST['id_cultivo'];

            $especificaciones = (new \yii\db\Query())
                ->select('ENA_TG_ESPECIFICACION.*')
                ->from('ENA_TG_ESPECIFICACION')
                ->innerJoin('ENA_TG_CULTIVO_ESPECIFICACION','ENA_TG_CULTIVO_ESPECIFICACION.ID_ESPECIFICACION=ENA_TG_ESPECIFICACION.ID_ESPECIFICACION')
                ->where('ENA_TG_CULTIVO_ESPECIFICACION.ID_CULTIVO=:ID_CULTIVO',[':ID_CULTIVO'=>$id_cultivo])
                ->orderBy('TXT_ESPECIFICACION ASC')
                ->all();
                
            return ['success'=>true,'especificaciones'=>$especificaciones];
        }
    }

    public function actionGetListaEspecificaciones(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $especificaciones = (new \yii\db\Query())
                ->select('*')
                ->from('ENA_TG_ESPECIFICACION')
                ->orderBy('TXT_ESPECIFICACION ASC')
                ->all();
                
            return ['success'=>true,'especificaciones'=>$especificaciones];
        }
    }

}
