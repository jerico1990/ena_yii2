<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
class ValidacionController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($k,$llave){
        $descriptado = Yii::$app->getSecurity()->decryptByPassword($k,'ENA2021');
        $datos = explode("|", $descriptado);
        if($datos[0]==$llave){



            $this->layout='validacion';
            $model = new LoginForm();
            if (!Yii::$app->user->isGuest) {
                return $this->redirect(['panel/index']);
            }

            if ($model->load(Yii::$app->request->post())) {
                if($model->login()){
                    $codigo_aleatorio = '';
                    $connection = \Yii::$app->db;
                    $transaction = $connection->beginTransaction();
                    try {
                        $commandCodigoAleatorio = $connection->createCommand('UPDATE ENA_TG_USUARIO SET TXT_CODIGO_VALIDACION=:TXT_CODIGO_VALIDACION WHERE TXT_USUARIO=:TXT_USUARIO AND FLG_ESTADO=1');
                        $commandCodigoAleatorio->bindParam(':TXT_USUARIO',$datos[0]);
                        $commandCodigoAleatorio->bindParam(':TXT_CODIGO_VALIDACION',$codigo_aleatorio);
                        $commandCodigoAleatorio->execute();

                        $transaction->commit();
                    } catch (\Exception $e) {
                        $transaction->rollBack();
                        throw $e;
                    } catch (\Throwable $e) {
                        $transaction->rollBack();
                        throw $e;
                    }

                    return $this->redirect(['panel/index']);
                }else{
                    $codigo_aleatorio = '';
                    $connection = \Yii::$app->db;
                    $transaction = $connection->beginTransaction();
                    try {
                        $commandCodigoAleatorio = $connection->createCommand('UPDATE ENA_TG_USUARIO SET TXT_CODIGO_VALIDACION=:TXT_CODIGO_VALIDACION WHERE TXT_USUARIO=:TXT_USUARIO AND FLG_ESTADO=1');
                        $commandCodigoAleatorio->bindParam(':TXT_USUARIO',$datos[0]);
                        $commandCodigoAleatorio->bindParam(':TXT_CODIGO_VALIDACION',$codigo_aleatorio);
                        $commandCodigoAleatorio->execute();

                        $transaction->commit();
                    } catch (\Exception $e) {
                        $transaction->rollBack();
                        throw $e;
                    } catch (\Throwable $e) {
                        $transaction->rollBack();
                        throw $e;
                    }
                    
                    return $this->redirect(['login/index']);
                }
                
            }

            return $this->render('index',['ruc'=>$datos[0],'us'=>$datos[0],'ps'=>$datos[1],'k'=>$k]);
        }else{
            echo "no permitido";
        }
    }

    public function actionGetEnviarCodigo(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST && isset($_POST['ruc'])){
            $ruc = $_POST['ruc'];
            //$descriptado = Yii::$app->getSecurity()->decryptByPassword($k,'ENA2021');
            //var_dump($descriptado);die();
            $codigo_aleatorio = random_int(1000, 9999); // Yii::$app->getSecurity()->generateRandomString();

            $usuario = (new \yii\db\Query())->select('TXT_CORREO')->from('ENA_TG_USUARIO')->where('TXT_USUARIO=:TXT_USUARIO',[':TXT_USUARIO'=>$ruc])->one();

            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {
                $commandCodigoAleatorio = $connection->createCommand('UPDATE ENA_TG_USUARIO SET TXT_CODIGO_VALIDACION=:TXT_CODIGO_VALIDACION WHERE TXT_USUARIO=:TXT_USUARIO AND FLG_ESTADO=1');
                $commandCodigoAleatorio->bindParam(':TXT_USUARIO',$ruc);
                $commandCodigoAleatorio->bindParam(':TXT_CODIGO_VALIDACION',$codigo_aleatorio);
                $commandCodigoAleatorio->execute();

                $transaction->commit();

                $body = Yii::$app->view->renderFile ( '@app/mail/layouts/plantilla.php' , [
                    'codigo_aleatorio' => $codigo_aleatorio 
                ] );
                
                Yii::$app->mail->compose('@app/mail/layouts/html',['content' => $body ])
                    ->setFrom('ena@midagri.gob.pe')
                    ->setTo(strtolower($usuario['TXT_CORREO']))
                    ->setSubject('Bienvenidos a la plataforma ENA-MIDAGRI 2021')
                    ->send();



                return ['success'=>true];
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
            return ['success'=>false];
        }
    }
}
