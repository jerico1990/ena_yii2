<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Encuesta;
use app\models\EnaTmEncuesta;
use app\models\EnaTmCapII;
use app\models\EnaTmCapIIPersona;





class EncuestaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($geocodigo_fdo=null){
        $this->layout='privado';
        $model = EnaTmEncuesta::find()->where('COD_GEOCODIGO_FUNDO=:COD_GEOCODIGO_FUNDO',[':COD_GEOCODIGO_FUNDO'=>$geocodigo_fdo])->one();
        $marcoLista = (new \yii\db\Query())->select('IDDIST,RUC,COD_IDENT,NRO_FDO,RAZON_SOC,AREA_HA_Z17,GEOCODIGO,GEOCODIGO_FDO,NRO_EMP,COD_CUEST,OBSERVACION,AREA_HA_Z18,ESTRATO,ID_SE,TIPO_EXPLO,C_JURIDICA,TIPO_MARCO,NRO_DIVISION,USO_MOD,SEGUIMIENTO,PISO_ECO,REG_NAT')->from('ENA_TM_MARCOLISTA_LA_LIBERTAD')->where('GEOCODIGO_FDO=:GEOCODIGO_FDO',[':GEOCODIGO_FDO'=>$geocodigo_fdo])->one();
        //var_dump($marcoLista);die;
        if($model == null){
            $seq = (new \yii\db\Query())->select('ENA_TM_ENCUESTA_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();

            $model                          = new EnaTmEncuesta;
            $model->ID_ENCUESTA             = $seq['contador'];
            $model->COD_GEOCODIGO_FUNDO     = $geocodigo_fdo;
            $model->COD_UBIGEO              = $marcoLista['IDDIST'];
            $model->COD_GEOCODIGO           = $marcoLista['GEOCODIGO'];
            $model->COD_REGION_NATURAL      = $marcoLista['REG_NAT'];
            $model->COD_PISO_ECOLOGICO      = $marcoLista['PISO_ECO'];
            $model->FEC_ENCUESTA            = date('d/m/Y');
            $model->FLG_ESTADO              = '1';
            $model->save();

            

        }
        return $this->render('index',['id_encuesta'=>$model->ID_ENCUESTA,'marcoLista'=>$marcoLista,'geocodigo_fdo'=>$geocodigo_fdo]);
    }

    public function actionView($id_encuesta=null){
        $this->layout='privado';
        $model = EnaTmEncuesta::find()->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
        return $this->render('view',['id_encuesta'=>$id_encuesta,'COD_GEOCODIGO_FUNDO'=>$model->COD_GEOCODIGO_FUNDO]);
    }

    public function actionUpdate($id_encuesta){
        $this->layout = 'vacio';
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = new Encuesta;
        if($request->isAjax){
            if ($model->load($request->post())) {
                
                $connection = \Yii::$app->db;
                $transaction = $connection->beginTransaction();
                try {
                    $infoGeneral = (new \yii\db\Query())->select('ENA_TM_ENCUESTA.COD_GEOCODIGO_FUNDO,ENA_TM_ENCUESTA.COD_UBIGEO')
                        ->from('ENA_TM_ENCUESTA')
                        ->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])
                        ->one();

                    $this->infoGeneral($model,$id_encuesta,$connection);
                    $this->capituloI($model,$id_encuesta,$connection);
                    $this->capituloII($model,$id_encuesta,$connection);
                    $this->capituloIII($model,$id_encuesta,$connection);
                    $this->capituloIV($model,$id_encuesta,$connection);
                    $this->capituloV($model,$id_encuesta,$connection);
                    $this->capituloVI($model,$id_encuesta,$connection);
                    $this->capituloVII($model,$id_encuesta,$connection);
                    $this->capituloVIII($model,$id_encuesta,$connection);
                    $this->capituloIX($model,$id_encuesta,$connection);
                    $this->capituloXII($model,$id_encuesta,$connection);

                    $transaction->commit();
                    
                    if($model->general_distrito!="" && $infoGeneral['COD_UBIGEO']!=$model->general_distrito){
                        return ['success'=>true,'msg'=>'GEOCODIGO NUEVO','ban'=>1];
                    }
                    return ['success'=>true];
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    throw $e;
                }
                return ['success'=>false];
                // if($model->save()){
                //     return ['success'=>true];
                // }else{
                //     return ['success'=>false];
                // }
            }
            return ['success'=>false];
        }
    }

    public function actionFinalizar(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $id_encuesta=$_POST['id_encuesta'];
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {
                $fecha = date('d/m/Y');
                $estado = 2;
                $commandFinalizado = $connection->createCommand('UPDATE ENA_TM_ENCUESTA SET FEC_FINALIZADO=:FEC_FINALIZADO,FLG_ESTADO=:FLG_ESTADO WHERE ID_ENCUESTA=:ID_ENCUESTA');
                $commandFinalizado->bindParam(':ID_ENCUESTA',$id_encuesta);
                $commandFinalizado->bindParam(':FLG_ESTADO',$estado);
                $commandFinalizado->bindParam(':FEC_FINALIZADO',$fecha);
                $commandFinalizado->execute();

                $transaction->commit();
                return ['success'=>true];
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
            return ['success'=>false];
            
        }
    }

    public function actionGetEncuesta(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $id_encuesta = $_POST['id_encuesta'];
            $capituloIIDetalle = null;
            $capituloIDetalle = null;
            $capituloIIIDetalle = null;
            $capituloIIIDetalleModuloA = null;
            $capituloIIIDetalleModuloB = null;
            $capituloIVmoduloDP437 = null;
            $capituloIVmoduloDP438 = null;
            $capituloIVmoduloEP440 = null;
            $capituloIVmoduloEP443 = null;
            $capituloIVmoduloEP444 = null;
            $capituloVmoduloBP502 = null;
            $capituloVmoduloHP524 = null;
            $capituloVmoduloIP530 = null;
            $capituloVmoduloIP533 = null;
            $capituloVmoduloJP534 = null;
            $capituloVmoduloJP535 = null;
            $capituloVmoduloKP536 = null;
            $capituloVmoduloKP539 = null;
            $capituloVmoduloKP541 = null;
            $capituloVIP601 = null;
            $capituloVIP602 = null;
            $capituloVIP605 = null;
            $capituloVIP606 = null;
            $capituloVIP610 = null;
            $capituloVIImoduloCAsociaciones = null;
            $capituloVIImoduloCP714 = null;
            $capituloVIImoduloCP715 = null;
            $capituloVIImoduloCP716 = null;
            $capituloVIImoduloCP717 = null;
            $capituloVIImoduloCP718 = null;
            $capituloVIImoduloCP719 = null;
            
            $capituloVIImoduloEP726 = null; 
            $capituloVIImoduloEP727 = null;
            $capituloVIImoduloEP729 = null;
            $capituloVIImoduloEP731 = null;

            $capituloVIIIP801 = null;
            $capituloVIIIP802 = null;
            $capituloVIIIP803 = null;
            $capituloIXDetalle = null;

            $infoGeneral = (new \yii\db\Query())->select('ENA_TR_ML_CABECERA.*,ENA_TM_ENCUESTA.NUM_PARCELA_SM,ENA_TM_ENCUESTA.NUM_TOTAL_PARCELAS_SM,ENA_TM_ENCUESTA.COD_CCPP,ENA_TM_MARCOLISTA_LA_LIBERTAD.IDDPTO')
                        ->from('ENA_TM_ENCUESTA')
                        ->innerJoin('ENA_TR_ML_CABECERA','ENA_TR_ML_CABECERA.COD_GEOCODIGO_FDO=ENA_TM_ENCUESTA.COD_GEOCODIGO_FUNDO')
                        ->innerJoin('ENA_TM_MARCOLISTA_LA_LIBERTAD','ENA_TM_MARCOLISTA_LA_LIBERTAD.GEOCODIGO_FDO=ENA_TM_ENCUESTA.COD_GEOCODIGO_FUNDO')
                        ->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])
                        ->one();

            /* capitulo i */
            $capituloICabecera = (new \yii\db\Query())->select('ID_CAP_I,TXT_OBSERVACION')->from('ENA_TM_CAP_I')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
            if($capituloICabecera){
                $capituloIDetalle = (new \yii\db\Query())->select('ID_ACT_PARCELA')->from('ENA_TM_CAP_I_DET_TIP_ACT')->where('ID_CAP_I=:ID_CAP_I',[':ID_CAP_I'=>$capituloICabecera['ID_CAP_I']])->all();
            }
            
            
            /* capitulo ii */
            $capituloIICabecera = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_II')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
            if($capituloIICabecera){
                $capituloIIDetalle = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_II_PERSONA')->where("ID_CAP_II=:ID_CAP_II",[':ID_CAP_II'=>$capituloIICabecera['ID_CAP_II']])->all();
            }
            
            
            /* capitulo iii */
            $capituloIIICabecera = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_III')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
            if($capituloIIICabecera){
                $capituloIIIDetalle = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_TENENCIA')->where("ID_CAP_III=:ID_CAP_III",[':ID_CAP_III'=>$capituloIIICabecera['ID_CAP_III']])->all();
                $capituloIIIDetalleModuloA = (new \yii\db\Query())->select('ENA_TM_CAP_III_LOTE.*,ENA_TG_USO_COBERT_TIERRA.TXT_DESCRIPCION TXT_USO_COBERT_TIERRA,ENA_TG_UM_AREA.TXT_DESCRIPCION TXT_UNIDAD_MEDIDA')->from('ENA_TM_CAP_III_LOTE')
                                                //->innerJoin('ENA_TM_DET_CAP_III_LOTE','ENA_TM_DET_CAP_III_LOTE.ID_CAP_III_LOTE=ENA_TM_CAP_III_LOTE.ID_CAP_III_LOTE')
                                                ->leftJoin('ENA_TG_UM_AREA','ENA_TG_UM_AREA.ID_UM_AREA=ENA_TM_CAP_III_LOTE.COD_UM_LOTE_PARCELA')
                                                ->innerJoin('ENA_TG_USO_COBERT_TIERRA','ENA_TG_USO_COBERT_TIERRA.ID_USO_COBERT_TIERRA=ENA_TM_CAP_III_LOTE.ID_USO_COBERT_TIERRA')
                                                ->where("ID_CAP_III=:ID_CAP_III",[':ID_CAP_III'=>$capituloIIICabecera['ID_CAP_III']])
                                                ->orderBy('ENA_TM_CAP_III_LOTE.NUM_LOTE asc')->all();

                $capituloIIIDetalleModuloB = (new \yii\db\Query())->select('ENA_TM_OTRA_PARCELA.*,ENA_TG_UM_AREA.TXT_DESCRIPCION TXT_UNIDAD_MEDIDA')->from('ENA_TM_OTRA_PARCELA')
                                                ->leftJoin('ENA_TG_UM_AREA','ENA_TG_UM_AREA.ID_UM_AREA=ENA_TM_OTRA_PARCELA.ID_UM_PARCELA')
                                                //->innerJoin('ENA_TM_DET_CAP_III_OTRA_PARC','ENA_TM_DET_CAP_III_OTRA_PARC.ID_OTRA_PARCELA=ENA_TM_OTRA_PARCELA.ID_OTRA_PARCELA')
                                                ->where("ENA_TM_OTRA_PARCELA.ID_CAP_III=:ID_CAP_III",[':ID_CAP_III'=>$capituloIIICabecera['ID_CAP_III']])
                                                ->all();
            }

            /* capitulo iv */
            $capituloIVmoduloACabecera = (new \yii\db\Query())->select('ENA_TM_CAP_IV_A.*,ENA_TG_PARAMETROS.TXT_DESCRIPCION CULTIVO_TIEMPO,ENA_TG_CULTIVO.TXT_CULTIVO')->from('ENA_TM_CAP_IV_A')
                ->leftJoin('ENA_TG_PARAMETROS','ENA_TG_PARAMETROS.ID_PARAMETRO=ENA_TM_CAP_IV_A.ID_TIEMPO_CULTIVO AND ENA_TG_PARAMETROS.COD_PREGUNTA=401')
                ->leftJoin('ENA_TG_CULTIVO','ENA_TG_CULTIVO.ID_CULTIVO=ENA_TM_CAP_IV_A.ID_CULTIVO')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->orderBy('NUM_LOTE asc')->all();
            $capituloIVmoduloBCabecera = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_IV_B_ARB_FRUT')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->orderBy('NUM_ORDEN asc')->all();
            $capituloIVmoduloCCabecera = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_IV_C_SEMILLEROS')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->orderBy('NUM_ORDEN_SEMILLERO asc')->all();


            $capituloIVmoduloDCabecera = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_IV_D')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
            if($capituloIVmoduloDCabecera){
                $capituloIVmoduloDP437 = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_IV_D_PASTOS')->where('ID_CAP_IV_D=:ID_CAP_IV_D',[':ID_CAP_IV_D'=>$capituloIVmoduloDCabecera['ID_CAP_IV_D']])->all();
                $capituloIVmoduloDP438 = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_IV_D_LABORES')->where('ID_CAP_IV_D=:ID_CAP_IV_D',[':ID_CAP_IV_D'=>$capituloIVmoduloDCabecera['ID_CAP_IV_D']])->all();
            }
            


            $capituloIVmoduloECabecera = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_IV_E')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
            if($capituloIVmoduloECabecera){
                $capituloIVmoduloEP440 = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_IV_E_PRACT_AGRI')->where('ID_CAP_IV_E=:ID_CAP_IV_E',[':ID_CAP_IV_E'=>$capituloIVmoduloECabecera['ID_CAP_IV_E']])->all();
                $capituloIVmoduloEP443 = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_IV_E_EST_ENVA')->where('ID_CAP_IV_E=:ID_CAP_IV_E',[':ID_CAP_IV_E'=>$capituloIVmoduloECabecera['ID_CAP_IV_E']])->all();
                $capituloIVmoduloEP444 = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_IV_E_CONTROL_PLAGA')->where('ID_CAP_IV_E=:ID_CAP_IV_E',[':ID_CAP_IV_E'=>$capituloIVmoduloECabecera['ID_CAP_IV_E']])->all();
            }
            
            
            /* capitulo v */
            

            $capituloVmoduloACabecera = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_V_A')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->all();

            $capituloVmoduloBCabecera = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_V_B')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
            
            if($capituloVmoduloBCabecera){
                $capituloVmoduloBP502 = (new \yii\db\Query())->select('ID_ESPECIE_CATEGORIA')->from('ENA_TM_DET_CAP_V_B')->where('ID_CAP_V_B=:ID_CAP_V_B',[':ID_CAP_V_B'=>$capituloVmoduloBCabecera['ID_CAP_V_B']])->all();
            }

            $capituloVmoduloGP509 = (new \yii\db\Query())->select('ID_AVICOLA')->from('ENA_TM_CAP_V_G_CARNE')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->all();
            $capituloVmoduloGP516 = (new \yii\db\Query())->select('ID_AVICOLA')->from('ENA_TM_CAP_V_G_POSTURA')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->all();
            

            $capituloVmoduloHCabecera = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_V_H')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
            if($capituloVmoduloHCabecera){
                $capituloVmoduloHP524 = (new \yii\db\Query())->select('ID_PRODUCTO')->from('ENA_TM_DET_CAP_V_H_PECUARIO')->where('ID_CAP_V_H=:ID_CAP_V_H',[':ID_CAP_V_H'=>$capituloVmoduloHCabecera['ID_CAP_V_H']])->all();
            }
            
            

            $capituloVmoduloICabecera = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_V_I')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
            if($capituloVmoduloICabecera){
                $capituloVmoduloIP530 = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_V_TIPO_ALIMENTACION')->where('ID_CAP_V_I=:ID_CAP_V_I',[':ID_CAP_V_I'=>$capituloVmoduloICabecera['ID_CAP_V_I']])->all();
                $capituloVmoduloIP533 = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_V_INSUMO')->where('ID_CAP_V_I=:ID_CAP_V_I',[':ID_CAP_V_I'=>$capituloVmoduloICabecera['ID_CAP_V_I']])->all();
            }
            
            $capituloVmoduloJCabecera = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_V_J')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
            if($capituloVmoduloJCabecera){
                $capituloVmoduloJP534 = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_V_J_VACUNA')->where('ID_CAP_V_J=:ID_CAP_V_J',[':ID_CAP_V_J'=>$capituloVmoduloJCabecera['ID_CAP_V_J']])->all();
                $capituloVmoduloJP535 = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_V_J_PARASITO')->where('ID_CAP_V_J=:ID_CAP_V_J',[':ID_CAP_V_J'=>$capituloVmoduloJCabecera['ID_CAP_V_J']])->all();
            }
            
            $capituloVmoduloKCabecera = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_V_K')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
            if($capituloVmoduloKCabecera){
                $capituloVmoduloKP536 = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_V_K_MEJOR_GENETICO')->where('ID_CAP_V_K=:ID_CAP_V_K',[':ID_CAP_V_K'=>$capituloVmoduloKCabecera['ID_CAP_V_K']])->all();
                $capituloVmoduloKP539 = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_V_K_INFO_GENETICO')->where('ID_CAP_V_K=:ID_CAP_V_K',[':ID_CAP_V_K'=>$capituloVmoduloKCabecera['ID_CAP_V_K']])->all();
                $capituloVmoduloKP541 = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_V_K_RAZA')->where('ID_CAP_V_K=:ID_CAP_V_K',[':ID_CAP_V_K'=>$capituloVmoduloKCabecera['ID_CAP_V_K']])->all();
            }
            
            

            

            /* capitulo vi */
            $capituloVICabecera = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_VI')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
            if($capituloVICabecera){
                $capituloVIP601 = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_VI_RESIDUO_CUL')->where('ID_CAP_VI=:ID_CAP_VI',[':ID_CAP_VI'=>$capituloVICabecera['ID_CAP_VI']])->all();
                $capituloVIP602 = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_VI_RESIDUO_ANIM')->where('ID_CAP_VI=:ID_CAP_VI',[':ID_CAP_VI'=>$capituloVICabecera['ID_CAP_VI']])->all();
                $capituloVIP605 = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_VI_ENTIDAD')->where('ID_CAP_VI=:ID_CAP_VI',[':ID_CAP_VI'=>$capituloVICabecera['ID_CAP_VI']])->all();
                $capituloVIP606 = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_VI_CONSERVA')->where('ID_CAP_VI=:ID_CAP_VI',[':ID_CAP_VI'=>$capituloVICabecera['ID_CAP_VI']])->all();
                $capituloVIP610 = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_VI_CERTIFICADO')->where('ID_CAP_VI=:ID_CAP_VI',[':ID_CAP_VI'=>$capituloVICabecera['ID_CAP_VI']])->all();
            }
            

            /* capitulo vii */
            $capituloVIImoduloCCabecera = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_VII_C')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
            if($capituloVIImoduloCCabecera){
                $capituloVIImoduloCAsociaciones = (new \yii\db\Query())->select('ENA_TM_CAP_VII_C_ASO_COOP_COM.*,ENA_TG_PARAMETROS.TXT_DESCRIPCION TXT_TIPO')->from('ENA_TM_CAP_VII_C_ASO_COOP_COM')
                ->leftJoin('ENA_TG_PARAMETROS','ENA_TG_PARAMETROS.ID_PARAMETRO=ENA_TM_CAP_VII_C_ASO_COOP_COM.COD_TIPO AND ENA_TG_PARAMETROS.COD_PREGUNTA=712')
                ->where('ID_CAP_VII_C=:ID_CAP_VII_C',[':ID_CAP_VII_C'=>$capituloVIImoduloCCabecera['ID_CAP_VII_C']])->all();
                $capituloVIImoduloCP714 = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_VII_C_BENEFICIOS_ASO')->where('ID_CAP_VII_C=:ID_CAP_VII_C',[':ID_CAP_VII_C'=>$capituloVIImoduloCCabecera['ID_CAP_VII_C']])->all();
                $capituloVIImoduloCP715 = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_VII_C_ACTIVIDADES')->where('ID_CAP_VII_C=:ID_CAP_VII_C',[':ID_CAP_VII_C'=>$capituloVIImoduloCCabecera['ID_CAP_VII_C']])->all();
                $capituloVIImoduloCP716 = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_VII_C_LOGRO')->where('ID_CAP_VII_C=:ID_CAP_VII_C',[':ID_CAP_VII_C'=>$capituloVIImoduloCCabecera['ID_CAP_VII_C']])->all();
                $capituloVIImoduloCP717 = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_VII_C_EQUIPO')->where('ID_CAP_VII_C=:ID_CAP_VII_C',[':ID_CAP_VII_C'=>$capituloVIImoduloCCabecera['ID_CAP_VII_C']])->all();
                
                //$capituloVIImoduloCP718 = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_VII_C_EQUIPO')->where('ID_CAP_VII_C=:ID_CAP_VII_C',[':ID_CAP_VII_C'=>$capituloVIImoduloCCabecera['ID_CAP_VII_C']])->all();
                $capituloVIImoduloCP719 = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_VII_C_MOTIVO_ASO')->where('ID_CAP_VII_C=:ID_CAP_VII_C',[':ID_CAP_VII_C'=>$capituloVIImoduloCCabecera['ID_CAP_VII_C']])->all();
            }

            
            $capituloVIImoduloECabecera = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_VII_E')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
            
            if($capituloVIImoduloECabecera){
                $capituloVIImoduloEP726 = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_VII_E_ENT_CRED')->where('ID_CAP_VII_E=:ID_CAP_VII_E',[':ID_CAP_VII_E'=>$capituloVIImoduloECabecera['ID_CAP_VII_E']])->all();
                $capituloVIImoduloEP727 = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_VII_E_UTI_PRES')->where('ID_CAP_VII_E=:ID_CAP_VII_E',[':ID_CAP_VII_E'=>$capituloVIImoduloECabecera['ID_CAP_VII_E']])->all();
                $capituloVIImoduloEP729 = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_VII_E_PROP_SEG')->where('ID_CAP_VII_E=:ID_CAP_VII_E',[':ID_CAP_VII_E'=>$capituloVIImoduloECabecera['ID_CAP_VII_E']])->all();
                $capituloVIImoduloEP731 = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_VII_E_CTA_AHORR')->where('ID_CAP_VII_E=:ID_CAP_VII_E',[':ID_CAP_VII_E'=>$capituloVIImoduloECabecera['ID_CAP_VII_E']])->all();
            }
            

            /* capitulo viii */
            $capituloVIIICabecera = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_VIII')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
            if($capituloVIIICabecera){
                $capituloVIIIP801 = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_VIII_FUENTE_ABAST')->where('ID_CAP_VIII=:ID_CAP_VIII',[':ID_CAP_VIII'=>$capituloVIIICabecera['ID_CAP_VIII']])->all();
                $capituloVIIIP802 = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_VIII_CLASE_AGUA')->where('ID_CAP_VIII=:ID_CAP_VIII',[':ID_CAP_VIII'=>$capituloVIIICabecera['ID_CAP_VIII']])->all();
                $capituloVIIIP803 = (new \yii\db\Query())->select('*')->from('ENA_TM_DET_CAP_VIII_ENERGIA')->where('ID_CAP_VIII=:ID_CAP_VIII',[':ID_CAP_VIII'=>$capituloVIIICabecera['ID_CAP_VIII']])->all();
            }
            
            
            /* capitulo ix */
            $capituloIXCabecera = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_IX')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
            if($capituloIXCabecera){
                $capituloIXDetalle = (new \yii\db\Query())->select('ENA_TM_DET_CAP_IX_MAQUINARIA_EQUIPO.*,ENA_TG_PARAMETROS.TXT_DESCRIPCION MAQUINARIA_EQUIPO_ES,ENA_TG_MAQ_EQUIP.TXT_DESCRIPCION MAQ_EQUIP')->from('ENA_TM_DET_CAP_IX_MAQUINARIA_EQUIPO')
                ->leftJoin('ENA_TG_PARAMETROS','ENA_TG_PARAMETROS.ID_PARAMETRO=ENA_TM_DET_CAP_IX_MAQUINARIA_EQUIPO.ID_TENENCIA AND ENA_TG_PARAMETROS.COD_PREGUNTA=904')
                ->leftJoin('ENA_TG_MAQ_EQUIP','ENA_TG_MAQ_EQUIP.ID_MAQ_EQUIP=ENA_TM_DET_CAP_IX_MAQUINARIA_EQUIPO.ID_TIPO_MAQUINARIA_EQUIPO')
                ->where('ID_CAP_IX=:ID_CAP_IX',[':ID_CAP_IX'=>$capituloIXCabecera['ID_CAP_IX']])->all();
            }
            
                                
            /* capitulo x */
            $capituloX = (new \yii\db\Query())->select('ENA_TM_DET_CAP_X.*,ENA_TG_PARAMETROS.TXT_DESCRIPCION ESTADO,ENA_TG_INFRAESTRUCTURA.TXT_DESCRIPCION TIPO_INFRAESTRUCTURA')->from('ENA_TM_DET_CAP_X')
                    ->leftJoin('ENA_TG_PARAMETROS','ENA_TG_PARAMETROS.COD_OPCION=ENA_TM_DET_CAP_X.TIP_ESTADO AND ENA_TG_PARAMETROS.COD_PREGUNTA=1003')
                    ->leftJoin('ENA_TG_INFRAESTRUCTURA','ENA_TG_INFRAESTRUCTURA.COD_INFRA_INST=ENA_TM_DET_CAP_X.ID_INFRAESTRUCTURA')
                    ->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->all();
            /* capitulo xii */
            $capituloXIICabecera = (new \yii\db\Query())->select(["FLG_RESULTADO_FINAL,TXT_OTRO_RESULTADO_FINAL,TXT_OBSERVACION,TO_CHAR( TXT_FECHA_FINAL, 'YYYY-MM-DD') TXT_FECHA_FINAL_F"])->from('ENA_TM_CAP_XII')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();

            return  [
                        'success'=>true,
                        'encuesta'=>[
                                        'infoGeneral' => $infoGeneral,
                                        'capitulo_i'=>[
                                                        'cabecera'=>$capituloICabecera,
                                                        'detalle'=>$capituloIDetalle
                                        ],
                                        'capitulo_ii'=>[
                                            'cabecera'=>$capituloIICabecera,
                                            'detalle'=>$capituloIIDetalle
                                        ],
                                        'capitulo_iii'=>[
                                            'cabecera'=>$capituloIIICabecera,
                                            'detalle'=>$capituloIIIDetalle,
                                            'capitulo_iii_detalle_modulo_a'=>$capituloIIIDetalleModuloA,
                                            'capitulo_iii_detalle_modulo_b'=>$capituloIIIDetalleModuloB
                                        ],
                                        'capitulo_iv'=>[
                                            'cabecera_modulo_a'=>$capituloIVmoduloACabecera,
                                            'cabecera_modulo_b'=>$capituloIVmoduloBCabecera,
                                            'cabecera_modulo_c'=>$capituloIVmoduloCCabecera,
                                            'cabecera_modulo_d'=>$capituloIVmoduloDCabecera,
                                            'cabecera_modulo_e'=>$capituloIVmoduloECabecera,
                                            'detalle_p437'=>$capituloIVmoduloDP437,
                                            'detalle_p438'=>$capituloIVmoduloDP438,
                                            'detalle_p440'=>$capituloIVmoduloEP440,
                                            'detalle_p443'=>$capituloIVmoduloEP443,
                                            'detalle_p444'=>$capituloIVmoduloEP444,
                                        ],
                                        'capitulo_v'=>[
                                            'cabecera_modulo_a'=>$capituloVmoduloACabecera,
                                            'cabecera_modulo_b'=>$capituloVmoduloBCabecera,
                                            'cabecera_modulo_h'=>$capituloVmoduloHCabecera,
                                            'cabecera_modulo_i'=>$capituloVmoduloICabecera,
                                            'cabecera_modulo_j'=>$capituloVmoduloJCabecera,
                                            'cabecera_modulo_k'=>$capituloVmoduloKCabecera,

                                            'detalle_p502'=>$capituloVmoduloBP502,
                                            'detalle_p509'=>$capituloVmoduloGP509,
                                            'detalle_p516'=>$capituloVmoduloGP516,
                                            'detalle_p524'=>$capituloVmoduloHP524,
                                            'detalle_p530'=>$capituloVmoduloIP530,
                                            'detalle_p533'=>$capituloVmoduloIP533,
                                            'detalle_p534'=>$capituloVmoduloJP534,
                                            'detalle_p535'=>$capituloVmoduloJP535,

                                            'detalle_p536'=>$capituloVmoduloKP536,
                                            'detalle_p539'=>$capituloVmoduloKP539,
                                            'detalle_p541'=>$capituloVmoduloKP541,

                                            
                                            
                                        ],
                                        'capitulo_vi'=>[
                                            'cabecera'=>$capituloVICabecera,
                                            'detalle_p601'=>$capituloVIP601,
                                            'detalle_p602'=>$capituloVIP602,
                                            'detalle_p605'=>$capituloVIP605,
                                            'detalle_p606'=>$capituloVIP606,
                                            'detalle_p610'=>$capituloVIP610,

                                        ],
                                        'capitulo_vii'=>[
                                            'cabecera_modulo_c'=>$capituloVIImoduloCCabecera,
                                            'cabecera_modulo_e'=>$capituloVIImoduloECabecera,
                                            'detalle_asociaciones'=>$capituloVIImoduloCAsociaciones,
                                            'detalle_p714'=>$capituloVIImoduloCP714,
                                            'detalle_p715'=>$capituloVIImoduloCP715,
                                            'detalle_p716'=>$capituloVIImoduloCP716,
                                            'detalle_p717'=>$capituloVIImoduloCP717,
                                            //'detalle_p718'=>$capituloVIImoduloCP718,
                                            'detalle_p719'=>$capituloVIImoduloCP719,
                                            'detalle_p726'=>$capituloVIImoduloEP726,
                                            'detalle_p727'=>$capituloVIImoduloEP727,
                                            'detalle_p729'=>$capituloVIImoduloEP729,
                                            'detalle_p731'=>$capituloVIImoduloEP731,
                                        ],
                                        'capitulo_viii'=>[
                                            'cabecera'=>$capituloVIIICabecera,
                                            'detalle_p801'=>$capituloVIIIP801,
                                            'detalle_p802'=>$capituloVIIIP802,
                                            'detalle_p803'=>$capituloVIIIP803,
                                        ],
                                        'capitulo_ix'=>[
                                            'cabecera'=>$capituloIXCabecera,
                                            'detalle'=>$capituloIXDetalle,
                                        ],
                                        'capitulo_x'=>[
                                            'detalle'=>$capituloX
                                        ],
                                        'capitulo_xii'=>[
                                            'cabecera'=>$capituloXIICabecera
                                        ]

                        ]
                    ];
        }
    }

    private function infoGeneral($model,$id_encuesta,$connection){
        /* UPDATE  CABECERA */
        $ruc = Yii::$app->user->identity->username;
        
        $infoGeneral = (new \yii\db\Query())->select('ENA_TM_ENCUESTA.COD_GEOCODIGO_FUNDO,ENA_TM_ENCUESTA.COD_UBIGEO')
                        ->from('ENA_TM_ENCUESTA')
                        ->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])
                        ->one();

        $ubigeo = (new \yii\db\Query())->select('*')
                        ->from('ENA_TG_UBIGEO')
                        ->where('COD_UBIGEO=:COD_UBIGEO',[':COD_UBIGEO'=>$model->general_distrito])
                        ->one();

        $codRegion = substr($model->general_distrito, 0,2);
        $codProvincia = substr($model->general_distrito, 0,4);
        if($codRegion==""){
            $codRegion = substr($infoGeneral['COD_GEOCODIGO_FUNDO'], 0,2);
        }

        $commandCorrelativoParcelaUbigeo = (new \yii\db\Query())->select(['
                MAX(NRO_FDO) NRO_FDO
            '])->from('ENA_TM_MARCOLISTA_LA_LIBERTAD')->where('RUC=:RUC AND IDDIST=:IDDIST',[':RUC'=>$ruc,':IDDIST'=>$model->general_distrito])->one();

        $fecha = date('d/m/Y');
        $commandEncuesta = $connection->createCommand('UPDATE ENA_TM_ENCUESTA SET NUM_PARCELA_SM=:NUM_PARCELA_SM,NUM_TOTAL_PARCELAS_SM=:NUM_TOTAL_PARCELAS_SM,COD_CCPP=:COD_CCPP,FEC_EN_PROCESO=:FEC_EN_PROCESO WHERE ID_ENCUESTA=:ID_ENCUESTA');
        $commandEncuesta->bindParam(':ID_ENCUESTA',$id_encuesta);
        $commandEncuesta->bindParam(':NUM_PARCELA_SM',$model->general_numero_parcela);
        $commandEncuesta->bindParam(':NUM_TOTAL_PARCELAS_SM',$model->general_total_parcela);
        $commandEncuesta->bindParam(':COD_CCPP',$model->general_centro_poblado);
        $commandEncuesta->bindParam(':FEC_EN_PROCESO',$fecha);
        $commandEncuesta->execute();
/* 
        $commandCabecera = $connection->createCommand('UPDATE ENA_TR_ML_CABECERA SET COD_UBIGEO=:COD_UBIGEO,TXT_PROVINCIA=:TXT_PROVINCIA,TXT_DISTRITO=:TXT_DISTRITO WHERE COD_GEOCODIGO_FDO=:COD_GEOCODIGO_FDO');
        $commandCabecera->bindParam(':COD_GEOCODIGO_FDO',$infoGeneral['COD_GEOCODIGO_FUNDO']);
        $commandCabecera->bindParam(':COD_UBIGEO',$model->general_distrito);
        $commandCabecera->bindParam(':TXT_PROVINCIA',$ubigeo['TXT_PROVINCIA']);
        $commandCabecera->bindParam(':TXT_DISTRITO',$ubigeo['TXT_DISTRITO']);
        $commandCabecera->execute();

        $commandMarco = $connection->createCommand('UPDATE ENA_TM_MARCOLISTA_LA_LIBERTAD SET IDPROV=:IDPROV,IDDIST=:IDDIST WHERE GEOCODIGO_FDO=:GEOCODIGO_FDO');
        $commandMarco->bindParam(':GEOCODIGO_FDO',$infoGeneral['COD_GEOCODIGO_FUNDO']);
        $commandMarco->bindParam(':IDPROV',$codProvincia);
        $commandMarco->bindParam(':IDDIST',$model->general_distrito);
        $commandMarco->execute(); */

        if($model->general_distrito!="" && $infoGeneral['COD_UBIGEO']!=$model->general_distrito){
            
            $commandEncuesta = $connection->createCommand('UPDATE ENA_TM_ENCUESTA SET COD_UBIGEO=:COD_UBIGEO WHERE ID_ENCUESTA=:ID_ENCUESTA');
            $commandEncuesta->bindParam(':ID_ENCUESTA',$id_encuesta);
            $commandEncuesta->bindParam(':COD_UBIGEO',$model->general_distrito);
            $commandEncuesta->execute();

            $commandCabecera = $connection->createCommand('UPDATE ENA_TR_ML_CABECERA SET COD_UBIGEO=:COD_UBIGEO,TXT_PROVINCIA=:TXT_PROVINCIA,TXT_DISTRITO=:TXT_DISTRITO WHERE COD_GEOCODIGO_FDO=:COD_GEOCODIGO_FDO');
            $commandCabecera->bindParam(':COD_GEOCODIGO_FDO',$infoGeneral['COD_GEOCODIGO_FUNDO']);
            $commandCabecera->bindParam(':COD_UBIGEO',$model->general_distrito);
            $commandCabecera->bindParam(':TXT_PROVINCIA',$ubigeo['TXT_PROVINCIA']);
            $commandCabecera->bindParam(':TXT_DISTRITO',$ubigeo['TXT_DISTRITO']);
            $commandCabecera->execute();

            $commandMarco = $connection->createCommand('UPDATE ENA_TM_MARCOLISTA_LA_LIBERTAD SET IDPROV=:IDPROV,IDDIST=:IDDIST WHERE GEOCODIGO_FDO=:GEOCODIGO_FDO');
            $commandMarco->bindParam(':GEOCODIGO_FDO',$infoGeneral['COD_GEOCODIGO_FUNDO']);
            $commandMarco->bindParam(':IDPROV',$codProvincia);
            $commandMarco->bindParam(':IDDIST',$model->general_distrito);
            $commandMarco->execute();

            $commandCorrelativoParcela = (new \yii\db\Query())->select(['
                    MAX(NRO_FDO) NRO_FDO, 
                    MAX(GEOCODIGO_FDO) GEOCODIGO_FDO,
                    MAX(IDDIST) IDDIST,
                    MAX(RUC) RUC,
                    MAX(COD_IDENT) COD_IDENT,
                    MAX(GEOCODIGO) GEOCODIGO
                '])->from('ENA_TM_MARCOLISTA_LA_LIBERTAD')->where('RUC=:RUC',[':RUC'=>$ruc])->one();

            

            $correlativoNuevoFDO = str_pad(((($commandCorrelativoParcelaUbigeo['NRO_FDO'])?$commandCorrelativoParcelaUbigeo['NRO_FDO']:0) + 1),2,"0", STR_PAD_LEFT);
            $geoCodigoFundo = $commandCorrelativoParcela['IDDIST'] . $commandCorrelativoParcela['RUC'] . $commandCorrelativoParcela['COD_IDENT'] . $correlativoNuevoFDO;



            $commandEncuesta = $connection->createCommand('UPDATE ENA_TM_ENCUESTA SET COD_GEOCODIGO_FUNDO=:COD_GEOCODIGO_FUNDO_NUEVO WHERE ID_ENCUESTA=:ID_ENCUESTA');
            $commandEncuesta->bindParam(':ID_ENCUESTA',$id_encuesta);
            $commandEncuesta->bindParam(':COD_GEOCODIGO_FUNDO_NUEVO',$geoCodigoFundo);
            $commandEncuesta->execute();

            $commandCabecera = $connection->createCommand('UPDATE ENA_TR_ML_CABECERA SET COD_GEOCODIGO_FDO=:COD_GEOCODIGO_FUNDO_NUEVO WHERE COD_GEOCODIGO_FDO=:COD_GEOCODIGO_FDO');
            $commandCabecera->bindParam(':COD_GEOCODIGO_FDO',$infoGeneral['COD_GEOCODIGO_FUNDO']);
            $commandCabecera->bindParam(':COD_GEOCODIGO_FUNDO_NUEVO',$geoCodigoFundo);
            $commandCabecera->execute();

            $commandMarco = $connection->createCommand('UPDATE ENA_TM_MARCOLISTA_LA_LIBERTAD SET GEOCODIGO_FDO=:COD_GEOCODIGO_FUNDO_NUEVO WHERE GEOCODIGO_FDO=:GEOCODIGO_FDO');
            $commandMarco->bindParam(':GEOCODIGO_FDO',$infoGeneral['COD_GEOCODIGO_FUNDO']);
            $commandMarco->bindParam(':COD_GEOCODIGO_FUNDO_NUEVO',$geoCodigoFundo);
            $commandMarco->execute();

        }
    }

    private function capituloI($model,$id_encuesta,$connection){
        /* INSERTANDO  CABECERA */
        $p01 = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_I')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
                    
        if(!$p01){
            $seq = (new \yii\db\Query())->select('ENA_TM_CAP_I_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();
            $commandP01 = $connection->createCommand('INSERT INTO ENA_TM_CAP_I (ID_CAP_I,ID_ENCUESTA,TXT_OBSERVACION) values (:ID_CAP_I,:ID_ENCUESTA,:TXT_OBSERVACION)');
            $commandP01->bindParam(':ID_CAP_I',$seq['contador']);
            $commandP01->bindParam(':ID_ENCUESTA',$id_encuesta);
            $commandP01->bindParam(':TXT_OBSERVACION',$model->p01_observacion);
            $commandP01->execute();

            $p01 = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_I')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
        }else{
            $commandP01 = $connection->createCommand('UPDATE ENA_TM_CAP_I SET TXT_OBSERVACION=:TXT_OBSERVACION WHERE ID_ENCUESTA=:ID_ENCUESTA');
            $commandP01->bindParam(':ID_ENCUESTA',$id_encuesta);
            $commandP01->bindParam(':TXT_OBSERVACION',$model->p01_observacion);
            $commandP01->execute();
        }
        /* LIMPIANDO DETALLE */
        $commandP01Detalle = $connection->createCommand('DELETE FROM ENA_TM_CAP_I_DET_TIP_ACT WHERE ID_CAP_I=:ID_CAP_I');
        $commandP01Detalle->bindParam(':ID_CAP_I',$p01['ID_CAP_I']);
        $commandP01Detalle->execute();

        /* INSERTANDO DETALLE */
        if($model->p01_1){
            $this->capituloIDetalle($model->p01_1,$p01['ID_CAP_I'],$connection);
        }

        if($model->p01_2){
            $this->capituloIDetalle($model->p01_2,$p01['ID_CAP_I'],$connection);
        }

        if($model->p01_3){
            $this->capituloIDetalle($model->p01_3,$p01['ID_CAP_I'],$connection);
        }

        if($model->p01_4){
            $this->capituloIDetalle($model->p01_4,$p01['ID_CAP_I'],$connection);
        }

        if($model->p01_5){
            $this->capituloIDetalle($model->p01_5,$p01['ID_CAP_I'],$connection);
        }

        if($model->p01_6){
            $this->capituloIDetalle($model->p01_6,$p01['ID_CAP_I'],$connection);
        }

        if($model->p01_7){
            $this->capituloIDetalle($model->p01_7,$p01['ID_CAP_I'],$connection);
        }

        if($model->p01_8){
            $this->capituloIDetalle($model->p01_8,$p01['ID_CAP_I'],$connection);
        }

        if($model->p01_9){
            $this->capituloIDetalle($model->p01_9,$p01['ID_CAP_I'],$connection);
        }

        if($model->p01_10){
            $this->capituloIDetalle($model->p01_10,$p01['ID_CAP_I'],$connection);
        }

        if($model->p01_11){
            $this->capituloIDetalle($model->p01_11,$p01['ID_CAP_I'],$connection);
        }

        if($model->p01_12){
            $this->capituloIDetalle($model->p01_12,$p01['ID_CAP_I'],$connection);
        }

    }

    private function capituloIDetalle($valor,$id_cap_i,$connection){
        $commandP01Detalle = $connection->createCommand('INSERT INTO ENA_TM_CAP_I_DET_TIP_ACT (ID_CAP_I,ID_ACT_PARCELA) values (:ID_CAP_I,:ID_ACT_PARCELA)');
        $commandP01Detalle->bindParam(':ID_CAP_I',$id_cap_i);
        $commandP01Detalle->bindParam(':ID_ACT_PARCELA',$valor);
        $commandP01Detalle->execute();
    }

    private function capituloII($model,$id_encuesta,$connection){
        /* INSERTANDO  CABECERA */
        $p02 = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_II')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();

        /* iniciando variables */
        $idEncuesta = $id_encuesta;
        $codTipoProductor = null;
        $codTomaDecisionesN = null;
        $tipPertComuCampN = null;
        $txtNomParcelaJ = null;
        $txtObservaciones = null;

        /* variables de modulo a */
        $p02_tipo_productor=null;
        $p02_a_toma_decision=null;
        $p02_a_representante_nombres=null;
        $p02_a_representante_apellidos=null;
        $p02_a_txt_dni=null;
        $p02_a_representante_tiene_numero=null;
        $p02_a_representante_telefono_fijo=null;
        $p02_a_representante_telefono_celular=null;
        $p02_a_tiene_correo_electronico=null;
        $p02_a_correo_electronico=null;
        $p02_a_informacion_proporcionada=null;
        $p02_a_informacion_proporcionada_especifique=null;
        $p02_a_informante_nombres=null;
        $p02_a_informante_apellidos=null;
        $p02_a_informante_tiene_numero=null;
        $p02_a_informante_telefono_fijo=null;
        $p02_a_informante_telefono_celular=null;
        $p02_a_productor_agrario_vive=null;
        $p02_a_productor_agrario_vive_especifique=null;
        $p02_a_informante_departamento=null;
        $p02_a_informante_provincia=null;
        $p02_a_informante_distrito=null;
        $p02_a_informante_centro_poblado=null;
        $p02_a_es_productor=null;

        /* variables de modulo b */
        $p02_b_razon_social=null;
        $p02_b_ruc=null;
        $p02_b_representante_legal_nombres=null;
        $p02_b_representante_legal_apellidos=null;
        $p02_b_representante_departamento=null;
        $p02_b_representante_provincia=null;
        $p02_b_representante_distrito=null;
        $p02_b_representante_centro_poblado=null;
        $p02_b_representante_codigo_centro_poblado=null;
        $p02_b_tipo_via=null;
        $p02_b_tipo_via_especifique=null;
        $p02_b_avenida=null;
        $p02_b_nro_puerta=null;
        $p02_b_block=null;
        $p02_b_interior=null;
        $p02_b_piso=null;
        $p02_b_manzana=null;
        $p02_b_lote=null;
        $p02_b_informante_nombres=null;
        $p02_b_informante_apellidos=null;
        $p02_b_cargo=null;
        $p02_b_cargo_especifique=null;
        $p02_b_informante_tiene_numero=null;
        $p02_b_informante_telefono_fijo=null;
        $p02_b_informante_telefono_celular=null;
        $p02_b_informante_tiene_correo_electronico=null;
        $p02_b_informante_correo_electronico=null;
        $p02_b_nombre_fundo=null;
        $p02_observacion=null;

        $codTipoProductor = ($model->p02_tipo_productor)?$model->p02_tipo_productor:99999;
        $codTomaDecisionesN = ($model->p02_a_toma_decision)?$model->p02_a_toma_decision:99999;
        
        if($model->p02_tipo_productor == "1"){
            /* limpiando variable del modulo b */
            $model->p02_b_razon_social=null;
            $model->p02_b_ruc=null;
            $model->p02_b_representante_legal_nombres=null;
            $model->p02_b_representante_legal_apellidos=null;
            $model->p02_b_representante_departamento=null;
            $model->p02_b_representante_provincia=null;
            $model->p02_b_representante_distrito=null;
            $model->p02_b_representante_centro_poblado=null;
            $model->p02_b_representante_codigo_centro_poblado=null;
            $model->p02_b_tipo_via=null;
            $model->p02_b_tipo_via_especifique=null;
            $model->p02_b_avenida=null;
            $model->p02_b_nro_puerta=null;
            $model->p02_b_block=null;
            $model->p02_b_interior=null;
            $model->p02_b_piso=null;
            $model->p02_b_manzana=null;
            $model->p02_b_lote=null;
            $model->p02_b_informante_nombres=null;
            $model->p02_b_informante_apellidos=null;
            $model->p02_b_cargo=null;
            $model->p02_b_cargo_especifique=null;
            $model->p02_b_informante_tiene_numero=null;
            $model->p02_b_informante_telefono_fijo=null;
            $model->p02_b_informante_telefono_celular=null;
            $model->p02_b_informante_tiene_correo_electronico=null;
            $model->p02_b_informante_correo_electronico=null;
            $model->p02_b_nombre_fundo=null;

            /* seteando variables estandares de cabecera */

            $codTomaDecisionesN = ($model->p02_a_toma_decision)?$model->p02_a_toma_decision:99999;
            $tipPertComuCampN = $model->p02_a_pertenece_comunidad;
            $txtNomParcelaJ = null;
            $txtObservaciones = $model->p02_observacion;
            //$p02_a_es_productor=$model->p02_a_es_productor;
            $p02_a_es_productor=($model->p02_a_es_productor=="0")?"0":"1";
            //var_dump($p02_a_es_productor);die;
            /* seteando variables de detalle */
            
            $p02_a_representante_nombres=$model->p02_a_representante_nombres;
            $p02_a_representante_apellidos=$model->p02_a_representante_apellidos;
            $p02_a_txt_dni=$model->p02_a_txt_dni;
            $p02_a_representante_tiene_numero=$model->p02_a_representante_tiene_numero;
            $p02_a_representante_telefono_fijo=$model->p02_a_representante_telefono_fijo;
            $p02_a_representante_telefono_celular=$model->p02_a_representante_telefono_celular;
            $p02_a_tiene_correo_electronico=$model->p02_a_tiene_correo_electronico;
            $p02_a_correo_electronico=$model->p02_a_correo_electronico;
            $p02_a_informacion_proporcionada=($model->p02_a_informacion_proporcionada)?$model->p02_a_informacion_proporcionada:99999;
            $p02_a_informacion_proporcionada_especifique=$model->p02_a_informacion_proporcionada_especifique;


            $p02_a_informante_nombres=$model->p02_a_informante_nombres;
            $p02_a_informante_apellidos=$model->p02_a_informante_apellidos;
            $p02_a_informante_tiene_numero=$model->p02_a_informante_tiene_numero;
            $p02_a_informante_telefono_fijo=$model->p02_a_informante_telefono_fijo;
            $p02_a_informante_telefono_celular=$model->p02_a_informante_telefono_celular;
            $p02_a_productor_agrario_vive=($model->p02_a_productor_agrario_vive)?$model->p02_a_productor_agrario_vive:99999;
            $p02_a_productor_agrario_vive_especifique=$model->p02_a_productor_agrario_vive_especifique;
            $p02_a_informante_centro_poblado=$model->p02_a_informante_centro_poblado;

            
        }else if($model->p02_tipo_productor == "2"){
            /* limpiando variable del modulo a */
            $model->p02_a_toma_decision=null;
            $model->p02_a_representante_nombres=null;
            $model->p02_a_representante_apellidos=null;
            $model->p02_a_txt_dni=null;
            $model->p02_a_representante_tiene_numero=null;
            $model->p02_a_representante_telefono_fijo=null;
            $model->p02_a_representante_telefono_celular=null;
            $model->p02_a_tiene_correo_electronico=null;
            $model->p02_a_correo_electronico=null;
            $model->p02_a_informacion_proporcionada=null;
            $model->p02_a_informacion_proporcionada_especifique=null;
            $model->p02_a_informante_nombres=null;
            $model->p02_a_informante_apellidos=null;
            $model->p02_a_informante_tiene_numero=null;
            $model->p02_a_informante_telefono_fijo=null;
            $model->p02_a_informante_telefono_celular=null;
            $model->p02_a_productor_agrario_vive=null;
            $model->p02_a_productor_agrario_vive_especifique=null;
            $model->p02_a_informante_departamento=null;
            $model->p02_a_informante_provincia=null;
            $model->p02_a_informante_distrito=null;
            $model->p02_a_informante_centro_poblado=null;
            $model->p02_a_pertenece_comunidad=null;

            /* seteando variables estandares de cabecera */
            
            $codTomaDecisionesN = 99999;
            $tipPertComuCampN = null;
            $txtNomParcelaJ = $model->p02_b_nombre_fundo;
            $txtObservaciones = $model->p02_observacion;

            /* seteando variables de detalle */

            $p02_b_razon_social=$model->p02_b_razon_social;
            $p02_b_ruc=$model->p02_b_ruc;
            $p02_b_representante_legal_nombres=$model->p02_b_representante_legal_nombres;
            $p02_b_representante_legal_apellidos=$model->p02_b_representante_legal_apellidos;
            // $p02_b_representante_departamento=$model->p02_b_representante_departamento;
            // $p02_b_representante_provincia=$model->p02_b_representante_provincia;
            // $p02_b_representante_distrito=$model->p02_b_representante_distrito;
            $p02_b_representante_centro_poblado=$model->p02_b_representante_centro_poblado;
            $p02_b_representante_codigo_centro_poblado=$model->p02_b_representante_codigo_centro_poblado;
            $p02_b_tipo_via=$model->p02_b_tipo_via;
            $p02_b_tipo_via_especifique=$model->p02_b_tipo_via_especifique;
            $p02_b_avenida=$model->p02_b_avenida;
            $p02_b_nro_puerta=$model->p02_b_nro_puerta;
            $p02_b_block=$model->p02_b_block;
            $p02_b_interior=$model->p02_b_interior;
            $p02_b_piso=$model->p02_b_piso;
            $p02_b_manzana=$model->p02_b_manzana;
            $p02_b_lote=$model->p02_b_lote;


            $p02_b_informante_nombres=$model->p02_b_informante_nombres;
            $p02_b_informante_apellidos=$model->p02_b_informante_apellidos;
            $p02_b_cargo=($model->p02_b_cargo)?$model->p02_b_cargo:99999;
            $p02_b_cargo_especifique=$model->p02_b_cargo_especifique;
            $p02_b_informante_tiene_numero=$model->p02_b_informante_tiene_numero;
            $p02_b_informante_telefono_fijo=$model->p02_b_informante_telefono_fijo;
            $p02_b_informante_telefono_celular=$model->p02_b_informante_telefono_celular;
            $p02_b_informante_tiene_correo_electronico=$model->p02_b_informante_tiene_correo_electronico;
            $p02_b_informante_correo_electronico=$model->p02_b_informante_correo_electronico;
        }
                    
        if(!$p02){
            $seq = (new \yii\db\Query())->select('ENA_TM_CAP_II_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();
            $commandP02 = $connection->createCommand('INSERT INTO ENA_TM_CAP_II (ID_CAP_II,ID_ENCUESTA,COD_TIPO_PRODUCTOR,COD_TOMA_DECISIONES_N,TIP_PERT_COMU_CAMP_N,TXT_NOM_PARCELA_J,TXT_OBSERVACIONES) values (:ID_CAP_II,:ID_ENCUESTA,:COD_TIPO_PRODUCTOR,:COD_TOMA_DECISIONES_N,:TIP_PERT_COMU_CAMP_N,:TXT_NOM_PARCELA_J,:TXT_OBSERVACIONES)');
            $commandP02->bindParam(':ID_CAP_II',$seq['contador']);
            $commandP02->bindParam(':ID_ENCUESTA',$idEncuesta);
            $commandP02->bindParam(':COD_TIPO_PRODUCTOR',$codTipoProductor);
            $commandP02->bindParam(':COD_TOMA_DECISIONES_N',$codTomaDecisionesN);
            $commandP02->bindParam(':TIP_PERT_COMU_CAMP_N',$tipPertComuCampN);
            $commandP02->bindParam(':TXT_NOM_PARCELA_J',$txtNomParcelaJ);
            $commandP02->bindParam(':TXT_OBSERVACIONES',$txtObservaciones);
            $commandP02->execute();

            $p02 = (new \yii\db\Query())->select('ID_CAP_II')->from('ENA_TM_CAP_II')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();

            if($model->p02_tipo_productor=="1"){
                $seq01 = (new \yii\db\Query())->select('ENA_TM_CAP_II_PERSONA_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();
                $commandP02Detalle01 = $connection->createCommand('INSERT INTO ENA_TM_CAP_II_PERSONA (ID_PERSONA,ID_CAP_II,COD_PERFIL,COD_VIVE_PRODUCTOR,FLG_INFO) values (:ID_PERSONA,:ID_CAP_II,99999,99999,:FLG_INFO)');
                $commandP02Detalle01->bindParam(':ID_PERSONA',$seq01['contador']);
                $commandP02Detalle01->bindParam(':ID_CAP_II',$p02['ID_CAP_II']);
                $commandP02Detalle01->bindParam(':FLG_INFO',$p02_a_es_productor);
                $commandP02Detalle01->execute();
                if($p02_a_es_productor=="0"){
                    $seq02 = (new \yii\db\Query())->select('ENA_TM_CAP_II_PERSONA_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();
                    $commandP02Detalle02 = $connection->createCommand('INSERT INTO ENA_TM_CAP_II_PERSONA (ID_PERSONA,ID_CAP_II,COD_PERFIL,COD_VIVE_PRODUCTOR,FLG_INFO) values (:ID_PERSONA,:ID_CAP_II,99999,99999,1)');
                    $commandP02Detalle02->bindParam(':ID_PERSONA',$seq02['contador']);
                    $commandP02Detalle02->bindParam(':ID_CAP_II',$p02['ID_CAP_II']);
                    $commandP02Detalle02->execute();
                }


            }else if($model->p02_tipo_productor=="2"){
                $seq01 = (new \yii\db\Query())->select('ENA_TM_CAP_II_PERSONA_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();
                $commandP02Detalle01 = $connection->createCommand('INSERT INTO ENA_TM_CAP_II_PERSONA (ID_PERSONA,ID_CAP_II,COD_PERFIL,COD_VIVE_PRODUCTOR,FLG_INFO) values (:ID_PERSONA,:ID_CAP_II,:COD_PERFIL,99999,1)');
                $commandP02Detalle01->bindParam(':ID_PERSONA',$seq01['contador']);
                $commandP02Detalle01->bindParam(':ID_CAP_II',$p02['ID_CAP_II']);
                $commandP02Detalle01->bindParam(':COD_PERFIL',$p02_b_cargo);
                $commandP02Detalle01->execute();
    
                $seq02 = (new \yii\db\Query())->select('ENA_TM_CAP_II_PERSONA_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();
                $commandP02Detalle02 = $connection->createCommand('INSERT INTO ENA_TM_CAP_II_PERSONA (ID_PERSONA,ID_CAP_II,COD_PERFIL,COD_VIVE_PRODUCTOR,FLG_INFO) values (:ID_PERSONA,:ID_CAP_II,99999,99999,0)');
                $commandP02Detalle02->bindParam(':ID_PERSONA',$seq02['contador']);
                $commandP02Detalle02->bindParam(':ID_CAP_II',$p02['ID_CAP_II']);
                //$commandP02Detalle02->bindParam(':COD_PERFIL','99999');
                $commandP02Detalle02->execute();
            }

            
        }else{
            $commandP02 = EnaTmCapII::findOne($p02['ID_CAP_II']);
            $commandP02->COD_TIPO_PRODUCTOR=$codTipoProductor;
            $commandP02->COD_TOMA_DECISIONES_N=$codTomaDecisionesN;
            $commandP02->TIP_PERT_COMU_CAMP_N=$tipPertComuCampN;
            $commandP02->TXT_NOM_PARCELA_J=$txtNomParcelaJ;
            $commandP02->TXT_OBSERVACIONES=$txtObservaciones;
            $commandP02->update();

            $commandP02DetalleEliminar = $connection->createCommand('DELETE FROM ENA_TM_CAP_II_PERSONA WHERE ID_CAP_II=:ID_CAP_II');
            $commandP02DetalleEliminar->bindParam(':ID_CAP_II',$p02['ID_CAP_II']);
            $commandP02DetalleEliminar->execute();
            
            if($model->p02_tipo_productor=="1"){
                $seq01 = (new \yii\db\Query())->select('ENA_TM_CAP_II_PERSONA_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();
                $commandP02Detalle01 = $connection->createCommand('INSERT INTO ENA_TM_CAP_II_PERSONA (ID_PERSONA,ID_CAP_II,COD_PERFIL,COD_VIVE_PRODUCTOR,FLG_INFO) values (:ID_PERSONA,:ID_CAP_II,99999,99999,:FLG_INFO)');
                $commandP02Detalle01->bindParam(':ID_PERSONA',$seq01['contador']);
                $commandP02Detalle01->bindParam(':ID_CAP_II',$p02['ID_CAP_II']);
                $commandP02Detalle01->bindParam(':FLG_INFO',$p02_a_es_productor);
                $commandP02Detalle01->execute();
                if($p02_a_es_productor=="0"){
                    $seq02 = (new \yii\db\Query())->select('ENA_TM_CAP_II_PERSONA_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();
                    $commandP02Detalle02 = $connection->createCommand('INSERT INTO ENA_TM_CAP_II_PERSONA (ID_PERSONA,ID_CAP_II,COD_PERFIL,COD_VIVE_PRODUCTOR,FLG_INFO) values (:ID_PERSONA,:ID_CAP_II,99999,99999,1)');
                    $commandP02Detalle02->bindParam(':ID_PERSONA',$seq02['contador']);
                    $commandP02Detalle02->bindParam(':ID_CAP_II',$p02['ID_CAP_II']);
                    $commandP02Detalle02->execute();
                }
            }else if($model->p02_tipo_productor=="2"){
                $seq01 = (new \yii\db\Query())->select('ENA_TM_CAP_II_PERSONA_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();
                $commandP02Detalle01 = $connection->createCommand('INSERT INTO ENA_TM_CAP_II_PERSONA (ID_PERSONA,ID_CAP_II,COD_PERFIL,COD_VIVE_PRODUCTOR,FLG_INFO) values (:ID_PERSONA,:ID_CAP_II,:COD_PERFIL,99999,1)');
                $commandP02Detalle01->bindParam(':ID_PERSONA',$seq01['contador']);
                $commandP02Detalle01->bindParam(':ID_CAP_II',$p02['ID_CAP_II']);
                $commandP02Detalle01->bindParam(':COD_PERFIL',$p02_b_cargo);
                $commandP02Detalle01->execute();
    
                $seq02 = (new \yii\db\Query())->select('ENA_TM_CAP_II_PERSONA_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();
                $commandP02Detalle02 = $connection->createCommand('INSERT INTO ENA_TM_CAP_II_PERSONA (ID_PERSONA,ID_CAP_II,COD_PERFIL,COD_VIVE_PRODUCTOR,FLG_INFO) values (:ID_PERSONA,:ID_CAP_II,99999,99999,0)');
                $commandP02Detalle02->bindParam(':ID_PERSONA',$seq02['contador']);
                $commandP02Detalle02->bindParam(':ID_CAP_II',$p02['ID_CAP_II']);
                //$commandP02Detalle02->bindParam(':COD_PERFIL','99999');
                $commandP02Detalle02->execute();
            }
        }



        if($model->p02_tipo_productor=="1"){
            
            $p02Detalle01 = EnaTmCapIIPersona::find()->where('FLG_INFO=:FLG_INFO and ID_CAP_II=:ID_CAP_II',[':ID_CAP_II'=>$p02['ID_CAP_II'],':FLG_INFO'=>$p02_a_es_productor])->one();
            
            $p02Detalle01->TXT_NOMBRES = $p02_a_representante_nombres;
            $p02Detalle01->TXT_APELLIDOS = $p02_a_representante_apellidos;
            $p02Detalle01->TXT_DNI_RUC = $p02_a_txt_dni;
            $p02Detalle01->FLG_NUMERO = $p02_a_representante_tiene_numero;
            $p02Detalle01->TXT_FIJO = $p02_a_representante_telefono_fijo;
            $p02Detalle01->TXT_CELULAR = $p02_a_representante_telefono_celular;
            $p02Detalle01->FLG_CORREO_ELECTRONICO = $p02_a_tiene_correo_electronico;
            $p02Detalle01->TXT_CORREO = $p02_a_correo_electronico;
            $p02Detalle01->COD_PERFIL = $p02_a_informacion_proporcionada;
            $p02Detalle01->TXT_OTRO_PERFIL = $p02_a_informacion_proporcionada_especifique;
            $p02Detalle01->update();

            //$p02Detalle02 = (new \yii\db\Query())->select('ID_PERSONA,ID_CAP_II')->from('ENA_TM_CAP_II_PERSONA')->where('FLG_INFO=:FLG_INFO and ID_CAP_II=:ID_CAP_II',[':ID_CAP_II'=>$p02['ID_CAP_II'],':FLG_INFO'=>'1'])->one();
            if($p02_a_es_productor=="0"){
                // if($p02Detalle02==null){
                //     $seq02 = (new \yii\db\Query())->select('ENA_TM_CAP_II_PERSONA_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();
                //     $commandP02Detalle02 = $connection->createCommand('INSERT INTO ENA_TM_CAP_II_PERSONA (ID_PERSONA,ID_CAP_II,COD_PERFIL,COD_VIVE_PRODUCTOR,FLG_INFO) values (:ID_PERSONA,:ID_CAP_II,99999,99999,1)');
                //     $commandP02Detalle02->bindParam(':ID_PERSONA',$seq02['contador']);
                //     $commandP02Detalle02->bindParam(':ID_CAP_II',$p02['ID_CAP_II']);
                //     $commandP02Detalle02->execute();
                // }
                
                $p02Detalle02 = EnaTmCapIIPersona::find()->where('FLG_INFO=:FLG_INFO and ID_CAP_II=:ID_CAP_II',[':ID_CAP_II'=>$p02['ID_CAP_II'],':FLG_INFO'=>'1'])->one();
                $p02Detalle02->TXT_NOMBRES = $p02_a_informante_nombres;
                $p02Detalle02->TXT_APELLIDOS = $p02_a_informante_apellidos;
                $p02Detalle02->FLG_NUMERO = $p02_a_informante_tiene_numero;
                $p02Detalle02->TXT_FIJO = $p02_a_informante_telefono_fijo;
                $p02Detalle02->TXT_CELULAR = $p02_a_informante_telefono_celular;
                $p02Detalle02->COD_VIVE_PRODUCTOR = $p02_a_productor_agrario_vive;
                $p02Detalle02->TXT_OTRO_VIVE_PRODUCTOR = $p02_a_productor_agrario_vive_especifique;
                $p02Detalle02->COD_CENTRO_POBLADO = $p02_a_informante_centro_poblado;
                $p02Detalle02->update();
            }
                
        }else if($model->p02_tipo_productor=="2"){
            
            /* Informante */
            $p02Detalle01 = EnaTmCapIIPersona::find()->where('FLG_INFO=1 and ID_CAP_II=:ID_CAP_II',[':ID_CAP_II'=>$p02['ID_CAP_II']])->one();

            $p02Detalle01->TXT_NOMBRES = $p02_b_informante_nombres;
            $p02Detalle01->TXT_APELLIDOS = $p02_b_informante_apellidos;
            $p02Detalle01->COD_PERFIL = $p02_b_cargo;
            $p02Detalle01->TXT_OTRO_PERFIL = $p02_b_cargo_especifique;
            $p02Detalle01->FLG_NUMERO = $p02_b_informante_tiene_numero;
            $p02Detalle01->TXT_FIJO = $p02_b_informante_telefono_fijo;
            $p02Detalle01->TXT_CELULAR = $p02_b_informante_telefono_celular;
            $p02Detalle01->FLG_CORREO_ELECTRONICO = $p02_b_informante_tiene_correo_electronico;
            $p02Detalle01->TXT_CORREO = $p02_b_informante_correo_electronico;
            $p02Detalle01->update();

            /* Representante legal */
            $p02Detalle02 = EnaTmCapIIPersona::find()->where('FLG_INFO=0 and ID_CAP_II=:ID_CAP_II',[':ID_CAP_II'=>$p02['ID_CAP_II']])->one();
            $p02Detalle02->TXT_RAZON_SOCIAL = $p02_b_razon_social;
            $p02Detalle02->TXT_DNI_RUC = $p02_b_ruc;
            $p02Detalle02->TXT_NOMBRES = $p02_b_representante_legal_nombres;
            $p02Detalle02->TXT_APELLIDOS = $p02_b_representante_legal_apellidos;
            $p02Detalle02->COD_CENTRO_POBLADO = $p02_b_representante_centro_poblado;
            $p02Detalle02->COD_TIPO_VIA = $p02_b_tipo_via;
            $p02Detalle02->TXT_OTRO_VIA = $p02_b_tipo_via_especifique;
            $p02Detalle02->TXT_AVENIDA = $p02_b_avenida;
            $p02Detalle02->TXT_PUERTA = $p02_b_nro_puerta;
            $p02Detalle02->TXT_BLOCK = $p02_b_block;
            $p02Detalle02->TXT_INTERIOR = $p02_b_interior;
            $p02Detalle02->TXT_PISO = $p02_b_piso;
            $p02Detalle02->TXT_MANZANA = $p02_b_manzana;
            $p02Detalle02->TXT_LOTE = $p02_b_lote;
            $p02Detalle02->update();
        }

    }

    private function capituloIII($model,$id_encuesta,$connection){
        /* INSERTANDO  CABECERA */
        $p03 = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_III')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
                    
        if(!$p03){
            $seq = (new \yii\db\Query())->select('ENA_TM_CAP_III_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();
            $commandP03 = $connection->createCommand('INSERT INTO ENA_TM_CAP_III (ID_CAP_III,TXT_OBSERVACIONES,FLG_CONDUCE_PARCELA,NUM_PARCELAS_CONDUCE,ID_ENCUESTA) values (:ID_CAP_III,:TXT_OBSERVACIONES,:FLG_CONDUCE_PARCELA,:NUM_PARCELAS_CONDUCE,:ID_ENCUESTA)');
            $commandP03->bindParam(':ID_CAP_III',$seq['contador']);
            $commandP03->bindParam(':TXT_OBSERVACIONES',$model->p03_observacion);
            $commandP03->bindParam(':FLG_CONDUCE_PARCELA',$model->p03_c_int_trabaja_otra_parcela);
            $commandP03->bindParam(':NUM_PARCELAS_CONDUCE',$model->p03_c_int_cantidad_parcelas);
            $commandP03->bindParam(':ID_ENCUESTA',$id_encuesta);
            $commandP03->execute();

            $p03 = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_III')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
        }else{
            $commandP03 = $connection->createCommand('UPDATE ENA_TM_CAP_III SET TXT_OBSERVACIONES=:TXT_OBSERVACIONES,FLG_CONDUCE_PARCELA=:FLG_CONDUCE_PARCELA,NUM_PARCELAS_CONDUCE=:NUM_PARCELAS_CONDUCE WHERE ID_ENCUESTA=:ID_ENCUESTA');
            $commandP03->bindParam(':ID_ENCUESTA',$id_encuesta);
            $commandP03->bindParam(':TXT_OBSERVACIONES',$model->p03_observacion);
            $commandP03->bindParam(':FLG_CONDUCE_PARCELA',$model->p03_c_int_trabaja_otra_parcela);
            $commandP03->bindParam(':NUM_PARCELAS_CONDUCE',$model->p03_c_int_cantidad_parcelas);
            $commandP03->execute();
        }

        /* LIMPIANDO DETALLE */
        $commandP03Detalle = $connection->createCommand('DELETE FROM ENA_TM_DET_TENENCIA WHERE ID_CAP_III=:ID_CAP_III');
        $commandP03Detalle->bindParam(':ID_CAP_III',$p03['ID_CAP_III']);
        $commandP03Detalle->execute();

        if($model->p03_c_int_tenencia){
            $p03_c_int_tenencia = explode(",",$model->p03_c_int_tenencia);
            foreach($p03_c_int_tenencia as $tenencia){
                if($tenencia == 31){
                    $commandP03Tenencia = $connection->createCommand('INSERT INTO ENA_TM_DET_TENENCIA (ID_CAP_III,ID_TENENCIA,TXT_OTRO_TENENCIA) values (:ID_CAP_III,:ID_TENENCIA,:TXT_OTRO_TENENCIA)');
                    $commandP03Tenencia->bindParam(':ID_CAP_III',$p03['ID_CAP_III']);
                    $commandP03Tenencia->bindParam(':ID_TENENCIA',$tenencia);
                    $commandP03Tenencia->bindParam(':TXT_OTRO_TENENCIA',$model->p03_c_int_tenencia_31_especifique);
                    $commandP03Tenencia->execute();
                }else{
                    $commandP03Tenencia = $connection->createCommand('INSERT INTO ENA_TM_DET_TENENCIA (ID_CAP_III,ID_TENENCIA) values (:ID_CAP_III,:ID_TENENCIA)');
                $commandP03Tenencia->bindParam(':ID_CAP_III',$p03['ID_CAP_III']);
                $commandP03Tenencia->bindParam(':ID_TENENCIA',$tenencia);
                $commandP03Tenencia->execute();
                }
                
            }
        }

    }

    private function capituloIV($model,$id_encuesta,$connection){
        /* INSERTANDO  CABECERA MODULO D*/
        $cap4_modd = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_IV_D')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
        $model->cap4_modd_p439 = ($model->cap4_modd_p439)?$model->cap4_modd_p439:99999;
        //json_decode($someJSON, true);
        //var_dump(json_decode($model->cap4_mode_p440, true));die;
        

        if(!$cap4_modd){
            $seq = (new \yii\db\Query())->select('ENA_TM_CAP_IV_D_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();
            $commandCap04ModD = $connection->createCommand('INSERT INTO ENA_TM_CAP_IV_D (ID_CAP_IV_D,ID_CONDICION_PASTOS_NAT,ID_ENCUESTA) values (:ID_CAP_IV_D,:ID_CONDICION_PASTOS_NAT,:ID_ENCUESTA)');
            $commandCap04ModD->bindParam(':ID_CAP_IV_D',$seq['contador']);
            $commandCap04ModD->bindParam(':ID_CONDICION_PASTOS_NAT',$model->cap4_modd_p439);
            $commandCap04ModD->bindParam(':ID_ENCUESTA',$id_encuesta);
            $commandCap04ModD->execute();

            $cap4_modd = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_IV_D')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
        }else{
            $commandCap04ModD = $connection->createCommand('UPDATE ENA_TM_CAP_IV_D SET ID_CONDICION_PASTOS_NAT=:ID_CONDICION_PASTOS_NAT WHERE ID_ENCUESTA=:ID_ENCUESTA');
            $commandCap04ModD->bindParam(':ID_ENCUESTA',$id_encuesta);
            $commandCap04ModD->bindParam(':ID_CONDICION_PASTOS_NAT',$model->cap4_modd_p439);
            $commandCap04ModD->execute();
        }

        /* LIMPIANDO DETALLE MODULO D P437 */
        $commandCap04ModDP437 = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_IV_D_PASTOS WHERE ID_CAP_IV_D=:ID_CAP_IV_D');
        $commandCap04ModDP437->bindParam(':ID_CAP_IV_D',$cap4_modd['ID_CAP_IV_D']);
        $commandCap04ModDP437->execute();

        if($model->cap4_modd_p437){
            $cap4_modd_p437 = explode(",",$model->cap4_modd_p437);
            foreach($cap4_modd_p437 as $p437){
                if($p437 == 8){
                    $commandCap04ModDP437Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_D_PASTOS (ID_CAP_IV_D,ID_PASTOS_NATURALES,TXT_OTRO) values (:ID_CAP_IV_D,:ID_PASTOS_NATURALES,:TXT_OTRO)');
                    $commandCap04ModDP437Det->bindParam(':ID_CAP_IV_D',$cap4_modd['ID_CAP_IV_D']);
                    $commandCap04ModDP437Det->bindParam(':ID_PASTOS_NATURALES',$p437);
                    $commandCap04ModDP437Det->bindParam(':TXT_OTRO',$model->cap4_modd_p437_8_especifique);
                    $commandCap04ModDP437Det->execute();
                }else{
                    $commandCap04ModDP437Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_D_PASTOS (ID_CAP_IV_D,ID_PASTOS_NATURALES) values (:ID_CAP_IV_D,:ID_PASTOS_NATURALES)');
                    $commandCap04ModDP437Det->bindParam(':ID_CAP_IV_D',$cap4_modd['ID_CAP_IV_D']);
                    $commandCap04ModDP437Det->bindParam(':ID_PASTOS_NATURALES',$p437);
                    $commandCap04ModDP437Det->execute();
                }
                
            }
        }




        /* LIMPIANDO DETALLE MODULO D P438 */
        $commandCap04ModDP438 = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_IV_D_LABORES WHERE ID_CAP_IV_D=:ID_CAP_IV_D');
        $commandCap04ModDP438->bindParam(':ID_CAP_IV_D',$cap4_modd['ID_CAP_IV_D']);
        $commandCap04ModDP438->execute();

        if($model->cap4_modd_p438){
            $cap4_modd_p438 = explode(",",$model->cap4_modd_p438);
            foreach($cap4_modd_p438 as $p438){
                if($p438 == 10){
                    $commandCap04ModDP438Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_D_LABORES (ID_CAP_IV_D,ID_LABORES_CULTURALES,TXT_OTRO) values (:ID_CAP_IV_D,:ID_LABORES_CULTURALES,:TXT_OTRO)');
                    $commandCap04ModDP438Det->bindParam(':ID_CAP_IV_D',$cap4_modd['ID_CAP_IV_D']);
                    $commandCap04ModDP438Det->bindParam(':ID_LABORES_CULTURALES',$p438);
                    $commandCap04ModDP438Det->bindParam(':TXT_OTRO',$model->cap4_modd_p438_10_especifique);
                    $commandCap04ModDP438Det->execute();
                }else{
                    $commandCap04ModDP438Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_D_LABORES (ID_CAP_IV_D,ID_LABORES_CULTURALES) values (:ID_CAP_IV_D,:ID_LABORES_CULTURALES)');
                    $commandCap04ModDP438Det->bindParam(':ID_CAP_IV_D',$cap4_modd['ID_CAP_IV_D']);
                    $commandCap04ModDP438Det->bindParam(':ID_LABORES_CULTURALES',$p438);
                    $commandCap04ModDP438Det->execute();
                }
                
            }
        }


        /* INSERTANDO  CABECERA MODULO E*/
        $cap4_mode = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_IV_E')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
                    
        if(!$cap4_mode){
            $seq = (new \yii\db\Query())->select('ENA_TM_CAP_IV_E_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();
            $commandCap04ModE = $connection->createCommand('INSERT INTO ENA_TM_CAP_IV_E (ID_CAP_IV_E,TIP_EQUIPO_PROTECCION,TIP_EVALUA_PLAGAS,TIP_CONTROL_BIOLOGICO,TXT_OBSERVACIONES,ID_ENCUESTA) values (:ID_CAP_IV_E,:TIP_EQUIPO_PROTECCION,:TIP_EVALUA_PLAGAS,:TIP_CONTROL_BIOLOGICO,:TXT_OBSERVACIONES,:ID_ENCUESTA)');
            $commandCap04ModE->bindParam(':ID_CAP_IV_E',$seq['contador']);
            $commandCap04ModE->bindParam(':TIP_EQUIPO_PROTECCION',$model->cap4_mode_p442);
            $commandCap04ModE->bindParam(':TIP_EVALUA_PLAGAS',$model->cap4_mode_p445);
            $commandCap04ModE->bindParam(':TIP_CONTROL_BIOLOGICO',$model->cap4_mode_p446);
            $commandCap04ModE->bindParam(':TXT_OBSERVACIONES',$model->cap4_mode_observacion);
            $commandCap04ModE->bindParam(':ID_ENCUESTA',$id_encuesta);
            $commandCap04ModE->execute();

            $cap4_mode = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_IV_E')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
        }else{
            $commandCap04ModE = $connection->createCommand('UPDATE ENA_TM_CAP_IV_E SET TIP_EQUIPO_PROTECCION=:TIP_EQUIPO_PROTECCION,TIP_EVALUA_PLAGAS=:TIP_EVALUA_PLAGAS,TIP_CONTROL_BIOLOGICO=:TIP_CONTROL_BIOLOGICO,TXT_OBSERVACIONES=:TXT_OBSERVACIONES WHERE ID_ENCUESTA=:ID_ENCUESTA');
            $commandCap04ModE->bindParam(':ID_ENCUESTA',$id_encuesta);
            $commandCap04ModE->bindParam(':TIP_EQUIPO_PROTECCION',$model->cap4_mode_p442);
            $commandCap04ModE->bindParam(':TIP_EVALUA_PLAGAS',$model->cap4_mode_p445);
            $commandCap04ModE->bindParam(':TIP_CONTROL_BIOLOGICO',$model->cap4_mode_p446);
            $commandCap04ModE->bindParam(':TXT_OBSERVACIONES',$model->cap4_mode_observacion);
            $commandCap04ModE->execute();
        }

        /* LIMPIANDO DETALLE MODULO E P440 */
        $commandCap04ModEP440 = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_IV_E_PRACT_AGRI WHERE ID_CAP_IV_E=:ID_CAP_IV_E');
        $commandCap04ModEP440->bindParam(':ID_CAP_IV_E',$cap4_mode['ID_CAP_IV_E']);
        $commandCap04ModEP440->execute();


        $cap4_mode_p440 = json_decode($model->cap4_mode_p440, true);
        if($model->cap4_mode_p440){
            foreach($cap4_mode_p440 as $p440){
                if(isset($p440["a"])){
                    foreach($p440["a"] as $p440_a){
                        $tip_practica = "1";
                        if($p440_a["efectua_practica"] || $p440_a["cuantos_anios"]){
                            $commandCap04ModEP440Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_E_PRACT_AGRI (ID_CAP_IV_E,ID_PRACTICA_AGRICOLA,TIP_PRACTICA,NUM_ANIOS) values (:ID_CAP_IV_E,:ID_PRACTICA_AGRICOLA,:TIP_PRACTICA,:NUM_ANIOS)');
                            $commandCap04ModEP440Det->bindParam(':ID_CAP_IV_E',$cap4_mode['ID_CAP_IV_E']);
                            $commandCap04ModEP440Det->bindParam(':ID_PRACTICA_AGRICOLA',$p440_a["nro_practica"]);
                            $commandCap04ModEP440Det->bindParam(':TIP_PRACTICA',$p440_a["efectua_practica"]);
                            $commandCap04ModEP440Det->bindParam(':NUM_ANIOS',$p440_a["cuantos_anios"]);
                            $commandCap04ModEP440Det->execute();
                        }
                        
                    }
                }else if(isset($p440["b"])){
                    foreach($p440["b"] as $p440_b){
                        $tip_practica = "2";
                        if($p440_b["efectua_practica"] || $p440_b["cuantos_anios"]){
                            $commandCap04ModEP440Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_E_PRACT_AGRI (ID_CAP_IV_E,ID_PRACTICA_AGRICOLA,TIP_PRACTICA,NUM_ANIOS) values (:ID_CAP_IV_E,:ID_PRACTICA_AGRICOLA,:TIP_PRACTICA,:NUM_ANIOS)');
                            $commandCap04ModEP440Det->bindParam(':ID_CAP_IV_E',$cap4_mode['ID_CAP_IV_E']);
                            $commandCap04ModEP440Det->bindParam(':ID_PRACTICA_AGRICOLA',$p440_b["nro_practica"]);
                            $commandCap04ModEP440Det->bindParam(':TIP_PRACTICA',$p440_b["efectua_practica"]);
                            $commandCap04ModEP440Det->bindParam(':NUM_ANIOS',$p440_b["cuantos_anios"]);
                            $commandCap04ModEP440Det->execute();
                        }
                        
                    }
                }else if(isset($p440["c"])){
                    foreach($p440["c"] as $p440_c){
                        $tip_practica = "3";
                        if($p440_c["efectua_practica"] || $p440_c["cuantos_anios"]){
                            $commandCap04ModEP440Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_E_PRACT_AGRI (ID_CAP_IV_E,ID_PRACTICA_AGRICOLA,TIP_PRACTICA,NUM_ANIOS) values (:ID_CAP_IV_E,:ID_PRACTICA_AGRICOLA,:TIP_PRACTICA,:NUM_ANIOS)');
                            $commandCap04ModEP440Det->bindParam(':ID_CAP_IV_E',$cap4_mode['ID_CAP_IV_E']);
                            $commandCap04ModEP440Det->bindParam(':ID_PRACTICA_AGRICOLA',$p440_c["nro_practica"]);
                            $commandCap04ModEP440Det->bindParam(':TIP_PRACTICA',$p440_c["efectua_practica"]);
                            $commandCap04ModEP440Det->bindParam(':NUM_ANIOS',$p440_c["cuantos_anios"]);
                            $commandCap04ModEP440Det->execute();
                        }
                            
                    }
                }else if(isset($p440["d"])){
                    foreach($p440["d"] as $p440_d){
                        $tip_practica = "4";
                        if($p440_d["efectua_practica"] || $p440_d["cuantos_anios"]){
                            $commandCap04ModEP440Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_E_PRACT_AGRI (ID_CAP_IV_E,ID_PRACTICA_AGRICOLA,TIP_PRACTICA,NUM_ANIOS) values (:ID_CAP_IV_E,:ID_PRACTICA_AGRICOLA,:TIP_PRACTICA,:NUM_ANIOS)');
                            $commandCap04ModEP440Det->bindParam(':ID_CAP_IV_E',$cap4_mode['ID_CAP_IV_E']);
                            $commandCap04ModEP440Det->bindParam(':ID_PRACTICA_AGRICOLA',$p440_d["nro_practica"]);
                            $commandCap04ModEP440Det->bindParam(':TIP_PRACTICA',$p440_d["efectua_practica"]);
                            $commandCap04ModEP440Det->bindParam(':NUM_ANIOS',$p440_d["cuantos_anios"]);
                            $commandCap04ModEP440Det->execute();
                        }
                        
                    }
                }
            }
        }

        /* LIMPIANDO DETALLE MODULO E P443 */
        $commandCap04ModEP443 = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_IV_E_EST_ENVA WHERE ID_CAP_IV_E=:ID_CAP_IV_E');
        $commandCap04ModEP443->bindParam(':ID_CAP_IV_E',$cap4_mode['ID_CAP_IV_E']);
        $commandCap04ModEP443->execute();

        if($model->cap4_mode_p443){
            $cap4_mode_p443 = explode(",",$model->cap4_mode_p443);
            foreach($cap4_mode_p443 as $p443){
                if($p443 == 7){
                    $commandCap04ModEP443Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_E_EST_ENVA (ID_CAP_IV_E,ID_ESTADO_ENVASE,TXT_OTRO) values (:ID_CAP_IV_E,:ID_ESTADO_ENVASE,:TXT_OTRO)');
                    $commandCap04ModEP443Det->bindParam(':ID_CAP_IV_E',$cap4_mode['ID_CAP_IV_E']);
                    $commandCap04ModEP443Det->bindParam(':ID_ESTADO_ENVASE',$p443);
                    $commandCap04ModEP443Det->bindParam(':TXT_OTRO',$model->cap4_mode_p443_7_especifique);
                    $commandCap04ModEP443Det->execute();
                }else{
                    $commandCap04ModEP443Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_E_EST_ENVA (ID_CAP_IV_E,ID_ESTADO_ENVASE) values (:ID_CAP_IV_E,:ID_ESTADO_ENVASE)');
                    $commandCap04ModEP443Det->bindParam(':ID_CAP_IV_E',$cap4_mode['ID_CAP_IV_E']);
                    $commandCap04ModEP443Det->bindParam(':ID_ESTADO_ENVASE',$p443);
                    $commandCap04ModEP443Det->execute();
                }
                
            }
        }

        /* LIMPIANDO DETALLE MODULO E P444 */
        $commandCap04ModEP444 = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_IV_E_CONTROL_PLAGA WHERE ID_CAP_IV_E=:ID_CAP_IV_E');
        $commandCap04ModEP444->bindParam(':ID_CAP_IV_E',$cap4_mode['ID_CAP_IV_E']);
        $commandCap04ModEP444->execute();

        if($model->cap4_mode_p444){
            $cap4_mode_p444 = explode(",",$model->cap4_mode_p444);
            foreach($cap4_mode_p444 as $p444){
                if($p444 == 8){
                    $commandCap04ModEP444Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_E_CONTROL_PLAGA (ID_CAP_IV_E,ID_TIPO_CONTROL_PLAGA,TXT_OTRO) values (:ID_CAP_IV_E,:ID_TIPO_CONTROL_PLAGA,:TXT_OTRO)');
                    $commandCap04ModEP444Det->bindParam(':ID_CAP_IV_E',$cap4_mode['ID_CAP_IV_E']);
                    $commandCap04ModEP444Det->bindParam(':ID_TIPO_CONTROL_PLAGA',$p444);
                    $commandCap04ModEP444Det->bindParam(':TXT_OTRO',$model->cap4_mode_p444_8_especifique);
                    $commandCap04ModEP444Det->execute();
                }else{
                    $commandCap04ModEP444Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_IV_E_CONTROL_PLAGA (ID_CAP_IV_E,ID_TIPO_CONTROL_PLAGA) values (:ID_CAP_IV_E,:ID_TIPO_CONTROL_PLAGA)');
                    $commandCap04ModEP444Det->bindParam(':ID_CAP_IV_E',$cap4_mode['ID_CAP_IV_E']);
                    $commandCap04ModEP444Det->bindParam(':ID_TIPO_CONTROL_PLAGA',$p444);
                    $commandCap04ModEP444Det->execute();
                }
                
            }
        }
    }

    private function capituloV($model,$id_encuesta,$connection){
        
        //$cap5_moda = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_V_A')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
        $cap5_moda_p501_b = json_decode($model->cap5_moda_p501_b, true);

        if($model->cap5_moda_p501_b){
            $commandCapVModA = $connection->createCommand('DELETE FROM ENA_TM_CAP_V_A WHERE ID_ENCUESTA=:ID_ENCUESTA');
            $commandCapVModA->bindParam(':ID_ENCUESTA',$id_encuesta);
            $commandCapVModA->execute();

            foreach($cap5_moda_p501_b as $p501){
                if($p501['cap5_moda_p501_b']!=0){
                    $cap5_moda = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_V_A')->where('ID_ENCUESTA=:ID_ENCUESTA AND ID_ESPECIE=:ID_ESPECIE',[':ID_ENCUESTA'=>$id_encuesta,':ID_ESPECIE'=>$p501['cap5_moda_p501_b']])->one();
                    if(!$cap5_moda){
                        $seqCapVModA = (new \yii\db\Query())->select('ENA_TM_CAP_V_A_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();
                        if($p501['cap5_moda_p501_b']=="27"){
                            $commandCapVModA = $connection->createCommand('INSERT INTO ENA_TM_CAP_V_A (ID_CAP_V_A,ID_ESPECIE,NUM_HEMBRA,NUM_MACHO,ID_ENCUESTA,TXT_OTRO) values (:ID_CAP_V_A,:ID_ESPECIE,:NUM_HEMBRA,:NUM_MACHO,:ID_ENCUESTA,:TXT_OTRO)');
                            $commandCapVModA->bindParam(':ID_CAP_V_A',$seqCapVModA['contador']);
                            $commandCapVModA->bindParam(':ID_ESPECIE',$p501['cap5_moda_p501_b']);
                            $commandCapVModA->bindParam(':NUM_HEMBRA',$p501['cap5_moda_p501_b_hembra']);
                            $commandCapVModA->bindParam(':NUM_MACHO',$p501['cap5_moda_p501_b_macho']);
                            $commandCapVModA->bindParam(':TXT_OTRO',$model->cap5_moda_p501_b_27_especifique);
                            $commandCapVModA->bindParam(':ID_ENCUESTA',$id_encuesta);
                            $commandCapVModA->execute();
                        }else{
                            $commandCapVModA = $connection->createCommand('INSERT INTO ENA_TM_CAP_V_A (ID_CAP_V_A,ID_ESPECIE,NUM_HEMBRA,NUM_MACHO,ID_ENCUESTA) values (:ID_CAP_V_A,:ID_ESPECIE,:NUM_HEMBRA,:NUM_MACHO,:ID_ENCUESTA)');
                            $commandCapVModA->bindParam(':ID_CAP_V_A',$seqCapVModA['contador']);
                            $commandCapVModA->bindParam(':ID_ESPECIE',$p501['cap5_moda_p501_b']);
                            $commandCapVModA->bindParam(':NUM_HEMBRA',$p501['cap5_moda_p501_b_hembra']);
                            $commandCapVModA->bindParam(':NUM_MACHO',$p501['cap5_moda_p501_b_macho']);
                            $commandCapVModA->bindParam(':ID_ENCUESTA',$id_encuesta);
                            $commandCapVModA->execute();
                        }
                        

                    }else{
                        $commandCapVModA = $connection->createCommand('UPDATE ENA_TM_CAP_V_A SET NUM_HEMBRA=:NUM_HEMBRA,NUM_MACHO=:NUM_MACHO,TXT_OTRO=:TXT_OTRO WHERE ID_CAP_V_A=:ID_CAP_V_A');
                        $commandCapVModA->bindParam(':NUM_HEMBRA',$p501['cap5_moda_p501_b_hembra']);
                        $commandCapVModA->bindParam(':NUM_MACHO',$p501['cap5_moda_p501_b_macho']);
                        $commandCapVModA->bindParam(':TXT_OTRO',$model->cap5_moda_p501_b_27_especifique);
                        $commandCapVModA->bindParam(':ID_CAP_V_A',$cap5_moda['ID_CAP_V_A']);
                        $commandCapVModA->execute();
                    }
                }
            }
        }

        $cap5_mod_b_c_d_e_f = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_V_B')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();

        if(!$cap5_mod_b_c_d_e_f){
            $seq = (new \yii\db\Query())->select('ENA_TM_CAP_V_B_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();
            $commandCap5ModBCDEF = $connection->createCommand('INSERT INTO ENA_TM_CAP_V_B (ID_CAP_V_B,ID_ENCUESTA,FLG_VACUNO,FLG_PORCINO,FLG_CUY,FLG_OVINO,FLG_CAPRINO,FLG_ALPACA,FLG_LLAMA,FLG_VICUNIA) values (:ID_CAP_V_B,:ID_ENCUESTA,:FLG_VACUNO,:FLG_PORCINO,:FLG_CUY,:FLG_OVINO,:FLG_CAPRINO,:FLG_ALPACA,:FLG_LLAMA,:FLG_VICUNIA)');
            $commandCap5ModBCDEF->bindParam(':ID_CAP_V_B',$seq['contador']);
            $commandCap5ModBCDEF->bindParam(':ID_ENCUESTA',$id_encuesta);
            $commandCap5ModBCDEF->bindParam(':FLG_VACUNO',$model->cap5_modb_p502_flag);
            $commandCap5ModBCDEF->bindParam(':FLG_PORCINO',$model->cap5_modc_p502_flag);
            $commandCap5ModBCDEF->bindParam(':FLG_CUY',$model->cap5_modd_p502_flag);
            $commandCap5ModBCDEF->bindParam(':FLG_OVINO',$model->cap5_mode_p502_a_flag);
            $commandCap5ModBCDEF->bindParam(':FLG_CAPRINO',$model->cap5_mode_p502_b_flag);
            $commandCap5ModBCDEF->bindParam(':FLG_ALPACA',$model->cap5_modf_p502_a_flag);
            $commandCap5ModBCDEF->bindParam(':FLG_LLAMA',$model->cap5_modf_p502_b_flag);
            $commandCap5ModBCDEF->bindParam(':FLG_VICUNIA',$model->cap5_modf_p502_c_flag);
            $commandCap5ModBCDEF->execute();

            $p05_mod_b_c_d_e_f = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_V_B')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
        }else{
            $commandCap5ModBCDEF = $connection->createCommand('UPDATE ENA_TM_CAP_V_B SET FLG_VACUNO=:FLG_VACUNO,FLG_PORCINO=:FLG_PORCINO,FLG_CUY=:FLG_CUY,FLG_OVINO=:FLG_OVINO,FLG_CAPRINO=:FLG_CAPRINO,FLG_ALPACA=:FLG_ALPACA,FLG_LLAMA=:FLG_LLAMA,FLG_VICUNIA=:FLG_VICUNIA WHERE ID_ENCUESTA=:ID_ENCUESTA');
            $commandCap5ModBCDEF->bindParam(':ID_ENCUESTA',$id_encuesta);
            $commandCap5ModBCDEF->bindParam(':FLG_VACUNO',$model->cap5_modb_p502_flag);
            $commandCap5ModBCDEF->bindParam(':FLG_PORCINO',$model->cap5_modc_p502_flag);
            $commandCap5ModBCDEF->bindParam(':FLG_CUY',$model->cap5_modd_p502_flag);
            $commandCap5ModBCDEF->bindParam(':FLG_OVINO',$model->cap5_mode_p502_a_flag);
            $commandCap5ModBCDEF->bindParam(':FLG_CAPRINO',$model->cap5_mode_p502_b_flag);
            $commandCap5ModBCDEF->bindParam(':FLG_ALPACA',$model->cap5_modf_p502_a_flag);
            $commandCap5ModBCDEF->bindParam(':FLG_LLAMA',$model->cap5_modf_p502_b_flag);
            $commandCap5ModBCDEF->bindParam(':FLG_VICUNIA',$model->cap5_modf_p502_c_flag);
            $commandCap5ModBCDEF->execute();
        }

        


        
        $cap5_mod_h = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_V_H')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
        if(!$cap5_mod_h){
            $seq = (new \yii\db\Query())->select('ENA_TM_CAP_V_H_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();
            $commandCap5ModH = $connection->createCommand('INSERT INTO ENA_TM_CAP_V_H (ID_CAP_V_H,ID_ENCUESTA,TIP_PRODUCTOS_PECUARIOS) values (:ID_CAP_V_H,:ID_ENCUESTA,:TIP_PRODUCTOS_PECUARIOS)');
            $commandCap5ModH->bindParam(':ID_CAP_V_H',$seq['contador']);
            $commandCap5ModH->bindParam(':ID_ENCUESTA',$id_encuesta);
            $commandCap5ModH->bindParam(':TIP_PRODUCTOS_PECUARIOS',$model->cap5_modh_p502_flag);
            $commandCap5ModH->execute();

            $cap5_mod_h = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_V_H')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
        }else{
            $commandCap5ModH = $connection->createCommand('UPDATE ENA_TM_CAP_V_H SET TIP_PRODUCTOS_PECUARIOS=:TIP_PRODUCTOS_PECUARIOS WHERE ID_ENCUESTA=:ID_ENCUESTA');
            $commandCap5ModH->bindParam(':ID_ENCUESTA',$id_encuesta);
            $commandCap5ModH->bindParam(':TIP_PRODUCTOS_PECUARIOS',$model->cap5_modh_p502_flag);
            $commandCap5ModH->execute();
        }




        $cap5_mod_i = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_V_I')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();

        $cap5_modi_p531 = explode(",",$model->cap5_modi_p531);
        $cap5_modi_p532 = explode(",",$model->cap5_modi_p532);
        $txt_alimento_otro = "";
        if(!$cap5_mod_i){
            $seq = (new \yii\db\Query())->select('ENA_TM_CAP_V_I_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();
            $commandCap5ModI = $connection->createCommand('INSERT INTO ENA_TM_CAP_V_I (ID_CAP_V_I,ID_ENCUESTA,FLG_PROCEDENCIA_COMERCIAL,FLG_PROCEDENCIA_PROPIA,FLG_ALIMENTO_PELETIZADO,FLG_ALIMENTO_HARINA,FLG_ALIMENTO_OTRO,TXT_ALIMENTO_OTRO) values (:ID_CAP_V_I,:ID_ENCUESTA,:FLG_PROCEDENCIA_COMERCIAL,:FLG_PROCEDENCIA_PROPIA,:FLG_ALIMENTO_PELETIZADO,:FLG_ALIMENTO_HARINA,:FLG_ALIMENTO_OTRO,:TXT_ALIMENTO_OTRO)');
            $commandCap5ModI->bindParam(':ID_CAP_V_I',$seq['contador']);
            $commandCap5ModI->bindParam(':ID_ENCUESTA',$id_encuesta);
            $commandCap5ModI->bindParam(':FLG_PROCEDENCIA_COMERCIAL',$cap5_modi_p531[0]);
            $commandCap5ModI->bindParam(':FLG_PROCEDENCIA_PROPIA',$cap5_modi_p531[1]);
            $commandCap5ModI->bindParam(':FLG_ALIMENTO_PELETIZADO',$cap5_modi_p532[0]);
            $commandCap5ModI->bindParam(':FLG_ALIMENTO_HARINA',$cap5_modi_p532[1]);
            $commandCap5ModI->bindParam(':FLG_ALIMENTO_OTRO',$cap5_modi_p532[2]);
            $commandCap5ModI->bindParam(':TXT_ALIMENTO_OTRO',$txt_alimento_otro);
            $commandCap5ModI->execute();

            $cap5_mod_i = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_V_I')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
        }else{
            $commandCap5ModI = $connection->createCommand('UPDATE ENA_TM_CAP_V_I SET FLG_PROCEDENCIA_COMERCIAL=:FLG_PROCEDENCIA_COMERCIAL,FLG_PROCEDENCIA_PROPIA=:FLG_PROCEDENCIA_PROPIA,FLG_ALIMENTO_PELETIZADO=:FLG_ALIMENTO_PELETIZADO,FLG_ALIMENTO_HARINA=:FLG_ALIMENTO_HARINA,FLG_ALIMENTO_OTRO=:FLG_ALIMENTO_OTRO,TXT_ALIMENTO_OTRO=:TXT_ALIMENTO_OTRO WHERE ID_ENCUESTA=:ID_ENCUESTA');
            $commandCap5ModI->bindParam(':ID_ENCUESTA',$id_encuesta);
            $commandCap5ModI->bindParam(':FLG_PROCEDENCIA_COMERCIAL',$cap5_modi_p531[0]);
            $commandCap5ModI->bindParam(':FLG_PROCEDENCIA_PROPIA',$cap5_modi_p531[1]);
            $commandCap5ModI->bindParam(':FLG_ALIMENTO_PELETIZADO',$cap5_modi_p532[0]);
            $commandCap5ModI->bindParam(':FLG_ALIMENTO_HARINA',$cap5_modi_p532[1]);
            $commandCap5ModI->bindParam(':FLG_ALIMENTO_OTRO',$cap5_modi_p532[2]);
            $commandCap5ModI->bindParam(':TXT_ALIMENTO_OTRO',$txt_alimento_otro);
            $commandCap5ModI->execute();
        }



        /* LIMPIANDO DETALLE  P530 */
        $commandCap5ModIP530 = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_V_TIPO_ALIMENTACION WHERE ID_CAP_V_I=:ID_CAP_V_I');
        $commandCap5ModIP530->bindParam(':ID_CAP_V_I',$cap5_mod_i['ID_CAP_V_I']);
        $commandCap5ModIP530->execute();

        if($model->cap5_modi_p530){
            $cap5_modi_p530 = explode(",",$model->cap5_modi_p530);
            foreach($cap5_modi_p530 as $p530){
                if($p530 == 9){
                    $commandCap5ModIP530Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_V_TIPO_ALIMENTACION (ID_CAP_V_I,ID_TIPO_ALIMENTACION,TXT_OTRO) values (:ID_CAP_V_I,:ID_TIPO_ALIMENTACION,:TXT_OTRO)');
                    $commandCap5ModIP530Det->bindParam(':ID_CAP_V_I',$cap5_mod_i['ID_CAP_V_I']);
                    $commandCap5ModIP530Det->bindParam(':ID_TIPO_ALIMENTACION',$p530);
                    $commandCap5ModIP530Det->bindParam(':TXT_OTRO',$model->cap5_modi_p530_9_especifique);
                    $commandCap5ModIP530Det->execute();
                }else{
                    $commandCap5ModIP530Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_V_TIPO_ALIMENTACION (ID_CAP_V_I,ID_TIPO_ALIMENTACION) values (:ID_CAP_V_I,:ID_TIPO_ALIMENTACION)');
                    $commandCap5ModIP530Det->bindParam(':ID_CAP_V_I',$cap5_mod_i['ID_CAP_V_I']);
                    $commandCap5ModIP530Det->bindParam(':ID_TIPO_ALIMENTACION',$p530);
                    $commandCap5ModIP530Det->execute();
                }
            }
        }

        /* LIMPIANDO DETALLE  p533 */
        $commandCap5ModIP533 = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_V_INSUMO WHERE ID_CAP_V_I=:ID_CAP_V_I');
        $commandCap5ModIP533->bindParam(':ID_CAP_V_I',$cap5_mod_i['ID_CAP_V_I']);
        $commandCap5ModIP533->execute();

        if($model->cap5_modi_p533){
            $cap5_modi_p533 = explode(",",$model->cap5_modi_p533);
            foreach($cap5_modi_p533 as $p533){
                if($p533 == 24){
                    $commandCap5ModIp533Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_V_INSUMO (ID_CAP_V_I,ID_INSUMO,TXT_OTRO) values (:ID_CAP_V_I,:ID_INSUMO,:TXT_OTRO)');
                    $commandCap5ModIp533Det->bindParam(':ID_CAP_V_I',$cap5_mod_i['ID_CAP_V_I']);
                    $commandCap5ModIp533Det->bindParam(':ID_INSUMO',$p533);
                    $commandCap5ModIp533Det->bindParam(':TXT_OTRO',$model->cap5_modi_p533_24_especifique);
                    $commandCap5ModIp533Det->execute();
                }else{
                    $commandCap5ModIP533Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_V_INSUMO (ID_CAP_V_I,ID_INSUMO) values (:ID_CAP_V_I,:ID_INSUMO)');
                    $commandCap5ModIP533Det->bindParam(':ID_CAP_V_I',$cap5_mod_i['ID_CAP_V_I']);
                    $commandCap5ModIP533Det->bindParam(':ID_INSUMO',$p533);
                    $commandCap5ModIP533Det->execute();
                }
            }
        }



        $cap5_mod_j = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_V_J')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();

        if(!$cap5_mod_j){
            $seq = (new \yii\db\Query())->select('ENA_TM_CAP_V_J_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();
            $commandCap5ModJ = $connection->createCommand('INSERT INTO ENA_TM_CAP_V_J (ID_CAP_V_J,ID_ENCUESTA,FLG_APLICO_VACUNA,FLG_DESPARASITACION) values (:ID_CAP_V_J,:ID_ENCUESTA,:FLG_APLICO_VACUNA,:FLG_DESPARASITACION)');
            $commandCap5ModJ->bindParam(':ID_CAP_V_J',$seq['contador']);
            $commandCap5ModJ->bindParam(':ID_ENCUESTA',$id_encuesta);
            $commandCap5ModJ->bindParam(':FLG_APLICO_VACUNA',$model->cap5_modj_p534_a);
            $commandCap5ModJ->bindParam(':FLG_DESPARASITACION',$model->cap5_modj_p535);
            $commandCap5ModJ->execute();

            $cap5_mod_j = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_V_J')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
        }else{
            $commandCap5ModJ = $connection->createCommand('UPDATE ENA_TM_CAP_V_J SET FLG_APLICO_VACUNA=:FLG_APLICO_VACUNA,FLG_DESPARASITACION=:FLG_DESPARASITACION WHERE ID_ENCUESTA=:ID_ENCUESTA');
            $commandCap5ModJ->bindParam(':ID_ENCUESTA',$id_encuesta);
            $commandCap5ModJ->bindParam(':FLG_APLICO_VACUNA',$model->cap5_modj_p534_a);
            $commandCap5ModJ->bindParam(':FLG_DESPARASITACION',$model->cap5_modj_p535);
            $commandCap5ModJ->execute();
        }


        /* LIMPIANDO DETALLE  p534 b */
        $commandCap5ModJP534B = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_V_J_VACUNA WHERE ID_CAP_V_J=:ID_CAP_V_J');
        $commandCap5ModJP534B->bindParam(':ID_CAP_V_J',$cap5_mod_j['ID_CAP_V_J']);
        $commandCap5ModJP534B->execute();

        if($model->cap5_modj_p534_b){
            $cap5_modj_p534_b = explode(",",$model->cap5_modj_p534_b);
            foreach($cap5_modj_p534_b as $p534_b){
                if($p534_b == 5 || $p534_b == 11 | $p534_b == 18 || $p534_b == 27 || $p534_b == 37){
                    $seq = (new \yii\db\Query())->select('ENA_TM_DET_CAP_V_J_VACUNA_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();
                    $commandCap5ModJP534BDet = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_V_J_VACUNA (ID_DET_VACUNAS,ID_CAP_V_J,ID_VACUNAS,TXT_OTRO) values (:ID_DET_VACUNAS,:ID_CAP_V_J,:ID_VACUNAS,:TXT_OTRO)');
                    $commandCap5ModJP534BDet->bindParam(':ID_DET_VACUNAS',$seq['contador']);
                    $commandCap5ModJP534BDet->bindParam(':ID_CAP_V_J',$cap5_mod_j['ID_CAP_V_J']);
                    $commandCap5ModJP534BDet->bindParam(':ID_VACUNAS',$p534_b);
                    if($p534_b == 5){
                        $commandCap5ModJP534BDet->bindParam(':TXT_OTRO',$model->cap5_modj_p534_b_5_especifique);
                    }else if($p534_b == 11){
                        $commandCap5ModJP534BDet->bindParam(':TXT_OTRO',$model->cap5_modj_p534_b_11_especifique);
                    }else if($p534_b == 18){
                        $commandCap5ModJP534BDet->bindParam(':TXT_OTRO',$model->cap5_modj_p534_b_18_especifique);
                    }else if($p534_b == 27){
                        $commandCap5ModJP534BDet->bindParam(':TXT_OTRO',$model->cap5_modj_p534_b_27_especifique);
                    }else if($p534_b == 37){
                        $commandCap5ModJP534BDet->bindParam(':TXT_OTRO',$model->cap5_modj_p534_b_37_especifique);
                    }
                    
                    $commandCap5ModJP534BDet->execute();
                }else{
                    $seq = (new \yii\db\Query())->select('ENA_TM_DET_CAP_V_J_VACUNA_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();

                    $commandCap5ModJP534BDet = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_V_J_VACUNA (ID_DET_VACUNAS,ID_CAP_V_J,ID_VACUNAS) values (:ID_DET_VACUNAS,:ID_CAP_V_J,:ID_VACUNAS)');
                    $commandCap5ModJP534BDet->bindParam(':ID_DET_VACUNAS',$seq['contador']);
                    $commandCap5ModJP534BDet->bindParam(':ID_CAP_V_J',$cap5_mod_j['ID_CAP_V_J']);
                    $commandCap5ModJP534BDet->bindParam(':ID_VACUNAS',$p534_b);
                    $commandCap5ModJP534BDet->execute();
                }
            }
        }


        /* LIMPIANDO DETALLE  p535 a */
        $commandCap5ModJP535A = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_V_J_PARASITO WHERE ID_CAP_V_J=:ID_CAP_V_J');
        $commandCap5ModJP535A->bindParam(':ID_CAP_V_J',$cap5_mod_j['ID_CAP_V_J']);
        $commandCap5ModJP535A->execute();

        if($model->cap5_modj_p535_a){
            $cap5_modj_p535_a = explode(",",$model->cap5_modj_p535_a);
            foreach($cap5_modj_p535_a as $p535_a){
                $seq = (new \yii\db\Query())->select('ENA_TM_DET_CAP_V_J_PARASITO_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();

                $commandCap5ModJP535ADet = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_V_J_PARASITO (ID_DET_PARASITOS,ID_CAP_V_J,ID_PARASITO) values (:ID_DET_PARASITOS,:ID_CAP_V_J,:ID_PARASITO)');
                $commandCap5ModJP535ADet->bindParam(':ID_DET_PARASITOS',$seq['contador']);
                $commandCap5ModJP535ADet->bindParam(':ID_CAP_V_J',$cap5_mod_j['ID_CAP_V_J']);
                $commandCap5ModJP535ADet->bindParam(':ID_PARASITO',$p535_a);
                $commandCap5ModJP535ADet->execute();
            }
        }



        $cap5_mod_k = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_V_K')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
        $cap5_mod_k_p537 = explode(",",$model->cap5_modk_p537);
        if(!$cap5_mod_k){
            $seq = (new \yii\db\Query())->select('ENA_TM_CAP_V_K_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();
            $commandCap5ModK = $connection->createCommand('INSERT INTO ENA_TM_CAP_V_K (ID_CAP_V_K,ID_ENCUESTA,FLG_MEJORAMIENTO_GENETICO,FLG_REPRODUCTORES_RAZA,FLG_REPRODUCTORES_MEJORADO,FLG_REPRODUCTORES_CRIOLLO,FLG_EMBRION_CERTIFICADO,FLG_AFILIADO_SIST_PROD,TXT_OBSERVACIONES) values (:ID_CAP_V_K,:ID_ENCUESTA,:FLG_MEJORAMIENTO_GENETICO,:FLG_REPRODUCTORES_RAZA,:FLG_REPRODUCTORES_MEJORADO,:FLG_REPRODUCTORES_CRIOLLO,:FLG_EMBRION_CERTIFICADO,:FLG_AFILIADO_SIST_PROD,:TXT_OBSERVACIONES)');
            $commandCap5ModK->bindParam(':ID_CAP_V_K',$seq['contador']);
            $commandCap5ModK->bindParam(':ID_ENCUESTA',$id_encuesta);
            $commandCap5ModK->bindParam(':FLG_MEJORAMIENTO_GENETICO',$model->cap5_modk_p536);
            $commandCap5ModK->bindParam(':FLG_REPRODUCTORES_RAZA',$cap5_mod_k_p537[0]);
            $commandCap5ModK->bindParam(':FLG_REPRODUCTORES_MEJORADO',$cap5_mod_k_p537[1]);
            $commandCap5ModK->bindParam(':FLG_REPRODUCTORES_CRIOLLO',$cap5_mod_k_p537[2]);
            $commandCap5ModK->bindParam(':FLG_EMBRION_CERTIFICADO',$model->cap5_modk_p538);
            $commandCap5ModK->bindParam(':FLG_AFILIADO_SIST_PROD',$model->cap5_modk_p540);
            $commandCap5ModK->bindParam(':TXT_OBSERVACIONES',$model->cap5_observacion);
            $commandCap5ModK->execute();

            $cap5_mod_k = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_V_K')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
        }else{
            $commandCap5ModK = $connection->createCommand('UPDATE ENA_TM_CAP_V_K SET FLG_MEJORAMIENTO_GENETICO=:FLG_MEJORAMIENTO_GENETICO,FLG_REPRODUCTORES_RAZA=:FLG_REPRODUCTORES_RAZA,FLG_REPRODUCTORES_MEJORADO=:FLG_REPRODUCTORES_MEJORADO,FLG_REPRODUCTORES_CRIOLLO=:FLG_REPRODUCTORES_CRIOLLO,FLG_EMBRION_CERTIFICADO=:FLG_EMBRION_CERTIFICADO,FLG_AFILIADO_SIST_PROD=:FLG_AFILIADO_SIST_PROD,TXT_OBSERVACIONES=:TXT_OBSERVACIONES WHERE ID_ENCUESTA=:ID_ENCUESTA');
            
            $commandCap5ModK->bindParam(':ID_ENCUESTA',$id_encuesta);
            $commandCap5ModK->bindParam(':FLG_MEJORAMIENTO_GENETICO',$model->cap5_modk_p536);
            $commandCap5ModK->bindParam(':FLG_REPRODUCTORES_RAZA',$cap5_mod_k_p537[0]);
            $commandCap5ModK->bindParam(':FLG_REPRODUCTORES_MEJORADO',$cap5_mod_k_p537[1]);
            $commandCap5ModK->bindParam(':FLG_REPRODUCTORES_CRIOLLO',$cap5_mod_k_p537[2]);
            $commandCap5ModK->bindParam(':FLG_EMBRION_CERTIFICADO',$model->cap5_modk_p538);
            $commandCap5ModK->bindParam(':FLG_AFILIADO_SIST_PROD',$model->cap5_modk_p540);
            $commandCap5ModK->bindParam(':TXT_OBSERVACIONES',$model->cap5_observacion);
            $commandCap5ModK->execute();
        }



        /* LIMPIANDO DETALLE  p536 a */
        $commandCap5ModKP536A = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_V_K_MEJOR_GENETICO WHERE ID_CAP_V_K=:ID_CAP_V_K');
        $commandCap5ModKP536A->bindParam(':ID_CAP_V_K',$cap5_mod_k['ID_CAP_V_K']);
        $commandCap5ModKP536A->execute();

        if($model->cap5_modk_p536_a){
            $cap5_modk_p536_a = explode(",",$model->cap5_modk_p536_a);
            foreach($cap5_modk_p536_a as $p536_a){
                if($p536_a == 9){
                    $commandCap5ModKP536ADet = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_V_K_MEJOR_GENETICO (ID_CAP_V_K,ID_MEJORAMIENTO_GENETICO,TXT_OTRO) values (:ID_CAP_V_K,:ID_MEJORAMIENTO_GENETICO,:TXT_OTRO)');
                    $commandCap5ModKP536ADet->bindParam(':ID_CAP_V_K',$cap5_mod_k['ID_CAP_V_K']);
                    $commandCap5ModKP536ADet->bindParam(':ID_MEJORAMIENTO_GENETICO',$p536_a);
                    $commandCap5ModKP536ADet->bindParam(':TXT_OTRO',$model->cap5_modk_p536_a_9_especifique);
                    $commandCap5ModKP536ADet->execute();
                }else{
                    $commandCap5ModKP536ADet = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_V_K_MEJOR_GENETICO (ID_CAP_V_K,ID_MEJORAMIENTO_GENETICO) values (:ID_CAP_V_K,:ID_MEJORAMIENTO_GENETICO)');
                    $commandCap5ModKP536ADet->bindParam(':ID_CAP_V_K',$cap5_mod_k['ID_CAP_V_K']);
                    $commandCap5ModKP536ADet->bindParam(':ID_MEJORAMIENTO_GENETICO',$p536_a);
                    $commandCap5ModKP536ADet->execute();
                }
            }
        }

        /* LIMPIANDO DETALLE  p539 a */
        $commandCap5ModKP539A = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_V_K_INFO_GENETICO WHERE ID_CAP_V_K=:ID_CAP_V_K');
        $commandCap5ModKP539A->bindParam(':ID_CAP_V_K',$cap5_mod_k['ID_CAP_V_K']);
        $commandCap5ModKP539A->execute();

        if($model->cap5_modk_p539_a){
            $cap5_modk_p539_a = explode(",",$model->cap5_modk_p539_a);
            foreach($cap5_modk_p539_a as $p539_a){
                $commandCap5ModKP539ADet = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_V_K_INFO_GENETICO (ID_CAP_V_K,ID_INFO_GENETICO) values (:ID_CAP_V_K,:ID_INFO_GENETICO)');
                $commandCap5ModKP539ADet->bindParam(':ID_CAP_V_K',$cap5_mod_k['ID_CAP_V_K']);
                $commandCap5ModKP539ADet->bindParam(':ID_INFO_GENETICO',$p539_a);
                $commandCap5ModKP539ADet->execute();
            }
        }



        /* LIMPIANDO DETALLE  p541 a */
        $commandCap5ModKP541A = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_V_K_RAZA WHERE ID_CAP_V_K=:ID_CAP_V_K');
        $commandCap5ModKP541A->bindParam(':ID_CAP_V_K',$cap5_mod_k['ID_CAP_V_K']);
        $commandCap5ModKP541A->execute();

        if($model->cap5_modk_p541_a){
            $cap5_modk_p541_a = explode(",",$model->cap5_modk_p541_a);
            foreach($cap5_modk_p541_a as $p541_a){
                if($p541_a == 8 || $p541_a == 15 || $p541_a == 24 || $p541_a == 30 || $p541_a == 39){
                    $seq = (new \yii\db\Query())->select('ENA_TM_DET_CAP_V_K_RAZA_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();
                    $commandCap5ModKP541ADet = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_V_K_RAZA (ID_DET_RAZAS,ID_CAP_V_K,ID_RAZA,TXT_OTRO) values (:ID_DET_RAZAS,:ID_CAP_V_K,:ID_RAZA,:TXT_OTRO)');
                    $commandCap5ModKP541ADet->bindParam(':ID_DET_RAZAS',$seq['contador']);
                    $commandCap5ModKP541ADet->bindParam(':ID_CAP_V_K',$cap5_mod_k['ID_CAP_V_K']);
                    $commandCap5ModKP541ADet->bindParam(':ID_RAZA',$p541_a);
                    if($p541_a == 8){
                        $commandCap5ModKP541ADet->bindParam(':TXT_OTRO',$model->cap5_modk_p541_a_8_especifique);
                    }else if($p541_a == 15){
                        $commandCap5ModKP541ADet->bindParam(':TXT_OTRO',$model->cap5_modk_p541_a_15_especifique);
                    }else if($p541_a == 24){
                        $commandCap5ModKP541ADet->bindParam(':TXT_OTRO',$model->cap5_modk_p541_a_24_especifique);
                    }else if($p541_a == 30){
                        $commandCap5ModKP541ADet->bindParam(':TXT_OTRO',$model->cap5_modk_p541_a_30_especifique);
                    }else if($p541_a == 39){
                        $commandCap5ModKP541ADet->bindParam(':TXT_OTRO',$model->cap5_modk_p541_a_39_especifique);
                    }
                    
                    $commandCap5ModKP541ADet->execute();
                }else{
                    $seq = (new \yii\db\Query())->select('ENA_TM_DET_CAP_V_K_RAZA_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();

                    $commandCap5ModKP541ADet = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_V_K_RAZA (ID_DET_RAZAS,ID_CAP_V_K,ID_RAZA) values (:ID_DET_RAZAS,:ID_CAP_V_K,:ID_RAZA)');
                    $commandCap5ModKP541ADet->bindParam(':ID_DET_RAZAS',$seq['contador']);
                    $commandCap5ModKP541ADet->bindParam(':ID_CAP_V_K',$cap5_mod_k['ID_CAP_V_K']);
                    $commandCap5ModKP541ADet->bindParam(':ID_RAZA',$p541_a);
                    $commandCap5ModKP541ADet->execute();
                }
            }
        }




    }

    private function capituloVI($model,$id_encuesta,$connection){
        /* INSERTANDO  CABECERA*/
        $cap6 = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_VI')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
        
        

        if(!$cap6){
            $seq = (new \yii\db\Query())->select('ENA_TM_CAP_VI_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();
            $commandCap06 = $connection->createCommand('INSERT INTO ENA_TM_CAP_VI (ID_CAP_VI,TIP_MINAS_EXPLOTACION,TIP_INFO_CONTAMINACION,TIP_PRODUCTOS_IDENTIFICADOS,TIP_TIPO_CERTIFICACION,ID_CERTIFICACION_OTORGADA,ID_ENCUESTA) values (:ID_CAP_VI,:TIP_MINAS_EXPLOTACION,:TIP_INFO_CONTAMINACION,:TIP_PRODUCTOS_IDENTIFICADOS,:TIP_TIPO_CERTIFICACION,:ID_CERTIFICACION_OTORGADA,:ID_ENCUESTA)');
            $commandCap06->bindParam(':ID_CAP_VI',$seq['contador']);
            $commandCap06->bindParam(':TIP_MINAS_EXPLOTACION',$model->cap6_p603);
            $commandCap06->bindParam(':TIP_INFO_CONTAMINACION',$model->cap6_p604);
            $commandCap06->bindParam(':TIP_PRODUCTOS_IDENTIFICADOS',$model->cap6_p607);
            $commandCap06->bindParam(':TIP_TIPO_CERTIFICACION',$model->cap6_p608);
            $commandCap06->bindParam(':ID_CERTIFICACION_OTORGADA',$model->cap6_p609);
            $commandCap06->bindParam(':ID_ENCUESTA',$id_encuesta);
            $commandCap06->execute();

            $cap6 = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_VI')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
        }else{
            $commandCap06 = $connection->createCommand('UPDATE ENA_TM_CAP_VI SET TIP_MINAS_EXPLOTACION=:TIP_MINAS_EXPLOTACION,TIP_INFO_CONTAMINACION=:TIP_INFO_CONTAMINACION,TIP_PRODUCTOS_IDENTIFICADOS=:TIP_PRODUCTOS_IDENTIFICADOS,TIP_TIPO_CERTIFICACION=:TIP_TIPO_CERTIFICACION,ID_CERTIFICACION_OTORGADA=:ID_CERTIFICACION_OTORGADA WHERE ID_ENCUESTA=:ID_ENCUESTA');
            $commandCap06->bindParam(':ID_ENCUESTA',$id_encuesta);
            $commandCap06->bindParam(':TIP_MINAS_EXPLOTACION',$model->cap6_p603);
            $commandCap06->bindParam(':TIP_INFO_CONTAMINACION',$model->cap6_p604);
            $commandCap06->bindParam(':TIP_PRODUCTOS_IDENTIFICADOS',$model->cap6_p607);
            $commandCap06->bindParam(':TIP_TIPO_CERTIFICACION',$model->cap6_p608);
            $commandCap06->bindParam(':ID_CERTIFICACION_OTORGADA',$model->cap6_p609);
            $commandCap06->execute();
        }

        /* LIMPIANDO DETALLE  P601 */
        $commandCap06P601 = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_VI_RESIDUO_CUL WHERE ID_CAP_VI=:ID_CAP_VI');
        $commandCap06P601->bindParam(':ID_CAP_VI',$cap6['ID_CAP_VI']);
        $commandCap06P601->execute();

        if($model->cap6_p601){
            $cap6_p601 = explode(",",$model->cap6_p601);
            foreach($cap6_p601 as $p601){
                if($p601 == 7){
                    $commandCap06P601Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_VI_RESIDUO_CUL (ID_CAP_VI,ID_RESIDUO,TXT_OTRO) values (:ID_CAP_VI,:ID_RESIDUO,:TXT_OTRO)');
                    $commandCap06P601Det->bindParam(':ID_CAP_VI',$cap6['ID_CAP_VI']);
                    $commandCap06P601Det->bindParam(':ID_RESIDUO',$p601);
                    $commandCap06P601Det->bindParam(':TXT_OTRO',$model->cap6_p601_7_especifique);
                    $commandCap06P601Det->execute();
                }else{
                    $commandCap06P601Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_VI_RESIDUO_CUL (ID_CAP_VI,ID_RESIDUO) values (:ID_CAP_VI,:ID_RESIDUO)');
                    $commandCap06P601Det->bindParam(':ID_CAP_VI',$cap6['ID_CAP_VI']);
                    $commandCap06P601Det->bindParam(':ID_RESIDUO',$p601);
                    $commandCap06P601Det->execute();
                }
                
            }
        }


        /* LIMPIANDO DETALLE  p602 */
        $commandCap06p602 = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_VI_RESIDUO_ANIM WHERE ID_CAP_VI=:ID_CAP_VI');
        $commandCap06p602->bindParam(':ID_CAP_VI',$cap6['ID_CAP_VI']);
        $commandCap06p602->execute();

        if($model->cap6_p602){
            $cap6_p602 = explode(",",$model->cap6_p602);
            foreach($cap6_p602 as $p602){
                if($p602 == 7){
                    $commandCap06p602Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_VI_RESIDUO_ANIM (ID_CAP_VI,ID_RESIDUO,TXT_OTRO) values (:ID_CAP_VI,:ID_RESIDUO,:TXT_OTRO)');
                    $commandCap06p602Det->bindParam(':ID_CAP_VI',$cap6['ID_CAP_VI']);
                    $commandCap06p602Det->bindParam(':ID_RESIDUO',$p602);
                    $commandCap06p602Det->bindParam(':TXT_OTRO',$model->cap6_p602_7_especifique);
                    $commandCap06p602Det->execute();
                }else{
                    $commandCap06p602Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_VI_RESIDUO_ANIM (ID_CAP_VI,ID_RESIDUO) values (:ID_CAP_VI,:ID_RESIDUO)');
                    $commandCap06p602Det->bindParam(':ID_CAP_VI',$cap6['ID_CAP_VI']);
                    $commandCap06p602Det->bindParam(':ID_RESIDUO',$p602);
                    $commandCap06p602Det->execute();
                }
                
            }
        }


        /* LIMPIANDO DETALLE  p605 */
        $commandCap06p605 = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_VI_ENTIDAD WHERE ID_CAP_VI=:ID_CAP_VI');
        $commandCap06p605->bindParam(':ID_CAP_VI',$cap6['ID_CAP_VI']);
        $commandCap06p605->execute();

        if($model->cap6_p605){
            $cap6_p605 = explode(",",$model->cap6_p605);
            foreach($cap6_p605 as $p605){
                if($p605 == 6){
                    $commandCap06p605Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_VI_ENTIDAD (ID_CAP_VI,ID_ENTIDAD_ESTADO,TXT_OTRO) values (:ID_CAP_VI,:ID_ENTIDAD_ESTADO,:TXT_OTRO)');
                    $commandCap06p605Det->bindParam(':ID_CAP_VI',$cap6['ID_CAP_VI']);
                    $commandCap06p605Det->bindParam(':ID_ENTIDAD_ESTADO',$p605);
                    $commandCap06p605Det->bindParam(':TXT_OTRO',$model->cap6_p605_6_especifique);
                    $commandCap06p605Det->execute();
                }else{
                    $commandCap06p605Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_VI_ENTIDAD (ID_CAP_VI,ID_ENTIDAD_ESTADO) values (:ID_CAP_VI,:ID_ENTIDAD_ESTADO)');
                    $commandCap06p605Det->bindParam(':ID_CAP_VI',$cap6['ID_CAP_VI']);
                    $commandCap06p605Det->bindParam(':ID_ENTIDAD_ESTADO',$p605);
                    $commandCap06p605Det->execute();
                }
                
            }
        }


        /* LIMPIANDO DETALLE  p606 */
        $commandCap06p606 = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_VI_CONSERVA WHERE ID_CAP_VI=:ID_CAP_VI');
        $commandCap06p606->bindParam(':ID_CAP_VI',$cap6['ID_CAP_VI']);
        $commandCap06p606->execute();

        if($model->cap6_p606){
            $cap6_p606 = explode(",",$model->cap6_p606);
            foreach($cap6_p606 as $p606){
                if($p606 == 5){
                    $commandCap06p606Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_VI_CONSERVA (ID_CAP_VI,ID_CONSERVA_ALIMENTOS,TXT_OTRO) values (:ID_CAP_VI,:ID_CONSERVA_ALIMENTOS,:TXT_OTRO)');
                    $commandCap06p606Det->bindParam(':ID_CAP_VI',$cap6['ID_CAP_VI']);
                    $commandCap06p606Det->bindParam(':ID_CONSERVA_ALIMENTOS',$p606);
                    $commandCap06p606Det->bindParam(':TXT_OTRO',$model->cap6_p606_5_especifique);
                    $commandCap06p606Det->execute();
                }else{
                    $commandCap06p606Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_VI_CONSERVA (ID_CAP_VI,ID_CONSERVA_ALIMENTOS) values (:ID_CAP_VI,:ID_CONSERVA_ALIMENTOS)');
                    $commandCap06p606Det->bindParam(':ID_CAP_VI',$cap6['ID_CAP_VI']);
                    $commandCap06p606Det->bindParam(':ID_CONSERVA_ALIMENTOS',$p606);
                    $commandCap06p606Det->execute();
                }
                
            }
        }


        /* LIMPIANDO DETALLE  p610 */
        $commandCap06p610 = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_VI_CERTIFICADO WHERE ID_CAP_VI=:ID_CAP_VI');
        $commandCap06p610->bindParam(':ID_CAP_VI',$cap6['ID_CAP_VI']);
        $commandCap06p610->execute();

        if($model->cap6_p610){
            $cap6_p610 = explode(",",$model->cap6_p610);
            foreach($cap6_p610 as $p610){
                if($p610 == 7){
                    $commandCap06p610Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_VI_CERTIFICADO (ID_CAP_VI,ID_CERTIFICADO,TXT_OTRO) values (:ID_CAP_VI,:ID_CERTIFICADO,:TXT_OTRO)');
                    $commandCap06p610Det->bindParam(':ID_CAP_VI',$cap6['ID_CAP_VI']);
                    $commandCap06p610Det->bindParam(':ID_CERTIFICADO',$p610);
                    $commandCap06p610Det->bindParam(':TXT_OTRO',$model->cap6_p610_7_especifique);
                    $commandCap06p610Det->execute();
                }else{
                    $commandCap06p610Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_VI_CERTIFICADO (ID_CAP_VI,ID_CERTIFICADO) values (:ID_CAP_VI,:ID_CERTIFICADO)');
                    $commandCap06p610Det->bindParam(':ID_CAP_VI',$cap6['ID_CAP_VI']);
                    $commandCap06p610Det->bindParam(':ID_CERTIFICADO',$p610);
                    $commandCap06p610Det->execute();
                }
                
            }
        }



    }

    private function capituloVII($model,$id_encuesta,$connection){
        /* INSERTANDO  CABECERA*/
        $cap7Modc = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_VII_C')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
        
        

        if(!$cap7Modc){
            $seqModc = (new \yii\db\Query())->select('ENA_TM_CAP_VII_C_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();
            $commandCap07 = $connection->createCommand('INSERT INTO ENA_TM_CAP_VII_C 
            (ID_CAP_VII_C,ID_ENCUESTA,TIP_PERT_ASO_COOP_COM,NUM_CUANTAS_PERT) values 
            (:ID_CAP_VII_C,:ID_ENCUESTA,:TIP_PERT_ASO_COOP_COM,:NUM_CUANTAS_PERT)');
            $commandCap07->bindParam(':ID_CAP_VII_C',$seqModc['contador']);
            $commandCap07->bindParam(':ID_ENCUESTA',$id_encuesta);
            $commandCap07->bindParam(':TIP_PERT_ASO_COOP_COM',$model->cap7_modc_p710_a);
            $commandCap07->bindParam(':NUM_CUANTAS_PERT',$model->cap7_modc_p710_b);
            $commandCap07->execute();

            $cap7Modc = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_VII_C')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
        }else{
            $commandCap07 = $connection->createCommand('UPDATE ENA_TM_CAP_VII_C SET TIP_PERT_ASO_COOP_COM=:TIP_PERT_ASO_COOP_COM,NUM_CUANTAS_PERT=:NUM_CUANTAS_PERT WHERE ID_ENCUESTA=:ID_ENCUESTA');
            $commandCap07->bindParam(':ID_ENCUESTA',$id_encuesta);
            $commandCap07->bindParam(':TIP_PERT_ASO_COOP_COM',$model->cap7_modc_p710_a);
            $commandCap07->bindParam(':NUM_CUANTAS_PERT',$model->cap7_modc_p710_b);
            $commandCap07->execute();
        }

        /* LIMPIANDO DETALLE  P714 */
        $commandCap07P714 = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_VII_C_BENEFICIOS_ASO WHERE ID_CAP_VII_C=:ID_CAP_VII_C');
        $commandCap07P714->bindParam(':ID_CAP_VII_C',$cap7Modc['ID_CAP_VII_C']);
        $commandCap07P714->execute();

        if($model->cap7_modc_p714){
            $cap7_modc_p714 = explode(",",$model->cap7_modc_p714);
            foreach($cap7_modc_p714 as $P714){
                if($P714 == 6){
                    $commandCap07P714Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_VII_C_BENEFICIOS_ASO (ID_CAP_VII_C,ID_BENEFICIO,TXT_OTRO) values (:ID_CAP_VII_C,:ID_BENEFICIO,:TXT_OTRO)');
                    $commandCap07P714Det->bindParam(':ID_CAP_VII_C',$cap7Modc['ID_CAP_VII_C']);
                    $commandCap07P714Det->bindParam(':ID_BENEFICIO',$P714);
                    $commandCap07P714Det->bindParam(':TXT_OTRO',$model->cap7_modc_p714_6_especifique);
                    $commandCap07P714Det->execute();
                }else{
                    $commandCap07P714Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_VII_C_BENEFICIOS_ASO (ID_CAP_VII_C,ID_BENEFICIO) values (:ID_CAP_VII_C,:ID_BENEFICIO)');
                    $commandCap07P714Det->bindParam(':ID_CAP_VII_C',$cap7Modc['ID_CAP_VII_C']);
                    $commandCap07P714Det->bindParam(':ID_BENEFICIO',$P714);
                    $commandCap07P714Det->execute();
                }
                
            }
        }


        /* LIMPIANDO DETALLE  P715 */
        $commandCap07P715 = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_VII_C_ACTIVIDADES WHERE ID_CAP_VII_C=:ID_CAP_VII_C');
        $commandCap07P715->bindParam(':ID_CAP_VII_C',$cap7Modc['ID_CAP_VII_C']);
        $commandCap07P715->execute();

        if($model->cap7_modc_p715){
            $cap7_modc_p715 = explode(",",$model->cap7_modc_p715);
            foreach($cap7_modc_p715 as $P715){
                $commandCap07P715Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_VII_C_ACTIVIDADES (ID_CAP_VII_C,ID_ACTIVIDADES) values (:ID_CAP_VII_C,:ID_ACTIVIDADES)');
                $commandCap07P715Det->bindParam(':ID_CAP_VII_C',$cap7Modc['ID_CAP_VII_C']);
                $commandCap07P715Det->bindParam(':ID_ACTIVIDADES',$P715);
                $commandCap07P715Det->execute();
            }
        }


        /* LIMPIANDO DETALLE  P716 */
        $commandCap07P716 = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_VII_C_LOGRO WHERE ID_CAP_VII_C=:ID_CAP_VII_C');
        $commandCap07P716->bindParam(':ID_CAP_VII_C',$cap7Modc['ID_CAP_VII_C']);
        $commandCap07P716->execute();

        if($model->cap7_modc_p716){
            $cap7_modc_p716 = explode(",",$model->cap7_modc_p716);
            foreach($cap7_modc_p716 as $P716){
                if($P716 == 6){
                    $commandCap07P716Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_VII_C_LOGRO (ID_CAP_VII_C,ID_LOGRO,TXT_OTRO) values (:ID_CAP_VII_C,:ID_LOGRO,:TXT_OTRO)');
                    $commandCap07P716Det->bindParam(':ID_CAP_VII_C',$cap7Modc['ID_CAP_VII_C']);
                    $commandCap07P716Det->bindParam(':ID_LOGRO',$P716);
                    $commandCap07P716Det->bindParam(':TXT_OTRO',$model->cap7_modc_p716_6_especifique);
                    $commandCap07P716Det->execute();
                }else{
                    $commandCap07P716Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_VII_C_LOGRO (ID_CAP_VII_C,ID_LOGRO) values (:ID_CAP_VII_C,:ID_LOGRO)');
                    $commandCap07P716Det->bindParam(':ID_CAP_VII_C',$cap7Modc['ID_CAP_VII_C']);
                    $commandCap07P716Det->bindParam(':ID_LOGRO',$P716);
                    $commandCap07P716Det->execute();
                }
                
            }
        }

        /* LIMPIANDO DETALLE  P717 */
        $commandCap07P717 = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_VII_C_EQUIPO WHERE ID_CAP_VII_C=:ID_CAP_VII_C');
        $commandCap07P717->bindParam(':ID_CAP_VII_C',$cap7Modc['ID_CAP_VII_C']);
        $commandCap07P717->execute();

        if($model->cap7_modc_p717){
            $cap7_modc_p717 = explode(",",$model->cap7_modc_p717);
            foreach($cap7_modc_p717 as $P717){
                if($P717 == 1){
                    $commandCap07P717Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_VII_C_EQUIPO (ID_CAP_VII_C,ID_EQUIPO,TIP_RESPUESTA,TXT_OTRO_RESPUESTA) values (:ID_CAP_VII_C,:ID_EQUIPO,:TIP_RESPUESTA,:TXT_OTRO_RESPUESTA)');
                    $commandCap07P717Det->bindParam(':ID_CAP_VII_C',$cap7Modc['ID_CAP_VII_C']);
                    $commandCap07P717Det->bindParam(':ID_EQUIPO',$P717);
                    $commandCap07P717Det->bindParam(':TIP_RESPUESTA',$model->cap7_modc_p718_1);
                    $commandCap07P717Det->bindParam(':TXT_OTRO_RESPUESTA',$model->cap7_modc_p718_1_especifique);
                    $commandCap07P717Det->execute();
                }else if($P717 == 2){
                    $commandCap07P717Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_VII_C_EQUIPO (ID_CAP_VII_C,ID_EQUIPO,TIP_RESPUESTA,TXT_OTRO_RESPUESTA) values (:ID_CAP_VII_C,:ID_EQUIPO,:TIP_RESPUESTA,:TXT_OTRO_RESPUESTA)');
                    $commandCap07P717Det->bindParam(':ID_CAP_VII_C',$cap7Modc['ID_CAP_VII_C']);
                    $commandCap07P717Det->bindParam(':ID_EQUIPO',$P717);
                    $commandCap07P717Det->bindParam(':TIP_RESPUESTA',$model->cap7_modc_p718_2);
                    $commandCap07P717Det->bindParam(':TXT_OTRO_RESPUESTA',$model->cap7_modc_p718_2_especifique);
                    $commandCap07P717Det->execute();
                }else if($P717 == 3){
                    $commandCap07P717Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_VII_C_EQUIPO (ID_CAP_VII_C,ID_EQUIPO,TIP_RESPUESTA,TXT_OTRO_RESPUESTA) values (:ID_CAP_VII_C,:ID_EQUIPO,:TIP_RESPUESTA,:TXT_OTRO_RESPUESTA)');
                    $commandCap07P717Det->bindParam(':ID_CAP_VII_C',$cap7Modc['ID_CAP_VII_C']);
                    $commandCap07P717Det->bindParam(':ID_EQUIPO',$P717);
                    $commandCap07P717Det->bindParam(':TIP_RESPUESTA',$model->cap7_modc_p718_3);
                    $commandCap07P717Det->bindParam(':TXT_OTRO_RESPUESTA',$model->cap7_modc_p718_3_especifique);
                    $commandCap07P717Det->execute();
                }else if($P717 == 4){
                    $commandCap07P717Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_VII_C_EQUIPO (ID_CAP_VII_C,ID_EQUIPO,TIP_RESPUESTA,TXT_OTRO_RESPUESTA) values (:ID_CAP_VII_C,:ID_EQUIPO,:TIP_RESPUESTA,:TXT_OTRO_RESPUESTA)');
                    $commandCap07P717Det->bindParam(':ID_CAP_VII_C',$cap7Modc['ID_CAP_VII_C']);
                    $commandCap07P717Det->bindParam(':ID_EQUIPO',$P717);
                    $commandCap07P717Det->bindParam(':TIP_RESPUESTA',$model->cap7_modc_p718_4);
                    $commandCap07P717Det->bindParam(':TXT_OTRO_RESPUESTA',$model->cap7_modc_p718_4_especifique);
                    $commandCap07P717Det->execute();
                }else if($P717 == 5){
                    $commandCap07P717Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_VII_C_EQUIPO (ID_CAP_VII_C,ID_EQUIPO,TIP_RESPUESTA,TXT_OTRO_RESPUESTA) values (:ID_CAP_VII_C,:ID_EQUIPO,:TIP_RESPUESTA,:TXT_OTRO_RESPUESTA)');
                    $commandCap07P717Det->bindParam(':ID_CAP_VII_C',$cap7Modc['ID_CAP_VII_C']);
                    $commandCap07P717Det->bindParam(':ID_EQUIPO',$P717);
                    $commandCap07P717Det->bindParam(':TIP_RESPUESTA',$model->cap7_modc_p718_5);
                    $commandCap07P717Det->bindParam(':TXT_OTRO_RESPUESTA',$model->cap7_modc_p718_5_especifique);
                    $commandCap07P717Det->execute();
                }else if($P717 == 6){
                    $commandCap07P717Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_VII_C_EQUIPO (ID_CAP_VII_C,ID_EQUIPO,TIP_RESPUESTA,TXT_OTRO_RESPUESTA) values (:ID_CAP_VII_C,:ID_EQUIPO,:TIP_RESPUESTA,:TXT_OTRO_RESPUESTA)');
                    $commandCap07P717Det->bindParam(':ID_CAP_VII_C',$cap7Modc['ID_CAP_VII_C']);
                    $commandCap07P717Det->bindParam(':ID_EQUIPO',$P717);
                    $commandCap07P717Det->bindParam(':TIP_RESPUESTA',$model->cap7_modc_p718_6);
                    $commandCap07P717Det->bindParam(':TXT_OTRO_RESPUESTA',$model->cap7_modc_p718_6_especifique);
                    $commandCap07P717Det->execute();
                }else if($P717 == 7){
                    $commandCap07P717Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_VII_C_EQUIPO (ID_CAP_VII_C,ID_EQUIPO,TXT_OTRO,TIP_RESPUESTA,TXT_OTRO_RESPUESTA) values (:ID_CAP_VII_C,:ID_EQUIPO,:TXT_OTRO,:TIP_RESPUESTA,:TXT_OTRO_RESPUESTA)');
                    $commandCap07P717Det->bindParam(':ID_CAP_VII_C',$cap7Modc['ID_CAP_VII_C']);
                    $commandCap07P717Det->bindParam(':ID_EQUIPO',$P717);
                    $commandCap07P717Det->bindParam(':TXT_OTRO',$model->cap7_modc_p717_7_especifique);
                    $commandCap07P717Det->bindParam(':TIP_RESPUESTA',$model->cap7_modc_p718_7);
                    $commandCap07P717Det->bindParam(':TXT_OTRO_RESPUESTA',$model->cap7_modc_p718_7_especifique);
                    $commandCap07P717Det->execute();
                }
                
            }
        }

        /* LIMPIANDO DETALLE  P718 */
        /*
        $commandCap07P718 = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_VII_C_EQUIPO WHERE ID_CAP_VII_C=:ID_CAP_VII_C');
        $commandCap07P718->bindParam(':ID_CAP_VII_C',$cap7Modc['ID_CAP_VII_C']);
        $commandCap07P718->execute();

        if($model->cap7_modc_p718){
            $cap7_modc_p718 = explode(",",$model->cap7_modc_p718);
            foreach($cap7_modc_p718 as $P718){
                if($P718 == 7){
                    $commandCap07P718Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_VII_C_EQUIPO (ID_CAP_VII_C,ID_EQUIPO,TXT_OTRO) values (:ID_CAP_VII_C,:ID_EQUIPO,:TXT_OTRO)');
                    $commandCap07P718Det->bindParam(':ID_CAP_VII_C',$cap7Modc['ID_CAP_VII_C']);
                    $commandCap07P718Det->bindParam(':ID_EQUIPO',$P718);
                    $commandCap07P718Det->bindParam(':TXT_OTRO',$model->cap7_modc_p718_7_especifique);
                    $commandCap07P718Det->execute();
                }else{
                    $commandCap07P718Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_VII_C_EQUIPO (ID_CAP_VII_C,ID_EQUIPO) values (:ID_CAP_VII_C,:ID_EQUIPO)');
                    $commandCap07P718Det->bindParam(':ID_CAP_VII_C',$cap7Modc['ID_CAP_VII_C']);
                    $commandCap07P718Det->bindParam(':ID_EQUIPO',$P718);
                    $commandCap07P718Det->execute();
                }
                
            }
        }
        */

        /* LIMPIANDO DETALLE  P719 */
        $commandCap07P719 = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_VII_C_MOTIVO_ASO WHERE ID_CAP_VII_C=:ID_CAP_VII_C');
        $commandCap07P719->bindParam(':ID_CAP_VII_C',$cap7Modc['ID_CAP_VII_C']);
        $commandCap07P719->execute();

        if($model->cap7_modc_p719){
            $cap7_modc_p719 = explode(",",$model->cap7_modc_p719);
            foreach($cap7_modc_p719 as $P719){
                if($P719 == 6){
                    $commandCap07P719Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_VII_C_MOTIVO_ASO (ID_CAP_VII_C,ID_MOTIVO_ASO,TXT_OTRO) values (:ID_CAP_VII_C,:ID_MOTIVO_ASO,:TXT_OTRO)');
                    $commandCap07P719Det->bindParam(':ID_CAP_VII_C',$cap7Modc['ID_CAP_VII_C']);
                    $commandCap07P719Det->bindParam(':ID_MOTIVO_ASO',$P719);
                    $commandCap07P719Det->bindParam(':TXT_OTRO',$model->cap7_modc_p719_6_especifique);
                    $commandCap07P719Det->execute();
                }else{
                    $commandCap07P719Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_VII_C_MOTIVO_ASO (ID_CAP_VII_C,ID_MOTIVO_ASO) values (:ID_CAP_VII_C,:ID_MOTIVO_ASO)');
                    $commandCap07P719Det->bindParam(':ID_CAP_VII_C',$cap7Modc['ID_CAP_VII_C']);
                    $commandCap07P719Det->bindParam(':ID_MOTIVO_ASO',$P719);
                    $commandCap07P719Det->execute();
                }
                
            }
        }



        /* INSERTANDO  CABECERA*/
        $cap7Mode = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_VII_E')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
        
        

        if(!$cap7Mode){
            $seqMode = (new \yii\db\Query())->select('ENA_TM_CAP_VII_E_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();
            $commandCap07 = $connection->createCommand('INSERT INTO ENA_TM_CAP_VII_E 
            (ID_CAP_VII_E,ID_ENCUESTA,TIP_SOLICITO_CREDITO,TIP_OBTUVO_CREDITO,TIP_FUE_BENEFICIARIO,TIP_TIENE_CTA_AHORRO) values 
            (:ID_CAP_VII_E,:ID_ENCUESTA,:TIP_SOLICITO_CREDITO,:TIP_OBTUVO_CREDITO,:TIP_FUE_BENEFICIARIO,:TIP_TIENE_CTA_AHORRO)');
            $commandCap07->bindParam(':ID_CAP_VII_E',$seqMode['contador']);
            $commandCap07->bindParam(':ID_ENCUESTA',$id_encuesta);
            $commandCap07->bindParam(':TIP_SOLICITO_CREDITO',$model->cap7_mode_p724);
            $commandCap07->bindParam(':TIP_OBTUVO_CREDITO',$model->cap7_mode_p725);
            $commandCap07->bindParam(':TIP_FUE_BENEFICIARIO',$model->cap7_mode_p728);
            $commandCap07->bindParam(':TIP_TIENE_CTA_AHORRO',$model->cap7_mode_p730);
            $commandCap07->execute();

            $cap7Mode = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_VII_E')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
        }else{
            $commandCap07 = $connection->createCommand('UPDATE ENA_TM_CAP_VII_E SET TIP_SOLICITO_CREDITO=:TIP_SOLICITO_CREDITO,TIP_OBTUVO_CREDITO=:TIP_OBTUVO_CREDITO,TIP_FUE_BENEFICIARIO=:TIP_FUE_BENEFICIARIO,TIP_TIENE_CTA_AHORRO=:TIP_TIENE_CTA_AHORRO WHERE ID_ENCUESTA=:ID_ENCUESTA');
            $commandCap07->bindParam(':ID_ENCUESTA',$id_encuesta);
            $commandCap07->bindParam(':TIP_SOLICITO_CREDITO',$model->cap7_mode_p724);
            $commandCap07->bindParam(':TIP_OBTUVO_CREDITO',$model->cap7_mode_p725);
            $commandCap07->bindParam(':TIP_FUE_BENEFICIARIO',$model->cap7_mode_p728);
            $commandCap07->bindParam(':TIP_TIENE_CTA_AHORRO',$model->cap7_mode_p730);
            $commandCap07->execute();
        }


        /* LIMPIANDO DETALLE  P726 */
        $commandCap07P726 = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_VII_E_ENT_CRED WHERE ID_CAP_VII_E=:ID_CAP_VII_E');
        $commandCap07P726->bindParam(':ID_CAP_VII_E',$cap7Mode['ID_CAP_VII_E']);
        $commandCap07P726->execute();

        if($model->cap7_mode_p726){
            $cap7_mode_p726 = explode(",",$model->cap7_mode_p726);
            foreach($cap7_mode_p726 as $P726){
                if($P726 == 11){
                    $commandCap07P726Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_VII_E_ENT_CRED (ID_CAP_VII_E,ID_ENTIDAD_CREDITO,TXT_OTRO) values (:ID_CAP_VII_E,:ID_ENTIDAD_CREDITO,:TXT_OTRO)');
                    $commandCap07P726Det->bindParam(':ID_CAP_VII_E',$cap7Mode['ID_CAP_VII_E']);
                    $commandCap07P726Det->bindParam(':ID_ENTIDAD_CREDITO',$P726);
                    $commandCap07P726Det->bindParam(':TXT_OTRO',$model->cap7_mode_p726_11_especifique);
                    $commandCap07P726Det->execute();
                }else{
                    $commandCap07P726Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_VII_E_ENT_CRED (ID_CAP_VII_E,ID_ENTIDAD_CREDITO) values (:ID_CAP_VII_E,:ID_ENTIDAD_CREDITO)');
                    $commandCap07P726Det->bindParam(':ID_CAP_VII_E',$cap7Mode['ID_CAP_VII_E']);
                    $commandCap07P726Det->bindParam(':ID_ENTIDAD_CREDITO',$P726);
                    $commandCap07P726Det->execute();
                }
                
            }
        }


        /* LIMPIANDO DETALLE  P727 */
        $commandCap07P727 = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_VII_E_UTI_PRES WHERE ID_CAP_VII_E=:ID_CAP_VII_E');
        $commandCap07P727->bindParam(':ID_CAP_VII_E',$cap7Mode['ID_CAP_VII_E']);
        $commandCap07P727->execute();

        if($model->cap7_mode_p727){
            $cap7_mode_p727 = explode(",",$model->cap7_mode_p727);
            foreach($cap7_mode_p727 as $P727){
                if($P727 == 9){
                    $commandCap07P727Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_VII_E_UTI_PRES (ID_CAP_VII_E,ID_UTILIDAD_PRESTAMO,TXT_OTRO) values (:ID_CAP_VII_E,:ID_UTILIDAD_PRESTAMO,:TXT_OTRO)');
                    $commandCap07P727Det->bindParam(':ID_CAP_VII_E',$cap7Mode['ID_CAP_VII_E']);
                    $commandCap07P727Det->bindParam(':ID_UTILIDAD_PRESTAMO',$P727);
                    $commandCap07P727Det->bindParam(':TXT_OTRO',$model->cap7_mode_p727_9_especifique);
                    $commandCap07P727Det->execute();
                }else{
                    $commandCap07P727Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_VII_E_UTI_PRES (ID_CAP_VII_E,ID_UTILIDAD_PRESTAMO) values (:ID_CAP_VII_E,:ID_UTILIDAD_PRESTAMO)');
                    $commandCap07P727Det->bindParam(':ID_CAP_VII_E',$cap7Mode['ID_CAP_VII_E']);
                    $commandCap07P727Det->bindParam(':ID_UTILIDAD_PRESTAMO',$P727);
                    $commandCap07P727Det->execute();
                }
                
            }
        }


        /* LIMPIANDO DETALLE  P729 */
        $commandCap07P729 = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_VII_E_PROP_SEG WHERE ID_CAP_VII_E=:ID_CAP_VII_E');
        $commandCap07P729->bindParam(':ID_CAP_VII_E',$cap7Mode['ID_CAP_VII_E']);
        $commandCap07P729->execute();

        if($model->cap7_mode_p729){
            $cap7_mode_p729 = explode(",",$model->cap7_mode_p729);
            foreach($cap7_mode_p729 as $P729){
                if($P729 == 5){
                    $commandCap07P729Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_VII_E_PROP_SEG (ID_CAP_VII_E,ID_ENTIDAD,TXT_OTRO) values (:ID_CAP_VII_E,:ID_ENTIDAD,:TXT_OTRO)');
                    $commandCap07P729Det->bindParam(':ID_CAP_VII_E',$cap7Mode['ID_CAP_VII_E']);
                    $commandCap07P729Det->bindParam(':ID_ENTIDAD',$P729);
                    $commandCap07P729Det->bindParam(':TXT_OTRO',$model->cap7_mode_p729_5_especifique);
                    $commandCap07P729Det->execute();
                }else{
                    $commandCap07P729Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_VII_E_PROP_SEG (ID_CAP_VII_E,ID_ENTIDAD) values (:ID_CAP_VII_E,:ID_ENTIDAD)');
                    $commandCap07P729Det->bindParam(':ID_CAP_VII_E',$cap7Mode['ID_CAP_VII_E']);
                    $commandCap07P729Det->bindParam(':ID_ENTIDAD',$P729);
                    $commandCap07P729Det->execute();
                }
                
            }
        }


        /* LIMPIANDO DETALLE  P731 */
        $commandCap07P731 = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_VII_E_CTA_AHORR WHERE ID_CAP_VII_E=:ID_CAP_VII_E');
        $commandCap07P731->bindParam(':ID_CAP_VII_E',$cap7Mode['ID_CAP_VII_E']);
        $commandCap07P731->execute();

        if($model->cap7_mode_p731){
            $cap7_mode_p731 = explode(",",$model->cap7_mode_p731);
            foreach($cap7_mode_p731 as $P731){
                if($P731 == 6){
                    $commandCap07P731Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_VII_E_CTA_AHORR (ID_CAP_VII_E,ID_ENTIDAD_BANCARIA,TXT_OTRO) values (:ID_CAP_VII_E,:ID_ENTIDAD_BANCARIA,:TXT_OTRO)');
                    $commandCap07P731Det->bindParam(':ID_CAP_VII_E',$cap7Mode['ID_CAP_VII_E']);
                    $commandCap07P731Det->bindParam(':ID_ENTIDAD_BANCARIA',$P731);
                    $commandCap07P731Det->bindParam(':TXT_OTRO',$model->cap7_mode_p731_6_especifique);
                    $commandCap07P731Det->execute();
                }else{
                    $commandCap07P731Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_VII_E_CTA_AHORR (ID_CAP_VII_E,ID_ENTIDAD_BANCARIA) values (:ID_CAP_VII_E,:ID_ENTIDAD_BANCARIA)');
                    $commandCap07P731Det->bindParam(':ID_CAP_VII_E',$cap7Mode['ID_CAP_VII_E']);
                    $commandCap07P731Det->bindParam(':ID_ENTIDAD_BANCARIA',$P731);
                    $commandCap07P731Det->execute();
                }
                
            }
        }




    }

    private function capituloVIII($model,$id_encuesta,$connection){
        /* INSERTANDO  CABECERA*/
        $cap8 = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_VIII')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
        
        

        if(!$cap8){
            $seq = (new \yii\db\Query())->select('ENA_TM_CAP_VIII_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();
            $commandCap08 = $connection->createCommand('INSERT INTO ENA_TM_CAP_VIII 
            (ID_CAP_VIII,ID_ENCUESTA,TXT_OBSERVACION) values 
            (:ID_CAP_VIII,:ID_ENCUESTA,:TXT_OBSERVACION)');

            $commandCap08->bindParam(':ID_CAP_VIII',$seq['contador']);
            $commandCap08->bindParam(':ID_ENCUESTA',$id_encuesta);
            $commandCap08->bindParam(':TXT_OBSERVACION',$model->cap8_observacion);
            $commandCap08->execute();

            $cap8 = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_VIII')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
        }else{
            $commandCap08 = $connection->createCommand('UPDATE ENA_TM_CAP_VIII SET TXT_OBSERVACION=:TXT_OBSERVACION WHERE ID_ENCUESTA=:ID_ENCUESTA');
            $commandCap08->bindParam(':ID_ENCUESTA',$id_encuesta);
            $commandCap08->bindParam(':TXT_OBSERVACION',$model->cap8_observacion);
            $commandCap08->execute();
        }

        /* LIMPIANDO DETALLE  P801 */
        $commandCap08P801 = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_VIII_FUENTE_ABAST WHERE ID_CAP_VIII=:ID_CAP_VIII');
        $commandCap08P801->bindParam(':ID_CAP_VIII',$cap8['ID_CAP_VIII']);
        $commandCap08P801->execute();

        if($model->cap8_p801){
            $cap8_p801 = explode(",",$model->cap8_p801);
            foreach($cap8_p801 as $P801){
                if($P801 == 8){
                    $commandCap08P801Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_VIII_FUENTE_ABAST (ID_CAP_VIII,ID_FUENTE_ABAST,TXT_OTRO) values (:ID_CAP_VIII,:ID_FUENTE_ABAST,:TXT_OTRO)');
                    $commandCap08P801Det->bindParam(':ID_CAP_VIII',$cap8['ID_CAP_VIII']);
                    $commandCap08P801Det->bindParam(':ID_FUENTE_ABAST',$P801);
                    $commandCap08P801Det->bindParam(':TXT_OTRO',$model->cap8_p801_8_especifique);
                    $commandCap08P801Det->execute();
                }else{
                    $commandCap08P801Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_VIII_FUENTE_ABAST (ID_CAP_VIII,ID_FUENTE_ABAST) values (:ID_CAP_VIII,:ID_FUENTE_ABAST)');
                    $commandCap08P801Det->bindParam(':ID_CAP_VIII',$cap8['ID_CAP_VIII']);
                    $commandCap08P801Det->bindParam(':ID_FUENTE_ABAST',$P801);
                    $commandCap08P801Det->execute();
                }

                
            }
        }


        /* LIMPIANDO DETALLE  P802 */
        $commandCap08P802 = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_VIII_CLASE_AGUA WHERE ID_CAP_VIII=:ID_CAP_VIII');
        $commandCap08P802->bindParam(':ID_CAP_VIII',$cap8['ID_CAP_VIII']);
        $commandCap08P802->execute();

        if($model->cap8_p802){
            $cap8_p802 = explode(",",$model->cap8_p802);
            foreach($cap8_p802 as $P802){
                if($P802 == 6){
                    $commandCap08P802Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_VIII_CLASE_AGUA (ID_CAP_VIII,ID_CLASE_AGUA,TXT_OTRO) values (:ID_CAP_VIII,:ID_CLASE_AGUA,:TXT_OTRO)');
                    $commandCap08P802Det->bindParam(':ID_CAP_VIII',$cap8['ID_CAP_VIII']);
                    $commandCap08P802Det->bindParam(':ID_CLASE_AGUA',$P802);
                    $commandCap08P802Det->bindParam(':TXT_OTRO',$model->cap8_p802_6_especifique);
                    $commandCap08P802Det->execute();
                }else{
                    $commandCap08P802Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_VIII_CLASE_AGUA (ID_CAP_VIII,ID_CLASE_AGUA) values (:ID_CAP_VIII,:ID_CLASE_AGUA)');
                    $commandCap08P802Det->bindParam(':ID_CAP_VIII',$cap8['ID_CAP_VIII']);
                    $commandCap08P802Det->bindParam(':ID_CLASE_AGUA',$P802);
                    $commandCap08P802Det->execute();
                }
                
            }
        }


        /* LIMPIANDO DETALLE  P803 */
        $commandCap08P803 = $connection->createCommand('DELETE FROM ENA_TM_DET_CAP_VIII_ENERGIA WHERE ID_CAP_VIII=:ID_CAP_VIII');
        $commandCap08P803->bindParam(':ID_CAP_VIII',$cap8['ID_CAP_VIII']);
        $commandCap08P803->execute();

        if($model->cap8_p803){
            $cap8_p803 = explode(",",$model->cap8_p803);
            foreach($cap8_p803 as $P803){
                if($P803 == 9){
                    $commandCap08P803Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_VIII_ENERGIA (ID_CAP_VIII,ID_ENERGIA,TXT_OTRO) values (:ID_CAP_VIII,:ID_ENERGIA,:TXT_OTRO)');
                    $commandCap08P803Det->bindParam(':ID_CAP_VIII',$cap8['ID_CAP_VIII']);
                    $commandCap08P803Det->bindParam(':ID_ENERGIA',$P803);
                    $commandCap08P803Det->bindParam(':TXT_OTRO',$model->cap8_p803_9_especifique);
                    $commandCap08P803Det->execute();
                }else{
                    $commandCap08P803Det = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_VIII_ENERGIA (ID_CAP_VIII,ID_ENERGIA) values (:ID_CAP_VIII,:ID_ENERGIA)');
                    $commandCap08P803Det->bindParam(':ID_CAP_VIII',$cap8['ID_CAP_VIII']);
                    $commandCap08P803Det->bindParam(':ID_ENERGIA',$P803);
                    $commandCap08P803Det->execute();
                }
                
            }
        }





    }

    private function capituloIX($model,$id_encuesta,$connection){
        /* INSERTANDO  CABECERA*/
        $cap9 = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_IX')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
        
        if(!$cap9){
            $seq = (new \yii\db\Query())->select('ENA_TM_CAP_IX_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();
            $commandCap09 = $connection->createCommand('INSERT INTO ENA_TM_CAP_IX 
            (ID_CAP_IX,ID_ENCUESTA,TIP_USO_MAQUINARIA) values 
            (:ID_CAP_IX,:ID_ENCUESTA,:TIP_USO_MAQUINARIA)');

            $commandCap09->bindParam(':ID_CAP_IX',$seq['contador']);
            $commandCap09->bindParam(':ID_ENCUESTA',$id_encuesta);
            $commandCap09->bindParam(':TIP_USO_MAQUINARIA',$model->cap9_p901);
            $commandCap09->execute();

            $cap9 = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_IX')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
        }else{
            $commandCap09 = $connection->createCommand('UPDATE ENA_TM_CAP_IX SET TIP_USO_MAQUINARIA=:TIP_USO_MAQUINARIA WHERE ID_ENCUESTA=:ID_ENCUESTA');
            $commandCap09->bindParam(':ID_ENCUESTA',$id_encuesta);
            $commandCap09->bindParam(':TIP_USO_MAQUINARIA',$model->cap9_p901);
            $commandCap09->execute();
        }




    }

    private function capituloXII($model,$id_encuesta,$connection){
        /* INSERTANDO  CABECERA*/
        $cap12 = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_XII')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
        
        $model->cap12_moda_p1206 = date('d/m/Y',strtotime($model->cap12_moda_p1206));


        if(!$cap12){
            $seq = (new \yii\db\Query())->select('ENA_TM_CAP_XII_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();
            $commandCap12 = $connection->createCommand('INSERT INTO ENA_TM_CAP_XII 
            (ID_CAP_XII,ID_ENCUESTA,TXT_FECHA_FINAL,FLG_RESULTADO_FINAL,TXT_OTRO_RESULTADO_FINAL,TXT_OBSERVACION) values 
            (:ID_CAP_XII,:ID_ENCUESTA,:TXT_FECHA_FINAL,:FLG_RESULTADO_FINAL,:TXT_OTRO_RESULTADO_FINAL,:TXT_OBSERVACION)');

            $commandCap12->bindParam(':ID_CAP_XII',$seq['contador']);
            $commandCap12->bindParam(':ID_ENCUESTA',$id_encuesta);
            $commandCap12->bindParam(':TXT_FECHA_FINAL',$model->cap12_moda_p1206);
            $commandCap12->bindParam(':FLG_RESULTADO_FINAL',$model->cap12_moda_p1207);
            $commandCap12->bindParam(':TXT_OTRO_RESULTADO_FINAL',$model->cap12_moda_p1207_6_especifique);
            $commandCap12->bindParam(':TXT_OBSERVACION',$model->cap12_observacion);
            $commandCap12->execute();

            $cap12 = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_XII')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
        }else{
            $commandCap12 = $connection->createCommand('UPDATE ENA_TM_CAP_XII SET TXT_FECHA_FINAL=:TXT_FECHA_FINAL,FLG_RESULTADO_FINAL=:FLG_RESULTADO_FINAL,TXT_OTRO_RESULTADO_FINAL=:TXT_OTRO_RESULTADO_FINAL,TXT_OBSERVACION=:TXT_OBSERVACION WHERE ID_ENCUESTA=:ID_ENCUESTA');
            $commandCap12->bindParam(':ID_ENCUESTA',$id_encuesta);
            $commandCap12->bindParam(':TXT_FECHA_FINAL',$model->cap12_moda_p1206);
            $commandCap12->bindParam(':FLG_RESULTADO_FINAL',$model->cap12_moda_p1207);
            $commandCap12->bindParam(':TXT_OTRO_RESULTADO_FINAL',$model->cap12_moda_p1207_6_especifique);
            $commandCap12->bindParam(':TXT_OBSERVACION',$model->cap12_observacion);
            $commandCap12->execute();
        }




    }
}


