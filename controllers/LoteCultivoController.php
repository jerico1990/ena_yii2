<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Parcela;
use app\models\CultivoForm;


class LoteCultivoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        echo 'no permitido';
        return;
    }


    public function actionCreate($p03_id_capitulo_iii_lote=null){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = new CultivoForm();
        $model->titulo = 'Agregar cultivo en lote';
        $cultivos = (new \yii\db\Query())
                ->select('ENA_TG_CULTIVO.ID_CULTIVO,ENA_TG_CULTIVO.TXT_CULTIVO')
                ->from('ENA_TG_CULTIVO')
                ->orderBy('ENA_TG_CULTIVO.TXT_CULTIVO ASC')
                ->all();

        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $connection = \Yii::$app->db;
                $transaction = $connection->beginTransaction();
                try {
                    
                    // if($model->p03_cultivos){
                    //     $p03_cultivos = explode(",",$model->p03_cultivos);
                    //     foreach($p03_cultivos as $cultivo){
                    //         $commandP03ModuloACultivo = $connection->createCommand('INSERT INTO ENA_TM_DET_LOTE_CULTIVO (ID_CAP_III_LOTE,ID_CULTIVO) values (:ID_CAP_III_LOTE,:ID_CULTIVO)');
                    //         $commandP03ModuloACultivo->bindParam(':ID_CAP_III_LOTE',$seq['contador']);
                    //         $commandP03ModuloACultivo->bindParam(':ID_CULTIVO',$cultivo);
                    //         $commandP03ModuloACultivo->execute();
                    //     }
                    // }
                    
                    $commandP03ModuloACultivo = $connection->createCommand('INSERT INTO ENA_TM_DET_LOTE_CULTIVO (ID_CAP_III_LOTE,ID_CULTIVO,TXT_OTRO) values (:ID_CAP_III_LOTE,:ID_CULTIVO,:TXT_OTRO)');
                    $commandP03ModuloACultivo->bindParam(':ID_CAP_III_LOTE',$p03_id_capitulo_iii_lote);
                    $commandP03ModuloACultivo->bindParam(':ID_CULTIVO',$model->id_cultivo);
                    $commandP03ModuloACultivo->bindParam(':TXT_OTRO',$model->cultivo_9999_especifique);
                    $commandP03ModuloACultivo->execute();
                    
                    $transaction->commit();
                    return ['success'=>true];
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    throw $e;
                }
                
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'cultivos'=>$cultivos
                ]);
            }
        }
    }

    public function actionEliminarCultivo(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $p03_id_capitulo_iii_lote = $_POST['p03_id_capitulo_iii_lote'];
            $id_cultivo = $_POST['id_cultivo'];

            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {

                $commandP03ModuloACultivo = $connection->createCommand('DELETE FROM ENA_TM_DET_LOTE_CULTIVO where ID_CAP_III_LOTE=:ID_CAP_III_LOTE AND ID_CULTIVO=:ID_CULTIVO');
                $commandP03ModuloACultivo->bindParam(':ID_CAP_III_LOTE',$p03_id_capitulo_iii_lote);
                $commandP03ModuloACultivo->bindParam(':ID_CULTIVO',$id_cultivo);
                $commandP03ModuloACultivo->execute();
                
                $transaction->commit();
                return ['success'=>true];
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }

            return ['success'=>false];
        }
        return ['success'=>false];
    }

    public function actionGetListaLoteCultivos(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST && isset($_POST['lote_id'])){
            $lote_id = $_POST['lote_id'];
            $cultivos = (new \yii\db\Query())
                ->select('ENA_TG_CULTIVO.ID_CULTIVO,ENA_TG_CULTIVO.TXT_CULTIVO,ENA_TM_DET_LOTE_CULTIVO.TXT_OTRO')
                ->from('ENA_TM_DET_LOTE_CULTIVO')
                ->innerJoin('ENA_TG_CULTIVO','ENA_TG_CULTIVO.ID_CULTIVO = ENA_TM_DET_LOTE_CULTIVO.ID_CULTIVO')
                ->where('ID_CAP_III_LOTE=:ID_CAP_III_LOTE',[':ID_CAP_III_LOTE'=>$lote_id])
                ->orderBy('ENA_TG_CULTIVO.TXT_CULTIVO ASC')
                ->all();

                
            return ['success'=>true,'cultivos'=>$cultivos];
        }
        return ['success'=>false];
    }

    

}
