<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Parcela;


class ParcelaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        $this->layout='privado';
        return $this->render('index');
    }


    public function actionGetListaParcelas(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $parcelas = (new \yii\db\Query())
                ->select('ENA_TM_MARCOLISTA_LA_LIBERTAD.NRO_FDO,ENA_TM_MARCOLISTA_LA_LIBERTAD.GEOCODIGO_FDO,ENA_TM_MARCOLISTA_LA_LIBERTAD.RUC,ENA_TM_ENCUESTA.FLG_ESTADO,ENA_TM_ENCUESTA.ID_ENCUESTA')
                ->from('ENA_TM_MARCOLISTA_LA_LIBERTAD')
                ->leftJoin('ENA_TM_ENCUESTA','ENA_TM_ENCUESTA.COD_GEOCODIGO_FUNDO=ENA_TM_MARCOLISTA_LA_LIBERTAD.GEOCODIGO_FDO')
                ->where('RUC=:RUC',[':RUC'=>Yii::$app->user->identity->username])
                ->all();
            return ['success'=>true,'parcelas'=>$parcelas];
        }
    }

    public function actionGis($geocodigo_fdo=null){
        $this->layout='privado';

        return $this->render('gis',['geocodigo_fdo'=>$geocodigo_fdo]);
    }

    public function actionCreate(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $ruc = Yii::$app->user->identity->username;

            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {
                $commandCorrelativoMarcoLista = (new \yii\db\Query())->select(['MAX(OBJECTID) OBJECTID'])->from('ENA_TM_MARCOLISTA_LA_LIBERTAD')->one();
                $commandCorrelativoParcela = (new \yii\db\Query())->select(['
                    MAX(NRO_FDO) NRO_FDO, 
                    MAX(GEOCODIGO_FDO) GEOCODIGO_FDO,
                    MAX(IDDIST) IDDIST,
                    MAX(RUC) RUC,
                    MAX(COD_IDENT) COD_IDENT,
                    MAX(GEOCODIGO) GEOCODIGO
                '])->from('ENA_TM_MARCOLISTA_LA_LIBERTAD')->where('RUC=:RUC',[':RUC'=>$ruc])->one();

                $commandCorrelativoCabecera = (new \yii\db\Query())->select(['MIN(COD_GEOCODIGO_FDO) COD_GEOCODIGO_FDO,MIN(COD_GEOCODIGO) COD_GEOCODIGO'])->from('ENA_TR_ML_CABECERA')->where('COD_GEOCODIGO=:COD_GEOCODIGO',[':COD_GEOCODIGO'=>$commandCorrelativoParcela['GEOCODIGO']])->one();

                $correlativoNuevoMarcoLista = $commandCorrelativoMarcoLista['OBJECTID'] + 1;
                $correlativoNuevoFDO = str_pad(($commandCorrelativoParcela['NRO_FDO'] + 1),2,"0", STR_PAD_LEFT);
                $correlativoNuevoGeoCodigo = $commandCorrelativoParcela['IDDIST'] . $commandCorrelativoParcela['RUC'] . $commandCorrelativoParcela['COD_IDENT'] . $correlativoNuevoFDO;
                //var_dump($correlativoNuevoGeoCodigo);die;

                $commandParcela = $connection->createCommand('
                INSERT INTO ENA_TM_MARCOLISTA_LA_LIBERTAD (OBJECTID,IDDPTO,IDPROV,IDDIST,RUC,COD_IDENT,NRO_FDO,RAZON_SOC,AREA_HA_Z17,GEOCODIGO,GEOCODIGO_FDO,NRO_EMP,COD_CUEST,OBSERVACION,AREA_HA_Z18,FECHA_IMG,FECHA_ACT,ESTRATO,REPLICA,ID_SE,TIPO_EXPLO,C_JURIDICA,TIPO_MARCO,NRO_DIVISION,USO_MOD,IMAGEN,SEGUIMIENTO,PISO_ECO,REG_NAT)
                SELECT 
                    :OBJECTID,IDDPTO,IDPROV,IDDIST,RUC,COD_IDENT,:NRO_FDO,RAZON_SOC,AREA_HA_Z17,GEOCODIGO,:GEOCODIGO_FDO,NRO_EMP,COD_CUEST,OBSERVACION,AREA_HA_Z18,FECHA_IMG,FECHA_ACT,ESTRATO,REPLICA,ID_SE,TIPO_EXPLO,C_JURIDICA,TIPO_MARCO,NRO_DIVISION,USO_MOD,IMAGEN,SEGUIMIENTO,PISO_ECO,REG_NAT 
                FROM ENA_TM_MARCOLISTA_LA_LIBERTAD WHERE RUC=:RUC AND NRO_FDO=:NRO_FDO_D');
                $commandParcela->bindParam(':RUC',$ruc);
                $commandParcela->bindParam(':OBJECTID',$correlativoNuevoMarcoLista);
                $commandParcela->bindParam(':NRO_FDO',$correlativoNuevoFDO);
                $commandParcela->bindParam(':NRO_FDO_D',$commandCorrelativoParcela['NRO_FDO']);
                $commandParcela->bindParam(':GEOCODIGO_FDO',$correlativoNuevoGeoCodigo);
                $commandParcela->execute();


                $commandCabecera = $connection->createCommand('
                INSERT INTO ENA_TR_ML_CABECERA (COD_GEOCODIGO,COD_GEOCODIGO_FDO,COD_UBIGEO,TXT_DEPARTAMENTO,TXT_PROVINCIA,TXT_DISTRITO,COD_REGION_NATURAL,TXT_REGION_NATURAL,COD_PISO_ECOLOGICO,TXT_PISO_ECOLOGICO,COD_SUBESTRATO,COD_CC,TXT_COMUNIDAD_CAMPESINA,COD_CN,TXT_COMUNIDAD_NATIVA,COD_SEGMENTO_EMPRESA)
                SELECT 
                    COD_GEOCODIGO,:COD_GEOCODIGO_FDO_NUEVO,COD_UBIGEO,TXT_DEPARTAMENTO,TXT_PROVINCIA,TXT_DISTRITO,COD_REGION_NATURAL,TXT_REGION_NATURAL,COD_PISO_ECOLOGICO,TXT_PISO_ECOLOGICO,COD_SUBESTRATO,COD_CC,TXT_COMUNIDAD_CAMPESINA,COD_CN,TXT_COMUNIDAD_NATIVA,COD_SEGMENTO_EMPRESA
                FROM ENA_TR_ML_CABECERA WHERE COD_GEOCODIGO_FDO=:COD_GEOCODIGO_FDO AND COD_GEOCODIGO=:COD_GEOCODIGO');
                $commandCabecera->bindParam(':COD_GEOCODIGO',$commandCorrelativoCabecera['COD_GEOCODIGO']);
                $commandCabecera->bindParam(':COD_GEOCODIGO_FDO',$commandCorrelativoCabecera['COD_GEOCODIGO_FDO']);
                $commandCabecera->bindParam(':COD_GEOCODIGO_FDO_NUEVO',$correlativoNuevoGeoCodigo);
                $commandCabecera->execute();

                
                $transaction->commit();
                return ['success'=>true];
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }

            return ['success'=>true];
        }
        return ['success'=>true];
    }

}
