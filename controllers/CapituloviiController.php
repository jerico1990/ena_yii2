<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\CapituloVIIModuloCAsociaciones;


class CapituloviiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        echo 'no permitido';
        return;
    }
    
    public function actionCreateCapituloViiModuloCAsociaciones($id_encuesta = null,$geocodigo_fdo=null){
        $this->layout='privado';
        $cap_vii_mod_c = (new \yii\db\Query())->select('ID_CAP_VII_C')->from('ENA_TM_CAP_VII_C')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
        // if($cap_iv_mod_a['ID_CAP_IV_A']==null){
            
        // }

        $request = Yii::$app->request;
        $model = new CapituloVIIModuloCAsociaciones;
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $connection = \Yii::$app->db;
                $transaction = $connection->beginTransaction();
                try {
                    $seq = (new \yii\db\Query())->select('ENA_TM_CAP_VII_C_ASO_COOP_COM_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();
                    $commandCapViiModC = $connection->createCommand('INSERT INTO ENA_TM_CAP_VII_C_ASO_COOP_COM (
                        ID_ASO_COOP_COM,
                        ID_CAP_VII_C,
                        TXT_NOMBRE,
                        COD_TIPO,
                        TXT_ANIO
                        ) values (
                        :ID_ASO_COOP_COM,
                        :ID_CAP_VII_C,
                        :TXT_NOMBRE,
                        :COD_TIPO,
                        :TXT_ANIO
                        )');
                    $commandCapViiModC->bindParam(':ID_ASO_COOP_COM',$seq['contador']);
                    $commandCapViiModC->bindParam(':ID_CAP_VII_C',$cap_vii_mod_c['ID_CAP_VII_C']);
                    $commandCapViiModC->bindParam(':TXT_NOMBRE',$model->cap7_modc_p711);
                    $commandCapViiModC->bindParam(':COD_TIPO',$model->cap7_modc_p712);
                    $commandCapViiModC->bindParam(':TXT_ANIO',$model->cap7_modc_p713);
                    $commandCapViiModC->execute();

                    $transaction->commit();
                    return ['success'=>true,'id_cap_vii_c_asociacion'=>$seq['contador']];
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    throw $e;
                }
            }
        }
        return $this->render('create-capitulo-vii-modulo-c-asociaciones',['geocodigo_fdo'=>$geocodigo_fdo]);
    }


    public function actionUpdateCapituloViiModuloCAsociaciones($id_cap_vii_c_asociacion = null,$geocodigo_fdo=null){
        $this->layout='privado';
        $cap_vii_mod_c_asociaciones = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_VII_C_ASO_COOP_COM')->where('ID_ASO_COOP_COM=:ID_ASO_COOP_COM',[':ID_ASO_COOP_COM'=>$id_cap_vii_c_asociacion])->one();

        // $cap_iv_mod_a = (new \yii\db\Query())->select('ID_CAP_IV_A')->from('ENA_TM_CAP_IV_A')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
        // if($cap_iv_mod_a['ID_CAP_IV_A']==null){
            
        // }

        $request = Yii::$app->request;
        $model = new CapituloVIIModuloCAsociaciones;
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $connection = \Yii::$app->db;
                $transaction = $connection->beginTransaction();
                try {
                    
                    $commandCapViiModC = $connection->createCommand('UPDATE ENA_TM_CAP_VII_C_ASO_COOP_COM SET
                        TXT_NOMBRE=:TXT_NOMBRE,
                        COD_TIPO=:COD_TIPO,
                        TXT_ANIO=:TXT_ANIO
                        WHERE ID_ASO_COOP_COM=:ID_ASO_COOP_COM');
                    $commandCapViiModC->bindParam(':ID_ASO_COOP_COM',$id_cap_vii_c_asociacion);
                    $commandCapViiModC->bindParam(':TXT_NOMBRE',$model->cap7_modc_p711);
                    $commandCapViiModC->bindParam(':COD_TIPO',$model->cap7_modc_p712);
                    $commandCapViiModC->bindParam(':TXT_ANIO',$model->cap7_modc_p713);
                    $commandCapViiModC->execute();

                    $transaction->commit();
                    return ['success'=>true];
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    throw $e;
                }
            }
        }
        return $this->render('update-capitulo-vii-modulo-c-asociaciones',['cap_vii_mod_c_asociaciones'=>$cap_vii_mod_c_asociaciones,'geocodigo_fdo'=>$geocodigo_fdo]);
    }

    public function actionViewCapituloViiModuloCAsociaciones($id_cap_vii_c_asociacion = null){
        $this->layout='privado';
        $cap_vii_mod_c_asociaciones = (new \yii\db\Query())->select('*')->from('ENA_TM_CAP_VII_C_ASO_COOP_COM')->where('ID_ASO_COOP_COM=:ID_ASO_COOP_COM',[':ID_ASO_COOP_COM'=>$id_cap_vii_c_asociacion])->one();

        return $this->render('view-capitulo-vii-modulo-c-asociaciones',['cap_vii_mod_c_asociaciones'=>$cap_vii_mod_c_asociaciones]);
    }

    
    public function actionEliminarModuloC(){
        $this->layout='privado';
        if($_POST){
            $id_cap_vii_c_asociacion = $_POST['id_cap_vii_c_asociacion'];

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {
                $commandCapViimodc = $connection->createCommand('DELETE FROM ENA_TM_CAP_VII_C_ASO_COOP_COM WHERE ID_ASO_COOP_COM=:ID_ASO_COOP_COM');
                $commandCapViimodc->bindParam(':ID_ASO_COOP_COM',$id_cap_vii_c_asociacion);
                $commandCapViimodc->execute();
                 
                $transaction->commit();
                return ['success'=>true];
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
        }
    }


}
