<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class LoginController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if ($action->id == 'error'){
            $this->layout = 'login';
        }
        return parent::beforeAction($action);
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->layout='login';
        $model = new LoginForm();

        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['panel/index']);
        }
        //&& $model->login()
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $encriptado = Yii::$app->getSecurity()->encryptByPassword($model->username."|".$model->password, 'ENA2021');

            return $this->redirect(['validacion/index','k'=>$encriptado,'llave'=>$model->username]);
        }

        return $this->render('index', [
            'model' => $model,
        ]);
    }


    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }

}
