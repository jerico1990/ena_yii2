<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Parcela;
use app\models\CapituloIIIModuloA;
use app\models\CapituloIIIModuloB;
use app\models\CultivoForm;

class LoteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        echo 'no ingresar';
        return;
    }

    public function actionCreateCapituloIiiModuloA($id_encuesta = null,$geocodigo_fdo=null){
        $this->layout='privado';
        $cap_iii = (new \yii\db\Query())->select('ID_CAP_III')->from('ENA_TM_CAP_III')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
        if(!$cap_iii){
            echo 'Es necesario guardar primero';
            return;
        }

        $request = Yii::$app->request;
        $model = new CapituloIIIModuloA;
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $connection = \Yii::$app->db;
                $transaction = $connection->beginTransaction();
                try {
                    $id_cap_iii_lote = 0;
                    $numeracionLote = (new \yii\db\Query())->select('MAX(NUM_LOTE) numeracion')->from('ENA_TM_CAP_III_LOTE')->where('ID_CAP_III=:ID_CAP_III',[':ID_CAP_III'=>$cap_iii['ID_CAP_III']])->one();
                    if(!$numeracionLote){
                        $seq = (new \yii\db\Query())->select('ENA_TM_CAP_III_LOTE_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();
                        $commandP03ModuloA = $connection->createCommand('INSERT INTO ENA_TM_CAP_III_LOTE (ID_CAP_III_LOTE,NUM_LOTE,ID_USO_COBERT_TIERRA,NUM_CANT_LOTE_PARCELA,COD_UM_LOTE_PARCELA,NUM_EQ_LOTE_PARCELA,NUM_CANTIDAD_SM,ID_CAP_III,COD_UM_SM,NUM_EQ_SM) values (:ID_CAP_III_LOTE,1,:ID_USO_COBERT_TIERRA,:NUM_CANT_LOTE_PARCELA,:COD_UM_LOTE_PARCELA,:NUM_EQ_LOTE_PARCELA,0,:ID_CAP_III,99999,0)');
                        $commandP03ModuloA->bindParam(':ID_CAP_III_LOTE',$seq['contador']);
                        $commandP03ModuloA->bindParam(':ID_USO_COBERT_TIERRA',$model->p03_detalle_cobertura);
                        $commandP03ModuloA->bindParam(':NUM_CANT_LOTE_PARCELA',$model->p03_cantidad);
                        $commandP03ModuloA->bindParam(':COD_UM_LOTE_PARCELA',$model->p03_unidad_medida);
                        $commandP03ModuloA->bindParam(':NUM_EQ_LOTE_PARCELA',$model->p03_equivalencia);
                        $commandP03ModuloA->bindParam(':ID_CAP_III',$cap_iii['ID_CAP_III']);
                        
                        $commandP03ModuloA->execute();

                        // $commandP03ModuloAIntermediario = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_III_LOTE (ID_CAP_III,ID_CAP_III_LOTE) values (:ID_CAP_III,:ID_CAP_III_LOTE)');
                        // $commandP03ModuloAIntermediario->bindParam(':ID_CAP_III',$model->p03_id_capitulo_iii);
                        // $commandP03ModuloAIntermediario->bindParam(':ID_CAP_III_LOTE',$seq['contador']);
                        // $commandP03ModuloAIntermediario->execute();
                        $id_cap_iii_lote = $seq['contador'];
                        
                    }else{
                        $numeracionLoteSiguiente = ($numeracionLote['numeracion'] + 1);
                        
                        $seq = (new \yii\db\Query())->select('ENA_TM_CAP_III_LOTE_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();
                        $commandP03ModuloA = $connection->createCommand('INSERT INTO ENA_TM_CAP_III_LOTE (ID_CAP_III_LOTE,NUM_LOTE,ID_USO_COBERT_TIERRA,NUM_CANT_LOTE_PARCELA,COD_UM_LOTE_PARCELA,NUM_EQ_LOTE_PARCELA,NUM_CANTIDAD_SM,ID_CAP_III,COD_UM_SM,NUM_EQ_SM) values (:ID_CAP_III_LOTE,:NUM_LOTE,:ID_USO_COBERT_TIERRA,:NUM_CANT_LOTE_PARCELA,:COD_UM_LOTE_PARCELA,:NUM_EQ_LOTE_PARCELA,0,:ID_CAP_III,99999,0)');
                        $commandP03ModuloA->bindParam(':ID_CAP_III_LOTE',$seq['contador']);
                        $commandP03ModuloA->bindParam(':NUM_LOTE',$numeracionLoteSiguiente);
                        $commandP03ModuloA->bindParam(':ID_USO_COBERT_TIERRA',$model->p03_detalle_cobertura);
                        $commandP03ModuloA->bindParam(':NUM_CANT_LOTE_PARCELA',$model->p03_cantidad);
                        $commandP03ModuloA->bindParam(':COD_UM_LOTE_PARCELA',$model->p03_unidad_medida);
                        $commandP03ModuloA->bindParam(':NUM_EQ_LOTE_PARCELA',$model->p03_equivalencia);
                        $commandP03ModuloA->bindParam(':ID_CAP_III',$cap_iii['ID_CAP_III']);
                        $commandP03ModuloA->execute();
                        $id_cap_iii_lote = $seq['contador'];
                        // $commandP03ModuloAIntermediario = $connection->createCommand('INSERT INTO ENA_TM_DET_CAP_III_LOTE (ID_CAP_III,ID_CAP_III_LOTE) values (:ID_CAP_III,:ID_CAP_III_LOTE)');
                        // $commandP03ModuloAIntermediario->bindParam(':ID_CAP_III',$model->p03_id_capitulo_iii);
                        // $commandP03ModuloAIntermediario->bindParam(':ID_CAP_III_LOTE',$seq['contador']);
                        // $commandP03ModuloAIntermediario->execute();
                    }

                    

                    // if($model->p03_cultivos){
                    //     $p03_cultivos = explode(",",$model->p03_cultivos);
                    //     foreach($p03_cultivos as $cultivo){
                    //         $commandP03ModuloACultivo = $connection->createCommand('INSERT INTO ENA_TM_DET_LOTE_CULTIVO (ID_CAP_III_LOTE,ID_CULTIVO) values (:ID_CAP_III_LOTE,:ID_CULTIVO)');
                    //         $commandP03ModuloACultivo->bindParam(':ID_CAP_III_LOTE',$seq['contador']);
                    //         $commandP03ModuloACultivo->bindParam(':ID_CULTIVO',$cultivo);
                    //         $commandP03ModuloACultivo->execute();
                    //     }
                    // }
                    

                    
                    $transaction->commit();
                    return ['success'=>true,'id_cap_iii_lote'=>$id_cap_iii_lote];
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    throw $e;
                }
            }
        }
        return $this->render('create-capitulo-iii-modulo-a',['cap_iii'=>$cap_iii,'geocodigo_fdo'=>$geocodigo_fdo]);
    }

    public function actionUpdateCapituloIiiModuloA($id_cap_iii_lote = null,$geocodigo_fdo=null){
        $this->layout='privado';
        $cap_iii_lote = (new \yii\db\Query())->select('ID_CAP_III_LOTE')->from('ENA_TM_CAP_III_LOTE')->where('ID_CAP_III_LOTE=:ID_CAP_III_LOTE',[':ID_CAP_III_LOTE'=>$id_cap_iii_lote])->one();
      

        $request = Yii::$app->request;
        $model = new CapituloIIIModuloA;
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $connection = \Yii::$app->db;
                $transaction = $connection->beginTransaction();
                try {
                    

                    $commandP03ModuloA = $connection->createCommand('UPDATE ENA_TM_CAP_III_LOTE SET ID_USO_COBERT_TIERRA=:ID_USO_COBERT_TIERRA,NUM_CANT_LOTE_PARCELA=:NUM_CANT_LOTE_PARCELA,COD_UM_LOTE_PARCELA=:COD_UM_LOTE_PARCELA,NUM_EQ_LOTE_PARCELA=:NUM_EQ_LOTE_PARCELA WHERE ID_CAP_III_LOTE=:ID_CAP_III_LOTE');
                    $commandP03ModuloA->bindParam(':ID_CAP_III_LOTE',$cap_iii_lote['ID_CAP_III_LOTE']);
                    $commandP03ModuloA->bindParam(':ID_USO_COBERT_TIERRA',$model->p03_detalle_cobertura);
                    $commandP03ModuloA->bindParam(':NUM_CANT_LOTE_PARCELA',$model->p03_cantidad);
                    $commandP03ModuloA->bindParam(':COD_UM_LOTE_PARCELA',$model->p03_unidad_medida);
                    $commandP03ModuloA->bindParam(':NUM_EQ_LOTE_PARCELA',$model->p03_equivalencia);
                    $commandP03ModuloA->execute();


                    if($model->p03_cultivos){
                        $commandP03ModuloACultivoDelete = $connection->createCommand('DELETE FROM ENA_TM_DET_LOTE_CULTIVO WHERE ID_CAP_III_LOTE=:ID_CAP_III_LOTE');
                        $commandP03ModuloACultivoDelete->bindParam(':ID_CAP_III_LOTE',$cap_iii_lote['ID_CAP_III_LOTE']);
                        $commandP03ModuloACultivoDelete->execute();

                        $p03_cultivos = explode(",",$model->p03_cultivos);
                        foreach($p03_cultivos as $cultivo){
                            $commandP03ModuloACultivo = $connection->createCommand('INSERT INTO ENA_TM_DET_LOTE_CULTIVO (ID_CAP_III_LOTE,ID_CULTIVO) values (:ID_CAP_III_LOTE,:ID_CULTIVO)');
                            $commandP03ModuloACultivo->bindParam(':ID_CAP_III_LOTE',$cap_iii_lote['ID_CAP_III_LOTE']);
                            $commandP03ModuloACultivo->bindParam(':ID_CULTIVO',$cultivo);
                            $commandP03ModuloACultivo->execute();
                        }
                    }
                    

                    
                    $transaction->commit();
                    return ['success'=>true];
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    throw $e;
                }
            }
        }
        return $this->render('update-capitulo-iii-modulo-a',['cap_iii_lote'=>$cap_iii_lote,'geocodigo_fdo'=>$geocodigo_fdo]);
    }

    public function actionViewCapituloIiiModuloA($id_cap_iii_lote = null){
        $this->layout='privado';
        $cap_iii_lote = (new \yii\db\Query())->select('ID_CAP_III_LOTE')->from('ENA_TM_CAP_III_LOTE')->where('ID_CAP_III_LOTE=:ID_CAP_III_LOTE',[':ID_CAP_III_LOTE'=>$id_cap_iii_lote])->one();
        
        return $this->render('view-capitulo-iii-modulo-a',['cap_iii_lote'=>$cap_iii_lote]);
    }


    public function actionGetLote(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST && isset($_POST['lote_id'])){
            $lote_id = $_POST['lote_id'];
            $lote = (new \yii\db\Query())
                ->select('*')
                ->from('ENA_TM_CAP_III_LOTE')
                ->where('ID_CAP_III_LOTE=:ID_CAP_III_LOTE',[':ID_CAP_III_LOTE'=>$lote_id])
                ->one();

                
            return ['success'=>true,'lote'=>$lote];
        }
        return ['success'=>true];
    }

    public function actionGetListaLoteCapituloiiiModuloA(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST && isset($_POST['id_encuesta'])){
            $id_encuesta = $_POST['id_encuesta'];

            $lotes = (new \yii\db\Query())
                ->select('ENA_TM_CAP_III_LOTE.ID_CAP_III_LOTE,ENA_TM_CAP_III_LOTE.NUM_LOTE,ENA_TM_CAP_III_LOTE.NUM_CANT_LOTE_PARCELA,ENA_TM_CAP_III_LOTE.COD_UM_LOTE_PARCELA,ENA_TM_CAP_III_LOTE.NUM_EQ_LOTE_PARCELA,ENA_TM_CAP_III_LOTE.ID_USO_COBERT_TIERRA')
                ->from('ENA_TM_CAP_III_LOTE')
                ->innerJoin('ENA_TM_CAP_III','ENA_TM_CAP_III.ID_CAP_III = ENA_TM_CAP_III_LOTE.ID_CAP_III')
                ->where('ENA_TM_CAP_III.ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])
                ->orderBy('ENA_TM_CAP_III_LOTE.NUM_LOTE asc')
                ->all();

                
            return ['success'=>true,'lotes'=>$lotes];
        }
        return ['success'=>true];
    }

    public function actionEliminarLote(){
        $this->layout='privado';
        if($_POST){
            $id_lote = $_POST['id_lote'];
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {
                
                $cap_iii_lote = (new \yii\db\Query())->select('ID_CAP_III_LOTE')->from('ENA_TM_CAP_III_LOTE')->where('ID_CAP_III_LOTE=:ID_CAP_III_LOTE',[':ID_CAP_III_LOTE'=>$id_lote])->one();

                

                $commandP03ModuloADelete2 = $connection->createCommand('DELETE FROM ENA_TM_DET_LOTE_CULTIVO WHERE ID_CAP_III_LOTE=:ID_CAP_III_LOTE');
                $commandP03ModuloADelete2->bindParam(':ID_CAP_III_LOTE',$cap_iii_lote['ID_CAP_III_LOTE']);
                $commandP03ModuloADelete2->execute();

                $commandP03ModuloADelete3 = $connection->createCommand('DELETE FROM ENA_TM_CAP_III_LOTE WHERE ID_CAP_III_LOTE=:ID_CAP_III_LOTE');
                $commandP03ModuloADelete3->bindParam(':ID_CAP_III_LOTE',$cap_iii_lote['ID_CAP_III_LOTE']);
                $commandP03ModuloADelete3->execute();
                 
                

                
                $transaction->commit();
                return ['success'=>true];
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
        }

    }

    public function actionCreateCapituloIiiModuloB($id_encuesta = null,$geocodigo_fdo=null){
        $this->layout='privado';
        $cap_iii = (new \yii\db\Query())->select('ID_CAP_III')->from('ENA_TM_CAP_III')->where('ID_ENCUESTA=:ID_ENCUESTA',[':ID_ENCUESTA'=>$id_encuesta])->one();
        if(!$cap_iii){
            echo 'Es necesario guardar primero';
            return;
        }

        $request = Yii::$app->request;
        $model = new CapituloIIIModuloB;
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $connection = \Yii::$app->db;
                $transaction = $connection->beginTransaction();
                try {
                    
                    $model->p03_superficie_total = (($model->p03_superficie_total)?$model->p03_superficie_total:0);
                    $model->p03_equivalencia = (($model->p03_equivalencia)?$model->p03_equivalencia:0);

                    $seq = (new \yii\db\Query())->select('ENA_TM_OTRA_PARCELA_SQ_CONTADOR.NEXTVAL contador')->from('DUAL')->one();
                    $commandP03ModuloB = $connection->createCommand('INSERT INTO ENA_TM_OTRA_PARCELA (ID_OTRA_PARCELA,NUM_PARCELA,NUM_SUP_TOTAL,NUM_EQ_PARCELA,ID_CAP_III,ID_UM_PARCELA) values (:ID_OTRA_PARCELA,:NUM_PARCELA,:NUM_SUP_TOTAL,:NUM_EQ_PARCELA,:ID_CAP_III,:ID_UM_PARCELA)');
                    $commandP03ModuloB->bindParam(':ID_OTRA_PARCELA',$seq['contador']);
                    $commandP03ModuloB->bindParam(':NUM_PARCELA',$model->p03_nro_parcela);
                    $commandP03ModuloB->bindParam(':NUM_SUP_TOTAL',$model->p03_superficie_total);
                    $commandP03ModuloB->bindParam(':NUM_EQ_PARCELA',$model->p03_equivalencia);
                    $commandP03ModuloB->bindParam(':ID_CAP_III',$cap_iii['ID_CAP_III']);
                    $commandP03ModuloB->bindParam(':ID_UM_PARCELA',$model->p03_unidad_medida);
                    $commandP03ModuloB->execute();
                    
    
                    $transaction->commit();
                    return ['success'=>true,'id_otra_parcela'=>$seq['contador']];
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    throw $e;
                }
            }
        }
        return $this->render('create-capitulo-iii-modulo-b',['geocodigo_fdo'=>$geocodigo_fdo]);
    }

    public function actionUpdateCapituloIiiModuloB($id_otra_parcela = null,$geocodigo_fdo=null){
        $this->layout='privado';
        $otra_parcela = (new \yii\db\Query())->select('ID_OTRA_PARCELA')->from('ENA_TM_OTRA_PARCELA')->where('ID_OTRA_PARCELA=:ID_OTRA_PARCELA',[':ID_OTRA_PARCELA'=>$id_otra_parcela])->one();
      

        $request = Yii::$app->request;
        $model = new CapituloIIIModuloB;
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $connection = \Yii::$app->db;
                $transaction = $connection->beginTransaction();
                try {
                    
                    $model->p03_superficie_total = (($model->p03_superficie_total)?$model->p03_superficie_total:0);
                    $model->p03_equivalencia = (($model->p03_equivalencia)?$model->p03_equivalencia:0);

                    $commandP03ModuloB = $connection->createCommand('UPDATE ENA_TM_OTRA_PARCELA SET NUM_PARCELA=:NUM_PARCELA,NUM_SUP_TOTAL=:NUM_SUP_TOTAL,NUM_EQ_PARCELA=:NUM_EQ_PARCELA,ID_UM_PARCELA=:ID_UM_PARCELA WHERE ID_OTRA_PARCELA=:ID_OTRA_PARCELA');
                    $commandP03ModuloB->bindParam(':ID_OTRA_PARCELA',$otra_parcela['ID_OTRA_PARCELA']);
                    $commandP03ModuloB->bindParam(':NUM_PARCELA',$model->p03_nro_parcela);
                    $commandP03ModuloB->bindParam(':NUM_SUP_TOTAL',$model->p03_superficie_total);
                    $commandP03ModuloB->bindParam(':NUM_EQ_PARCELA',$model->p03_equivalencia);
                    $commandP03ModuloB->bindParam(':ID_UM_PARCELA',$model->p03_unidad_medida);
                    $commandP03ModuloB->execute();


                    
                    $transaction->commit();
                    return ['success'=>true];
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    throw $e;
                }
            }
        }
        return $this->render('update-capitulo-iii-modulo-b',['otra_parcela'=>$otra_parcela,'geocodigo_fdo'=>$geocodigo_fdo]);
    }

    public function actionViewCapituloIiiModuloB($id_otra_parcela = null){
        $this->layout='privado';
        $otra_parcela = (new \yii\db\Query())->select('ID_OTRA_PARCELA')->from('ENA_TM_OTRA_PARCELA')->where('ID_OTRA_PARCELA=:ID_OTRA_PARCELA',[':ID_OTRA_PARCELA'=>$id_otra_parcela])->one();
        
        return $this->render('view-capitulo-iii-modulo-b',['otra_parcela'=>$otra_parcela]);
    }

    public function actionGetOtraParcela(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST && isset($_POST['id_otra_parcela'])){
            $id_otra_parcela = $_POST['id_otra_parcela'];
            $otra_parcela = (new \yii\db\Query())
                ->select('*')
                ->from('ENA_TM_OTRA_PARCELA')
                ->where('ID_OTRA_PARCELA=:ID_OTRA_PARCELA',[':ID_OTRA_PARCELA'=>$id_otra_parcela])
                ->one();

            return ['success'=>true,'otra_parcela'=>$otra_parcela];
        }
        return ['success'=>true];
    }

    public function actionEliminarOtraParcela(){
        $this->layout='privado';
        if($_POST){
            $id_otra_parcela = $_POST['id_otra_parcela'];
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {
                

                $commandP03ModuloADelete2 = $connection->createCommand('DELETE FROM ENA_TM_OTRA_PARCELA WHERE ID_OTRA_PARCELA=:ID_OTRA_PARCELA');
                $commandP03ModuloADelete2->bindParam(':ID_OTRA_PARCELA',$id_otra_parcela);
                $commandP03ModuloADelete2->execute();
                
                $transaction->commit();
                return ['success'=>true];
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
        }

    }

    public function actionCreateCapituloIvModuloA($id_encuesta = null){
        $this->layout='privado';
        return $this->render('create-capitulo-iv-modulo-a');
    }

    public function actionCapituloIvModuloB($lote_id = null){
        $this->layout='privado';
        return $this->render('capitulo-iv-modulo-b');
    }

    public function actionCapituloIvModuloC($lote_id = null){
        $this->layout='privado';
        return $this->render('capitulo-iv-modulo-c');
    }

    public function actionGetListaLoteCultivos(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            // $cultivos = (new \yii\db\Query())
            //     ->select('ENA_TG_CULTIVO_ESPECIFICACION.ID_CULTIVO_ESPECIFICACION,ENA_TG_CULTIVO.TXT_CULTIVO,ENA_TG_ESPECIFICACION.TXT_ESPECIFICACION')
            //     ->from('ENA_TG_CULTIVO_ESPECIFICACION')
            //     ->innerJoin('ENA_TG_CULTIVO','ENA_TG_CULTIVO.ID_CULTIVO = ENA_TG_CULTIVO_ESPECIFICACION.ID_CULTIVO')
            //     ->innerJoin('ENA_TG_ESPECIFICACION','ENA_TG_ESPECIFICACION.ID_ESPECIFICACION = ENA_TG_CULTIVO_ESPECIFICACION.ID_ESPECIFICACION')
            //     ->all();

                
            // return ['success'=>true,'cultivos'=>$cultivos];
        }
    }

}
