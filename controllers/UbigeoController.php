<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;


class UbigeoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        $this->layout='privado';
        echo 'retorno';
        return;
    }


    public function actionGetRegiones(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $regiones = (new \yii\db\Query())
                ->select('COD_DEP,TXT_DEPARTAMENTO')
                ->from('ENA_TG_UBIGEO')
                ->groupBy('COD_DEP,TXT_DEPARTAMENTO')
                ->orderBy('TXT_DEPARTAMENTO asc')
                ->all();
            return ['success'=>true,'regiones'=>$regiones];
        }
    }

    public function actionGetProvincias(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $cod_dep = $_POST['cod_dep'];

            $provincias = (new \yii\db\Query())
                ->select('COD_PROV,TXT_PROVINCIA')
                ->from('ENA_TG_UBIGEO')
                ->where('COD_DEP=:COD_DEP',[':COD_DEP'=>$cod_dep])
                ->groupBy('COD_PROV,TXT_PROVINCIA')
                ->orderBy('TXT_PROVINCIA asc')
                ->all();
            return ['success'=>true,'provincias'=>$provincias];
        }
    }

    public function actionGetDistritos(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $cod_prov = $_POST['cod_prov'];

            $distritos = (new \yii\db\Query())
                ->select('COD_UBIGEO,TXT_DISTRITO')
                ->from('ENA_TG_UBIGEO')
                ->where('COD_PROV=:COD_PROV',[':COD_PROV'=>$cod_prov])
                ->groupBy('COD_UBIGEO,TXT_DISTRITO')
                ->orderBy('TXT_DISTRITO asc')
                ->all();
            return ['success'=>true,'distritos'=>$distritos];
        }
    }

}
