<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class CapituloIXMaquinarias extends Model
{
    public $cap9_p902;
    public $cap9_p902_888_889_especifique;

    public $cap9_p903;
    public $cap9_p904;
    public $cap9_p904_5_especifique;

    public $cap9_p905_a;
    public $cap9_p905_b;
    public $cap9_p906;
    public $cap9_p907;
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
           [[
            'cap9_p902',
            'cap9_p902_888_889_especifique',
            'cap9_p903',
            'cap9_p904',
            'cap9_p904_5_especifique',
            'cap9_p905_a',
            'cap9_p905_b',
            'cap9_p906',
            'cap9_p907'],'safe']
        ];
    }

}
