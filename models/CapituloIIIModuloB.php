<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class CapituloIIIModuloB extends Model
{
    public $p03_nro_parcela;
    public $p03_superficie_total;
    public $p03_unidad_medida;
    public $p03_equivalencia;
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
           [['p03_nro_parcela','p03_superficie_total','p03_unidad_medida','p03_equivalencia'],'safe']
        ];
    }

}
