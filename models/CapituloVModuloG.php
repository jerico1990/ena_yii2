<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class CapituloVModuloG extends Model
{
    public $cap5_modg_p510;
    public $cap5_modg_p511;
    public $cap5_modg_p512;
    public $cap5_modg_p513_a;
    public $cap5_modg_p513_b;
    public $cap5_modg_p513_c;
    public $cap5_modg_p513_d;
    public $cap5_modg_p513_e;
    public $cap5_modg_p513_f;
    public $cap5_modg_p514_a;
    public $cap5_modg_p514_b;
    public $cap5_modg_p515_a_1;
    public $cap5_modg_p515_a_2;
    public $cap5_modg_p515_a_3;
    public $cap5_modg_p515_a_4;
    public $cap5_modg_p515_a_5;
    public $cap5_modg_p515_b_1;
    public $cap5_modg_p515_b_2;
    public $cap5_modg_p515_b_3;
    
    public $cap5_modg_p517;
    public $cap5_modg_p518;
    public $cap5_modg_p519;
    public $cap5_modg_p520_a;
    public $cap5_modg_p520_b;
    public $cap5_modg_p520_c;
    public $cap5_modg_p520_d;
    public $cap5_modg_p520_e;
    public $cap5_modg_p520_f;
    public $cap5_modg_p520_g;
    public $cap5_modg_p520_h;
    public $cap5_modg_p520_i;
    public $cap5_modg_p521_a;
    public $cap5_modg_p521_b;
    public $cap5_modg_p521_c;
    public $cap5_modg_p521_d;
    public $cap5_modg_p522_a_1;
    public $cap5_modg_p522_a_2;
    public $cap5_modg_p522_a_3;
    public $cap5_modg_p522_a_4;
    public $cap5_modg_p522_a_5;
    public $cap5_modg_p522_b;
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
           [['cap5_modg_p510',
           'cap5_modg_p511',
           'cap5_modg_p512',
           'cap5_modg_p513_a',
           'cap5_modg_p513_b',
           'cap5_modg_p513_c',
           'cap5_modg_p513_d',
           'cap5_modg_p513_e',
           'cap5_modg_p513_f',
           'cap5_modg_p514_a',
           'cap5_modg_p514_b',
           'cap5_modg_p515_a_1',
           'cap5_modg_p515_a_2',
           'cap5_modg_p515_a_3',
           'cap5_modg_p515_a_4',
           'cap5_modg_p515_a_5',
           'cap5_modg_p515_b_1',
           'cap5_modg_p515_b_2',
           'cap5_modg_p515_b_3',
           'cap5_modg_p517',
           'cap5_modg_p518',
           'cap5_modg_p519',
           'cap5_modg_p520_a',
           'cap5_modg_p520_b',
           'cap5_modg_p520_c',
           'cap5_modg_p520_d',
           'cap5_modg_p520_e',
           'cap5_modg_p520_f',
           'cap5_modg_p520_g',
           'cap5_modg_p520_h',
           'cap5_modg_p520_i',
           'cap5_modg_p521_a',
           'cap5_modg_p521_b',
           'cap5_modg_p521_c',
           'cap5_modg_p521_d',
           'cap5_modg_p522_a_1',
           'cap5_modg_p522_a_2',
           'cap5_modg_p522_a_3',
           'cap5_modg_p522_a_4',
           'cap5_modg_p522_a_5',
           'cap5_modg_p522_b'],'safe']
        ];
    }

}
