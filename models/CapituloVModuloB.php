<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class CapituloVModuloB extends Model
{
    public $cap5_modb_p503;
    public $cap5_modb_p504_a;
    public $cap5_modb_p504_b;
    public $cap5_modb_p504_c;
    public $cap5_modb_p504_d_1;
    public $cap5_modb_p504_d_2;
    public $cap5_modb_p504_e_1;
    public $cap5_modb_p504_e_2;
    public $cap5_modb_p504_e_3;
    public $cap5_modb_p504_f;
    public $cap5_modb_p504_g;
    public $cap5_modb_p504_h;
    public $cap5_modb_p504_i;
    public $cap5_modb_p504_j;
    public $cap5_modb_p505;
    public $cap5_modb_p505_11_especifique;

    public $cap5_modb_p506;
    public $cap5_modb_p507;
    public $cap5_modb_p507_10_especifique;
    public $cap5_modb_p508;
    
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
           [['cap5_modb_p503',
           'cap5_modb_p504_a',
           'cap5_modb_p504_b',
           'cap5_modb_p504_c',
           'cap5_modb_p504_d_1',
           'cap5_modb_p504_d_2',
           'cap5_modb_p504_e_1',
           'cap5_modb_p504_e_2',
           'cap5_modb_p504_e_3',
           'cap5_modb_p504_f',
           'cap5_modb_p504_g',
           'cap5_modb_p504_h',
           'cap5_modb_p504_i',
           'cap5_modb_p504_j',
           'cap5_modb_p505',
           'cap5_modb_p505_11_especifique',
           'cap5_modb_p506',
           'cap5_modb_p507',
           'cap5_modb_p507_10_especifique',
           'cap5_modb_p508'],'safe']
        ];
    }

}
