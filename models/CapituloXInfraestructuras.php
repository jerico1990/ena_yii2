<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class CapituloXInfraestructuras extends Model
{
    public $cap10_p1001;
    public $cap10_p1002;
    public $cap10_p1003;
    public $cap10_p1001_888_especifique;
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
           [[
            'cap10_p1001',
            'cap10_p1002',
            'cap10_p1003',
            'cap10_p1001_888_especifique'],'safe']
        ];
    }

}
