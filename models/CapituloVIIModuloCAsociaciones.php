<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class CapituloVIIModuloCAsociaciones extends Model
{
    public $cap7_modc_p711;
    public $cap7_modc_p712;
    public $cap7_modc_p713;
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
           [['cap7_modc_p711',
           'cap7_modc_p712',
           'cap7_modc_p713',],'safe']
        ];
    }

}
