<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class CapituloIVModuloB extends Model
{
    public $cap4_modb_p428_a;
    public $cap4_modb_p428_b;
    public $cap4_modb_p429;
    public $cap4_modb_p430;
    public $cap4_modb_p431_1;
    public $cap4_modb_p431_2;
    public $cap4_modb_p431_3;
    public $cap4_modb_p431_4;
    public $cap4_modb_p431_5;
    public $cap4_modb_p431_6;
    public $cap4_modb_p431_7;
    public $cap4_modb_p431_8;
    public $cap4_modb_p431_9;
    public $cap4_modb_p431_10;
    public $cap4_modb_p431_11;
    public $cap4_modb_p431_12;
    
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
           [['cap4_modb_p428_a',
           'cap4_modb_p428_b',
           'cap4_modb_p429',
           'cap4_modb_p430',
           'cap4_modb_p431_1',
            'cap4_modb_p431_2',
            'cap4_modb_p431_3',
            'cap4_modb_p431_4',
            'cap4_modb_p431_5',
            'cap4_modb_p431_6',
            'cap4_modb_p431_7',
            'cap4_modb_p431_8',
            'cap4_modb_p431_9',
            'cap4_modb_p431_10',
            'cap4_modb_p431_11',
            'cap4_modb_p431_12'],'safe']
        ];
    }

}
