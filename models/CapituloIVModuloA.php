<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class CapituloIVModuloA extends Model
{
    public $cap4_moda_p401_a;
    public $cap4_moda_p401;
    public $cap4_moda_p402;
    public $cap4_moda_p403_mes;
    public $cap4_moda_p403_anio;
    public $cap4_moda_p404;
    public $cap4_moda_p405_a;
    public $cap4_moda_p405_b;
    public $cap4_moda_p405_c;
    public $cap4_moda_p406;
    public $cap4_moda_p407;
    public $cap4_moda_p407_10_especifique;
    public $cap4_moda_p408;
    public $cap4_moda_p408_9_especifique;
    public $cap4_moda_p409;
    public $cap4_moda_p410_a_mes;
    public $cap4_moda_p410_a_anio;
    public $cap4_moda_p410_b_mes;
    public $cap4_moda_p410_b_anio;
    public $cap4_moda_p411_1;
    public $cap4_moda_p411_2;
    public $cap4_moda_p411_3;
    public $cap4_moda_p411_4;
    public $cap4_moda_p411_5;
    public $cap4_moda_p411_6;
    public $cap4_moda_p411_7;
    public $cap4_moda_p411_8;
    public $cap4_moda_p411_9;
    public $cap4_moda_p411_10;
    public $cap4_moda_p411_11;
    public $cap4_moda_p411_12;
    public $cap4_moda_p412_a;
    public $cap4_moda_p412_b;
    public $cap4_moda_p412_c;
    public $cap4_moda_p413;
    public $cap4_moda_p414;
    public $cap4_moda_p415;
    public $cap4_moda_p416;
    public $cap4_moda_p417;
    public $cap4_moda_p418;
    public $cap4_moda_p419_a;
    public $cap4_moda_p419_b;
    public $cap4_moda_p419_c;
    public $cap4_moda_p419_d;
    public $cap4_moda_p419_e;
    public $cap4_moda_p420;
    public $cap4_moda_p421;
    public $cap4_moda_p422;
    public $cap4_moda_p422_calidad;

    public $cap4_moda_p423_a;
    public $cap4_moda_p423_b;
    public $cap4_moda_p423_c;
    public $cap4_moda_p423_d;
    public $cap4_moda_p424;
    public $cap4_moda_p424_13_especifique;
    public $cap4_moda_p425_a;
    public $cap4_moda_p425_b;
    public $cap4_moda_p425_c;
    public $cap4_moda_p425_d;
    public $cap4_moda_p425_d_moneda;

    public $cap4_moda_p426_a;
    public $cap4_moda_p426_a_6_especifique;
    public $cap4_moda_p426_b;
    public $cap4_moda_p426_b_9_especifique;


    public $cap4_moda_p427;
    public $cap4_moda_p427_10_especifique;

    public $cap4_moda_p402_9999_especifique;
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
           [['cap4_moda_p401_a',
            'cap4_moda_p401',
            'cap4_moda_p402',
            'cap4_moda_p403_mes',
            'cap4_moda_p403_anio',
            'cap4_moda_p404',
            'cap4_moda_p405_a',
            'cap4_moda_p405_b',
            'cap4_moda_p405_c',
            'cap4_moda_p406',
            'cap4_moda_p407',
            'cap4_moda_p407_10_especifique',
            'cap4_moda_p408',
            'cap4_moda_p408_9_especifique',
            'cap4_moda_p409',
            'cap4_moda_p410_a_mes',
            'cap4_moda_p410_a_anio',
            'cap4_moda_p410_b_mes',
            'cap4_moda_p410_b_anio',
            'cap4_moda_p411_1',
            'cap4_moda_p411_2',
            'cap4_moda_p411_3',
            'cap4_moda_p411_4',
            'cap4_moda_p411_5',
            'cap4_moda_p411_6',
            'cap4_moda_p411_7',
            'cap4_moda_p411_8',
            'cap4_moda_p411_9',
            'cap4_moda_p411_10',
            'cap4_moda_p411_11',
            'cap4_moda_p411_12',
            'cap4_moda_p412_a',
            'cap4_moda_p412_b',
            'cap4_moda_p412_c',
            'cap4_moda_p413',
            'cap4_moda_p414',
            'cap4_moda_p415',
            'cap4_moda_p416',
            'cap4_moda_p417',
            'cap4_moda_p418',
            'cap4_moda_p419_a',
            'cap4_moda_p419_b',
            'cap4_moda_p419_c',
            'cap4_moda_p419_d',
            'cap4_moda_p419_e',
            'cap4_moda_p420',
            'cap4_moda_p421',
            'cap4_moda_p422',
            'cap4_moda_p422_calidad',
            'cap4_moda_p423_a',
            'cap4_moda_p423_b',
            'cap4_moda_p423_c',
            'cap4_moda_p423_d',
            'cap4_moda_p424',
            'cap4_moda_p424_13_especifique',
            'cap4_moda_p425_a',
            'cap4_moda_p425_b',
            'cap4_moda_p425_c',
            'cap4_moda_p425_d',
            'cap4_moda_p425_d_moneda',
            'cap4_moda_p426_a',
            'cap4_moda_p426_a_6_especifique',
            'cap4_moda_p426_b',
            'cap4_moda_p426_b_9_especifique',
            'cap4_moda_p427',
            'cap4_moda_p427_10_especifique',
            'cap4_moda_p402_9999_especifique'],'safe']
        ];
    }

}
