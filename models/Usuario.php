<?php

namespace app\models;

use Yii;
use yii\web\IdentityInterface;
/**
 * This is the model class for table "usuario".
 *
 * @property int $id
 * @property string $usuario
 * @property string $clave
 * @property int $estado
 */
class Usuario extends \yii\db\ActiveRecord implements IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public $auth_key;
    public $primaryKey = "IDE_USUARIO";

    public static function tableName()
    {
        return 'ENA_TG_USUARIO';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['FLG_ESTADO'], 'integer'],
            [['TXT_NUMERO_DOCUMENTO', 'TXT_CLAVE'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'TXT_NUMERO_DOCUMENTO' => 'Usuario',
            'TXT_CLAVE' => 'Clave',
            'FLG_ESTADO' => 'Estado',
        ];
    }

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }


    public function getUsername(){

        $datos_generales = (new \yii\db\Query())
                ->select('*')
                ->from('ENA_TG_USUARIO')
                ->where('TXT_NUMERO_DOCUMENTO=:TXT_NUMERO_DOCUMENTO',[':TXT_NUMERO_DOCUMENTO'=>$this->TXT_NUMERO_DOCUMENTO])
                ->one();
        return $datos_generales['TXT_NUMERO_DOCUMENTO'];
    }

    public static function findByUsername($password,$username)
    {
      return static::find()->where('TXT_NUMERO_DOCUMENTO=:TXT_NUMERO_DOCUMENTO and FLG_ESTADO=1 and TIP_PERFIL=0',[':TXT_NUMERO_DOCUMENTO' => $username])->one();
    }

    public static function  findByCode($username,$code)
    {
        $model = (new \yii\db\Query())
                ->select('*')
                ->from('ENA_TG_USUARIO')
                ->where('TXT_NUMERO_DOCUMENTO=:TXT_NUMERO_DOCUMENTO and TXT_CODIGO_VALIDACION=:TXT_CODIGO_VALIDACION and FLG_ESTADO=1 and TIP_PERFIL=0',[':TXT_NUMERO_DOCUMENTO' => $username,':TXT_CODIGO_VALIDACION'=>$code])->one();
        //var_dump($code);die;
        if($model){
            return true;
        }
        return false;
    }

    public function validatePassword($password,$username){

        $model=static::find()->where('TXT_NUMERO_DOCUMENTO=:TXT_NUMERO_DOCUMENTO and FLG_ESTADO=1 and TIP_PERFIL=0',[':TXT_NUMERO_DOCUMENTO' => $username])->one();
        if ($password == $model->TXT_CLAVE) {
            return $model;
        }
        return false;
    }

    public static function findIdentityByAccessToken($token, $type = null){
        if ($user['accessToken'] === $token) {
           return new static($user);
        }
        return null;
    }

    public function getAuthKey(){
        return $this->auth_key;
    }

    public function validateAuthKey($authKey){
        return $this->getAuthKey() === $authKey;
    }
}
