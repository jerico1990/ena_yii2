<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ENA_TM_ENCUESTA".
 *
 * @property int $ID_ENCUESTA
 * @property string|null $COD_GEOCODIGO
 * @property string|null $COD_UBIGEO
 * @property int|null $COD_REGION_NATURAL
 * @property int|null $COD_PISO_ECOLOGICO
 * @property string|null $COD_ESTRATIFICACION
 * @property int|null $COD_TIPO_GRILLA
 * @property string|null $COD_SERPENTIN
 * @property string|null $COD_CC
 * @property string|null $COD_CN
 * @property string|null $COD_SEGMENTO_EMPRESA
 * @property int|null $NUM_PARCELA_SM
 * @property int|null $NUM_TOTAL_PARCELAS_SM
 * @property string|null $COD_CCPP
 * @property string|null $FEC_ENCUESTA
 * @property string|null $FEC_ACTUALIZACION
 * @property string|null $FLG_ESTADO
 * @property string|null $COD_GEOCODIGO_FUNDO
 *
 * @property ENATMCAPI[] $eNATMCAPIs
 * @property ENATMCAPIII[] $eNATMCAPIIIs
 * @property ENATMCAPIVA[] $eNATMCAPIVAs
 * @property ENATMCAPIVD[] $eNATMCAPIVDs
 * @property ENATMCAPIVE[] $eNATMCAPIVEs
 * @property ENATMCAPIX[] $eNATMCAPIXes
 * @property ENATMCAPVA[] $eNATMCAPVAs
 * @property ENATMCAPVB[] $eNATMCAPVBs
 * @property ENATMCAPVGPOSTURA[] $eNATMCAPVGPOSTURAs
 * @property ENATMCAPVH[] $eNATMCAPVHs
 * @property ENATMCAPVI[] $eNATMCAPVIs
 * @property ENATMCAPVI[] $eNATMCAPVIs0
 * @property ENATMCAPVIIA[] $eNATMCAPVIIAs
 * @property ENATMCAPVIIB[] $eNATMCAPVIIBs
 * @property ENATMCAPVIIC[] $eNATMCAPVIICs
 * @property ENATMCAPVIID[] $eNATMCAPVIs1
 * @property ENATMCAPVIIE[] $eNATMCAPVIIEs
 * @property ENATMCAPVIIF[] $eNATMCAPVIIFs
 * @property ENATMCAPVIII[] $eNATMCAPVIIIs
 * @property ENATMCAPVJ[] $eNATMCAPVJs
 * @property ENATMCAPVK[] $eNATMCAPVKs
 * @property ENATMCAPXI[] $eNATMCAPXIs
 * @property ENATMCAPXII[] $eNATMCAPXIIs
 * @property ENATMDETCAPIVBARBFRUT[] $eNATMDETCAPIVBARBFRUTs
 * @property ENATMDETCAPIVCSEMILLEROS[] $eNATMDETCAPIVCSEMILLEROSs
 * @property ENATMDETCAPX[] $eNATMDETCAPXes
 * @property ENATGCENTROPOBLADO $cODCCPP
 */
class EnaTmEncuesta extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ENA_TM_ENCUESTA';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
           // [['ID_ENCUESTA'], 'required'],
            [['ID_ENCUESTA', 'COD_REGION_NATURAL', 'COD_PISO_ECOLOGICO', 'COD_TIPO_GRILLA', 'NUM_PARCELA_SM', 'NUM_TOTAL_PARCELAS_SM'], 'integer'],
            [['COD_GEOCODIGO'], 'string', 'max' => 22],
            [['COD_UBIGEO'], 'string', 'max' => 6],
            [['COD_ESTRATIFICACION'], 'string', 'max' => 4],
            [['COD_SERPENTIN', 'COD_CC', 'COD_CN', 'COD_SEGMENTO_EMPRESA'], 'string', 'max' => 9],
            [['COD_CCPP'], 'string', 'max' => 10],
            [['FEC_ACTUALIZACION'], 'string', 'max' => 7],
            [['FLG_ESTADO'], 'string', 'max' => 1],
            [['COD_GEOCODIGO_FUNDO'], 'string', 'max' => 23],
            [['ID_ENCUESTA'], 'unique'],
            [['FEC_ENCUESTA'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID_ENCUESTA' => 'Id Encuesta',
            'COD_GEOCODIGO' => 'Cod Geocodigo',
            'COD_UBIGEO' => 'Cod Ubigeo',
            'COD_REGION_NATURAL' => 'Cod Region Natural',
            'COD_PISO_ECOLOGICO' => 'Cod Piso Ecologico',
            'COD_ESTRATIFICACION' => 'Cod Estratificacion',
            'COD_TIPO_GRILLA' => 'Cod Tipo Grilla',
            'COD_SERPENTIN' => 'Cod Serpentin',
            'COD_CC' => 'Cod Cc',
            'COD_CN' => 'Cod Cn',
            'COD_SEGMENTO_EMPRESA' => 'Cod Segmento Empresa',
            'NUM_PARCELA_SM' => 'Num Parcela Sm',
            'NUM_TOTAL_PARCELAS_SM' => 'Num Total Parcelas Sm',
            'COD_CCPP' => 'Cod Ccpp',
            'FEC_ENCUESTA' => 'Fec Encuesta',
            'FEC_ACTUALIZACION' => 'Fec Actualizacion',
            'FLG_ESTADO' => 'Flg Estado',
            'COD_GEOCODIGO_FUNDO' => 'Cod Geocodigo Fundo',
        ];
    }

    /**
     * Gets query for [[ENATMCAPIs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getENATMCAPIs()
    {
        return $this->hasMany(ENATMCAPI::className(), ['ID_ENCUESTA' => 'ID_ENCUESTA']);
    }

    /**
     * Gets query for [[ENATMCAPIIIs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getENATMCAPIIIs()
    {
        return $this->hasMany(ENATMCAPIII::className(), ['ID_ENCUESTA' => 'ID_ENCUESTA']);
    }

    /**
     * Gets query for [[ENATMCAPIVAs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getENATMCAPIVAs()
    {
        return $this->hasMany(ENATMCAPIVA::className(), ['ID_ENCUESTA' => 'ID_ENCUESTA']);
    }

    /**
     * Gets query for [[ENATMCAPIVDs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getENATMCAPIVDs()
    {
        return $this->hasMany(ENATMCAPIVD::className(), ['ID_ENCUESTA' => 'ID_ENCUESTA']);
    }

    /**
     * Gets query for [[ENATMCAPIVEs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getENATMCAPIVEs()
    {
        return $this->hasMany(ENATMCAPIVE::className(), ['ID_ENCUESTA' => 'ID_ENCUESTA']);
    }

    /**
     * Gets query for [[ENATMCAPIXes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getENATMCAPIXes()
    {
        return $this->hasMany(ENATMCAPIX::className(), ['ID_ENCUESTA' => 'ID_ENCUESTA']);
    }

    /**
     * Gets query for [[ENATMCAPVAs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getENATMCAPVAs()
    {
        return $this->hasMany(ENATMCAPVA::className(), ['ID_ENCUESTA' => 'ID_ENCUESTA']);
    }

    /**
     * Gets query for [[ENATMCAPVBs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getENATMCAPVBs()
    {
        return $this->hasMany(ENATMCAPVB::className(), ['ID_ENCUESTA' => 'ID_ENCUESTA']);
    }

    /**
     * Gets query for [[ENATMCAPVGPOSTURAs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getENATMCAPVGPOSTURAs()
    {
        return $this->hasMany(ENATMCAPVGPOSTURA::className(), ['ID_ENCUESTA' => 'ID_ENCUESTA']);
    }

    /**
     * Gets query for [[ENATMCAPVHs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getENATMCAPVHs()
    {
        return $this->hasMany(ENATMCAPVH::className(), ['ID_ENCUESTA' => 'ID_ENCUESTA']);
    }

    /**
     * Gets query for [[ENATMCAPVIs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getENATMCAPVIs()
    {
        return $this->hasMany(ENATMCAPVI::className(), ['ID_ENCUESTA' => 'ID_ENCUESTA']);
    }

    /**
     * Gets query for [[ENATMCAPVIs0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getENATMCAPVIs0()
    {
        return $this->hasMany(ENATMCAPVI::className(), ['ID_ENCUESTA' => 'ID_ENCUESTA']);
    }

    /**
     * Gets query for [[ENATMCAPVIIAs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getENATMCAPVIIAs()
    {
        return $this->hasMany(ENATMCAPVIIA::className(), ['ID_ENCUESTA' => 'ID_ENCUESTA']);
    }

    /**
     * Gets query for [[ENATMCAPVIIBs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getENATMCAPVIIBs()
    {
        return $this->hasMany(ENATMCAPVIIB::className(), ['ID_ENCUESTA' => 'ID_ENCUESTA']);
    }

    /**
     * Gets query for [[ENATMCAPVIICs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getENATMCAPVIICs()
    {
        return $this->hasMany(ENATMCAPVIIC::className(), ['ID_ENCUESTA' => 'ID_ENCUESTA']);
    }

    /**
     * Gets query for [[ENATMCAPVIs1]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getENATMCAPVIs1()
    {
        return $this->hasMany(ENATMCAPVIID::className(), ['ID_ENCUESTA' => 'ID_ENCUESTA']);
    }

    /**
     * Gets query for [[ENATMCAPVIIEs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getENATMCAPVIIEs()
    {
        return $this->hasMany(ENATMCAPVIIE::className(), ['ID_ENCUESTA' => 'ID_ENCUESTA']);
    }

    /**
     * Gets query for [[ENATMCAPVIIFs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getENATMCAPVIIFs()
    {
        return $this->hasMany(ENATMCAPVIIF::className(), ['ID_ENCUESTA' => 'ID_ENCUESTA']);
    }

    /**
     * Gets query for [[ENATMCAPVIIIs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getENATMCAPVIIIs()
    {
        return $this->hasMany(ENATMCAPVIII::className(), ['ID_ENCUESTA' => 'ID_ENCUESTA']);
    }

    /**
     * Gets query for [[ENATMCAPVJs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getENATMCAPVJs()
    {
        return $this->hasMany(ENATMCAPVJ::className(), ['ID_ENCUESTA' => 'ID_ENCUESTA']);
    }

    /**
     * Gets query for [[ENATMCAPVKs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getENATMCAPVKs()
    {
        return $this->hasMany(ENATMCAPVK::className(), ['ID_ENCUESTA' => 'ID_ENCUESTA']);
    }

    /**
     * Gets query for [[ENATMCAPXIs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getENATMCAPXIs()
    {
        return $this->hasMany(ENATMCAPXI::className(), ['ID_ENCUESTA' => 'ID_ENCUESTA']);
    }

    /**
     * Gets query for [[ENATMCAPXIIs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getENATMCAPXIIs()
    {
        return $this->hasMany(ENATMCAPXII::className(), ['ID_ENCUESTA' => 'ID_ENCUESTA']);
    }

    /**
     * Gets query for [[ENATMDETCAPIVBARBFRUTs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getENATMDETCAPIVBARBFRUTs()
    {
        return $this->hasMany(ENATMDETCAPIVBARBFRUT::className(), ['ID_ENCUESTA' => 'ID_ENCUESTA']);
    }

    /**
     * Gets query for [[ENATMDETCAPIVCSEMILLEROSs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getENATMDETCAPIVCSEMILLEROSs()
    {
        return $this->hasMany(ENATMDETCAPIVCSEMILLEROS::className(), ['ID_ENCUESTA' => 'ID_ENCUESTA']);
    }

    /**
     * Gets query for [[ENATMDETCAPXes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getENATMDETCAPXes()
    {
        return $this->hasMany(ENATMDETCAPX::className(), ['ID_ENCUESTA' => 'ID_ENCUESTA']);
    }

    /**
     * Gets query for [[CODCCPP]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCODCCPP()
    {
        return $this->hasOne(ENATGCENTROPOBLADO::className(), ['COD_CCPP' => 'COD_CCPP']);
    }
}
