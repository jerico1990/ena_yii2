<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class CultivoForm extends Model
{
    public $titulo;
    public $id_cultivo;
    public $cultivo_9999_especifique;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
           [['id_cultivo','cultivo_9999_especifique'],'safe']
        ];
    }

}
