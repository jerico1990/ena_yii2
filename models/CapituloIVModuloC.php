<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class CapituloIVModuloC extends Model
{
    public $cap4_modc_p432_a;
    public $cap4_modc_p432_b;
    public $cap4_modc_p433_a;
    public $cap4_modc_p433_b;
    public $cap4_modc_p433_c;
    public $cap4_modc_p434;
    public $cap4_modc_p435_a;
    public $cap4_modc_p435_b;
    public $cap4_modc_p435_c;
    public $cap4_modc_p435_d;
    public $cap4_modc_p436_a;
    public $cap4_modc_p436_b;
    
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
           [['cap4_modc_p432_a',
           'cap4_modc_p432_b',
           'cap4_modc_p433_a',
           'cap4_modc_p433_b',
           'cap4_modc_p433_c',
           'cap4_modc_p434',
           'cap4_modc_p435_a',
           'cap4_modc_p435_b',
           'cap4_modc_p435_c',
           'cap4_modc_p435_d',
           'cap4_modc_p436_a',
           'cap4_modc_p436_b'],'safe']
        ];
    }

}
