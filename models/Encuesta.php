<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class Encuesta extends Model
{   
    public $general_numero_parcela;
    public $general_total_parcela;
    public $general_centro_poblado;
    public $general_distrito;

    /* variables de Capitulo I */
    public $p01_1;
    public $p01_2;
    public $p01_3;
    public $p01_4;
    public $p01_5;
    public $p01_6;
    public $p01_7;
    public $p01_8;
    public $p01_9;
    public $p01_10;
    public $p01_11;
    public $p01_12;
    public $p01_observacion;

    /* variables de Capitulo II */
    public $p02_tipo_productor;
    public $p02_a_toma_decision;
    public $p02_a_representante_nombres;
    public $p02_a_representante_apellidos;
    public $p02_a_txt_dni;
    public $p02_a_representante_tiene_numero;
    public $p02_a_representante_telefono_fijo;
    public $p02_a_representante_telefono_celular;
    public $p02_a_tiene_correo_electronico;
    public $p02_a_correo_electronico;
    public $p02_a_es_productor;

    public $p02_a_informacion_proporcionada;
    public $p02_a_informacion_proporcionada_especifique;
    public $p02_a_informante_nombres;
    public $p02_a_informante_apellidos;
    public $p02_a_informante_tiene_numero;
    public $p02_a_informante_telefono_fijo;
    public $p02_a_informante_telefono_celular;
    public $p02_a_productor_agrario_vive;
    public $p02_a_productor_agrario_vive_especifique;
    public $p02_a_informante_departamento;
    public $p02_a_informante_provincia;
    public $p02_a_informante_distrito;
    public $p02_a_informante_centro_poblado;
    public $p02_a_pertenece_comunidad;

    public $p02_b_razon_social;
    public $p02_b_ruc;
    public $p02_b_representante_legal_nombres;
    public $p02_b_representante_legal_apellidos;
    public $p02_b_representante_departamento;
    public $p02_b_representante_provincia;
    public $p02_b_representante_distrito;
    public $p02_b_representante_centro_poblado;
    public $p02_b_representante_codigo_centro_poblado;
    public $p02_b_tipo_via;
    public $p02_b_tipo_via_especifique;
    public $p02_b_avenida;
    public $p02_b_nro_puerta;
    public $p02_b_block;
    public $p02_b_interior;
    public $p02_b_piso;
    public $p02_b_manzana;
    public $p02_b_lote;
    public $p02_b_informante_nombres;
    public $p02_b_informante_apellidos;
    public $p02_b_cargo;
    public $p02_b_cargo_especifique;
    public $p02_b_informante_tiene_numero;
    public $p02_b_informante_telefono_fijo;
    public $p02_b_informante_telefono_celular;
    public $p02_b_informante_tiene_correo_electronico;
    public $p02_b_informante_correo_electronico;
    public $p02_b_nombre_fundo;
    public $p02_observacion;

    public $p03_observacion;
    public $p03_c_int_trabaja_otra_parcela;
    public $p03_c_int_cantidad_parcelas;
    public $p03_c_int_tenencia;
    public $p03_c_int_tenencia_31_especifique;


    public $cap4_modd_p437;
    public $cap4_modd_p437_8_especifique;
    public $cap4_modd_p438;
    public $cap4_modd_p438_10_especifique;
    public $cap4_modd_p439;
    public $cap4_mode_p440;

    public $cap4_mode_p440_1;
    public $cap4_mode_p441_1;
    public $cap4_mode_p440_2;
    public $cap4_mode_p441_2;
    public $cap4_mode_p440_3;
    public $cap4_mode_p441_3;
    public $cap4_mode_p440_4;
    public $cap4_mode_p441_4;
    public $cap4_mode_p440_5;
    public $cap4_mode_p441_5;
    public $cap4_mode_p440_6;
    public $cap4_mode_p441_6;
    public $cap4_mode_p440_7;
    public $cap4_mode_p441_7;
    public $cap4_mode_p440_8;
    public $cap4_mode_p441_8;
    public $cap4_mode_p440_9;
    public $cap4_mode_p441_9;
    public $cap4_mode_p440_10;
    public $cap4_mode_p441_10;
    public $cap4_mode_p440_11;
    public $cap4_mode_p441_11;
    public $cap4_mode_p440_12;
    public $cap4_mode_p441_12;
    public $cap4_mode_p440_13;
    public $cap4_mode_p441_13;
    public $cap4_mode_p440_14;
    public $cap4_mode_p441_14;
    public $cap4_mode_p440_15;
    public $cap4_mode_p441_15;
    public $cap4_mode_p440_16;
    public $cap4_mode_p441_16;
    public $cap4_mode_p440_17;
    public $cap4_mode_p441_17;
    public $cap4_mode_p440_18;
    public $cap4_mode_p441_18;
    public $cap4_mode_p440_19;
    public $cap4_mode_p441_19;
    public $cap4_mode_p440_20;
    public $cap4_mode_p441_20;
    public $cap4_mode_p440_21;
    public $cap4_mode_p441_21;
    public $cap4_mode_p440_22;
    public $cap4_mode_p441_22;
    public $cap4_mode_p442;
    public $cap4_mode_p443;
    public $cap4_mode_p443_7_especifique;
    public $cap4_mode_p444;
    public $cap4_mode_p444_8_especifique;
    public $cap4_mode_p445;
    public $cap4_mode_p446;
    public $cap4_mode_observacion;

    public $cap5_moda_p501_a;
    public $cap5_moda_p501_b;
    public $cap5_moda_p501_b_27_especifique;

    public $cap5_modb_p502_flag;
    public $cap5_modc_p502_flag;
    public $cap5_modd_p502_flag;
    public $cap5_mode_p502_a_flag;
    public $cap5_mode_p502_b_flag;
    public $cap5_modf_p502_a_flag;
    public $cap5_modf_p502_b_flag;
    public $cap5_modf_p502_c_flag;
    public $cap5_modg_p502_a_flag;
    public $cap5_modg_p502_b_flag;

    public $cap5_modh_p502_flag;

    public $cap5_modi_p530;
    public $cap5_modi_p530_9_especifique;
    public $cap5_modi_p531;
    public $cap5_modi_p532;
    public $cap5_modi_p533;
    public $cap5_modi_p533_24_especifique;

    public $cap5_modj_p534_a;
    public $cap5_modj_p535;

    public $cap5_modj_p534_b;
    public $cap5_modj_p534_b_5_especifique;
    public $cap5_modj_p534_b_11_especifique;
    public $cap5_modj_p534_b_18_especifique;
    public $cap5_modj_p534_b_27_especifique;
    public $cap5_modj_p534_b_37_especifique;

    public $cap5_modj_p535_a;

    public $cap5_modk_p536;
    public $cap5_modk_p536_a;
    public $cap5_modk_p536_a_9_especifique;
    public $cap5_modk_p537;
    public $cap5_modk_p538;
    public $cap5_modk_p539_a;
    public $cap5_modk_p540;
    public $cap5_modk_p541_a;
    public $cap5_modk_p541_a_8_especifique;
    public $cap5_modk_p541_a_15_especifique;
    public $cap5_modk_p541_a_24_especifique;
    public $cap5_modk_p541_a_30_especifique;
    public $cap5_modk_p541_a_39_especifique;

    

    public $cap5_observacion;

    public $cap6_p601;
    public $cap6_p601_7_especifique;
    public $cap6_p602;
    public $cap6_p602_7_especifique;
    public $cap6_p603;
    public $cap6_p604;
    public $cap6_p605;
    public $cap6_p605_6_especifique;
    public $cap6_p606;
    public $cap6_p606_5_especifique;
    public $cap6_p607;
    public $cap6_p608;
    public $cap6_p609;
    public $cap6_p610;
    public $cap6_p610_7_especifique;



    public $cap7_modc_p710_a;
    public $cap7_modc_p710_b;
    public $cap7_modc_p714;
    public $cap7_modc_p714_6_especifique;
    public $cap7_modc_p715;
    public $cap7_modc_p716;
    public $cap7_modc_p716_6_especifique;
    public $cap7_modc_p717;
    public $cap7_modc_p717_7_especifique;
    public $cap7_modc_p718;
    public $cap7_modc_p718_1;
    public $cap7_modc_p718_1_especifique;
    public $cap7_modc_p718_2;
    public $cap7_modc_p718_2_especifique;
    public $cap7_modc_p718_3;
    public $cap7_modc_p718_3_especifique;
    public $cap7_modc_p718_4;
    public $cap7_modc_p718_4_especifique;
    public $cap7_modc_p718_5;
    public $cap7_modc_p718_5_especifique;
    public $cap7_modc_p718_6;
    public $cap7_modc_p718_6_especifique;
    public $cap7_modc_p718_7;
    public $cap7_modc_p718_7_especifique;
    public $cap7_modc_p719;
    public $cap7_modc_p719_6_especifique;
    public $cap7_mode_p724;
    public $cap7_mode_p725;
    public $cap7_mode_p726;
    public $cap7_mode_p726_11_especifique;
    public $cap7_mode_p727;
    public $cap7_mode_p727_9_especifique;
    public $cap7_mode_p728;
    public $cap7_mode_p729;
    public $cap7_mode_p729_5_especifique;
    public $cap7_mode_p730;
    public $cap7_mode_p731;
    public $cap7_mode_p731_6_especifique;

    public $cap8_p801;
    public $cap8_p801_8_especifique;
    public $cap8_p802;
    public $cap8_p802_6_especifique;
    public $cap8_p803;
    public $cap8_p803_9_especifique;
    public $cap8_observacion;
    
    public $cap9_p901;
    public $cap9_observacion;

    public $cap10_observacion;
    
    public $cap12_moda_p1206;
    public $cap12_moda_p1207_6_especifique;
    public $cap12_moda_p1207;
    public $cap12_observacion;



    /**
     * @return array the validation rules.
     */
    public function rules()
    {

  

    
        return [
            [['general_numero_parcela','general_total_parcela','general_centro_poblado','general_distrito'],'safe'],

            /*capitulo I */
            [['p01_1', 'p01_2','p01_3', 'p01_4','p01_5', 'p01_6','p01_7', 'p01_8','p01_9', 'p01_10','p01_11', 'p01_12','p01_observacion'],'safe'],
            /*capitulo II */
            [['p02_tipo_productor', 'p02_observacion'],'safe'],
            [['p02_a_toma_decision','p02_a_representante_nombres','p02_a_representante_apellidos','p02_a_txt_dni','p02_a_representante_tiene_numero','p02_a_representante_telefono_fijo','p02_a_representante_telefono_celular','p02_a_tiene_correo_electronico','p02_a_es_productor','p02_a_correo_electronico','p02_a_informacion_proporcionada','p02_a_informacion_proporcionada_especifique','p02_a_informante_nombres','p02_a_informante_apellidos','p02_a_informante_tiene_numero','p02_a_informante_telefono_fijo','p02_a_informante_telefono_celular','p02_a_productor_agrario_vive','p02_a_productor_agrario_vive_especifique','p02_a_informante_departamento','p02_a_informante_provincia','p02_a_informante_distrito','p02_a_informante_centro_poblado','p02_a_pertenece_comunidad',],'safe'],
            [['p02_b_razon_social','p02_b_ruc','p02_b_representante_legal_nombres','p02_b_representante_legal_apellidos','p02_b_representante_departamento','p02_b_representante_provincia','p02_b_representante_distrito','p02_b_representante_centro_poblado','p02_b_representante_codigo_centro_poblado','p02_b_tipo_via','p02_b_tipo_via_especifique','p02_b_avenida','p02_b_nro_puerta','p02_b_block','p02_b_interior','p02_b_piso','p02_b_manzana','p02_b_lote','p02_b_informante_nombres','p02_b_informante_apellidos','p02_b_cargo','p02_b_cargo_especifique','p02_b_informante_tiene_numero','p02_b_informante_telefono_fijo','p02_b_informante_telefono_celular','p02_b_informante_tiene_correo_electronico','p02_b_informante_correo_electronico','p02_b_nombre_fundo'],'safe'],
            [['p03_observacion'],'safe'],
            [['p03_c_int_trabaja_otra_parcela', 'p03_c_int_cantidad_parcelas','p03_c_int_tenencia','p03_c_int_tenencia_31_especifique'],'safe'],
            [['cap4_mode_observacion'],'safe'],
            [['cap4_modd_p437','cap4_modd_p437_8_especifique','cap4_modd_p438','cap4_modd_p438_10_especifique','cap4_modd_p439','cap4_mode_p440','cap4_mode_p440_1','cap4_mode_p441_1','cap4_mode_p440_2','cap4_mode_p441_2','cap4_mode_p440_3','cap4_mode_p441_3','cap4_mode_p440_4','cap4_mode_p441_4','cap4_mode_p440_5','cap4_mode_p441_5','cap4_mode_p440_6','cap4_mode_p441_6','cap4_mode_p440_7','cap4_mode_p441_7','cap4_mode_p440_8','cap4_mode_p441_8','cap4_mode_p440_9','cap4_mode_p441_9','cap4_mode_p440_10','cap4_mode_p441_10','cap4_mode_p440_11','cap4_mode_p441_11','cap4_mode_p440_12','cap4_mode_p441_12','cap4_mode_p440_13','cap4_mode_p441_13','cap4_mode_p440_14','cap4_mode_p441_14','cap4_mode_p440_15','cap4_mode_p441_15','cap4_mode_p440_16','cap4_mode_p441_16','cap4_mode_p440_17','cap4_mode_p441_17','cap4_mode_p440_18','cap4_mode_p441_18','cap4_mode_p440_19','cap4_mode_p441_19','cap4_mode_p440_20','cap4_mode_p441_20','cap4_mode_p440_21','cap4_mode_p441_21','cap4_mode_p440_22','cap4_mode_p441_22','cap4_mode_p442','cap4_mode_p443','cap4_mode_p443_7_especifique','cap4_mode_p444','cap4_mode_p444_8_especifique','cap4_mode_p445','cap4_mode_p446','cap4_mode_observacion'],'safe'],
            [['cap5_moda_p501_a','cap5_moda_p501_b','cap5_moda_p501_b_27_especifique'],'safe'],

            [['cap5_modb_p502_flag','cap5_modc_p502_flag','cap5_modd_p502_flag','cap5_mode_p502_a_flag','cap5_mode_p502_b_flag','cap5_modf_p502_a_flag','cap5_modf_p502_b_flag','cap5_modf_p502_c_flag','cap5_modg_p502_a_flag','cap5_modg_p502_b_flag','cap5_modh_p502_flag'],'safe'],

            

            [['cap5_modi_p530','cap5_modi_p530_9_especifique','cap5_modi_p531','cap5_modi_p532','cap5_modi_p533','cap5_modi_p533_24_especifique'],'safe'],
            [['cap5_modj_p534_a','cap5_modj_p535','cap5_modj_p534_b','cap5_modj_p534_b_5_especifique','cap5_modj_p534_b_11_especifique','cap5_modj_p534_b_18_especifique','cap5_modj_p534_b_27_especifique','cap5_modj_p534_b_37_especifique','cap5_modj_p535_a'],'safe'],
            [['cap5_modk_p536','cap5_modk_p537','cap5_modk_p538','cap5_modk_p540','cap5_observacion', 'cap5_modk_p536_a','cap5_modk_p536_a_9_especifique','cap5_modk_p539_a','cap5_modk_p541_a','cap5_modk_p541_a_8_especifique','cap5_modk_p541_a_15_especifique','cap5_modk_p541_a_24_especifique','cap5_modk_p541_a_30_especifique','cap5_modk_p541_a_39_especifique'],'safe'],

            [['cap6_p601','cap6_p601_7_especifique','cap6_p602','cap6_p602_7_especifique','cap6_p603','cap6_p604','cap6_p605','cap6_p605_6_especifique','cap6_p606','cap6_p606_5_especifique','cap6_p607','cap6_p608','cap6_p609','cap6_p610','cap6_p610_7_especifique'],'safe'],
            [['cap7_modc_p710_a','cap7_modc_p710_b','cap7_modc_p714','cap7_modc_p714_6_especifique','cap7_modc_p715','cap7_modc_p716','cap7_modc_p716_6_especifique','cap7_modc_p717','cap7_modc_p717_7_especifique','cap7_modc_p718','cap7_modc_p718_1','cap7_modc_p718_1_especifique','cap7_modc_p718_2','cap7_modc_p718_2_especifique','cap7_modc_p718_3','cap7_modc_p718_3_especifique','cap7_modc_p718_4','cap7_modc_p718_4_especifique','cap7_modc_p718_5','cap7_modc_p718_5_especifique','cap7_modc_p718_6','cap7_modc_p718_6_especifique','cap7_modc_p718_7','cap7_modc_p718_7_especifique','cap7_modc_p719','cap7_modc_p719_6_especifique','cap7_mode_p724','cap7_mode_p725','cap7_mode_p726','cap7_mode_p726_11_especifique','cap7_mode_p727','cap7_mode_p727_9_especifique','cap7_mode_p728','cap7_mode_p729','cap7_mode_p729_5_especifique','cap7_mode_p730','cap7_mode_p731','cap7_mode_p731_6_especifique'],'safe'],
            [['cap8_p801','cap8_p801_8_especifique','cap8_p802','cap8_p802_6_especifique','cap8_p803','cap8_p803_9_especifique','cap8_observacion'],'safe'],
            [['cap9_p901','cap9_observacion'],'safe'],
            [['cap12_moda_p1206','cap12_moda_p1207','cap12_moda_p1207_6_especifique','cap12_observacion'],'safe'],
            
        ];
    }

}
