<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class CapituloVModuloH extends Model
{
    public $cap5_modh_p525_a;
    public $cap5_modh_p525_b;
    public $cap5_modh_p525_c;
    public $cap5_modh_p525_d;
    public $cap5_modh_p526;
    public $cap5_modh_p526_a;
    public $cap5_modh_p527_a;
    public $cap5_modh_p527_b;
    public $cap5_modh_p527_c;
    public $cap5_modh_p528_a;
    public $cap5_modh_p528_b;
    public $cap5_modh_p528_c;
    public $cap5_modh_p529_a;
    public $cap5_modh_p529_b;
    public $cap5_modh_p529_c;
    
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
           [['cap5_modh_p525_a',
           'cap5_modh_p525_b',
           'cap5_modh_p525_c',
           'cap5_modh_p525_d',
           'cap5_modh_p526',
           'cap5_modh_p526_a',
           'cap5_modh_p527_a',
           'cap5_modh_p527_b',
           'cap5_modh_p527_c',
           'cap5_modh_p528_a',
           'cap5_modh_p528_b',
           'cap5_modh_p528_c',
           'cap5_modh_p529_a',
           'cap5_modh_p529_b',
           'cap5_modh_p529_c'],'safe']
        ];
    }

}
