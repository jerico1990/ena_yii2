<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class CapituloIIIModuloA extends Model
{
    public $p03_detalle_cobertura;
    public $p03_cantidad;
    public $p03_unidad_medida;
    public $p03_equivalencia;
    public $p03_cultivos;
    public $p03_id_capitulo_iii;
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
           [['p03_cultivos','p03_equivalencia','p03_unidad_medida','p03_cantidad','p03_detalle_cobertura','p03_id_capitulo_iii'],'safe']
        ];
    }

}
