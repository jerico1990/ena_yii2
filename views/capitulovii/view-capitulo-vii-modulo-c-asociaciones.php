

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Encuesta</h4>
                </div>
            </div>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Parcelas</li>
                </ol>
            </div>

        </div>
    </div>
</div>

<?php $form = ActiveForm::begin(['options' => ['id' => 'formCapituloViiModuloCAsociaciones']]); ?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">711. ¿Cuál es el nombre de la asociación, cooperativa, comité o comunidad a la que pertenece?</label>
                            <input type="text" class="form-control" name="CapituloVIIModuloCAsociaciones[cap7_modc_p711]" id="cap7_modc_p711" value="<?= $cap_vii_mod_c_asociaciones['TXT_NOMBRE'] ?>" maxlength="300">
                        </div>
                    </div>
                </div>
                

                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">712. ¿Cuál es el tipo: </label>
                            <select name="CapituloVIIModuloCAsociaciones[cap7_modc_p712]" id="cap7_modc_p712"class="form-control">
                                <option value>Seleccionar</option>
                                <option value="76" <?= (($cap_vii_mod_c_asociaciones['COD_TIPO']=="76")?"selected":""); ?> >Asociacion</option>
                                <option value="77" <?= (($cap_vii_mod_c_asociaciones['COD_TIPO']=="77")?"selected":""); ?>>Cooperativa</option>
                                <option value="78" <?= (($cap_vii_mod_c_asociaciones['COD_TIPO']=="78")?"selected":""); ?>>Comité</option>
                                <option value="79" <?= (($cap_vii_mod_c_asociaciones['COD_TIPO']=="79")?"selected":""); ?>>Comunidad</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">713. Desde qué año pertenece a la /el?</label>
                            <input type="text" class="form-control numerico"  maxlength="4" name="CapituloVIIModuloCAsociaciones[cap7_modc_p713]" id="cap7_modc_p713" value="<?= $cap_vii_mod_c_asociaciones['TXT_ANIO'] ?>">
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<?php ActiveForm::end(); ?>

<script>
$('input,select,textarea').prop('disabled',true)
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var loading =   $('#staticBackdrop');


</script>