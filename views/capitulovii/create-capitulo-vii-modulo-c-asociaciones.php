

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Encuesta</h4>
                    <a class="btn btn-primary" href="<?= \Yii::$app->request->BaseUrl ?>/encuesta/index?geocodigo_fdo=<?= $geocodigo_fdo ?>">< Regresar</a>
                </div>
            </div>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Parcelas</li>
                </ol>
            </div>

        </div>
    </div>
</div>

<?php $form = ActiveForm::begin(['options' => ['id' => 'formCapituloViiModuloCAsociaciones']]); ?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">711. ¿Cuál es el nombre de la asociación, cooperativa, comité o comunidad a la que pertenece?</label>
                            <input type="text" class="form-control" name="CapituloVIIModuloCAsociaciones[cap7_modc_p711]" id="cap7_modc_p711" maxlength="300">
                        </div>
                    </div>
                </div>
                

                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">712. ¿Cuál es el tipo: </label>
                            <select name="CapituloVIIModuloCAsociaciones[cap7_modc_p712]" id="cap7_modc_p712"class="form-control">
                                <option value>Seleccionar</option>
                                <option value="76">Asociacion</option>
                                <option value="77">Cooperativa</option>
                                <option value="78">Comité</option>
                                <option value="79">Comunidad</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">713. Desde qué año pertenece a la /el?</label>
                            <input type="text" class="form-control numerico numerico" name="CapituloVIIModuloCAsociaciones[cap7_modc_p713]" id="cap7_modc_p713" maxlength="4">
                        </div>
                    </div>
                </div>

                <hr>
                <button class="btn btn-success btn-grabar-capitulo-vii-modulo-c-asociaciones">Grabar</button>

            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<?php ActiveForm::end(); ?>

<script>

var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var loading =   $('#staticBackdrop');

$("body").on("click", ".btn-grabar-capitulo-vii-modulo-c-asociaciones", function (e) {

    e.preventDefault();

    var cap7_modc_p711   = $("[id=\"cap7_modc_p711\"]").val();
    var cap7_modc_p712   = $("[id=\"cap7_modc_p712\"]").val();
    var cap7_modc_p713   = $("[id=\"cap7_modc_p713\"]").val();

    var form = $("#formCapituloViiModuloCAsociaciones");
    var formData = $("#formCapituloViiModuloCAsociaciones").serializeArray();

    /* Seteando variables del capitulo IV Modulo A */
    
    formData.push({name: "CapituloVIIModuloCAsociaciones[cap7_modc_p711]", value: cap7_modc_p711});
    formData.push({name: "CapituloVIIModuloCAsociaciones[cap7_modc_p712]", value: cap7_modc_p712});
    formData.push({name: "CapituloVIIModuloCAsociaciones[cap7_modc_p713]", value: cap7_modc_p713});

    if (form.find(".has-error").length) {
        return false;
    }

    $.ajax({
        url:form.attr("action"),
        type: form.attr("method"),
        data: formData,
        dataType: "json",
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        beforeSend:function()
        {
            loading.modal("show");
        },
        success: function (results) {
            if(results.success){
                window.location.href = "<?= \Yii::$app->request->BaseUrl ?>/capitulovii/update-capitulo-vii-modulo-c-asociaciones?id_cap_vii_c_asociacion="+results.id_cap_vii_c_asociacion + "&geocodigo_fdo=<?= $geocodigo_fdo ?>";
                //loading.modal("hide");
            }
        },
    });
});

</script>