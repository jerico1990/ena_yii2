<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8" />
    <title>ENA | Administrador</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
    <meta content="Themesbrand" name="author" />
    <!-- App favicon -->
    <!-- <link rel="shortcut icon" href="<?= \Yii::$app->request->BaseUrl ?>//img/conectaideas_login.png">
 -->

    <!-- Bootstrap Css -->
    <link href="<?= \Yii::$app->request->BaseUrl ?>/skote/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="<?= \Yii::$app->request->BaseUrl ?>/skote/css/icons.min.css" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="<?= \Yii::$app->request->BaseUrl ?>/skote/css/app.min.css" rel="stylesheet" type="text/css" />
    <!-- Custom Css-->
    <link href="<?= \Yii::$app->request->BaseUrl ?>/skote/css/custom.css" rel="stylesheet" type="text/css" />
    <!-- JAVASCRIPT -->
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/jquery/jquery.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/metismenu/metisMenu.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/simplebar/simplebar.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/node-waves/waves.min.js"></script>

    <link href="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />


    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    

    
    <!-- Required datatable js -->
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/jszip/jszip.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/pdfmake/build/vfs_fonts.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-buttons/js/buttons.colVis.min.js"></script>

    <!-- Responsive examples -->
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
    <script src="https://cdn.rawgit.com/kevindb/jquery-load-json/v1.3.4/dist/jquery.loadJSON.min.js" integrity="sha384-ivtX4sn4dcdfHiO4e0/956wIQSerxsy2QZ6EHzdCVLlyGYYjSb8bqdxKY8IsfDGh" crossorigin="anonymous"></script>

    <!-- <link href="<?= \Yii::$app->request->BaseUrl ?>/css/pagination.min.css" rel="stylesheet" type="text/css" />
    <script src="<?= \Yii::$app->request->BaseUrl ?>/js/pagination.min.js"></script> -->

    <script>
	$.fn.serializeObject = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
	</script>

    <style>

    .form-control:disabled, .form-control[readonly]{
        background-color:#dfdfdf;
    }
    </style>
</head>

<body data-sidebar="dark">
    <div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog text-center" role="document">
            Cargando <br>
            <div class="spinner-border text-primary m-1" role="status"></div>
        </div>
    </div>
    <!-- Begin page -->
    <div id="layout-wrapper">

        <header id="page-topbar">
            <div class="navbar-header">
                <div class="d-flex">
                    <!-- LOGO -->
                    <div class="navbar-brand-box">
                        
                    </div>

                    <button type="button" class="btn btn-sm px-3 font-size-16 header-item waves-effect"
                        id="vertical-menu-btn">
                        <i class="fa fa-fw fa-bars"></i>
                    </button>

                </div>

                <div class="d-flex">


                    <div class="dropdown d-inline-block">
                        <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?= Yii::$app->user->identity->username ?>
                                
                            <span class="d-none d-xl-inline-block ml-1"></span>
                            <i class="mdi mdi-chevron-down d-none d-xl-inline-block"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right">
                          
                            <!-- <a class="dropdown-item" href="#"><i class="bx bx-user font-size-16 align-middle mr-1"></i>
                                Profile</a>
                            <a class="dropdown-item" href="#"><i
                                    class="bx bx-wallet font-size-16 align-middle mr-1"></i> My Wallet</a>
                            <a class="dropdown-item d-block" href="#"><span
                                    class="badge badge-success float-right">11</span><i
                                    class="bx bx-wrench font-size-16 align-middle mr-1"></i> Settings</a>
                            <a class="dropdown-item" href="#"><i
                                    class="bx bx-lock-open font-size-16 align-middle mr-1"></i> Lock screen</a>
                            <div class="dropdown-divider"></div> -->
                            <a class="dropdown-item text-danger" href="<?= \Yii::$app->request->BaseUrl ?>/login/logout"><i
                                    class="bx bx-power-off font-size-16 align-middle mr-1 text-danger"></i> Cerrar sesión</a>
                        </div>
                    </div>


                </div>
            </div>
        </header> <!-- ========== Left Sidebar Start ========== -->
        <div class="vertical-menu">

            <div data-simplebar class="h-100">

                <!--- Sidemenu -->
                <div id="sidebar-menu">
                    <!-- Left Menu Start -->
                    <ul class="metismenu list-unstyled" id="side-menu">
                        <li class="menu-title">Menu</li>

                        <li>
                            <a href="<?= \Yii::$app->request->BaseUrl ?>/panel" class=" waves-effect">
                                <i class="bx bx-home-circle"></i>
                                <span>Panel</span>
                            </a>
                        </li>

                        <li>
                            <a href="<?= \Yii::$app->request->BaseUrl ?>/parcela" class=" waves-effect">
                                <i class="bx bx-collection"></i>
                                <span>Parcelas</span>
                            </a>
                        </li>
                        

                    </ul>
                </div>
                <!-- Sidebar -->
            </div>
        </div>
        <!-- Left Sidebar End -->

        <div class="main-content" id="result">
            <div class="page-content">
                <div class="container-fluid">
                    <?= $content ?>
                </div>
            </div>
        </div>

    </div>
    <!-- END layout-wrapper -->

    
    <!-- /Right-bar -->
    <!-- Right bar overlay-->
    <script>
    
    $('.numerico').keypress(function (tecla) {
        var reg = /^[0-9.,\s]+$/;
        if(!reg.test(String.fromCharCode(tecla.which))){
            return false;
        }
        return true;
    });		
    $('.texto').keypress(function(tecla) {
        var reg = /^[a-zA-ZáéíóúàèìòùÀÈÌÒÙÁÉÍÓÚñÑüÜ'_\s]+$/;
        if(!reg.test(String.fromCharCode(tecla.which))){
            return false;
        }
        return true;
    });
        
    </script>
    <!-- App js -->
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/js/app.js"></script>
</body>

</html>