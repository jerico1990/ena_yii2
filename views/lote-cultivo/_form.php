<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(['options' => ['id' => 'formCultivo']]); ?>

    <div class="modal-header">
        <h4 class="modal-title"><?= $model->titulo ?></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label >Cultivo:</label>
                    <select name="CultivoForm[id_cultivo]" id="id_cultivo" class="form-control">
                        <option value>Seleccionar</option>
                        <?php foreach($cultivos as $cultivo){ ?>
                        <option data-cultivo="<?= $cultivo['TXT_CULTIVO'] ?>" value="<?= $cultivo['ID_CULTIVO'] ?>"><?= $cultivo['TXT_CULTIVO'] ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="row cultivo_9999" style="display:none">
            <div class="col-md-6">
                <div class="form-group">
                    <input type="text" class="form-control" id="cultivo_9999_especifique" placeholder="Especifique" name="CultivoForm[cultivo_9999_especifique]" maxlength="250">
                </div>
            </div>
        </div>
        
    </div>
    <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary btn-grabar-cultivo">Grabar</button>
    </div>

<?php ActiveForm::end(); ?>