
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Encuesta</h4>
                </div>
            </div>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Parcelas</li>
                </ol>
            </div>

        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">401. ¿El cultivo que va a registrar es:</label>
                            <select class="form-control" name="CapituloIVModuloA[cap4_moda_p401]" id="cap4_moda_p401">
                                <option value>Seleccionar</option>
                                <option value="32">1. Presente?</option>
                                <option value="33">2. Pasado?</option>
                                <option value="34">3. Futuro?</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">402. ¿Cuál es el nombre y variedad del cultivo?</label>
                            <table class="table">
                                <thead>
                                    <th>Nombre</th>
                                    <th>Variedad</th>
                                    <th>Acciones</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>xxxxx</td>
                                        <td>xxxxx</td>
                                        <td>
                                            <button class="btn btn-primary"><i class="fas fa-pen-square"></i></button> 
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">403. ¿Cuál es la fecha de siembra o plantación en este lote?</label>
                            <input type="date" class="form-control" name="CapituloIVModuloA[cap4_moda_p403]" id="cap4_moda_p403">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">404. ¿El cultivo o plantación está/estuvo/estará:</label>
                            <select class="form-control" name="CapituloIVModuloA[cap4_moda_p404]" id="cap4_moda_p404">
                                <option value>Seleccionar</option>
                                <option value="35">1. Solo?</option>
                                <option value="36">2. Asociado?</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">405. ¿Cuánta es el área sembrada o plantada?</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">405.a Cantidad</label>
                            <input type="text" class="form-control" name="CapituloIVModuloA[cap4_moda_p405_a]" id="cap4_moda_p405_a">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">405.b Unidad de medida</label>
                            <select class="form-control" name="CapituloIVModuloA[cap4_moda_p405_b]" id="cap4_moda_p405_b">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">405.c Equivalencia en metros cuadrados</label>
                            <input type="text" class="form-control" name="CapituloIVModuloA[cap4_moda_p405_c]" id="cap4_moda_p405_c">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">406. ¿Qué cantdad de plantones plantó/plantará en este lote?</label>
                            <input type="text" class="form-control" name="CapituloIVModuloA[cap4_moda_p406]" id="cap4_moda_p406">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <label for="">407. El origen de las semillas o plantones que usa es: </label>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p407_1">
                                    <label class="custom-control-label" for="cap4_moda_p407_1"> 1. ¿Propio?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p407_2" >
                                    <label class="custom-control-label" for="cap4_moda_p407_2"> 2. ¿Familiar o amigo?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p407_3" >
                                    <label class="custom-control-label" for="cap4_moda_p407_3"> 3. ¿Tienda agroinsumos?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p407_4" >
                                    <label class="custom-control-label" for="cap4_moda_p407_4"> 4. ¿Empresa?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p407_5" >
                                    <label class="custom-control-label" for="cap4_moda_p407_5">  5.  ¿Feria?</label>
                                </div>

                            </div>

                            <div class="col-md-6">
                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p407_6" >
                                    <label class="custom-control-label" for="cap4_moda_p407_6"> 6. ¿Productor de semilla registrada?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p407_7" >
                                    <label class="custom-control-label" for="cap4_moda_p407_7"> 7. ¿INIA?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p407_8" >
                                    <label class="custom-control-label" for="cap4_moda_p407_8"> 8. ¿Universidad?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p407_9" >
                                    <label class="custom-control-label" for="cap4_moda_p407_9"> 9.  ¿Importado?</label>
                                </div>
                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p407_10" >
                                    <label class="custom-control-label" for="cap4_moda_p407_10"> 10. ¿Otro?:</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">409. ¿Cuál es la finalidad de la plantación forestal?	</label>
                            <select class="form-control" name="CapituloIVModuloA[cap4_moda_p409]" id="cap4_moda_p409">
                                <option value>Seleccionar</option>
                                <option value="37">1. ¿Producción?</option>
                                <option value="38">2. ¿Protección?</option>
                                <option value="39">3. ¿Rodal Semillero?</option>
                            </select>
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">410.a ¿Cuál es la fecha de inicio de cosecha?</label>
                            <input type="date" class="form-control" name="CapituloIVModuloA[cap4_moda_p410_a]" id="cap4_moda_p410_a">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for=""> 410.b ¿Cuál es la fecha final de cosecha?</label>
                            <input type="date" class="form-control" name="CapituloIVModuloA[cap4_moda_p410_b]" id="cap4_moda_p410_b">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">411. ¿En qué meses del 2020 se cosechan los cultivos permanentes?</label>
                            <table class="table">
                                <thead>
                                    <th>Mes</th>
                                    <th>Acciones</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>xxxxx</td>
                                        <td>
                                            <button class="btn btn-primary"><i class="fas fa-pen-square"></i></button> 
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">412. ¿Cuánto es el área cosechada?</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">412.a Cantidad</label>
                            <input type="text" class="form-control" name="CapituloIVModuloA[cap4_moda_p412_a]" id="cap4_moda_p412_a">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">412.b Unidad de medida</label>
                            <select class="form-control" name="CapituloIVModuloA[cap4_moda_p412_b]" id="cap4_moda_p412_b">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">412.c Equivalencia en metros cuadrados</label>
                            <input type="text" class="form-control" name="CapituloIVModuloA[cap4_moda_p412_c]" id="cap4_moda_p412_c">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">413. ¿Cuànta es el área perdida en porcentaje?</label>
                            <input type="text" class="form-control" name="CapituloIVModuloA[cap4_moda_p413]" id="cap4_moda_p413">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">414. ¿Cuànta es el área afectada en porcentaje?</label>
                            <input type="text" class="form-control" name="CapituloIVModuloA[cap4_moda_p414]" id="cap4_moda_p414">
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12">
                        <label for="">415. ¿Qué fenomenos naturales afectaron la producción?</label>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p415_1" >
                                    <label class="custom-control-label" for="cap4_moda_p415_1">1. Sequía</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p415_2" >
                                    <label class="custom-control-label" for="cap4_moda_p415_2">2. Bajas temperaturas</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p415_3" >
                                    <label class="custom-control-label" for="cap4_moda_p415_3">3. Helada</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p415_4" >
                                    <label class="custom-control-label" for="cap4_moda_p415_4">4. Granizada</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p415_5" >
                                    <label class="custom-control-label" for="cap4_moda_p415_5">5. Friaje</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p415_6" >
                                    <label class="custom-control-label" for="cap4_moda_p415_6">6. Inundación</label>
                                </div>

                            </div>

                            <div class="col-md-6">
                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p415_7" >
                                    <label class="custom-control-label" for="cap4_moda_p415_7">7. Huaicos</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p415_8" >
                                    <label class="custom-control-label" for="cap4_moda_p415_8">8. Lluvias a destiempo</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p415_9" >
                                    <label class="custom-control-label" for="cap4_moda_p415_9">9. Movimiento de tierras</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p415_10" >
                                    <label class="custom-control-label" for="cap4_moda_p415_10">10. Incendios</label>
                                </div>
                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p415_11" >
                                    <label class="custom-control-label" for="cap4_moda_p415_11">11. Plagas y enfermedades</label>
                                </div>
                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p415_12" >
                                    <label class="custom-control-label" for="cap4_moda_p415_12">12. Ninguna</label>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <label for="">416. ¿Qué factores económicos afectaron la producción?</label>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p416_40" >
                                    <label class="custom-control-label" for="cap4_moda_p416_40">1. ¿Escasez de mano de obra?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p416_41" >
                                    <label class="custom-control-label" for="cap4_moda_p416_41">2. ¿Falta de maquinaria?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p416_42" >
                                    <label class="custom-control-label" for="cap4_moda_p416_42">3. ¿Precios bajos?</label>
                                </div>

                            </div>

                            <div class="col-md-6">
                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p416_43" >
                                    <label class="custom-control-label" for="cap4_moda_p416_43">4. ¿Por dificultades en la comercialización?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p416_44" >
                                    <label class="custom-control-label" for="cap4_moda_p416_44">5. Ninguna</label>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <label for="">417. ¿Qué actividades de mejoramiento realizó en los pastos en el 2020?</label>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p417_45" >
                                    <label class="custom-control-label" for="cap4_moda_p417_45">1. ¿Deshierbo?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p417_46" >
                                    <label class="custom-control-label" for="cap4_moda_p417_46">2. ¿Fertilización?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p417_47" >
                                    <label class="custom-control-label" for="cap4_moda_p417_47">3. ¿Abonamiento?</label>
                                </div>

                            </div>

                            <div class="col-md-6">
                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p417_48" >
                                    <label class="custom-control-label" for="cap4_moda_p417_48">4. ¿Resiembra?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p417_49" >
                                    <label class="custom-control-label" for="cap4_moda_p417_49">5. Ninguno</label>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">418. Principalmente la cosecha de los pastos sembrados fue:</label>
                            <select class="form-control" name="CapituloIVModuloA[cap4_moda_p418]" id="cap4_moda_p418">
                                <option value>Seleccionar</option>
                                <option value="50">1. ¿Por corte?</option>
                                <option value="51">2.¿ Pastoreo?</option>
                            </select>
                        </div>
                    </div>
                    
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">419. ¿Cuál fue la producción de ….......... entre ………… y ………...?</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">419.a Cantidad</label>
                            <input type="text" class="form-control" name="CapituloIVModuloA[cap4_moda_p419_a]" id="cap4_moda_p419_a">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">419.b Unidad de medida</label>
                            <select class="form-control" name="CapituloIVModuloA[cap4_moda_p419_b]" id="cap4_moda_p419_b">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">419.c Equivalencia en metros cuadrados</label>
                            <input type="text" class="form-control" name="CapituloIVModuloA[cap4_moda_p419_c]" id="cap4_moda_p419_c">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">419.d ¿Cuál es la especificación del producto? </label>
                            <select class="form-control" name="CapituloIVModuloA[cap4_moda_p419_d]" id="cap4_moda_p419_d">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">419.e ¿Cuánta es  la producción en el 2020?</label>
                            <input type="text" class="form-control" name="CapituloIVModuloA[cap4_moda_p419_e]" id="cap4_moda_p419_e">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">420. Rendimiento (kg/ha)</label>
                            <input type="text" class="form-control" name="CapituloIVModuloA[cap4_moda_p420]" id="cap4_moda_p420">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">421. ¿Su producción fue por calidad?</label>
                            <select class="form-control" name="CapituloIVModuloA[cap4_moda_p421]" id="cap4_moda_p421">
                                <option value>Seleccionar</option>
                                <option value="1">Si</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">422. ¿Cuántas calidades obtuvo?</label>
                            <input type="text" class="form-control" name="CapituloIVModuloA[cap4_moda_p422]" id="cap4_moda_p422">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">423. ¿Cuál fue la producción entre enero y diciembre de 2020 por calidades? </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">423.a Nombre del tipo de calidad</label>
                            <input type="text" class="form-control" name="CapituloIVModuloA[cap4_moda_p423_a]" id="cap4_moda_p423_a">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">423.b Cantidad cosechada por calidad</label>
                            <select class="form-control" name="CapituloIVModuloA[cap4_moda_p423_b]" id="cap4_moda_p423_b">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">423.c Unidad de medida (Peso)</label>
                            <input type="text" class="form-control" name="CapituloIVModuloA[cap4_moda_p423_c]" id="cap4_moda_p423_c">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">423.d Equivalencia (Kg)</label>
                            <input type="text" class="form-control" name="CapituloIVModuloA[cap4_moda_p423_d]" id="cap4_moda_p423_d">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <label for="">424. ¿El destino de la producción fue: </label>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p424_1" >
                                    <label class="custom-control-label" for="cap4_moda_p424_1">1. ¿Venta directa al consumidor? </label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p424_2" >
                                    <label class="custom-control-label" for="cap4_moda_p424_2">2. ¿Venta a intermediario? </label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p424_3" >
                                    <label class="custom-control-label" for="cap4_moda_p424_3">3. ¿Venta a mayorista?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p424_4" >
                                    <label class="custom-control-label" for="cap4_moda_p424_4">4. ¿Venta a una bioFeria? </label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p424_5" >
                                    <label class="custom-control-label" for="cap4_moda_p424_5">5. ¿Venta con valor agregado?  </label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p424_6" >
                                    <label class="custom-control-label" for="cap4_moda_p424_6">6. ¿Autoconsumo? </label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p424_7" >
                                    <label class="custom-control-label" for="cap4_moda_p424_7">7. ¿Semilla?</label>
                                </div>

                            </div>

                            <div class="col-md-6">
                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p424_8" >
                                    <label class="custom-control-label" for="cap4_moda_p424_8">8. ¿Consumo de animales? </label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p424_9" >
                                    <label class="custom-control-label" for="cap4_moda_p424_9">9. ¿Fertilización de suelos?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p424_10" >
                                    <label class="custom-control-label" for="cap4_moda_p424_10">10. ¿Intercambio o trueque?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p424_11" >
                                    <label class="custom-control-label" for="cap4_moda_p424_11">11. ¿Para la exportación?</label>
                                </div>
                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p424_12" >
                                    <label class="custom-control-label" for="cap4_moda_p424_12">12. ¿Insumo de la agroindustria?</label>
                                </div>
                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p424_13" >
                                    <label class="custom-control-label" for="cap4_moda_p424_13">13. ¿Otro?:</label>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">425. ¿Cuánto de la producción se destinó para venta? </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">425.a Cantidad</label>
                            <input type="text" class="form-control" name="CapituloIVModuloA[cap4_moda_p425_a]" id="cap4_moda_p425_a">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">425.b Unidad de medida</label>
                            <select class="form-control" name="CapituloIVModuloA[cap4_moda_p425_b]" id="cap4_moda_p425_b">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">425.c Especificación del producto</label>
                            <input type="text" class="form-control" name="CapituloIVModuloA[cap4_moda_p425_c]" id="cap4_moda_p425_c">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">425.d Precio de unidad (S/ x kg) (S/ x t)</label>
                            <input type="text" class="form-control" name="CapituloIVModuloA[cap4_moda_p425_d]" id="cap4_moda_p425_d">
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12">
                        <label for="">426a. ¿A quién vendió la producción: </label>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p426_a_1" >
                                    <label class="custom-control-label" for="cap4_moda_p426_a_1">1. Consumidor?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p426_a_2" >
                                    <label class="custom-control-label" for="cap4_moda_p426_a_2">2. Intermediario?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p426_a_3" >
                                    <label class="custom-control-label" for="cap4_moda_p426_a_3">3. Mayorista?</label>
                                </div>


                            </div>

                            <div class="col-md-6">
                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p426_a_4" >
                                    <label class="custom-control-label" for="cap4_moda_p426_a_4">4. Bioferia?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p426_a_5" >
                                    <label class="custom-control-label" for="cap4_moda_p426_a_5">5. Empresa/agroindustria?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p426_a_6" >
                                    <label class="custom-control-label" for="cap4_moda_p426_a_6">6. Otro? </label>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <label for="">426b. ¿Cuál fue el punto de venta?</label>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p426_b_1" >
                                    <label class="custom-control-label" for="cap4_moda_p426_b_1">1. ¿En la parcela? (cosechado)</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p426_b_2" >
                                    <label class="custom-control-label" for="cap4_moda_p426_a_2">2. ¿En el lote? (cultivo en pie)</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p426_b_3" >
                                    <label class="custom-control-label" for="cap4_moda_p426_b_3">3. ¿En el mercado local/regional?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p426_b_4" >
                                    <label class="custom-control-label" for="cap4_moda_p426_b_4">4. ¿En el mercado de Lima?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p426_b_5" >
                                    <label class="custom-control-label" for="cap4_moda_p426_b_5">5.  ¿Venta en la industria?</label>
                                </div>


                            </div>

                            <div class="col-md-6">
                                
                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p426_b_6" >
                                    <label class="custom-control-label" for="cap4_moda_p426_b_6">6. ¿En la cooperativa?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p426_b_7" >
                                    <label class="custom-control-label" for="cap4_moda_p426_b_7">7. ¿En la Bioferia?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p426_b_8" >
                                    <label class="custom-control-label" for="cap4_moda_p426_b_8">8. ¿En la asociación de productores?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p426_b_9" >
                                    <label class="custom-control-label" for="cap4_moda_p426_b_9">9. ¿Otro?:</label>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<div id="modal" class="fade modal" role="dialog"  tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        </div>
    </div>
</div>


