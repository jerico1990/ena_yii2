
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Encuesta</h4>
                    <a class="btn btn-primary" href="<?= \Yii::$app->request->BaseUrl ?>/encuesta/index?geocodigo_fdo=<?= $geocodigo_fdo ?>">< Regresar</a>
                </div>
            </div>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Parcelas</li>
                </ol>
            </div>

        </div>
    </div>
</div>
<!-- end page title -->
<?php $form = ActiveForm::begin(['options' => ['id' => 'formCapituloIIIModuloA']]); ?>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">301. Hoy, ¿Qué uso y/o cobertura de la tierra tienen los lotes de la parcela?</label>
                            <select class="form-control" name="CapituloIIIModuloA[p03_detalle_cobertura]" id="p03_detalle_cobertura">
                                <option value>Seleccionar</option>
                                <option value="1">1. Lote en cultivo transitorio a nivel de variedad</option>
                                <option value="2">2. Lote en cultivo permanente a nivel de variedad</option>
                                <option value="3">3. Lote en plantaciones forestales a nivel de variedad</option>
                                <option value="4">4. Lote en pastos sembrados a nivel de variedad</option>
                                <option value="5">5. Lote en barbecho</option>
                                <option value="6">6. Lote en descanso</option>
                                <option value="7">7. Lote en pastos naturales</option>
                                <option value="8">8. Lote en matorrales</option>
                                <option value="9">9. Lote en montes y bosques naturales</option>
                                <option value="10">10. Lote en infraestructura agrícola</option>
                                <option value="11">11. Lote en infraestructura pecuaria</option>
                                <option value="12">12. Lote en infraestructura no agraria</option>
                                <option value="13">13. Lote en afloramientos rocosos, áreas sin vegetación o erosionadas,eriazas</option>
                                <option value="14">14. Lote en cuerpos de agua (quebradas, riachuelos, lagunas, qochas o reservorios de agua)</option>
                                <option value="15">15. Lote en otros usos (vivienda, patio, piscina, cancha deportiva, etc)</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 mb-2">
                        <a class="btn btn-success btn-agregar-cultivos disabled" href="#">Agregar</a>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">301a. Tipos de Usos y Coberturas de la Tierra de la Parcela Agraria y No Agraria</label>
                            <table id="lista-cultivos" class="table table-bordered dt-responsive ">
                                <thead>
                                    <th>Nombre</th>
                                    <th>Acciones</th>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">301b. Área Total medida del Lote de la Parcela</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">301b. Cantidad</label>
                            <input type="text" class="form-control numerico" name="CapituloIIIModuloA[p03_cantidad]" id="p03_cantidad" maxlength="15">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">301b. Unidad de Medida </label>
                            <select class="form-control" name="CapituloIIIModuloA[p03_unidad_medida]" id="p03_unidad_medida">
                                <option value="99999" selected>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">301b. Equivalencia</label>
                            <input type="text" class="form-control numerico" name="CapituloIIIModuloA[p03_equivalencia]" id="p03_equivalencia" maxlength="5">
                        </div>
                    </div>
                </div>

                <hr>
                <button class="btn btn-success btn-grabar">Grabar</button>

               
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->

<?php ActiveForm::end(); ?>


<div id="modal" class="fade modal" role="dialog"  tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        </div>
    </div>
</div>


<script>
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var loading =   $('#staticBackdrop');
var p03_id_capitulo_iii = "<?= $cap_iii['ID_CAP_III'] ?>";
$('#lista-cultivos').DataTable({
                            "paging": true,
                            "lengthChange": true,
                            "searching": false,
                            "ordering": true,
                            "info": true,
                            "autoWidth": false,
                            "pageLength" : 5,
                            "language": {
                                "sProcessing":    "Procesando...",
                                "sLengthMenu":    "Mostrar _MENU_ registros",
                                "sZeroRecords":   "No se encontraron resultados",
                                "sEmptyTable":    "Ningun dato disponible en esta lista",
                                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":   "",
                                "sSearch":        "Buscar:",
                                "sUrl":           "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":    "Último",
                                    "sNext":    "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            },
                        });

LoteCultivos();
async function LoteCultivos(lote_id){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/lote-cultivo/get-lista-lote-cultivos',
                method: 'POST',
                data:{_csrf:csrf,lote_id:lote_id},
                dataType:'Json',
                beforeSend:function(){
                    loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var cultivos ="";
                        $('#lista-cultivos').DataTable().destroy();
                        $.each(results.cultivos, function( index, value ) {
                            
                            cultivos = cultivos + "<tr>";
                                cultivos = cultivos + "<td> " + value.TXT_CULTIVO + "</td>";

                                cultivos = cultivos + "<td>";
                                    cultivos = cultivos + '<button type="button" class="btn btn-danger btn-eliminar-cultivo"><i class="fas fa-trash"></i></button>'
                                    //cultivos = cultivos + '<a href="<?= \Yii::$app->request->BaseUrl ?>/encuesta/index?geocodigo_fdo='+ value.GEOCODIGO_FDO +'" class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> ';
                                cultivos = cultivos +"</td>";
                            cultivos = cultivos + "</tr>";
                        });
                        
                        $('#lista-cultivos tbody').html(cultivos);
                        $('#lista-cultivos').DataTable({
                            "paging": true,
                            "lengthChange": true,
                            "searching": false,
                            "ordering": true,
                            "info": true,
                            "autoWidth": false,
                            "pageLength" : 5,
                            "language": {
                                "sProcessing":    "Procesando...",
                                "sLengthMenu":    "Mostrar _MENU_ registros",
                                "sZeroRecords":   "No se encontraron resultados",
                                "sEmptyTable":    "Ningun dato disponible en esta lista",
                                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":   "",
                                "sSearch":        "Buscar:",
                                "sUrl":           "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":    "Último",
                                    "sNext":    "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            },
                        });

                        
                    }
                    setTimeout(function(){ loading.modal("hide"); }, 2000);
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}


//agregar

$('body').on('click', '.btn-agregar-cultivos', function (e) {
    e.preventDefault();
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/lote-cultivo/create');
    $('#modal').modal('show');
});

// $('body').on('click', '.btn-grabar-cultivo', function (e) {
//     e.preventDefault();
//     cultivo = $("#id_cultivo").find(':selected').attr('data-cultivo');

//     cultivosLista.push({'id_cultivo':$('#id_cultivo').val(),'TXT_CULTIVO':cultivo});
//     LoteCultivosJson({'success':true,'cultivos':cultivosLista});
//     console.log(cultivosLista);
//     $('#modal').modal('toggle');
// });

// function LoteCultivosJson(results){
//     if(results && results.success){
//         var cultivos ="";
//         $('#lista-cultivos').DataTable().destroy();
//         $.each(results.cultivos, function( index, value ) {
            
//             cultivos = cultivos + "<tr>";
//                 cultivos = cultivos + "<td> " + value.TXT_CULTIVO + "</td>";

//                 cultivos = cultivos + "<td>";
//                 cultivos = cultivos + '<button type="button" data-id_cultivo="' + value.id_cultivo + '" class="btn btn-danger btn-eliminar-cultivo"><i class="fas fa-trash"></i></button>'
//                 cultivos = cultivos +"</td>";
//             cultivos = cultivos + "</tr>";
//         });
        
//         $('#lista-cultivos tbody').html(cultivos);
//         $('#lista-cultivos').DataTable({
//             "paging": true,
//             "lengthChange": true,
//             "searching": false,
//             "ordering": true,
//             "info": true,
//             "autoWidth": false,
//             "pageLength" : 5,
//             "language": {
//                 "sProcessing":    "Procesando...",
//                 "sLengthMenu":    "Mostrar _MENU_ registros",
//                 "sZeroRecords":   "No se encontraron resultados",
//                 "sEmptyTable":    "Ningun dato disponible en esta lista",
//                 "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
//                 "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
//                 "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
//                 "sInfoPostFix":   "",
//                 "sSearch":        "Buscar:",
//                 "sUrl":           "",
//                 "sInfoThousands":  ",",
//                 "sLoadingRecords": "Cargando...",
//                 "oPaginate": {
//                     "sFirst":    "Primero",
//                     "sLast":    "Último",
//                     "sNext":    "Siguiente",
//                     "sPrevious": "Anterior"
//                 },
//                 "oAria": {
//                     "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
//                     "sSortDescending": ": Activar para ordenar la columna de manera descendente"
//                 }
//             },
//         });
//     }
// }


$("body").on("click", ".btn-grabar", function (e) {

    e.preventDefault();

    /* Seteando variables del capitulo III */
   
    var form = $("#formCapituloIIIModuloA");
    var formData = $("#formCapituloIIIModuloA").serializeArray();
    // var p03_cultivos="";

    // $.each(cultivosLista, function( index, value ) {
    //     if(index==0){
    //         p03_cultivos = p03_cultivos + value.id_cultivo;
    //     }else{
    //         p03_cultivos = p03_cultivos + "," + value.id_cultivo;
    //     }
    // });


    /* Agregando variables del capitulo III modulo A para el envio del formulario*/
    // formData.push({name: "CapituloIIIModuloA[p03_cultivos]", value: p03_cultivos});
    formData.push({name: "CapituloIIIModuloA[p03_id_capitulo_iii]", value: p03_id_capitulo_iii});

    
    if (form.find(".has-error").length) {
        return false;
    }

    $.ajax({
        url:form.attr("action"),
        type: form.attr("method"),
        data: formData,
        dataType: "json",
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        beforeSend:function()
        {
            loading.modal("show");
        },
        success: function (results) {
            if(results.success){
                window.location.href = "<?= \Yii::$app->request->BaseUrl ?>/lote/update-capitulo-iii-modulo-a?id_cap_iii_lote="+results.id_cap_iii_lote+"&geocodigo_fdo=<?= $geocodigo_fdo ?>";
                //loading.modal("hide");
            }
        },
    });
});


// $('body').on('change', '#p03_detalle_cobertura', function (e) {
//     e.preventDefault();
//     if($(this).val()=="1" || $(this).val()=="2" || $(this).val()=="3" || $(this).val()=="4" || $(this).val()=="7"){
//         $(".btn-agregar-cultivos").removeClass("disabled");
//     }else{
//         $(".btn-agregar-cultivos").addClass("disabled");
//     }
// });


// $('body').on('click', '.btn-eliminar-cultivo', function (e) {
//     e.preventDefault();
//     id_cultivo = $(this).attr('data-id_cultivo');
//     eliminarCultivoCodigo(id_cultivo);

//     LoteCultivosJson({'success':true,'cultivos':cultivosLista})
// });

// function eliminarCultivoCodigo(id_cultivo){
//     cultivosLista.forEach(function(currentValue, index, arr){
//         if(cultivosLista[index].id_cultivo==id_cultivo){
//             cultivosLista.splice(index, index);     
//         }
//     })
//     console.log(cultivosLista);
// }


UnidadesMedidas();
async function UnidadesMedidas(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/unidad-medida/get-lista-unidades-medidas-areas',
                method: 'POST',
                data:{_csrf:csrf},
                dataType:'Json',
                beforeSend:function(){
                    loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var opciones_unidades_medidas ="<option value>Seleccionar</option>";
                        $.each(results.unidades_medidas, function( index, value ) {
                            
                            opciones_unidades_medidas = opciones_unidades_medidas + "<option value='" + value.ID_UM + "'>" + value.TXT_DESCRIPCION + "</option>";
                        });
                        $("#p03_unidad_medida").html(opciones_unidades_medidas);

                        setTimeout(function(){ loading.modal("hide"); }, 2000);
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}
</script>
