
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Encuesta</h4>
                </div>
            </div>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Parcelas</li>
                </ol>
            </div>

        </div>
    </div>
</div>
<!-- end page title -->
<?php $form = ActiveForm::begin(['options' => ['id' => 'formCapituloIIIModuloB']]); ?>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">304. ¿Cual es la superficie total de las demás parcelas que conduce en este distrito?</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">304a. No Parcela</label>
                            <input type="text" class="form-control numerico" name="CapituloIIIModuloB[p03_nro_parcela]" id="p03_nro_parcela" maxlength="5">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">304b. Superficie total</label>
                            <input type="text" class="form-control numerico" name="CapituloIIIModuloB[p03_superficie_total]" id="p03_superficie_total" maxlength="10">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">304c. Unidad de medida</label>
                            <select name="CapituloIIIModuloB[p03_unidad_medida]" id="p03_unidad_medida" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">304d. Equivalencia</label>
                            <input type="text" class="form-control numerico" name="CapituloIIIModuloB[p03_equivalencia]" id="p03_equivalencia" maxlength="5">
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<?php ActiveForm::end(); ?>
<div id="modal" class="fade modal" role="dialog"  tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        </div>
    </div>
</div>


<script>
$('input,select,textarea').prop('disabled',true)
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var loading =   $('#staticBackdrop');
var p03_id_otra_parcela = "<?= $otra_parcela['ID_OTRA_PARCELA'] ?>";

async function UnidadesMedidas(unidad_medida){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/unidad-medida/get-lista-unidades-medidas-areas',
                method: 'POST',
                data:{_csrf:csrf},
                dataType:'Json',
                beforeSend:function(){
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var opciones_unidades_medidas ="<option value>Seleccionar</option>";
                        $.each(results.unidades_medidas, function( index, value ) {
                            
                            opciones_unidades_medidas = opciones_unidades_medidas + "<option value='" + value.ID_UM + "'>" + value.TXT_DESCRIPCION + "</option>";
                        });
                        $("#p03_unidad_medida").html(opciones_unidades_medidas);
                        if(unidad_medida){
                            unidad_medida = ("000" + unidad_medida).substr(-3,3)

                            $("#p03_unidad_medida").val(unidad_medida);
                        }
                        setTimeout(function(){ loading.modal("hide"); }, 2000);
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}




OtraParcela(p03_id_otra_parcela);
async function OtraParcela(id_otra_parcela){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/lote/get-otra-parcela',
                method: 'POST',
                data:{_csrf:csrf,id_otra_parcela:id_otra_parcela},
                dataType:'Json',
                beforeSend:function(){
                    loading.modal("show");
                },
                success:function(results)
                {   
                    
                    if(results && results.success){
                        $("#p03_nro_parcela").val(results.otra_parcela.NUM_PARCELA);
                        $("#p03_superficie_total").val(results.otra_parcela.NUM_SUP_TOTAL);
                        $("#p03_equivalencia").val(results.otra_parcela.NUM_EQ_PARCELA);
                        UnidadesMedidas(results.otra_parcela.ID_UM_PARCELA);
                        //loading.modal("hide");
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}
</script>