

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Encuesta</h4>
                    <a class="btn btn-primary" href="<?= \Yii::$app->request->BaseUrl ?>/encuesta/index?geocodigo_fdo=<?= $geocodigo_fdo ?>">< Regresar</a>
                </div>
            </div>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Parcelas</li>
                </ol>
            </div>

        </div>
    </div>
</div>

<?php $form = ActiveForm::begin(['options' => ['id' => 'formCapituloXInfraestructuras']]); ?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">1001. ¿Hoy que tipo de instalaciones e infraestructura tiene la parcela?</label>
                            <select name="CapituloXInfraestructuras[cap10_p1001]" id="cap10_p1001"class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row cap10_p1001_888" style="display:none">
                    <div class="col-md-6" >
                        <div class="form-group">
                            <label for="">Especifique</label>
                            <input type="text" class="form-control" id="cap10_p1001_888_especifique" placeholder="Especifique" name="CapituloXInfraestructuras[cap10_p1001_888_especifique]" value="<?= $cap_x['TXT_OTRO'] ?>" maxlength="250">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">1002. ¿Cuál es el área de las instalaciones e infraestructura? (m2)</label>
                            <input type="text" class="form-control numerico" name="CapituloXInfraestructuras[cap10_p1002]" id="cap10_p1002" value="<?= $cap_x['NUM_AREA_INFRAESTRUCTURA'] ?>" maxlength="13">
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">1003. ¿Cuál es el estado de las instalaciones e infraestructura?</label>
                            <select name="CapituloXInfraestructuras[cap10_p1003]" id="cap10_p1003"class="form-control">
                                <option value>Seleccionar</option>
                                <option value="1" <?= (($cap_x['TIP_ESTADO']=="1")?"selected":"") ?>>Buena</option>
                                <option value="2" <?= (($cap_x['TIP_ESTADO']=="2")?"selected":"") ?>>Regular</option>
                                <option value="3" <?= (($cap_x['TIP_ESTADO']=="3")?"selected":"") ?>>Mala</option>
                            </select>
                        </div>
                    </div>
                </div>

                
                <hr>
                <button class="btn btn-success btn-grabar-capitulo-x-infraestructuras">Grabar</button>

            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<?php ActiveForm::end(); ?>

<script>

var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var loading =   $('#staticBackdrop');
var id_infraestructura = "<?= $cap_x['ID_INFRAESTRUCTURA'] ?>";

$("body").on("click", ".btn-grabar-capitulo-x-infraestructuras", function (e) {

    e.preventDefault();

    var form = $("#formCapituloXInfraestructuras");
    var formData = $("#formCapituloXInfraestructuras").serializeArray();

    if (form.find(".has-error").length) {
        return false;
    }

    $.ajax({
        url:form.attr("action"),
        type: form.attr("method"),
        data: formData,
        dataType: "json",
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        beforeSend:function()
        {
            loading.modal("show");
        },
        success: function (results) {
            if(results.success){
                setTimeout(function(){ loading.modal("hide"); }, 2000);
            }
        },
    });
});

Infraestructuras(id_infraestructura)
async function Infraestructuras(id_infraestructura){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/capitulox/get-lista-infraestructuras',
                method: 'POST',
                data:{_csrf:csrf},
                dataType:'Json',
                beforeSend:function(){
                    loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var opciones_infraestructuras ="<option value>Seleccionar</option>";
                        $.each(results.infraestructuras, function( index, value ) {
                            opciones_infraestructuras = opciones_infraestructuras + "<option value='" + value.COD_INFRA_INST + "'>" + value.TXT_DESCRIPCION + "</option>";
                        });
                        $("#cap10_p1001").html(opciones_infraestructuras);
                        if(id_infraestructura){
                            $("#cap10_p1001").val(id_infraestructura);

                            if(id_infraestructura=="888"){
                                $(".cap10_p1001_888").show()
                            }
                        }
                        setTimeout(function(){ loading.modal("hide"); }, 2000);
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}

$("body").on("change", "#cap10_p1001", function (e) {
    e.preventDefault();
    $(".cap10_p1001_888").hide();
    $("#cap10_p1001_888_especifique").val("");
    if($("[id=\"cap10_p1001\"]").val()=="888"){
        $(".cap10_p1001_888").show()
    }
});

</script>