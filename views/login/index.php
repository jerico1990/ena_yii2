<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<div class="account-pages my-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 col-lg-6 col-xl-5">
                <div class="card overflow-hidden">
                    <div class="bg-success">
                        <div class="row">
                            <div class="col-9">
                                <div class="text-white p-4">
                                    <h5 class="text-white">MIDAGRI - Perú</h5>
                                    <p>Ingrese su sesión</p>
                                </div>
                            </div>
                            <div class="col-3 align-self-end">
                                <img src="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/images/profile-img.png" alt="" class="img-fluid">
                            </div>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        <div>
                            <a href="#">
                                <div class="avatar-md profile-user-wid mb-4">
                                    <span class="avatar-title rounded-circle bg-success">
                                        <img src="<?= \Yii::$app->request->BaseUrl ?>/img/midagri_login.jpg" alt="" class="rounded-circle" height="54">
                                    </span>
                                </div>
                            </a>
                        </div>
                        <div class="p-2">
                            <?php $form = ActiveForm::begin(); ?>

                                <div class="form-group">
                                    <label for="username">Usuario</label>
                                    <input type="text" class="form-control" id="username" name="LoginForm[username]" placeholder="Ingresa usuario">
                                </div>

                                <div class="form-group">
                                    <label for="userpassword">Clave</label>
                                    <input type="password" class="form-control" id="password" name="LoginForm[password]" placeholder="Ingresa clave">
                                </div>

                                <!-- <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customControlInline">
                                    <label class="custom-control-label" for="customControlInline">Remember
                                        me</label>
                                </div> -->

                                <div class="mt-3">
                                    <button class="btn btn-success btn-block waves-effect waves-light" type="submit">Ingresar</button>
                                </div>
<!-- 
                                <div class="mt-4 text-center">
                                    <h5 class="font-size-14 mb-3"> <a href="<?= \Yii::$app->request->BaseUrl ?>/registrar">Registrate</a> </h5>
                                </div> -->

<!-- 
                                <div class="mt-4 text-center">
                                    <h5 class="font-size-14 mb-3">Sign in with</h5>

                                </div>

                                <div class="mt-4 text-center">
                                    <a href="auth-recoverpw.html" class="text-muted"><i
                                            class="mdi mdi-lock mr-1"></i> Forgot your password?</a>
                                </div> -->
                            <?php ActiveForm::end(); ?>
                        </div>

                    </div>
                </div>
                <div class="mt-5 text-center">
                    <div>
                        <!-- <p>Don't have an account ? <a href="auth-register.html"
                                class="font-weight-medium text-primary"> Signup now </a> </p> -->
                        <p>© 2021 - MIDAGRI | PERÚ. </p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>