<link rel="stylesheet" href="https://js.arcgis.com/4.15/esri/themes/light/main.css"/>
<script src="https://js.arcgis.com/4.15/"></script>
<style>
    html,
    body,
    #viewDiv {
        padding: 0;
        margin: 0;
        height: 100%;
        width: 100%;
    }

    #seasons-filter {
        height: 470px;
        width: 100%;
        visibility: hidden;
    }

    .season-item {
        width: 100%;
        padding: 12px;
        text-align: center;
        vertical-align: baseline;
        cursor: pointer;
        height: 40px;
    }

    .season-item:focus {
        background-color: dimgrey;
    }

    .season-item:hover {
        background-color: dimgrey;
    }

    #titleDiv {
        padding: 10px;
    }

    #titleText {
        font-size: 20pt;
        font-weight: 60;
        padding-bottom: 10px;
    }

    .panel-container {
        position: relative;
        width: 100%;
        height: 100%;
    }

    .panel-side {
        padding: 2px;
        box-sizing: border-box;
        width: 300px;
        height: 100%;
        position: absolute;
        top: 0;
        right: 0;
        color: #fff;
        background-color: rgba(0, 0, 0, 0.6);
        overflow: auto;
        z-index: 60;
    }

    .panel-side h3 {
        padding: 0 20px;
        margin: 20px 0;
    }

    .panel-side ul {
        list-style: none;
        margin: 0;
        padding: 0;
    }

    .panel-side li {
        list-style: none;
        padding: 10px 20px;
    }

    .panel-result {
        cursor: pointer;
        margin: 2px 0;
        background-color: rgba(0, 0, 0, 0.3);
    }

    .panel-result:hover,
    .panel-result:focus {
        color: orange;
        background-color: rgba(0, 0, 0, 0.75);
    }
</style>



<input type="hidden" id="geocodigo_fdo" value="<?= $geocodigo_fdo ?>">
<div class="panel-container" style="height:650px;width:100%">
    <div class="panel-side esri-widget">
        <ul id="list_graphics">
        <li>Loading&hellip;</li>
        </ul>
    </div>
    <div id="viewDiv"></div>
    <div id="titleDiv" class="esri-widget">
        <div id="titleText">Marco de lista - MIDAGRI</div>
        <div>ENA (2021 - 2022)</div>
    </div>
</div>

<script src="<?= \Yii::$app->request->BaseUrl ?>/js/gisparcela.js"></script>