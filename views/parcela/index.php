
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Parcelas</h4>
                </div>
            </div>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Parcelas</li>
                </ol>
            </div>

        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">&nbsp&nbsp</label> <br>
                            <button class="btn btn-success btn-agregar-parcela">Agregar</button>
                        </div>
                    </div>
                </div>

                <table id="lista-parcelas" class="table table-bordered dt-responsive ">
                    <thead>
                        <tr>
                            <th>Nro fundo</th>
                            <th>Geocodigo Fundo</th>
                            <th>Estado</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<div id="modal" class="fade modal" role="dialog"  tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        </div>
    </div>
</div>



<script>
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var loading =   $('#staticBackdrop');


$('#lista-parcelas').DataTable();

Parcelas();
async function Parcelas(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/parcela/get-lista-parcelas',
                method: 'POST',
                data:{_csrf:csrf},
                dataType:'Json',
                beforeSend:function(){
                    loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var parcelas ="";
                        $('#lista-parcelas').DataTable().destroy();
                        $.each(results.parcelas, function( index, value ) {
                            var estado = "No iniciado";
                            if(value.FLG_ESTADO==1){
                                estado = "En proceso";
                            }else if(value.FLG_ESTADO==2){
                                estado = "Finalizado";
                            }
                            parcelas = parcelas + "<tr>";
                                parcelas = parcelas + "<td> " + value.NRO_FDO + "</td>";
                                parcelas = parcelas + "<td>" + value.GEOCODIGO_FDO + "</td>";
                                parcelas = parcelas + "<td>" + estado + "</td>";

                                parcelas = parcelas + "<td>";
                                    if(value.FLG_ESTADO == null || value.FLG_ESTADO == "1"){
                                        parcelas = parcelas + '<a href="<?= \Yii::$app->request->BaseUrl ?>/encuesta/index?geocodigo_fdo='+ value.GEOCODIGO_FDO +'" class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> ';
                                    }else if(value.FLG_ESTADO == "2") {
                                        parcelas = parcelas + '<a href="<?= \Yii::$app->request->BaseUrl ?>/encuesta/view?id_encuesta='+ value.ID_ENCUESTA +'" class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> ';
                                    }
                                    

                                    parcelas = parcelas + '<a href="<?= \Yii::$app->request->BaseUrl ?>/parcela/gis?geocodigo_fdo=' + value.GEOCODIGO_FDO + '" target="_blank" class="btn btn-primary" > Mapa </a> ';

                                parcelas = parcelas +"</td>";
                            parcelas = parcelas + "</tr>";
                        });
                        
                        $('#lista-parcelas tbody').html(parcelas);
                        $('#lista-parcelas').DataTable({
                            "paging": true,
                            "lengthChange": true,
                            "searching": false,
                            "ordering": true,
                            "info": true,
                            "autoWidth": false,
                            "pageLength" : 100,
                            "language": {
                                "sProcessing":    "Procesando...",
                                "sLengthMenu":    "Mostrar _MENU_ registros",
                                "sZeroRecords":   "No se encontraron resultados",
                                "sEmptyTable":    "Ningun dato disponible en esta lista",
                                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":   "",
                                "sSearch":        "Buscar:",
                                "sUrl":           "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":    "Último",
                                    "sNext":    "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            },
                        });

                        setTimeout(function(){ loading.modal("hide"); }, 2000);
                        
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}


$("body").on("click", ".btn-agregar-parcela", function (e) {
    Swal.fire({
        title: '¿Está seguro de agregar una nueva parcela?',
        //text: "Una vez finalizado la encuesta no se podrá editar ni modificar la información",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, agregar parcela',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
    if (result.isConfirmed) {

        $.ajax({
                url: "<?= \Yii::$app->request->BaseUrl ?>/parcela/create",
                method: "POST",
                data:{_csrf:csrf},
                dataType:"Json",
                beforeSend:function(xhr, settings)
                {
                    loading.modal("show");
                },
                success:function(results)
                {
                    if(results.success){
                        Parcelas();
                    }
                    
                },
                error:function(){
                    alert("Error al realizar el proceso.");
                }
            });
        
    }
    })
});

</script>
