
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>


<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Encuesta</h4>
                </div>
            </div>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Parcelas</li>
                    <li class="breadcrumb-item ">Encuesta</li>
                </ol>
            </div>

        </div>
    </div>
</div>
<?php $form = ActiveForm::begin(['action'=>\Yii::$app->request->BaseUrl.'/encuesta/update?id_encuesta='.$id_encuesta,'options' => ['id' => 'formEncuesta']]); ?>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h1>ENCUESTA NACIONAL AGRARIA - ENA, ENERO 2020 - JULIO 2021 <br> SISTEMAS DE PRODUCCIÓN DE AGRICULTURA FAMILIAR Y NO FAMILIAR. MINISTERIO DE DESARROLLO AGRARIO Y RIEGO-MIDAGRI</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 text-left">Secreto estadístico: Información confidencial amparada por Decreto Supremo No 043-2001-PCM </div>
                    <div class="col-md-6 text-right">Cuestonario No VO-13/10/2020</div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <p class="text-muted text-truncate">UBICACIÓN GEOGRÁFICA DEL SEGMENTO Y LA PARCELA AGRARIA: <span> <b> <?= $marcoLista['GEOCODIGO_FDO']?> </b></span></p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">UBIGEO:</label>
                            <input type="text" class="form-control" id="general_ubigeo" disabled value="<?= $marcoLista['IDDIST']?>">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">REGIÓN NATURAL:</label>
                            <input type="text" class="form-control" id="general_region_natural" disabled value="<?= $marcoLista['REG_NAT']?>">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">PISO ECOLÓGICO:</label>
                            <input type="text" class="form-control" id="general_piso_ecologico" disabled value="<?= $marcoLista['PISO_ECO']?>">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">SUBESTRATO:</label>
                            <input type="text" class="form-control" id="general_subestrato" disabled value="">
                        </div>
                    </div>
                    <!-- <div class="col-md-4">
                        <div class="form-group">
                            <label for="">TIPO DE GRILLA:</label>
                            <input type="text" class="form-control" disabled value="">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">NÚMERO DEL SERPENTÍN:</label>
                            <input type="text" class="form-control" disabled value="">
                        </div>
                    </div> -->
                </div>

                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="">NOMBRE DE LA COMUNIDAD CAMPESINA O COMUNIDAD NATIVA:</label>
                            <input type="text" class="form-control" id="general_comunidad_campesina" disabled value="">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">CÓDIGO:</label>
                            <input type="text" class="form-control" id="general_codigo" disabled value="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">NÚMERO DE SEGMENTO/EMPRESA:</label>
                            <input type="text" class="form-control" id="general_numero_segmento" disabled value="">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">NÚMERO DE PARCELA EN EL SEGMENTO/EN LA EMPRESA:</label>
                            <input type="text" class="form-control" name="Encuesta[general_numero_parcela]" id="general_numero_parcela" title="El número de parcela en el segmento">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">TOTAL PARCELAS EN EL SEGMENTO/EMPRESA:</label>
                            <input type="text" class="form-control" name="Encuesta[general_total_parcela]" id="general_total_parcela">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">REGIÓN:</label>
                            <input type="text" class="form-control" id="general_region" disabled value="">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">PROVINCIA:</label>
                            <select name="Encuesta[general_provincia]" id="general_provincia" class="form-control">
                                <option value>SELECCIONAR</option>
                            </select>
                            <!-- <input type="text" class="form-control" id="general_provincia" disabled value=""> -->
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">DISTRITO:</label>
                            <select name="Encuesta[general_distrito]" id="general_distrito" class="form-control">
                                <option value>SELECCIONAR</option>
                            </select>
                            <!-- <input type="text" class="form-control" id="general_distrito" disabled value=""> -->
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">NOMBRE CENTRO POBLADO MÁS CERCANO:</label>
                            <select name="Encuesta[general_centro_poblado]" id="general_centro_poblado" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<!-- end page title -->

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#capitulo_i" role="tab">
                            <span class="d-block d-sm-none"><i class="fas fa-home"></i> I</span>
                            <span class="d-none d-sm-block">CAPÍTULO I</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#capitulo_ii" role="tab">
                            <span class="d-block d-sm-none"><i class="fas fa-home"></i> II</span>
                            <span class="d-none d-sm-block">CAPÍTULO II</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#capitulo_iii" role="tab">
                            <span class="d-block d-sm-none"><i class="fas fa-home"></i> III</span>
                            <span class="d-none d-sm-block">CAPÍTULO III</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#capitulo_iv" role="tab">
                            <span class="d-block d-sm-none"><i class="fas fa-home"></i> IV</span>
                            <span class="d-none d-sm-block">CAPÍTULO IV</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#capitulo_v" role="tab">
                            <span class="d-block d-sm-none"><i class="fas fa-home"></i> V</span>
                            <span class="d-none d-sm-block">CAPÍTULO V</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#capitulo_vi" role="tab">
                            <span class="d-block d-sm-none"><i class="fas fa-home"></i> VI</span>
                            <span class="d-none d-sm-block">CAPÍTULO VI</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#capitulo_vii" role="tab">
                            <span class="d-block d-sm-none"><i class="fas fa-home"></i> VII</span>
                            <span class="d-none d-sm-block">CAPÍTULO VII</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#capitulo_viii" role="tab">
                            <span class="d-block d-sm-none"><i class="fas fa-home"></i> VIII</span>
                            <span class="d-none d-sm-block">CAPÍTULO VIII</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#capitulo_ix" role="tab">
                            <span class="d-block d-sm-none"><i class="fas fa-home"></i> IX</span>
                            <span class="d-none d-sm-block">CAPÍTULO IX</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#capitulo_x" role="tab">
                            <span class="d-block d-sm-none"><i class="fas fa-home"></i> X</span>
                            <span class="d-none d-sm-block">CAPÍTULO X</span>
                        </a>
                    </li>
                    <!-- <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#capitulo_xi" role="tab">
                            <span class="d-block d-sm-none"><i class="fas fa-home"></i> XI</span>
                            <span class="d-none d-sm-block">CAPÍTULO XI</span>
                        </a>
                    </li> -->
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#capitulo_xii" role="tab">
                            <span class="d-block d-sm-none"><i class="fas fa-home"></i> XII</span>
                            <span class="d-none d-sm-block">CAPÍTULO XII</span>
                        </a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content p-3 text-muted">
                    <div class="tab-pane active" id="capitulo_i" role="tabpanel">
                        <div class="row">
                            <label for="">101. Desde enero de 2020 hasta hoy, ¿Usted realizó actividad</label>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="p01_1" >
                                        <label class="custom-control-label" for="p01_1">¿Agrícola?</label>
                                    </div>

                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="p01_2" >
                                        <label class="custom-control-label" for="p01_2">¿Pecuaria?</label>
                                    </div>

                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="p01_3" >
                                        <label class="custom-control-label" for="p01_3">¿Vivero y/o almácigo y/o semillero y/o huerto
                                            semillero?</label>
                                    </div>

                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="p01_4" >
                                        <label class="custom-control-label" for="p01_4">¿Extracción (productos de los bosques naturales,
                                            etc)?</label>
                                    </div>

                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="p01_5" >
                                        <label class="custom-control-label" for="p01_5">¿Extracción (petróleo, gas, generación y
                                            transmisión de energía, minería, etc) ?</label>
                                    </div>


                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="p01_6" >
                                        <label class="custom-control-label" for="p01_6">¿Transformación (producción de chancaca,
                                            productos lácteos, produccion de biogas, artesanías, etc)?</label>
                                    </div>

                                </div>

                                <div class="col-md-6">
                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="p01_7" >
                                        <label class="custom-control-label" for="p01_7">¿Comercio (venta de productos agrarios y no
                                            agrarios (artesanías) ?</label>
                                    </div>

                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="p01_8" >
                                        <label class="custom-control-label" for="p01_8">¿Servicios (alquiler de maquinaria, taller de
                                            mecánica, agroturismo, servicios ambientales, etc)?</label>
                                    </div>

                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="p01_9" >
                                        <label class="custom-control-label" for="p01_9">¿Pesca artesanal?</label>
                                    </div>

                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="p01_10" >
                                        <label class="custom-control-label" for="p01_10">¿Acuicultura (cría y engorde de peces y
                                            camarones)?</label>
                                    </div>

                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="p01_11" >
                                        <label class="custom-control-label" for="p01_11">¿Tiene infraestructura agraria pero no está
                                            activa los últimos 12 meses? </label>
                                    </div>

                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="p01_12" >
                                        <label class="custom-control-label" for="p01_12">Ninguna.</label>
                                    </div>

                                </div>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for="">Observación</label>
                                <textarea name="Encuesta[p01_observacion]" id="p01_observacion" cols="30" rows="5" maxlength="500" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="capitulo_ii" role="tabpanel">
                        <div class="row">
                            <label for="">201. Usted/PRODUCTOR es</label> 
                            <div class="col-md-12"></div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="p02_tipo_productor_1" name="Encuesta[p02_tipo_productor]"
                                            class="custom-control-input " value="1">
                                        <label class="custom-control-label" for="p02_tipo_productor_1">¿Persona natural?</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="p02_tipo_productor_2" name="Encuesta[p02_tipo_productor]"
                                            class="custom-control-input " value="2">
                                        <label class="custom-control-label" for="p02_tipo_productor_2">¿Persona jurídica?</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <!-- MODULO A-->
                        <div class="capitulo_ii_modulo_a" style="display:none">
                            <div class="row">
                                <label for="">202. ¿En esta parcela quién o quiénes toman las principales decisiones de la actividad agraria:</label> 
                                <div class="col-md-12"></div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="p02_a_toma_decision_4" name="Encuesta[p02_a_toma_decision]"
                                                class="custom-control-input " value="4">
                                            <label class="custom-control-label" for="p02_a_toma_decision_4">El productor?</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="p02_a_toma_decision_5" name="Encuesta[p02_a_toma_decision]"
                                                class="custom-control-input " value="5">
                                            <label class="custom-control-label" for="p02_a_toma_decision_5">La productora?</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="p02_a_toma_decision_6" name="Encuesta[p02_a_toma_decision]"
                                                class="custom-control-input " value="6">
                                            <label class="custom-control-label" for="p02_a_toma_decision_6"> En Conjunto? (Esposo-esposa; Esposo-hijo/hija; Esposa-hijo/hija, otro)</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-2">
                                <div class="col-md-6" style="padding-left:0px">
                                    <div class="form-group">
                                        <label for="">203. ¿Cuáles son los apellidos y nombres del productor/a?</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6" style="padding-left:0px">
                                    <div class="form-group">
                                        <label for="">Nombres</label>
                                        <input type="text" class="form-control texto" id="p02_a_representante_nombres" placeholder="Nombres del productor" name="Encuesta[p02_a_representante_nombres]" maxlength="250">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Apellidos</label>
                                        <input type="text" class="form-control texto" id="p02_a_representante_apellidos" placeholder="Apellidos del productor" name="Encuesta[p02_a_representante_apellidos]" maxlength="250">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6" style="padding-left:0px">
                                    <div class="form-group">
                                        <label for="">204. ¿Cuál es su N° de DNI?</label>
                                        <input type="text" class="form-control numerico" id="p02_a_txt_dni" placeholder="Nombres del productor" name="Encuesta[p02_a_txt_dni]" maxlength="11">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6" style="padding-left:0px">
                                    <div class="form-group">
                                        <label for="">205. ¿Cuál es su número de teléfono/celular?</label>
                                        <select name="Encuesta[p02_a_representante_tiene_numero]" class="form-control" id="p02_a_representante_tiene_numero">
                                            <option value>¿Tiene número de teléfono/celular?</option>
                                            <option value="1">Si</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row p02_a_representante_tiene_numero" style="display:none">
                                <div class="col-md-6" style="padding-left:0px">
                                    <div class="form-group">
                                        <label for="">1. Fijo N°</label>
                                        <input type="text" class="form-control numerico" id="p02_a_representante_telefono_fijo" placeholder="Teléfono fijo" name="Encuesta[p02_a_representante_telefono_fijo]" maxlength="7">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">2. Celular N°:</label>
                                        <input type="text" class="form-control numerico" id="p02_a_representante_telefono_celular" placeholder="Celular" name="Encuesta[p02_a_representante_telefono_celular]" maxlength="9">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6" style="padding-left:0px">
                                    <div class="form-group">
                                        <label for="">206. ¿Cuál es el correo electrónico?</label>
                                        <select name="Encuesta[p02_a_tiene_correo_electronico]" class="form-control" id="p02_a_tiene_correo_electronico">
                                            <option value>¿Tiene correo electrónico?</option>
                                            <option value="1">Si</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6 p02_a_tiene_correo_electronico" style="display:none">
                                    <div class="form-group">
                                        <label for="">Correo electrónico</label>
                                        <input type="text" class="form-control" id="p02_a_correo_electronico" placeholder="Correo electrónico" name="Encuesta[p02_a_correo_electronico]" maxlength="250">
                                    </div>
                                </div>
                            </div>

                            

                            
                            
                            
                            <div class="row">
                                <label for="">207. La información fue proporcionada por:</label> 
                                <div class="col-md-12"></div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="p02_a_informacion_proporcionada_7" name="Encuesta[p02_a_informacion_proporcionada]"
                                                class="custom-control-input " value="7">
                                            <label class="custom-control-label" for="p02_a_informacion_proporcionada_7">¿Productor(a) agrario(a)? </label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="p02_a_informacion_proporcionada_8" name="Encuesta[p02_a_informacion_proporcionada]"
                                                class="custom-control-input " value="8">
                                            <label class="custom-control-label" for="p02_a_informacion_proporcionada_8">¿Administrador(a)?</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="p02_a_informacion_proporcionada_9" name="Encuesta[p02_a_informacion_proporcionada]"
                                                class="custom-control-input " value="9">
                                            <label class="custom-control-label" for="p02_a_informacion_proporcionada_9">¿Empleado(a)?</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="p02_a_informacion_proporcionada_10" name="Encuesta[p02_a_informacion_proporcionada]"
                                                class="custom-control-input " value="10">
                                            <label class="custom-control-label" for="p02_a_informacion_proporcionada_10">¿Familiar?</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="p02_a_informacion_proporcionada_11" name="Encuesta[p02_a_informacion_proporcionada]"
                                                class="custom-control-input " value="11">
                                            <label class="custom-control-label" for="p02_a_informacion_proporcionada_11">¿Encargado?</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="p02_a_informacion_proporcionada_12" name="Encuesta[p02_a_informacion_proporcionada]"
                                                class="custom-control-input " value="12">
                                            <label class="custom-control-label" for="p02_a_informacion_proporcionada_12">Otro:</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row p02_a_informacion_proporcionada_12 mt-2" style="display:none">
                                <div class="col-md-6" style="padding-left:0px">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="p02_a_informacion_proporcionada_especifique" placeholder="Especifique" name="Encuesta[p02_a_informacion_proporcionada_especifique]" maxlength="250">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6" style="padding-left:0px">
                                    <div class="form-group">
                                        <label for="">¿El informante es el productor?</label>
                                        <select name="Encuesta[p02_a_es_productor]" class="form-control" id="p02_a_es_productor">
                                            <option value='99999'>Seleccionar</option>
                                            <option value="1">Si</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                </div>
                            </div>


                            <div class="p02_a_es_productor" style="display:none">
                                <div class="row">
                                    <div class="col-md-6" style="padding-left:0px">
                                        <div class="form-group">
                                            <label for="">208. ¿Cuáles son los apellidos y nombres del informante?</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6" style="padding-left:0px">
                                        <div class="form-group">
                                            <label for="">Nombres</label>
                                            <input type="text" class="form-control texto" id="p02_a_informante_nombres" placeholder="Nombres" name="Encuesta[p02_a_informante_nombres]" maxlength="250">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Apellidos</label>
                                            <input type="text" class="form-control texto" id="p02_a_informante_apellidos" placeholder="Apellidos" name="Encuesta[p02_a_informante_apellidos]" maxlength="250">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6" style="padding-left:0px">
                                        <div class="form-group">
                                            <label for="">209. ¿Cuál es su número de teléfono/celular?</label>
                                            <select name="Encuesta[p02_a_informante_tiene_numero]" class="form-control" id="p02_a_informante_tiene_numero">
                                                <option value>¿Tiene número de teléfono/celular?</option>
                                                <option value="1">Si</option>
                                                <option value="0">No</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row p02_a_informante_tiene_numero" style="display:none">
                                    <div class="col-md-6" style="padding-left:0px">
                                        <div class="form-group">
                                            <label for="">1. Fijo N°</label>
                                            <input type="text" class="form-control numerico" id="p02_a_informante_telefono_fijo" placeholder="Teléfono fijo" name="Encuesta[p02_a_informante_telefono_fijo]"  maxlength="7">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">2. Celular N°:</label>
                                            <input type="text" class="form-control numerico" id="p02_a_informante_telefono_celular" placeholder="Celular" name="Encuesta[p02_a_informante_telefono_celular]"  maxlength="9">
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <label for="">210. El productor/a agrario/a vive:</label> 
                                    <div class="col-md-12"></div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="p02_a_productor_agrario_vive_13" name="Encuesta[p02_a_productor_agrario_vive]"
                                                    class="custom-control-input " value="13">
                                                <label class="custom-control-label" for="p02_a_productor_agrario_vive_13">¿En esta parcela?</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="p02_a_productor_agrario_vive_14" name="Encuesta[p02_a_productor_agrario_vive]"
                                                    class="custom-control-input " value="14">
                                                <label class="custom-control-label" for="p02_a_productor_agrario_vive_14">¿En otra parcela dentro del distrito?</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="p02_a_productor_agrario_vive_15" name="Encuesta[p02_a_productor_agrario_vive]"
                                                    class="custom-control-input " value="15">
                                                <label class="custom-control-label" for="p02_a_productor_agrario_vive_15">Otro:</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row p02_a_productor_agrario_vive_15 mt-2" style="display:none">
                                    <div class="col-md-6" style="padding-left:0px">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="p02_a_productor_agrario_vive_especifique" placeholder="Especifique" name="Encuesta[p02_a_productor_agrario_vive_especifique]" maxlength="250">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4" style="padding-left:0px">
                                        <div class="form-group">
                                            <label for="">Departamento</label>
                                            <select data-capitulo="2" data-modulo="a" id="p02_a_informante_departamento" class="form-control" name="Encuesta[p02_a_informante_departamento]">
                                                <option value='99999'>Seleccionar Departamento</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="">Provincia</label>
                                            <select data-capitulo="2" data-modulo="a" id="p02_a_informante_provincia" class="form-control" name="Encuesta[p02_a_informante_provincia]">
                                                <option value='99999'>Seleccionar Provincia</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="">Distrito</label>
                                            <select data-capitulo="2" data-modulo="a" id="p02_a_informante_distrito" class="form-control" name="Encuesta[p02_a_informante_distrito]">
                                                <option value='99999'>Seleccionar Distrito</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8" style="padding-left:0px">
                                        <div class="form-group">
                                            <label for="">Centro poblado</label>
                                            <select data-capitulo="2" data-modulo="a" id="p02_a_informante_centro_poblado" class="form-control" name="Encuesta[p02_a_informante_centro_poblado]">
                                                <option value='99999'>Seleccionar Centro poblado</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                    <div class="col-md-6" style="padding-left:0px">
                                        <div class="form-group">
                                            <label for="">211. ¿Usted pertenece a una comunidad campesina/comunidad nativa?</label>
                                            <select name="Encuesta[p02_a_pertenece_comunidad]" class="form-control" id="p02_a_pertenece_comunidad">
                                                <option value='99999'>Seleccionar la opción</option>
                                                <option value="1">Si</option>
                                                <option value="0">No</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                        </div>


                        <!-- MODULO B-->
                        <div class="capitulo_ii_modulo_b" style="display:none">
                            <div class="row">
                                <div class="col-md-12" style="padding-left:0px">
                                    <div class="form-group">
                                        <label for="">212. ¿Cuál es la razón social de productor/a agrario/a?</label>
                                        <input type="text" class="form-control" id="p02_b_razon_social" placeholder="Razón social de productor/a agrario/a" name="Encuesta[p02_b_razon_social]" maxlength="250">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12" style="padding-left:0px">
                                    <div class="form-group">
                                        <label for="">213. ¿Cuál es el número de RUC?</label>
                                        <input type="text" class="form-control numerico" id="p02_b_ruc" placeholder="Número de RUC" name="Encuesta[p02_b_ruc]" maxlength="11">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6" style="padding-left:0px">
                                    <div class="form-group">
                                        <label for="">214. ¿Cuáles son los apellidos y nombres del representante legal:</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6" style="padding-left:0px">
                                    <div class="form-group">
                                        <label for="">Nombres</label>
                                        <input type="text" class="form-control texto" id="p02_b_representante_legal_nombres" placeholder="Nombres del representate legal" name="Encuesta[p02_b_representante_legal_nombres]" maxlength="250">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Apellidos</label>
                                        <input type="text" class="form-control texto" id="p02_b_representante_legal_apellidos" placeholder="Apellidos del representate legal" name="Encuesta[p02_b_representante_legal_apellidos]" maxlength="250">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6" style="padding-left:0px">
                                    <div class="form-group">
                                        <label for="">215. ¿Cuál es el nombre del Centro Poblado o Ciudad donde está ubicada la empresa? Seleccione Departamento, Provincia y distrito.</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4" style="padding-left:0px">
                                    <div class="form-group">
                                        <label for="">Departamento</label>
                                        <select data-capitulo="2" data-modulo="b" id="p02_b_representante_departamento" class="form-control" name="Encuesta[p02_b_representante_departamento]">
                                            <option value='99999'>Seleccionar Departamento</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">Provincia</label>
                                        <select data-capitulo="2" data-modulo="b" id="p02_b_representante_provincia" class="form-control" name="Encuesta[p02_b_representante_provincia]">
                                            <option value='99999'>Seleccionar Provincia</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">Distrito</label>
                                        <select data-capitulo="2" data-modulo="b" id="p02_b_representante_distrito" class="form-control" name="Encuesta[p02_b_representante_distrito]">
                                            <option value='99999'>Seleccionar Distrito</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8" style="padding-left:0px">
                                    <div class="form-group">
                                        <label for="">Centro poblado</label>
                                        <select data-capitulo="2" data-modulo="b" id="p02_b_representante_centro_poblado" class="form-control" name="Encuesta[p02_b_representante_centro_poblado]">
                                            <option value='99999'>Seleccionar Centro poblado</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">Codigo</label>
                                        <input type="text" class="form-control" id="p02_b_representante_codigo_centro_poblado" name="Encuesta[p02_b_representante_codigo_centro_poblado]" placeholder="Codigo" disabled>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6" style="padding-left:0px">
                                    <div class="form-group">
                                        <label for="">216. ¿Cuál es el domicilio fiscal de la empresa?</label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <label for="">Tipo de Vía:</label>
                                <div class="col-md-12"></div>
                                <div class="row">
                                    <div class="col-md-12" >
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="p02_b_tipo_via_16" name="Encuesta[p02_b_tipo_via]"
                                                class="custom-control-input " value="16">
                                            <label class="custom-control-label" for="p02_b_tipo_via_16">Avenida</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="p02_b_tipo_via_17" name="Encuesta[p02_b_tipo_via]"
                                                class="custom-control-input " value="17">
                                            <label class="custom-control-label" for="p02_b_tipo_via_17">Calle</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="p02_b_tipo_via_18" name="Encuesta[p02_b_tipo_via]"
                                                class="custom-control-input " value="18">
                                            <label class="custom-control-label" for="p02_b_tipo_via_18">Jirón</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="p02_b_tipo_via_19" name="Encuesta[p02_b_tipo_via]"
                                                class="custom-control-input " value="19">
                                            <label class="custom-control-label" for="p02_b_tipo_via_19">Pasaje</label>
                                        </div>

                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="p02_b_tipo_via_20" name="Encuesta[p02_b_tipo_via]"
                                                class="custom-control-input " value="20">
                                            <label class="custom-control-label" for="p02_b_tipo_via_20">Carretera</label>
                                        </div>

                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="p02_b_tipo_via_21" name="Encuesta[p02_b_tipo_via]"
                                                class="custom-control-input " value="21">
                                            <label class="custom-control-label" for="p02_b_tipo_via_21">Otro</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row p02_b_tipo_via_21 mt-2" style="display:none">
                                <div class="col-md-6" style="padding-left:0px">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="p02_b_tipo_via_especifique" placeholder="Especifique" name="Encuesta[p02_b_tipo_via_especifique]" maxlength="250">
                                    </div>
                                </div>
                            </div>



                            <div class="row mt-2">
                                <div class="col-md-12" style="padding-left:0px">
                                    <div class="form-group"> 
                                        <label for="">Nombre de la avenida, calle, jirón, pasaje, carretera, etc:</label>
                                        <input type="text" class="form-control" id="p02_b_avenida" placeholder="Nombre de la avenida, calle, jirón, pasaje, carretera, etc" name="Encuesta[p02_b_avenida]" maxlength="250">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4" style="padding-left:0px">
                                    <div class="form-group">
                                        <label for="">1. N° Puerta:</label>
                                        <input type="text" class="form-control" id="p02_b_nro_puerta" placeholder="N° Puerta" name="Encuesta[p02_b_nro_puerta]" maxlength="6">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">2. Block:</label>
                                        <input type="text" class="form-control" id="p02_b_block" placeholder="Block" name="Encuesta[p02_b_block]" maxlength="6">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">3. Interior:</label>
                                        <input type="text" class="form-control" id="p02_b_interior" placeholder="Interior" name="Encuesta[p02_b_interior]" maxlength="6">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4" style="padding-left:0px">
                                    <div class="form-group">
                                        <label for="">4. Piso:</label>
                                        <input type="text" class="form-control" id="p02_b_piso" placeholder="Piso" name="Encuesta[p02_b_piso]" maxlength="6">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">5. Manzana:</label>
                                        <input type="text" class="form-control" id="p02_b_manzana" placeholder="Manzana" name="Encuesta[p02_b_manzana]" maxlength="6">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">6. Lote:</label>
                                        <input type="text" class="form-control" id="p02_b_lote" placeholder="Lote" name="Encuesta[p02_b_lote]" maxlength="6">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6" style="padding-left:0px">
                                    <div class="form-group">
                                        <label for="">217. ¿Cuáles son los apellidos y nombres del informante?</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6" style="padding-left:0px">
                                    <div class="form-group">
                                        <label for="">Nombres</label>
                                        <input type="text" class="form-control texto" id="p02_b_informante_nombres" placeholder="Nombres" name="Encuesta[p02_b_informante_nombres]" maxlength="250">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Apellidos</label>
                                        <input type="text" class="form-control texto" id="p02_b_informante_apellidos" placeholder="Apellidos" name="Encuesta[p02_b_informante_apellidos]" maxlength="250">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <label for="">218. ¿Qué cargo ocupa en la empresa?</label>
                                <div class="col-md-12"></div>
                                <div class="row">
                                    <div class="col-md-12" >
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="p02_b_cargo_22" name="Encuesta[p02_b_cargo]"
                                                class="custom-control-input" value="22">
                                            <label class="custom-control-label" for="p02_b_cargo_22">¿Representante legal?</label>
                                        </div>

                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="p02_b_cargo_23" name="Encuesta[p02_b_cargo]"
                                                class="custom-control-input" value="23">
                                            <label class="custom-control-label" for="p02_b_cargo_23">¿Directivo del área administrativa y/o financiera?</label>
                                        </div>

                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="p02_b_cargo_24" name="Encuesta[p02_b_cargo]"
                                                class="custom-control-input" value="24">
                                            <label class="custom-control-label" for="p02_b_cargo_24">¿Jefe de campo?</label>
                                        </div>

                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="p02_b_cargo_25" name="Encuesta[p02_b_cargo]"
                                                class="custom-control-input" value="25">
                                            <label class="custom-control-label" for="p02_b_cargo_25">¿Ingeniero de campo?</label>
                                        </div>

                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="p02_b_cargo_26" name="Encuesta[p02_b_cargo]"
                                                class="custom-control-input" value="26">
                                            <label class="custom-control-label" for="p02_b_cargo_26">¿Otro?</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row p02_b_cargo_26 mt-2" style="display:none">
                                <div class="col-md-6" style="padding-left:0px">
                                    <div class="form-group">
                                        <input type="text" class="form-control texto" id="p02_b_cargo_especifique" placeholder="Especifique" name="Encuesta[p02_b_cargo_especifique]" maxlength="250">
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-2">
                                <div class="col-md-6" style="padding-left:0px">
                                    <div class="form-group">
                                        <label for="">219. ¿Cuál es su número de teléfono/celular?</label>
                                        <select name="Encuesta[p02_b_informante_tiene_numero]" class="form-control" id="p02_b_informante_tiene_numero">
                                            <option value>¿Tiene número de teléfono/celular?</option>
                                            <option value="1">Si</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row p02_b_informante_tiene_numero" style="display:none">
                                <div class="col-md-6" style="padding-left:0px">
                                    <div class="form-group">
                                        <label for="">1. Fijo N°</label>
                                        <input type="text" class="form-control numerico" id="p02_b_informante_telefono_fijo" placeholder="Teléfono fijo" name="Encuesta[p02_b_informante_telefono_fijo]" maxlength="7">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">2. Celular N°:</label>
                                        <input type="text" class="form-control numerico" id="p02_b_informante_telefono_celular" placeholder="Celular" name="Encuesta[p02_b_informante_telefono_celular]" maxlength="9">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6" style="padding-left:0px">
                                    <div class="form-group">
                                        <label for="">220. ¿Cuál es el correo electrónico?</label>
                                        <select name="Encuesta[p02_b_informante_tiene_correo_electronico]" class="form-control" id="p02_b_informante_tiene_correo_electronico">
                                            <option value>¿Tiene correo electrónico?</option>
                                            <option value="1">Si</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6 p02_b_informante_tiene_correo_electronico" style="display:none" >
                                    <div class="form-group">
                                        <label for="">Correo electrónico</label>
                                        <input type="text" class="form-control" id="p02_b_informante_correo_electronico" placeholder="Correo electrónico" name="Encuesta[p02_b_informante_correo_electronico]" maxlength="250">
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12" style="padding-left:0px">
                                    <div class="form-group">
                                        <label for="">221. ¿Cuál es el nombre de esta parcela, chacra o fundo?</label>
                                        <input type="text" class="form-control" id="p02_b_nombre_fundo" placeholder="Nombre de esta parcela, chacra o fundo" name="Encuesta[p02_b_nombre_fundo]" maxlength="500">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for="">Observación</label>
                                <textarea id="p02_observacion" cols="30" rows="5" class="form-control" name="Encuesta[p02_observacion]" maxlength="500"></textarea>
                            </div>
                        </div>

                    </div>

                    <div class="tab-pane" id="capitulo_iii" role="tabpanel">
                        
                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for="">Módulo III.A. ÁREA TOTAL EN USO Y COBERTURAS DE LA TIERRA DE LA PARCELA AGRARIA Y NO AGRARIA, AL DÍA DE LA ENTREVISTA</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 mb-2" style="padding-left:0px">
                                <a class="btn btn-success" target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/lote/create-capitulo-iii-modulo-a?id_encuesta=<?= $id_encuesta ?>&geocodigo_fdo=<?= $geocodigo_fdo ?>">Agregar</a>
                            </div>
                            <div class="col-md-12" >
                                <table id="lista-usos-coberturas" class="table">
                                    <thead>
                                        <th>Lote</th>
                                        <th>301. Hoy, ¿Qué uso y/o cobertura de la tierra tienen los lotes de la parcela? </th>
                                        <th>301b1. Área Total medida del Lote de la Parcela - Cantidad</th>
                                        <th>301b2. Área Total medida del Lote de la Parcela - Unidad de Medida </th>
                                        <th>301b3. Área Total medida del Lote de la Parcela - Equivalencia</th>
                                        <th>Acciones</th>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for="">Observación</label>
                                <textarea name="Encuesta[p03_observacion]" id="p03_observacion" cols="30" rows="5" class="form-control" maxlength="250"></textarea>
                            </div>
                        </div>

                        <hr>
                        <!-- <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for="">Módulo III.B. NÚMERO DE PARCELAS AGRARIA EN EL DISTRITO, AL DIA DE LA ENTREVISTA</label>
                            </div>
                        </div>

                        <div class="row">
                            <label for="">302. Además de esta parcela, ¿conduce o trabaja como productor/a en otras parcelas dentro de este distrito?</label>
                            <select name="Encuesta[p03_c_int_trabaja_otra_parcela]"  id="p03_c_int_trabaja_otra_parcela" class="form-control">
                                <option value='99999'>Seleccionar</option>
                                <option value="1">Si</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                        <div class="row">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">303. ¿Incluyendo esta parcela, actualmente ¿Cuántas parcelas en total conduce en este distrito?</label>
                                    <input type="text" class="form-control numerico" id="p03_c_int_cantidad_parcelas" placeholder="¿Cuántas parcelas en total conduce en este distrito?" name="Encuesta[p03_c_int_cantidad_parcelas]" maxlength="5">
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12 mb-2 mt-2" style="padding-left:0px">
                                <a class="btn btn-success" target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/lote/create-capitulo-iii-modulo-b?id_encuesta=<?= $id_encuesta ?>&geocodigo_fdo=<?= $geocodigo_fdo ?>">Agregar</a>
                            </div>
                            <div class="col-md-12">
                                <table id="lista-parcelas-no-establecidas" class="table">
                                    <thead>
                                        <th>304a. No Parcela</th>
                                        <th>304b. Superficie total (Cantidad (enetero y dos decimales))</th>
                                        <th>304c. Unidad de medida (tabla de referencia)</th>
                                        <th>304d. Equivalencia (en metros cuadrados)</th>
                                        <th>Acciones</th>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div> -->
                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for="">Módulo III.C. RÉGIMEN DE TENENCIA DE LAS TIERRAS DE LA PARCELA AGRARIA, AL DÍA DE LA ENTREVISTA</label>
                            </div>
                        </div>
                        <div class="row">
                            <label for="">305. De esta parcela agraria, ¿Usted es:</label>
                            <div class="col-md-12"></div>
                            <div class="row">
                                
                                <div class="col-md-12">
                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="p03_c_int_tenencia_27" >
                                        <label class="custom-control-label" for="p03_c_int_tenencia_27">Propietario/a?</label>
                                    </div>

                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="p03_c_int_tenencia_28" >
                                        <label class="custom-control-label" for="p03_c_int_tenencia_28">¿Arrendatario/a?</label>
                                    </div>

                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="p03_c_int_tenencia_29" >
                                        <label class="custom-control-label" for="p03_c_int_tenencia_29">Comunero/a?</label>
                                    </div>
                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="p03_c_int_tenencia_30" >
                                        <label class="custom-control-label" for="p03_c_int_tenencia_30">¿Posesonario/a??</label>
                                    </div>
                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="p03_c_int_tenencia_31" >
                                        <label class="custom-control-label" for="p03_c_int_tenencia_31">¿Otro?</label>
                                    </div>
                                </div>
                            </div>
                            
                        </div>

                        <div class="row p03_c_int_tenencia_31" style="display:none">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">Especifique</label>
                                    <input type="text" class="form-control" id="p03_c_int_tenencia_31_especifique" placeholder="Especifique" name="Encuesta[p03_c_int_tenencia_31_especifique]" maxlength="500">
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="tab-pane" id="capitulo_iv" role="tabpanel">
                         <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for="">Módulo A. CULTIVOS TRANSITORIOS (CICLO CORTO), CULTIVOS PERMANENTES (CICLO LARGO), PLANTACIONES FORESTALES, PASTOS CULTIVADOS, BARBECHOS Y DESCANSO</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 mb-2" style="padding-left:0px">
                                <a class="btn btn-success" target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capituloiv/create-capitulo-iv-modulo-a?id_encuesta=<?= $id_encuesta ?>&geocodigo_fdo=<?= $geocodigo_fdo ?>">Agregar</a>
                            </div>
                            <div class="col-md-12" style="padding-left:0px">
                            
                                <table id="lista-cap4-moda-cultivos-transitorias" class="table">
                                    <thead>
                                        <th>N° de orden de lote</th>
                                        <th>401. El cultivo que va a registrar es:</th>
                                        <th>402. ¿Cuál es el nombre y variedad del cultivo?</th>
                                        <th>Acciones</th>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for="">Módulo B. ARBOLES FRUTALES, INDUSTRIALES Y ESPECIES FORESTALES PLANTADOS EN FORMA DISPERSA EN LA PARCELA AGRARIA, AL DÍA DE LA ENTREVISTA</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 mb-2" style="padding-left:0px">
                                <a class="btn btn-success" target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capituloiv/create-capitulo-iv-modulo-b?id_encuesta=<?= $id_encuesta ?>&geocodigo_fdo=<?= $geocodigo_fdo ?>">Agregar</a>
                            </div>
                            <div class="col-md-12" >
                                <table id="lista-cap4-modb-arboles-frutales" class="table">
                                    <thead>
                                        <th>428. ¿Qué especies de árboles frutales, industriales y forestales tiene en esta parcela o chacra?</th>
                                        <th>429. ¿Cuántas plantas tiene? </th>
                                        <th>430. ¿Cuántas plantas están en produccion?</th>
                                        <th>Acciones</th>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for="">Módulo C. VIVEROS, ALMÁCIGOS, SEMILLEROS Y HUERTOS SEMILLEROS AGRÍCOLAS Y FORESTALES EN LA PARCELA AGRARIA ENTRE ENERO A DICIEMBRE DE 2020</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 mb-2" style="padding-left:0px">
                                <a class="btn btn-success" target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capituloiv/create-capitulo-iv-modulo-c?id_encuesta=<?= $id_encuesta ?>&geocodigo_fdo=<?= $geocodigo_fdo ?>">Agregar</a>
                            </div>
                            <div class="col-md-12" >
                                <table id="lista-cap4-modc-viveros" class="table">
                                    <thead>
                                        <th>432.  N° Orden de lote</th>
                                        <th>433a. Cantidad</th>
                                        <th>433c. Equivalencia</th>
                                        <th>Acciones</th>
                                    </thead>
                                    <tbody>
                                         
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <hr >
                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for="">Módulo D. PASTOS NATURALES EN LA PARCELA AGRARIA  EL DÍA DE LA ENTREVISTA</label>
                            </div>
                        </div>
                        <div class="row">
                            <label for="">437. ¿Qué clase de pastos naturales hay en la parcela?  </label>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="cap4_modd_p437_1" value="1">
                                        <label class="custom-control-label" for="cap4_modd_p437_1">Pajonales (la chilligua, el iro ichu, ichu, hatun porke y el huaylla ichu)</label>
                                    </div>

                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="cap4_modd_p437_2" value="2">
                                        <label class="custom-control-label" for="cap4_modd_p437_2"> Césped de puna (pacu pacu, mula pilli, pilli rosado, pasto estrella y thurpa)</label>
                                    </div>

                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="cap4_modd_p437_3" value="3">
                                        <label class="custom-control-label" for="cap4_modd_p437_3">Bofedales u Oqonales (la kunkuna, hierbas como el libro libro, sillu sillu, pilli, y puna)</label>
                                    </div>

                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="cap4_modd_p437_4" value="4">
                                        <label class="custom-control-label" for="cap4_modd_p437_4">Tolares (chillca tola, pasto estrella, pesq`e, y también gramíneas como chilligua e iro ichu)</label>
                                    </div>

                                </div>

                                <div class="col-md-6">
                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="cap4_modd_p437_5" value="5">
                                        <label class="custom-control-label" for="cap4_modd_p437_5">Canllares (china kanlli y orqokanlli)</label>
                                    </div>

                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="cap4_modd_p437_6" value="6">
                                        <label class="custom-control-label" for="cap4_modd_p437_6">Totorales y juncales (aguazal, estero, carrizal, cañaveral, marisma o pantano)</label>
                                    </div>

                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="cap4_modd_p437_7" value="7">
                                        <label class="custom-control-label" for="cap4_modd_p437_7">Colchas, champa, Torourco o pasto natural</label>
                                    </div>

                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="cap4_modd_p437_8" value="8">
                                        <label class="custom-control-label" for="cap4_modd_p437_8">Otros:</label>
                                    </div>


                                </div>
                            </div>
                            
                        </div>

                        <div class="row cap4_modd_p437_8" style="display:none">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">Especifique</label>
                                    <input type="text" class="form-control" id="cap4_modd_p437_8_especifique" placeholder="Especifique" name="Encuesta[cap4_modd_p437_8_especifique]" maxlength="250">
                                </div>
                            </div>
                        </div>

                        

                        <div class="row">
                            <label for="">438. ¿Qué labores culturales realiza en los pastos naturales?   </label>
                            <div class="col-md-12"></div>
                            <div class="row">
                                <div class="col-md-6" >
                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="cap4_modd_p438_1" value="1">
                                        <label class="custom-control-label" for="cap4_modd_p438_1">¿Rotación de potreros?</label>
                                    </div>

                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="cap4_modd_p438_2" value="2">
                                        <label class="custom-control-label" for="cap4_modd_p438_2">¿Revegetación?</label>
                                    </div>

                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="cap4_modd_p438_3" value="3">
                                        <label class="custom-control-label" for="cap4_modd_p438_3">¿Entresíembra con trébol?</label>
                                    </div>

                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="cap4_modd_p438_4" value="4">
                                        <label class="custom-control-label" for="cap4_modd_p438_4">¿Sístemas Sílvopastoriles?</label>
                                    </div>

                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="cap4_modd_p438_5" value="5">
                                        <label class="custom-control-label" for="cap4_modd_p438_5">¿Fertilización?</label>
                                    </div>

                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="cap4_modd_p438_6" value="6">
                                        <label class="custom-control-label" for="cap4_modd_p438_6">¿Abonamiento?</label>
                                    </div>

                                </div>

                                <div class="col-md-6">
                                    

                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="cap4_modd_p438_7" value="7">
                                        <label class="custom-control-label" for="cap4_modd_p438_7">¿Riego?</label>
                                    </div>

                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="cap4_modd_p438_8" value="8">
                                        <label class="custom-control-label" for="cap4_modd_p438_8">¿Quema?</label>
                                    </div>

                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="cap4_modd_p438_9" value="9">
                                        <label class="custom-control-label" for="cap4_modd_p438_9">¿Construccion de terrazas, surcos, diques y waru waru?</label>
                                    </div>

                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="cap4_modd_p438_10" value="10">
                                        <label class="custom-control-label" for="cap4_modd_p438_10">¿Otro?:</label>
                                    </div>

                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="cap4_modd_p438_11" value="11">
                                        <label class="custom-control-label" for="cap4_modd_p438_11">Ninguna</label>
                                    </div>


                                </div>
                            </div>
                        </div>

                        <div class="row cap4_modd_p438_10" style="display:none">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">Especifique</label>
                                    <input type="text" class="form-control" id="cap4_modd_p438_10_especifique" placeholder="Especifique" name="Encuesta[cap4_modd_p438_10_especifique]" maxlength="250">
                                </div>
                            </div>
                        </div>


                        <div class="row">
                        
                            <div class="col-md-12" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">439. ¿Cómo consídera la condición de sus pastos naturales?  Nota: Selección unica</label>
                                    <select class="form-control" name="Encuesta[cap4_modd_p439]" id="cap4_modd_p439">
                                        <option value="99999">Seleccionar</option>
                                        <option value="1">Buena</option>
                                        <option value="2">Regular</option>
                                        <option value="3">Pobre</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <hr>
                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for="">Módulo E. PRÁCTICAS AGRÍCOLAS PARA LOS CULTIVOS COSECHADOS Y NO COSECHADOS EN LA PARCELA AGRARIA ENTRE ENERO 2020 AL DIA DE LA ENTREVISTA</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <small>A. Solo para cultivos transítorios</small>
                                <table class="table">
                                    <thead>
                                        <th colspan="2">440. ¿Ud efectúa la práctica agrícola de:</th>
                                        <th>441. ¿Hace cuántos años efectúa la práctica agrícola?</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1. Mezcla de la tierra con materia orgánica?</td>
                                            <td> 
                                                <select class="form-control" name="Encuesta[cap4_mode_p440_1]" id="cap4_mode_p440_1">
                                                    <option value>Seleccionar</option>
                                                    <option value="1">SI</option>
                                                    <option value="2">NO</option>
                                                </select> 
                                            </td>
                                            <td> 
                                                <input type="text" class="form-control numerico" name="Encuesta[cap4_mode_p441_1]" id="cap4_mode_p441_1" maxlength="4"> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>2. Arado o volteado de la tierra?</td>
                                            <td> 
                                                <select class="form-control" name="Encuesta[cap4_mode_p440_2]" id="cap4_mode_p440_2">
                                                    <option value>Seleccionar</option>
                                                    <option value="1">SI</option>
                                                    <option value="2">NO</option>
                                                </select> 
                                            </td>
                                            <td> 
                                                <input type="text" class="form-control numerico" name="Encuesta[cap4_mode_p441_2]" id="cap4_mode_p441_2" maxlength="4"> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>3. Desterronado o desmenuzado de la tierra?</td>
                                            <td> 
                                                <select class="form-control" name="Encuesta[cap4_mode_p440_3]" id="cap4_mode_p440_3">
                                                    <option value>Seleccionar</option>
                                                    <option value="1">SI</option>
                                                    <option value="2">NO</option>
                                                </select> 
                                            </td>
                                            <td> 
                                                <input type="text" class="form-control numerico" name="Encuesta[cap4_mode_p441_3]" id="cap4_mode_p441_3" maxlength="4"> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>4. Nivelado del terreno?</td>
                                            <td> 
                                                <select class="form-control" name="Encuesta[cap4_mode_p440_4]" id="cap4_mode_p440_4">
                                                    <option value>Seleccionar</option>
                                                    <option value="1">SI</option>
                                                    <option value="2">NO</option>
                                                </select> 
                                            </td>
                                            <td> 
                                                <input type="text" class="form-control numerico" name="Encuesta[cap4_mode_p441_4]" id="cap4_mode_p441_4" maxlength="4"> 
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <small>B. Para todos los cultivos</small>
                                <table class="table">
                                    <thead>
                                        <th colspan="2">440. ¿Ud efectúa la práctica agrícola de:</th>
                                        <th>441. ¿Hace cuántos años efectúa la práctica agrícola?</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>5. Análisis de suelos?</td>
                                            <td> 
                                                <select class="form-control" name="Encuesta[cap4_mode_p440_5]" id="cap4_mode_p440_5">
                                                    <option value>Seleccionar</option>
                                                    <option value="1">SI</option>
                                                    <option value="2">NO</option>
                                                </select> 
                                            </td>
                                            <td> 
                                                <input type="text" class="form-control numerico" name="Encuesta[cap4_mode_p441_5]" id="cap4_mode_p441_5" maxlength="4"> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>6. Recuperación de suelos erosionados?</td>
                                            <td> 
                                                <select class="form-control" name="Encuesta[cap4_mode_p440_6]" id="cap4_mode_p440_6">
                                                    <option value>Seleccionar</option>
                                                    <option value="1">SI</option>
                                                    <option value="2">NO</option>
                                                </select> 
                                            </td>
                                            <td> 
                                                <input type="text" class="form-control numerico" name="Encuesta[cap4_mode_p441_6]" id="cap4_mode_p441_6" maxlength="4"> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>7. Recuperación de suelos compactados?</td>
                                            <td> 
                                                <select class="form-control" name="Encuesta[cap4_mode_p440_7]" id="cap4_mode_p440_7">
                                                    <option value>Seleccionar</option>
                                                    <option value="1">SI</option>
                                                    <option value="2">NO</option>
                                                </select> 
                                            </td>
                                            <td> 
                                                <input type="text" class="form-control numerico" name="Encuesta[cap4_mode_p441_7]" id="cap4_mode_p441_7" maxlength="4"> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>8. Producción orgánica?</td>
                                            <td> 
                                                <select class="form-control" name="Encuesta[cap4_mode_p440_8]" id="cap4_mode_p440_8">
                                                    <option value>Seleccionar</option>
                                                    <option value="1">SI</option>
                                                    <option value="2">NO</option>
                                                </select> 
                                            </td>
                                            <td> 
                                                <input type="text" class="form-control numerico" name="Encuesta[cap4_mode_p441_8]" id="cap4_mode_p441_8" maxlength="4"> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>9.  Uso de bioinsumos?</td>
                                            <td> 
                                                <select class="form-control" name="Encuesta[cap4_mode_p440_9]" id="cap4_mode_p440_9">
                                                    <option value>Seleccionar</option>
                                                    <option value="1">SI</option>
                                                    <option value="2">NO</option>
                                                </select> 
                                            </td>
                                            <td> 
                                                <input type="text" class="form-control numerico" name="Encuesta[cap4_mode_p441_9]" id="cap4_mode_p441_9" maxlength="4"> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>10. Usar abonos (guano, estiercol, etc.)?</td>
                                            <td> 
                                                <select class="form-control" name="Encuesta[cap4_mode_p440_10]" id="cap4_mode_p440_10">
                                                    <option value>Seleccionar</option>
                                                    <option value="1">SI</option>
                                                    <option value="2">NO</option>
                                                </select> 
                                            </td>
                                            <td> 
                                                <input type="text" class="form-control numerico" name="Encuesta[cap4_mode_p441_10]" id="cap4_mode_p441_10" maxlength="4"> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>11. Usar fertilizantes químicos?</td>
                                            <td> 
                                                <select class="form-control" name="Encuesta[cap4_mode_p440_11]" id="cap4_mode_p440_11">
                                                    <option value>Seleccionar</option>
                                                    <option value="1">SI</option>
                                                    <option value="2">NO</option>
                                                </select> 
                                            </td>
                                            <td> 
                                                <input type="text" class="form-control numerico" name="Encuesta[cap4_mode_p441_11]" id="cap4_mode_p441_11" maxlength="4"> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>12. Usar plaguicidas? (si marca "SI" debe registrar información en la 442)</td>
                                            <td> 
                                                <select class="form-control" name="Encuesta[cap4_mode_p440_12]" id="cap4_mode_p440_12">
                                                    <option value>Seleccionar</option>
                                                    <option value="1">SI</option>
                                                    <option value="2">NO</option>
                                                </select> 
                                            </td>
                                            <td> 
                                                <input type="text" class="form-control numerico" name="Encuesta[cap4_mode_p441_12]" id="cap4_mode_p441_12" maxlength="4"> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>13. Aplicar manejo integrado de plagas y/o enfermedades? (Si marca "SI" debe registrar preg 444)</td>
                                            <td> 
                                                <select class="form-control" name="Encuesta[cap4_mode_p440_13]" id="cap4_mode_p440_13">
                                                    <option value>Seleccionar</option>
                                                    <option value="1">SI</option>
                                                    <option value="2">NO</option>
                                                </select> 
                                            </td>
                                            <td> 
                                                <input type="text" class="form-control numerico" name="Encuesta[cap4_mode_p441_13]" id="cap4_mode_p441_13" maxlength="4">  
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12">
                                <small>C. Solo para cultivos en la Síerra</small>
                                <table class="table">
                                    <thead>
                                        <th colspan="2">440. ¿Ud efectúa la práctica agrícola de:</th>
                                        <th>441. ¿Hace cuántos años efectúa la práctica agrícola?</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>14. Construcción de terrazas (andenes), zanjas de infiltración y/o rehabilitación de andenes?</td>
                                            <td> 
                                                <select class="form-control" name="Encuesta[cap4_mode_p440_14]" id="cap4_mode_p440_14">
                                                    <option value>Seleccionar</option>
                                                    <option value="1">SI</option>
                                                    <option value="2">NO</option>
                                                </select> 
                                            </td>
                                            <td> 
                                                <input type="text" class="form-control numerico" name="Encuesta[cap4_mode_p441_14]" id="cap4_mode_p441_14" maxlength="4">  
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>15. Construcción de surcos en contorno a la pendiente del terreno?</td>
                                            <td> 
                                                <select class="form-control" name="Encuesta[cap4_mode_p440_15]" id="cap4_mode_p440_15">
                                                    <option value>Seleccionar</option>
                                                    <option value="1">SI</option>
                                                    <option value="2">NO</option>
                                                </select> 
                                            </td>
                                            <td> 
                                                <input type="text" class="form-control numerico" name="Encuesta[cap4_mode_p441_15]" id="cap4_mode_p441_15" maxlength="4">  
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>16. Construcción de diques?</td>
                                            <td> 
                                                <select class="form-control" name="Encuesta[cap4_mode_p440_16]" id="cap4_mode_p440_16">
                                                    <option value>Seleccionar</option>
                                                    <option value="1">SI</option>
                                                    <option value="2">NO</option>
                                                </select> 
                                            </td>
                                            <td> 
                                                <input type="text" class="form-control numerico" name="Encuesta[cap4_mode_p441_16]" id="cap4_mode_p441_16" maxlength="4">  
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>17. Construcción de waru waru?</td>
                                            <td> 
                                                <select class="form-control" name="Encuesta[cap4_mode_p440_17]" id="cap4_mode_p440_17">
                                                    <option value>Seleccionar</option>
                                                    <option value="1">SI</option>
                                                    <option value="2">NO</option>
                                                </select> 
                                            </td>
                                            <td> 
                                                <input type="text" class="form-control numerico" name="Encuesta[cap4_mode_p441_17]" id="cap4_mode_p441_17" maxlength="4">  
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <small>D. Solo cuando la parcela tiene riego</small>
                                <table class="table">
                                    <thead>
                                        <th colspan="2">440. ¿Ud efectúa la práctica agrícola de:</th>
                                        <th>441. ¿Hace cuántos años efectúa la práctica agrícola?</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>18. Determinar la cantidad de agua que necesíta su cultivo antes de iniciar la campaña agrícola?</td>
                                            <td> 
                                                <select class="form-control" name="Encuesta[cap4_mode_p440_18]" id="cap4_mode_p440_18">
                                                    <option value>Seleccionar</option>
                                                    <option value="1">SI</option>
                                                    <option value="2">NO</option>
                                                </select> 
                                            </td>
                                            <td> 
                                                <input type="text" class="form-control numerico" name="Encuesta[cap4_mode_p441_18]" id="cap4_mode_p441_18" maxlength="4">  
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>19. Determina cada cuanto tiempo debe regar su cultivo antes de iniciar la campaña agrícola?</td>
                                            <td> 
                                                <select class="form-control" name="Encuesta[cap4_mode_p440_19]" id="cap4_mode_p440_19">
                                                    <option value>Seleccionar</option>
                                                    <option value="1">SI</option>
                                                    <option value="2">NO</option>
                                                </select> 
                                            </td>
                                            <td> 
                                                <input type="text" class="form-control numerico" name="Encuesta[cap4_mode_p441_19]" id="cap4_mode_p441_19" maxlength="4">   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>20. Medición de la cantidad de agua que entra a su parcela?</td>
                                            <td> 
                                                <select class="form-control" name="Encuesta[cap4_mode_p440_20]" id="cap4_mode_p440_20">
                                                    <option value>Seleccionar</option>
                                                    <option value="1">SI</option>
                                                    <option value="2">NO</option>
                                                </select> 
                                            </td>
                                            <td> 
                                                <input type="text" class="form-control numerico" name="Encuesta[cap4_mode_p441_20]" id="cap4_mode_p441_20" maxlength="4">  
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>21. Realizar el mantenimiento de su sistema de riego?</td>
                                            <td> 
                                                <select class="form-control" name="Encuesta[cap4_mode_p440_21]" id="cap4_mode_p440_21">
                                                    <option value>Seleccionar</option>
                                                    <option value="1">SI</option>
                                                    <option value="2">NO</option>
                                                </select> 
                                            </td>
                                            <td> 
                                                <input type="text" class="form-control numerico" name="Encuesta[cap4_mode_p441_21]" id="cap4_mode_p441_21" maxlength="4">  
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>22. Realizar análisís de agua?</td>
                                            <td> 
                                                <select class="form-control" name="Encuesta[cap4_mode_p440_22]" id="cap4_mode_p440_22">
                                                    <option value>Seleccionar</option>
                                                    <option value="1">SI</option>
                                                    <option value="2">NO</option>
                                                </select> 
                                            </td>
                                            <td> 
                                                <input type="text" class="form-control numerico" name="Encuesta[cap4_mode_p441_22]" id="cap4_mode_p441_22" maxlength="4">  
                                            </td>
                                        </tr>

                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for="">PLAGUICIDAS ENTRE ENERO 2020 AL DIA DE LA ENTREVISTA</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for="">442.  ¿Utiliza equipo de protección para la aplicación de los plaguicidas?    </label>
                                <select class="form-control"  name="Encuesta[cap4_mode_p442]" id="cap4_mode_p442">
                                    <option value>Seleccionar</option>
                                    <option value="1">SI</option>
                                    <option value="2">NO</option>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <label for="">443.  ¿Qué hace con los envases vacíos de los plaguicidas (insecticidas, fungicidas, herbicidas, acaricidas, bactericidas, nematicidas, rodenticidas, molusquicidas, etc)?</label>
                            <div class="col-md-12"></div>
                            <div class="row">
                                <div class="col-md-6" >
                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="cap4_mode_p443_1" value="1">
                                        <label class="custom-control-label" for="cap4_mode_p443_1">¿Quema/Incinera?</label>
                                    </div>

                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="cap4_mode_p443_2" value="2">
                                        <label class="custom-control-label" for="cap4_mode_p443_2">¿Entierra?</label>
                                    </div>

                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="cap4_mode_p443_3" value="3">
                                        <label class="custom-control-label" for="cap4_mode_p443_3">¿Arroja a canal de regadío, río, acequia?</label>
                                    </div>

                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="cap4_mode_p443_4" value="4">
                                        <label class="custom-control-label" for="cap4_mode_p443_4">¿Deja en el campo de cultivo?</label>
                                    </div>

                                </div>

                                <div class="col-md-6">
                                    

                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="cap4_mode_p443_5" value="6">
                                        <label class="custom-control-label" for="cap4_mode_p443_5">¿Efectúa el triple lavado?</label>
                                    </div>

                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="cap4_mode_p443_6" value="7">
                                        <label class="custom-control-label" for="cap4_mode_p443_6">¿Deposíta en almacén temporal?</label>
                                    </div>

                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="cap4_mode_p443_7" value="8">
                                        <label class="custom-control-label" for="cap4_mode_p443_7">¿Otro?:</label>
                                    </div>

                                </div>
                            </div>
                            
                        </div>

                        <div class="row cap4_mode_p443_7" style="display:none">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">Especifique</label>
                                    <input type="text" class="form-control" id="cap4_mode_p443_7_especifique" placeholder="Especifique" name="Encuesta[cap4_mode_p443_7_especifique]" maxlength="500">
                                </div>
                            </div>
                        </div>



                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for="">MANEJO DE PLAGAS, ENFERMEDADES Y MALEZAS EN LA PARCELA AGRARIA  ENTRE ENERO 2020 AL DIA DE LA ENTREVISTA</label>
                            </div>
                        </div>
                        <div class="row">
                            <label for="">444.  ¿Qué tipo de control utiliza cuando aplica manejo  de plagas, enfermedades y malezas?</label>
                            <div class="col-md-12"></div>
                            <div class="row">
                                <div class="col-md-6" >
                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="cap4_mode_p444_1" value="1">
                                        <label class="custom-control-label" for="cap4_mode_p444_1">¿Control cultural ? (podas, preparación, limpieza de campo, etc)</label>
                                    </div>

                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="cap4_mode_p444_2" value="2">
                                        <label class="custom-control-label" for="cap4_mode_p444_2">¿Control físíco? (barreras naturales, cercos vivos, plasticultura)</label>
                                    </div>

                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="cap4_mode_p444_3" value="3">
                                        <label class="custom-control-label" for="cap4_mode_p444_3">¿Control químico? (plaguicidas)</label>
                                    </div>

                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="cap4_mode_p444_4" value="4">
                                        <label class="custom-control-label" for="cap4_mode_p444_4">¿Control biológico?</label>
                                    </div>

                                </div>

                                <div class="col-md-6">
                                    

                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="cap4_mode_p444_5" value="5">
                                        <label class="custom-control-label" for="cap4_mode_p444_5">¿Uso de plantas repelentes o atrayentes?</label>
                                    </div>

                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="cap4_mode_p444_6" value="6">
                                        <label class="custom-control-label" for="cap4_mode_p444_6">¿Uso de feromonas para atraer los insectos y/o usa de trampas de luz, etc?</label>
                                    </div>

                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="cap4_mode_p444_7" value="7">
                                        <label class="custom-control-label" for="cap4_mode_p444_7">¿Control legal?</label>
                                    </div>

                                    <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                        <input type="checkbox" class="custom-control-input" id="cap4_mode_p444_8" value="8">
                                        <label class="custom-control-label" for="cap4_mode_p444_8">¿Otro?</label>
                                    </div>

                                </div>
                            </div>
                            
                        </div>

                        <div class="row cap4_mode_p444_8" style="display:none">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">Especifique</label>
                                    <input type="text" class="form-control" id="cap4_mode_p444_8_especifique" placeholder="Especifique" name="Encuesta[cap4_mode_p444_8_especifique]" maxlength="500">
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for="">CONTROL BIOLOGICO</label>
                            </div>
                        </div>
                        <div class="row">
                            <label for="">445. ¿Realiza evaluación de plagas después de aplicado el control biológico?</label>
                            <select class="form-control" name="Encuesta[cap4_mode_p445]" id="cap4_mode_p445">
                                <option value>Seleccionar</option>
                                <option value="1">SI</option>
                                <option value="2">NO</option>
                            </select>
                        </div>

                        <div class="row">
                            <label for="">446. ¿Anota o registra la aplicación del control biológico (fecha, cantidad usada, etc)?</label>
                            <select class="form-control" name="Encuesta[cap4_mode_p446]" id="cap4_mode_p446">
                                <option value>Seleccionar</option>
                                <option value="1">SI</option>
                                <option value="2">NO</option>
                            </select>
                        </div>


                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for="">Observaciones:</label>
                                <textarea name="Encuesta[cap4_mode_observacion]" id="cap4_mode_observacion" cols="30" rows="5" class="form-control" maxlength="250"></textarea>
                            </div>
                        </div> 
                    </div>

                    <div class="tab-pane" id="capitulo_v" role="tabpanel">
                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for="">Módulo V.A. POBLACIÓN  DE ESPECIES PECUARIAS EN LA UNIDAD AGRARIA AL DÍA DE LA ENTREVISTA </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">501.  Hoy, ¿cuántos …………………………………………… tiene, cría, engorda, y/o conserva en esta unidad agraria?</label>
                                    <!-- <input type="text" class="form-control" id="cap5_moda_p501_a" name="Encuesta[cap5_moda_p501_a]"> -->
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <!-- <div class="col-md-12 mb-2 mt-2" style="padding-left:0px">
                                <a class="btn btn-success" target="_blank" href="#">Agregar</a>
                            </div> -->
                            <div class="col-md-12">
                                <table id="lista-cap5-moda-especies" class="table">
                                    <thead>
                                        <th>Especie</th>
                                        <th>Hembra</th>
                                        <th>Macho</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_moda_p501_b_1" value="1">
                                                    <label class="custom-control-label" for="cap5_moda_p501_b_1"> 1. Vacunos</label>
                                                </div>
                                            </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_1_hembra" maxlength="7"> </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_1_macho" maxlength="7"> </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_moda_p501_b_2" value="2">
                                                    <label class="custom-control-label" for="cap5_moda_p501_b_2"> 2. Llamas</label>
                                                </div>
                                            </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_2_hembra" maxlength="7"> </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_2_macho" maxlength="7"> </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_moda_p501_b_3" value="3">
                                                    <label class="custom-control-label" for="cap5_moda_p501_b_3"> 3. Alpacas</label>
                                                </div>
                                            </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_3_hembra" maxlength="7"> </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_3_macho" maxlength="7"> </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_moda_p501_b_4" value="4">
                                                    <label class="custom-control-label" for="cap5_moda_p501_b_4"> 4. Vicuñas</label>
                                                </div>
                                            </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_4_hembra" maxlength="7"> </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_4_macho" maxlength="7"> </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_moda_p501_b_5" value="5">
                                                    <label class="custom-control-label" for="cap5_moda_p501_b_5"> 5. Ovinos</label>
                                                </div>
                                            </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_5_hembra" maxlength="7"> </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_5_macho" maxlength="7"> </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_moda_p501_b_6" value="6">
                                                    <label class="custom-control-label" for="cap5_moda_p501_b_6"> 6. Caprinos</label>
                                                </div>
                                            </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_6_hembra" maxlength="7"> </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_6_macho" maxlength="7"> </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_moda_p501_b_7" value="7">
                                                    <label class="custom-control-label" for="cap5_moda_p501_b_7"> 7. Porcinos</label>
                                                </div>
                                            </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_7_hembra" maxlength="7"> </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_7_macho" maxlength="7"> </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_moda_p501_b_8" value="8">
                                                    <label class="custom-control-label" for="cap5_moda_p501_b_8"> 8. Pollos</label>
                                                </div>
                                            </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_8_hembra" maxlength="7"> </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_8_macho" maxlength="7"> </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_moda_p501_b_9" value="9">
                                                    <label class="custom-control-label" for="cap5_moda_p501_b_9"> 9. Gallinas (incluye gallos de pelea)</label>
                                                </div>
                                            </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_9_hembra" maxlength="7"> </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_9_macho" maxlength="7"> </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_moda_p501_b_10" value="10">
                                                    <label class="custom-control-label" for="cap5_moda_p501_b_10"> 10. Gallinas en postura</label>
                                                </div>
                                            </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_10_hembra" maxlength="7"> </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_10_macho" maxlength="7"> </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_moda_p501_b_11" value="11">
                                                    <label class="custom-control-label" for="cap5_moda_p501_b_11"> 11. Pavos</label>
                                                </div>
                                            </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_11_hembra" maxlength="7"> </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_11_macho" maxlength="7"> </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_moda_p501_b_12" value="12">
                                                    <label class="custom-control-label" for="cap5_moda_p501_b_12"> 12. Patos</label>
                                                </div>
                                            </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_12_hembra" maxlength="7"> </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_12_macho" maxlength="7">  </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_moda_p501_b_13" value="13">
                                                    <label class="custom-control-label" for="cap5_moda_p501_b_13"> 13. Gansos</label>
                                                </div>
                                            </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_13_hembra" maxlength="7"> </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_13_macho" maxlength="7"> </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_moda_p501_b_14" value="14">
                                                    <label class="custom-control-label" for="cap5_moda_p501_b_14"> 14. Avestruces</label>
                                                </div>
                                            </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_14_hembra" maxlength="7"> </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_14_macho" maxlength="7"> </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_moda_p501_b_15" value="15">
                                                    <label class="custom-control-label" for="cap5_moda_p501_b_15"> 15. Codornices</label>
                                                </div>
                                            </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_15_hembra" maxlength="7"> </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_15_macho" maxlength="7"> </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_moda_p501_b_16" value="16">
                                                    <label class="custom-control-label" for="cap5_moda_p501_b_16"> 16. Suri / ñandú (ave) </label>
                                                </div>
                                            </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_16_hembra" maxlength="7"> </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_16_macho" maxlength="7"> </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_moda_p501_b_17" value="17">
                                                    <label class="custom-control-label" for="cap5_moda_p501_b_17"> 17. Cría y engorde de caracoles</label>
                                                </div>
                                            </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_17_hembra" maxlength="7"> </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_17_macho" maxlength="7"> </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_moda_p501_b_18" value="18">
                                                    <label class="custom-control-label" for="cap5_moda_p501_b_18"> 18. Cuyes</label>
                                                </div>
                                            </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_18_hembra" maxlength="7"> </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_18_macho" maxlength="7"> </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_moda_p501_b_19" value="19">
                                                    <label class="custom-control-label" for="cap5_moda_p501_b_19"> 19. Conejos</label>
                                                </div>
                                            </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_19_hembra" maxlength="7"> </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_19_macho" maxlength="7"> </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_moda_p501_b_20" value="20">
                                                    <label class="custom-control-label" for="cap5_moda_p501_b_20"> 20. Búfalos</label>
                                                </div>
                                            </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_20_hembra" maxlength="7"> </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_20_macho" maxlength="7"> </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_moda_p501_b_21" value="21">
                                                    <label class="custom-control-label" for="cap5_moda_p501_b_21"> 21. Caballos</label>
                                                </div>
                                            </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_21_hembra" maxlength="7"> </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_21_macho" maxlength="7"> </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_moda_p501_b_22" value="22">
                                                    <label class="custom-control-label" for="cap5_moda_p501_b_22"> 22. Burros</label>
                                                </div>
                                            </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_22_hembra" maxlength="7"> </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_22_macho" maxlength="7"> </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_moda_p501_b_23" value="23">
                                                    <label class="custom-control-label" for="cap5_moda_p501_b_23"> 23. Mulos</label>
                                                </div>
                                            </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_23_hembra" maxlength="7"> </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_23_macho" maxlength="7"> </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_moda_p501_b_24" value="24">
                                                    <label class="custom-control-label" for="cap5_moda_p501_b_24"> 24. Colmenas para producción de miel</label>
                                                </div>
                                            </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_24_hembra" maxlength="7"> </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_24_macho" maxlength="7"> </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_moda_p501_b_25" value="25">
                                                    <label class="custom-control-label" for="cap5_moda_p501_b_25"> 25. Colmenas para producción de polen</label>
                                                </div>
                                            </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_25_hembra" maxlength="7"> </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_25_macho" maxlength="7"> </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_moda_p501_b_26" value="26">
                                                    <label class="custom-control-label" for="cap5_moda_p501_b_26"> 26. Colmenas productoras de otros productos</label>
                                                </div>
                                            </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_26_hembra" maxlength="7"> </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_26_macho" maxlength="7"> </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_moda_p501_b_27" value="27">
                                                    <label class="custom-control-label" for="cap5_moda_p501_b_27"> 27. Otras:</label>

                                                        <div class="row cap5_moda_p501_b_27" style="display:none">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" id="cap5_moda_p501_b_27_especifique" placeholder="Especifique" name="Encuesta[cap5_moda_p501_b_27_especifique]" >
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                            </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_27_hembra" maxlength="7"> </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_27_macho" maxlength="7">  </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_moda_p501_b_28" value="28">
                                                    <label class="custom-control-label" for="cap5_moda_p501_b_28"> 28. Ninguna</label>
                                                </div>
                                            </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_28_hembra" disabled> </td>
                                            <td> <input type="text" class="form-control numerico" id="cap5_moda_p501_b_28_macho" disabled> </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for="">Módulo V.B. PRODUCCIÓN  DE GANADO VACUNO ENTRE ENERO A DICIEMBRE DE 2020 </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">Vacunos</label>
                                    <select class="form-control" name="Encuesta[cap5_modb_p502_flag]" id="cap5_modb_p502_flag">
                                        <option value>Seleccionar</option>
                                        <option value="1">SI</option>
                                        <option value="2">NO</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                            
                                <table id="lista-cap5-modb-vacunos" class="table">
                                    <thead>
                                        <th>502. ESPECIE / CATEGORÍA</th>
                                        <th>Acciones</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_1" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_1"> 2. Ternera (vacunos hembra hasta de 1 año)</label>
                                                </div>    
                                             </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=1&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_2" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_2"> 3. Vaquillas (vacunos hembra mayores a un 1 año y hasta los dos años)</label>
                                                </div>

                                             </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=2&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_3" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_3"> 4. Vaquillonas (vacunos hembra mayores a 2 años y hasta antes del primer parto)</label>
                                                </div>    
                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=3&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_4" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_4"> 5. Vacas en producción, (vacunos hembra  desde el primer parto )</label>
                                                </div>     
                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=4&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_5" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_5"> 6. Vacunos en seca, (hembra  desde el primer parto)</label>
                                                </div>     
                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=5&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_6" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_6"> 7. Ternero (vacunos machos hasta de 1 año)</label>
                                                </div> 
                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=6&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_7" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_7"> 8. Toretes (vacunos machos mayores a un 1 año y hasta los dos años)</label>
                                                </div> 

                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=7&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_8" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_8"> 9. Toros (vacunos machos mayores a  2 años sin castrar)</label>
                                                </div>     
                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=8&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_9" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_9"> 10. Bueyes (vacunos machos castrados)</label>
                                                </div>    
                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=9&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for="">Módulo V.C. PRODUCCIÓN  DE PORCINOS DESDE ENERO A DICIEMBRE DEL 2020</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">Vacunos</label>
                                    <select class="form-control" name="Encuesta[cap5_modc_p502_flag]" id="cap5_modc_p502_flag">
                                        <option value>Seleccionar</option>
                                        <option value="1">SI</option>
                                        <option value="2">NO</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                            
                                <table id="lista-cap5-modc-porcinos" class="table">
                                    <thead>
                                        <th>502. ESPECIE / CATEGORÍA</th>
                                        <th>Acciones</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_10" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_10"> 12. Lechones hembra (del nacimiento hasta el destete o hasta 2-4 meses)</label>
                                                </div>     
                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=10&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_11" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_11"> 13. Gorrinas (del destete hasta el primer parto o  hasta 7-12 meses)</label>
                                                </div>
                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=11&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_12" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_12"> 14. Marranas (desde el primer parto)</label>
                                                </div>
                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=12&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_13" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_13"> 15. Lechones macho (del nacimiento hasta el destete o hasta 2-4 meses edad)</label>
                                                </div>
                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=13&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_14" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_14"> 16. Gorrinos (desde el destete hasta los 6 - 8 meses)</label>
                                                </div>

                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=14&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_15" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_15"> 17. Verracos (Mayor a 6 meses)</label>
                                                </div>

                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=15&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                            </td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for="">Módulo V.D. PRODUCCIÓN  DE CUYES DESDE ENERO A DICIEMBRE DEL 2020</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">Cuyes</label>
                                    <select class="form-control" name="Encuesta[cap5_modd_p502_flag]" id="cap5_modd_p502_flag">
                                        <option value>Seleccionar</option>
                                        <option value="1">SI</option>
                                        <option value="2">NO</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                            
                                <table id="lista-cap5-modd-cuyes" class="table">
                                    <thead>
                                        <th>502. ESPECIE / CATEGORÍA</th>
                                        <th>Acciones</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_16" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_16"> 19. Gazapos (hasta los 15 - 21 días)</label>
                                                </div>

                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=16&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_17" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_17"> 20. Recría hembras (hasta los 70 - 90 días)</label>
                                                </div>
                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=17&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_18" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_18"> 21. Madres (hasta el final de su ciclo reproductivo)</label>
                                                </div>
                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=18&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_19" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_19"> 22. Recría machos (hasta los 70 - 90 días)</label>
                                                </div>    
                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=19&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_20" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_20"> 23. Padrillos</label>
                                                </div>      
                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=20&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                            </td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for="">Módulo V.E. PRODUCCIÓN DE OVINOS Y CAPRINOS DESDE ENERO A DICIEMBRE DEL 2020</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">Ovinos</label>
                                    <select class="form-control" name="Encuesta[cap5_mode_p502_a_flag]" id="cap5_mode_p502_a_flag">
                                        <option value>Seleccionar</option>
                                        <option value="1">SI</option>
                                        <option value="2">NO</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                            
                                <table id="lista-cap5-mode-ovinos" class="table">
                                    <thead>
                                        <th>502. ESPECIE / CATEGORÍA</th>
                                        <th>Acciones</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_21" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_21"> 25. Corderas (hasta 1 año)</label>
                                                </div>
                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=21&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_22" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_22"> 26. Borrequillas (mayor a 1 año y hasta 2 hasta antes del primer parto)</label>
                                                </div>
                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=22&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_23" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_23"> 27. Borregas (desde el primer parto)</label>
                                                </div>
                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=23&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_24" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_24"> 28. Corderos (hasta 1 año)</label>
                                                </div>
                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=24&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_25" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_25"> 29. Carnerillos (mayor a 1 año y hasta los 2 años)</label>
                                                </div>
                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=25&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_26" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_26"> 30. Carnero (mayor a 2 años)</label>
                                                </div>
                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=26&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_27" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_27"> 31. Capones (castrados)</label>
                                                </div>
                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=27&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                            </td>
                                        </tr>
                                        


                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">Caprinos</label>
                                    <select class="form-control" name="Encuesta[cap5_mode_p502_b_flag]" id="cap5_mode_p502_b_flag">
                                        <option value>Seleccionar</option>
                                        <option value="1">SI</option>
                                        <option value="2">NO</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                            
                                <table id="lista-cap5-mode-caprinos" class="table">
                                    <thead>
                                        <th>502. ESPECIE / CATEGORÍA</th>
                                        <th>Acciones</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_28" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_28"> 33. Cabritas (Menos de 1 año)</label>
                                                </div>
                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=28&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_29" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_29"> 34. Cabrillas (De 1 a nenos de 2 años)</label>
                                                </div>

                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=29&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_30" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_30"> 35. Cabras (De 2 y más años)</label>
                                                </div>
                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=30&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_31" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_31"> 36. Cabritos (Menos de 1 año)</label>
                                                </div>
                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=31&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_32" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_32"> 37. Chivatos (De 1 a nenos de 2 años)</label>
                                                </div>
                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=32&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_33" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_33"> 38. Chivos (De 2  y  más años)</label>
                                                </div>

                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=33&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>




                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for="">Módulo V.F. PRODUCCIÓN DE CAMÉLIDOS, DESDE ENERO A DICIEMBRE DEL 2020</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">Alpacas</label>
                                    <select class="form-control" name="Encuesta[cap5_modf_p502_a_flag]" id="cap5_modf_p502_a_flag">
                                        <option value>Seleccionar</option>
                                        <option value="1">SI</option>
                                        <option value="2">NO</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                            
                                <table id="lista-cap5-modf-alpacas" class="table">
                                    <thead>
                                        <th>502. ESPECIE / CATEGORÍA</th>
                                        <th>Acciones</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_34" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_34"> 40. Crías Hembra (hasta de 1 año)</label>
                                                </div>    
                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=34&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_35" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_35"> 41. Tuis hembra (mayor a 1 año hasta antes del primer parto)</label>
                                                </div> 

                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=35&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_36" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_36"> 42. Madres (desde el primer parto)</label>
                                                </div>

                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=36&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_37" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_37"> 43. Crias Macho (hasta de 1 año)</label>
                                                </div>

                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=37&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_38" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_38"> 44. Tuis macho (mayor a 1 año y hasta los 2 años)</label>
                                                </div>    
                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=38&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_39" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_39"> 45. Padrillos (mayor a 2 años de edad)</label>
                                                </div>

                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=39&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_40" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_40"> 46. Capones (castrados)</label>
                                                </div>
                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=40&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                            </td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">Llamas</label>
                                    <select class="form-control" name="Encuesta[cap5_modf_p502_b_flag]" id="cap5_modf_p502_b_flag">
                                        <option value>Seleccionar</option>
                                        <option value="1">SI</option>
                                        <option value="2">NO</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                            
                                <table id="lista-cap5-modf-llamas" class="table">
                                    <thead>
                                        <th>502. ESPECIE / CATEGORÍA</th>
                                        <th>Acciones</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_41" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_41"> 48. Crías hembras (menos de 1 año)</label>
                                                </div>

                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=41&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_42" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_42"> 49. Ancuta hembras (de 1 a menos de 2 años)</label>
                                                </div>

                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=42&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_43" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_43"> 50. Llamas hembras (de 2 y más años)</label>
                                                </div>

                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=43&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_44" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_44"> 51. Crías macho (menos de 1 año)</label>
                                                </div>

                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=44&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_45" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_45"> 52. Ancuta machos (de 1 a menos de 2 años)</label>
                                                </div>

                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=45&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_46" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_46"> 53. Llamas machos (de 2 y más años)</label>
                                                </div>

                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=46&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_47" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_47"> 54. Huariso (alpaca x llama): Hembra</label>
                                                </div>

                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=47&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_48" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_48"> 55. Huariso (alpaca x llama): Macho</label>
                                                </div>

                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=48&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                            </td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">Vicuñas</label>
                                    <select class="form-control" name="Encuesta[cap5_modf_p502_c_flag]" id="cap5_modf_p502_c_flag">
                                        <option value>Seleccionar</option>
                                        <option value="1">SI</option>
                                        <option value="2">NO</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                            
                                <table id="lista-cap5-modf-vicunas" class="table">
                                    <thead>
                                        <th>502. ESPECIE / CATEGORÍA</th>
                                        <th>Acciones</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_49" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_49"> 57. Vicuña: Hembra</label>
                                                </div>    
                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=49&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p502_50" disabled>
                                                    <label class="custom-control-label" for="cap5_modb_p502_50"> 58. Vicuña: Machos</label>
                                                </div> 

                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-b?id_encuesta=<?= $id_encuesta ?>&id_especie_categoria=50&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> 
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for="">Módulo V.G. PRODUCCIÓN DE AVES ENTRE ENERO A DICIEMBRE DEL 2020</label>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">GRANJAS AVICOLA LINEA CARNE</label>
                                    <!-- <select class="form-control" name="Encuesta[cap5_modg_p502_a_flag]" id="cap5_modg_p502_a_flag">
                                        <option value>Seleccionar</option>
                                        <option value="1">SI</option>
                                        <option value="2">NO</option>
                                    </select> -->
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                            
                                <table id="lista-cap5-modg-linea-carne" class="table">
                                    <thead>
                                        <th>509. ¿Durante los meses enero a diciembre del 2020 tuvo ……………:</th>
                                        <th>Acciones</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modg_p509_1" disabled>
                                                    <label class="custom-control-label" for="cap5_modg_p509_1"> 2. Abuelos de Pollos de Engorde (Línea hembra)?</label>
                                                </div> 
                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-g1?id_encuesta=<?= $id_encuesta ?>&id_avicola=1&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modg_p509_2" disabled>
                                                    <label class="custom-control-label" for="cap5_modg_p509_2"> 3. Reproductoras de Pollos de Engorde?</label>
                                                </div> 

                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-g1?id_encuesta=<?= $id_encuesta ?>&id_avicola=2&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modg_p509_3" disabled>
                                                    <label class="custom-control-label" for="cap5_modg_p509_3"> 4. Pollos de Engorde?</label>
                                                </div>     
                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-g1?id_encuesta=<?= $id_encuesta ?>&id_avicola=3&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modg_p509_4" disabled>
                                                    <label class="custom-control-label" for="cap5_modg_p509_4"> 5. Reproductoras de Pavos de Engorde?</label>
                                                </div>    
                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-g1?id_encuesta=<?= $id_encuesta ?>&id_avicola=4&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modg_p509_5" disabled>
                                                    <label class="custom-control-label" for="cap5_modg_p509_5"> 6. Pavos de Engorde?</label>
                                                </div>     
                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-g1?id_encuesta=<?= $id_encuesta ?>&id_avicola=5&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">GRANJAS AVICOLA LINEA POSTURA</label>
                                    <!-- <select class="form-control" name="Encuesta[cap5_modg_p502_b_flag]" id="cap5_modg_p502_b_flag">
                                        <option value>Seleccionar</option>
                                        <option value="1">SI</option>
                                        <option value="2">NO</option>
                                    </select> -->
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                            
                                <table id="lista-cap5-modg-linea-carne" class="table">
                                    <thead>
                                        <th>516. Durante el 2020 tuvo  …………………………………..?:</th>
                                        <th>Acciones</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modg_p516_6" disabled>
                                                    <label class="custom-control-label" for="cap5_modg_p516_6"> 2. Reproductoras de Gallinas de postura(REPRODUCTORAS DE GALLINAS DE POSTURA)</label>
                                                </div> 

                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-g2?id_encuesta=<?= $id_encuesta ?>&id_avicola=6&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modg_p516_7" disabled>
                                                    <label class="custom-control-label" for="cap5_modg_p516_7"> 3. Pollas de levante(GALLINAS DE POSTURA)</label>
                                                </div>     
                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-g2?id_encuesta=<?= $id_encuesta ?>&id_avicola=7&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modg_p516_8" disabled>
                                                    <label class="custom-control-label" for="cap5_modg_p516_8"> 4. Gallinas de postura en producción(GALLINAS DE POSTURA)</label>
                                                </div>
                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-g2?id_encuesta=<?= $id_encuesta ?>&id_avicola=8&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a>
                                            </td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for="">Módulo V.H. PRODUCCIÓN PECUARIA; SUBPRODUCTOS  DESDE ENERO A DICIEMBRE 2020</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">523.  Durante el 2020 de su producción pecuaria. ¿Obtuvo subproductos pecuarios?  </label>
                                    <select class="form-control" name="Encuesta[cap5_modh_p502_flag]" id="cap5_modh_p502_flag">
                                        <option value>Seleccionar</option>
                                        <option value="1">SI</option>
                                        <option value="2">NO</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                            
                                <table id="lista-cap5-modh-subproducto" class="table">
                                    <thead>
                                        <th>524. ¿Entre enero y diciembre del 2020, obtuvo? </th>
                                        <th>Acciones</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modh_p524_1" disabled>
                                                    <label class="custom-control-label" for="cap5_modh_p524_1"> 1. ¿Leche de vaca?</label>
                                                </div>
                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-h?id_encuesta=<?= $id_encuesta ?>&id_producto=1&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modh_p524_2" disabled>
                                                    <label class="custom-control-label" for="cap5_modh_p524_2"> 2. ¿Leche de cabra?</label>
                                                </div>    
                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-h?id_encuesta=<?= $id_encuesta ?>&id_producto=2&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modh_p524_3" disabled>
                                                    <label class="custom-control-label" for="cap5_modh_p524_3"> 3. ¿Leche de oveja?</label>
                                                </div>
                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-h?id_encuesta=<?= $id_encuesta ?>&id_producto=3&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modh_p524_4" disabled>
                                                    <label class="custom-control-label" for="cap5_modh_p524_4"> 4. ¿Lana esquilada de oveja?</label>
                                                </div>
                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-h?id_encuesta=<?= $id_encuesta ?>&id_producto=4&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modh_p524_5" disabled>
                                                    <label class="custom-control-label" for="cap5_modh_p524_5">5. ¿Fibra esquilada de alpaca?</label>
                                                </div>

                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-h?id_encuesta=<?= $id_encuesta ?>&id_producto=5&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modh_p524_6" disabled>
                                                    <label class="custom-control-label" for="cap5_modh_p524_6">6. ¿Fibra esquilada de llama?</label>
                                                </div>    
                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-h?id_encuesta=<?= $id_encuesta ?>&id_producto=6&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modh_p524_7" disabled>
                                                    <label class="custom-control-label" for="cap5_modh_p524_7">7. ¿Fibra esquilada de vicuña?</label>
                                                </div>
                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-h?id_encuesta=<?= $id_encuesta ?>&id_producto=7&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modh_p524_8" disabled>
                                                    <label class="custom-control-label" for="cap5_modh_p524_8">8. ¿Huevo de gallina? Solo para marco de area</label>
                                                </div>    
                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-h?id_encuesta=<?= $id_encuesta ?>&id_producto=8&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modh_p524_9" disabled>
                                                    <label class="custom-control-label" for="cap5_modh_p524_9">9. ¿Huevos de avestruces?</label>
                                                </div>

                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-h?id_encuesta=<?= $id_encuesta ?>&id_producto=9&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modh_p524_10" disabled>
                                                    <label class="custom-control-label" for="cap5_modh_p524_10">10. ¿Huevos de codornices</label>
                                                </div>    
                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-h?id_encuesta=<?= $id_encuesta ?>&id_producto=10&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="cap5_modh_p524_11" disabled>
                                                    <label class="custom-control-label" for="cap5_modh_p524_11">11. ¿Miel de abeja?</label>
                                                </div> 

                                            </td>
                                            <td>
                                                <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulov/capitulo-v-modulo-h?id_encuesta=<?= $id_encuesta ?>&id_producto=11&geocodigo_fdo=<?= $geocodigo_fdo ?>"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a>
                                            </td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for="">Módulo V.I.  BUENAS PRÁCTICAS PECUARIAS ENTRE ENERO DE 2020 HASTA EL DÍA DE ENTREVISTA </label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for="">ALIMENTACIÓN DESDE ENERO DE 2020 HASTA EL DÍA DE LA ENTREVISTA</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <label for="">530. ¿Qué tipo de alimentación brinda a los animales en esta unidad agraria? (Nota: Selección múltiple, leer alternativas) </label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modi_p530_1" value="1">
                                            <label class="custom-control-label" for="cap5_modi_p530_1">¿Pastoreo en pastos naturales?</label>
                                        </div>

                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modi_p530_2" value="2">
                                            <label class="custom-control-label" for="cap5_modi_p530_2">¿Pastoreo en pastos cultivados?</label>
                                        </div>

                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modi_p530_3" value="3">
                                            <label class="custom-control-label" for="cap5_modi_p530_3">¿Corte de pastos sembrados?</label>
                                        </div>

                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modi_p530_4" value="4">
                                            <label class="custom-control-label" for="cap5_modi_p530_4">¿Heno?</label>
                                        </div>

                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modi_p530_5" value="5">
                                            <label class="custom-control-label" for="cap5_modi_p530_5">¿Ensilaje?</label>
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modi_p530_6" value="6">
                                            <label class="custom-control-label" for="cap5_modi_p530_6">¿Suplementos alimenticios?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modi_p530_7" value="7">
                                            <label class="custom-control-label" for="cap5_modi_p530_7">¿Forraje verde hidropónico?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modi_p530_8" value="8">
                                            <label class="custom-control-label" for="cap5_modi_p530_8">¿Alimento balanceado?  (Pase a P531, P532, P533)</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modi_p530_9" value="9">
                                            <label class="custom-control-label" for="cap5_modi_p530_9">Otro:</label>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row cap5_modi_p530_9" style="display:none">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">Especifique</label>
                                    <input type="text" class="form-control" id="cap5_modi_p530_9_especifique" placeholder="Especifique" name="Encuesta[cap5_modi_p530_9_especifique]" maxlength="250">
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-6" >
                                <div class="form-group">
                                    <label for="">531. ¿Cuál es la procedencia del  alimento balanceado que utiliza en la unidad agraria?</label>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                <input type="checkbox" class="custom-control-input" id="cap5_modi_p531_1" value="1">
                                                <label class="custom-control-label" for="cap5_modi_p531_1">¿Comercial?</label>
                                            </div>

                                            <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                                <input type="checkbox" class="custom-control-input" id="cap5_modi_p531_2" value="2">
                                                <label class="custom-control-label" for="cap5_modi_p531_2">De elaboración propia?</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12" >
                                <label for="">532. ¿Qué tipo de alimento balanceado utiliza?</label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modi_p532_1" value="1">
                                            <label class="custom-control-label" for="cap5_modi_p532_1">¿Peletizado?</label>
                                        </div>

                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modi_p532_2" value="2">
                                            <label class="custom-control-label" for="cap5_modi_p532_2">¿Harina?</label>
                                        </div>

                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modi_p532_3" value="3">
                                            <label class="custom-control-label" for="cap5_modi_p532_3">¿Otra?:</label>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <label for="">533. ¿Qué tipo de insumos utiliza para la elaboración propia del alimento balanceado?      </label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modi_p533_1" value="1">
                                            <label class="custom-control-label" for="cap5_modi_p533_1">Alfalfa</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modi_p533_2" value="2">
                                            <label class="custom-control-label" for="cap5_modi_p533_2">Aceite hidrogenado de pescado</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modi_p533_3" value="3">
                                            <label class="custom-control-label" for="cap5_modi_p533_3">Alimento de maíz molido (Hominy Feed)</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modi_p533_4" value="4">
                                            <label class="custom-control-label" for="cap5_modi_p533_4">Avena forrajera</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modi_p533_5" value="5">
                                            <label class="custom-control-label" for="cap5_modi_p533_5">Broza de espárrago</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modi_p533_6" value="6">
                                            <label class="custom-control-label" for="cap5_modi_p533_6">Cebada</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modi_p533_7" value="7">
                                            <label class="custom-control-label" for="cap5_modi_p533_7">Harina de alfalfa</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modi_p533_8" value="8">
                                            <label class="custom-control-label" for="cap5_modi_p533_8">Harina de huesos</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modi_p533_9" value="9">
                                            <label class="custom-control-label" for="cap5_modi_p533_9">Harina de pescado</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modi_p533_10" value="10">
                                            <label class="custom-control-label" for="cap5_modi_p533_10">Hoja de camote</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modi_p533_11" value="11">
                                            <label class="custom-control-label" for="cap5_modi_p533_11">Forraje hidropónico de cebada</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modi_p533_12" value="12">
                                            <label class="custom-control-label" for="cap5_modi_p533_12">Leche descremada</label>
                                        </div>


                                    </div>

                                    <div class="col-md-6">
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modi_p533_13" value="13">
                                            <label class="custom-control-label" for="cap5_modi_p533_13">Maíz amarillo duro</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modi_p533_14" value="14">
                                            <label class="custom-control-label" for="cap5_modi_p533_14">Maíz chala</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modi_p533_15" value="15">
                                            <label class="custom-control-label" for="cap5_modi_p533_15">Maíz panca</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modi_p533_16" value="16">
                                            <label class="custom-control-label" for="cap5_modi_p533_16">Melaza de caña</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modi_p533_17" value="17">
                                            <label class="custom-control-label" for="cap5_modi_p533_17">Orujo de cebada</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modi_p533_18" value="18">
                                            <label class="custom-control-label" for="cap5_modi_p533_18">Pasta de algodón</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modi_p533_19" value="19">
                                            <label class="custom-control-label" for="cap5_modi_p533_19">Polvillo de arroz</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modi_p533_20" value="20">
                                            <label class="custom-control-label" for="cap5_modi_p533_20">Sorgo</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modi_p533_21" value="21">
                                            <label class="custom-control-label" for="cap5_modi_p533_21">Subproducto de trigo</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modi_p533_22" value="22">
                                            <label class="custom-control-label" for="cap5_modi_p533_22">Premezcla</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modi_p533_23" value="23">
                                            <label class="custom-control-label" for="cap5_modi_p533_23">Mezcla</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modi_p533_24" value="24">
                                            <label class="custom-control-label" for="cap5_modi_p533_24"> Otra:</label>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row cap5_modi_p533_24" style="display:none">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">Especifique</label>
                                    <input type="text" class="form-control" id="cap5_modi_p533_24_especifique" placeholder="Especifique" name="Encuesta[cap5_modi_p533_24_especifique]" maxlength="250">
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for="">Módulo V.J. MANEJO SANITARIO DESDE ENERO DE 2020 HASTA EL DÍA DE LA ENTREVISTA</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">534a. Desde enero del 2020 hasta hoy, ¿aplico vacunas?</label>
                                    <select class="form-control" name="Encuesta[cap5_modj_p534_a]" id="cap5_modj_p534_a">
                                        <option value>Seleccionar</option>
                                        <option value="1">Si</option>
                                        <option value="2">No</option>
                                    </select>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12">
                                <label for="">534b. ¿Qué vacunas aplicó?</label>
                                <label for="">POLLOS (Nota: Selección múltiple)</label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p534_b_1" value="1">
                                            <label class="custom-control-label" for="cap5_modj_p534_b_1">Enfermedad de marek</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p534_b_2" value="2">
                                            <label class="custom-control-label" for="cap5_modj_p534_b_2">Enfermedad de Gumboro</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p534_b_3" value="3">
                                            <label class="custom-control-label" for="cap5_modj_p534_b_3">Enfermedad New Castle</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p534_b_4" value="4">
                                            <label class="custom-control-label" for="cap5_modj_p534_b_4">Bronquitis infecciosa</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p534_b_5" value="5">
                                            <label class="custom-control-label" for="cap5_modj_p534_b_5">Otro:</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p534_b_6" value="6">
                                            <label class="custom-control-label" for="cap5_modj_p534_b_6">No sabe</label>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="row cap5_modj_p534_b_5" style="display:none">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">Especifique</label>
                                    <input type="text" class="form-control" id="cap5_modj_p534_b_5_especifique" placeholder="Especifique" name="Encuesta[cap5_modj_p534_b_5_especifique]" maxlength="250">
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12">
                                <label for="">534b. ¿Qué vacunas aplicó?</label>
                                <label for="">PAVOS (Nota: Selección múltiple)</label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p534_b_7" value="7">
                                            <label class="custom-control-label" for="cap5_modj_p534_b_7">Viruela aviar</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p534_b_8" value="8">
                                            <label class="custom-control-label" for="cap5_modj_p534_b_8">Cólera aviar</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p534_b_9" value="9">
                                            <label class="custom-control-label" for="cap5_modj_p534_b_9">Rinotraqueitis infecciosa </label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p534_b_10" value="10">
                                            <label class="custom-control-label" for="cap5_modj_p534_b_10">Enfermedad New Castle</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p534_b_11" value="11">
                                            <label class="custom-control-label" for="cap5_modj_p534_b_11">Otro:</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p534_b_12" value="12">
                                            <label class="custom-control-label" for="cap5_modj_p534_b_12">No sabe</label>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="row cap5_modj_p534_b_11" style="display:none">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">Especifique</label>
                                    <input type="text" class="form-control" id="cap5_modj_p534_b_11_especifique" placeholder="Especifique" name="Encuesta[cap5_modj_p534_b_11_especifique]" maxlength="250">
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12">
                                <label for="">534b. ¿Qué vacunas aplicó?</label>
                                <label for="">GALLINAS DE POSTURA (Nota: Selección múltiple)</label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p534_b_13" value="13">
                                            <label class="custom-control-label" for="cap5_modj_p534_b_13">Síndrome de caida de postura</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p534_b_14" value="14">
                                            <label class="custom-control-label" for="cap5_modj_p534_b_14">Coriza infecciosa</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p534_b_15" value="15">
                                            <label class="custom-control-label" for="cap5_modj_p534_b_15">Enfermedad de marek</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p534_b_16" value="16">
                                            <label class="custom-control-label" for="cap5_modj_p534_b_16">Enfermedad de Gumboro</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p534_b_17" value="17">
                                            <label class="custom-control-label" for="cap5_modj_p534_b_17">Enfermedad New Castle</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p534_b_18" value="18">
                                            <label class="custom-control-label" for="cap5_modj_p534_b_18">Otro:</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p534_b_19" value="19">
                                            <label class="custom-control-label" for="cap5_modj_p534_b_19">No sabe</label>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="row cap5_modj_p534_b_18" style="display:none">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">Especifique</label>
                                    <input type="text" class="form-control" id="cap5_modj_p534_b_18_especifique" placeholder="Especifique" name="Encuesta[cap5_modj_p534_b_18_especifique]" maxlength="250">
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12">
                                <label for="">534b. ¿Qué vacunas aplicó?</label>
                                <label for="">VACUNOS (Nota: Selección múltiple)</label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p534_b_20" value="20">
                                            <label class="custom-control-label" for="cap5_modj_p534_b_20">Fiebre Aftosa</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p534_b_21" value="21">
                                            <label class="custom-control-label" for="cap5_modj_p534_b_21">Fiebre carbonosa</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p534_b_22" value="22">
                                            <label class="custom-control-label" for="cap5_modj_p534_b_22">Carbunco sintomático</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p534_b_23" value="23">
                                            <label class="custom-control-label" for="cap5_modj_p534_b_23">Edema maligno</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p534_b_24" value="24">
                                            <label class="custom-control-label" for="cap5_modj_p534_b_24">Septicemia hemorrágica</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p534_b_25" value="25">
                                            <label class="custom-control-label" for="cap5_modj_p534_b_25">Enterotoxemia</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p534_b_26" value="26">
                                            <label class="custom-control-label" for="cap5_modj_p534_b_26">Rabia paralítica bovina</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p534_b_27" value="27">
                                            <label class="custom-control-label" for="cap5_modj_p534_b_27">Otro:</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p534_b_28" value="28">
                                            <label class="custom-control-label" for="cap5_modj_p534_b_28">No sabe</label>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="row cap5_modj_p534_b_27" style="display:none">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">Especifique</label>
                                    <input type="text" class="form-control" id="cap5_modj_p534_b_27_especifique" placeholder="Especifique" name="Encuesta[cap5_modj_p534_b_27_especifique]" maxlength="250">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <label for="">534b. ¿Qué vacunas aplicó?</label>
                                <label for="">PORCINOS (Nota: Selección múltiple)</label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p534_b_29" value="29">
                                            <label class="custom-control-label" for="cap5_modj_p534_b_29">Peste porcina  clásica</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p534_b_30" value="30">
                                            <label class="custom-control-label" for="cap5_modj_p534_b_30">Erisipela</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p534_b_31" value="31">
                                            <label class="custom-control-label" for="cap5_modj_p534_b_31">Leptospirosis</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p534_b_32" value="32">
                                            <label class="custom-control-label" for="cap5_modj_p534_b_32">Parvovirosis</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p534_b_33" value="33">
                                            <label class="custom-control-label" for="cap5_modj_p534_b_33">Mycoplasmosis</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p534_b_34" value="34">
                                            <label class="custom-control-label" for="cap5_modj_p534_b_34">Rinitis atrófica</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p534_b_35" value="35">
                                            <label class="custom-control-label" for="cap5_modj_p534_b_35">Coli costridium</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p534_b_36" value="36">
                                            <label class="custom-control-label" for="cap5_modj_p534_b_36">La pleuroneumonía</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p534_b_37" value="37">
                                            <label class="custom-control-label" for="cap5_modj_p534_b_37">Otro:</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p534_b_38" value="38">
                                            <label class="custom-control-label" for="cap5_modj_p534_b_38">No sabe</label>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="row cap5_modj_p534_b_37" style="display:none">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">Especifique</label>
                                    <input type="text" class="form-control" id="cap5_modj_p534_b_37_especifique" placeholder="Especifique" name="Encuesta[cap5_modj_p534_b_37_especifique]"  maxlength="250">
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">535. Desde enero del 2020 hasta hoy ¿Realizó desparasitación?</label>
                                    <select class="form-control" name="Encuesta[cap5_modj_p535]" id="cap5_modj_p535">
                                        <option value>Seleccionar</option>
                                        <option value="1">Si</option>
                                        <option value="2">No</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <label for="">535a. Contra que parásitos internos?</label>
                                <label for="">1. VACUNOS:</label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p535_a_1" value="1">
                                            <label class="custom-control-label" for="cap5_modj_p535_a_1">Gastrointestinales y pulmonares</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p535_a_2" value="2">
                                            <label class="custom-control-label" for="cap5_modj_p535_a_2">Fasciola hepática</label>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <label for="">535a. Contra que parásitos internos?</label>
                                <label for="">2. OVINOS:</label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p535_a_3" value="3">
                                            <label class="custom-control-label" for="cap5_modj_p535_a_3">Fasciola hepática</label>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <label for="">535b. Contra que parasitos externos?  (se debe abrir solo lo que corresponde)</label>
                                <label for="">1. AVES:</label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p535_a_4" value="4">
                                            <label class="custom-control-label" for="cap5_modj_p535_a_4">Piojillo</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <label for="">535b. Contra que parasitos externos?  (se debe abrir solo lo que corresponde)</label>
                                <label for="">2. PORCINOS:</label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p535_a_5" value="5">
                                            <label class="custom-control-label" for="cap5_modj_p535_a_5">Sarna</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <label for="">535b. Contra que parasitos externos?  (se debe abrir solo lo que corresponde)</label>
                                <label for="">3. VACUNOS:</label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p535_a_6" value="6">
                                            <label class="custom-control-label" for="cap5_modj_p535_a_6">Garrapata</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modj_p535_a_7" value="7">
                                            <label class="custom-control-label" for="cap5_modj_p535_a_7">Tupe (dermatovia ominis)</label>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for="">Módulo V.K.  MEJORAMIENTO GENÉTICO DESDE ENERO DE 2020 HASTA EL DÍA DE LA ENTREVISTA. (No aplica para aves)</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">536. ¿Desde enero del 2020 hasta el día de hoy realizó mejoramiento genético?</label>
                                    <select class="form-control" name="Encuesta[cap5_modk_p536]" id="cap5_modk_p536">
                                        <option value>Seleccionar</option>
                                        <option value="1">Si</option>
                                        <option value="2">No</option>
                                    </select>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12">
                                <label for="">536a. ¿Qué técnicas de mejoramiento genético aplicó?</label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p536_a_1" value="1">
                                            <label class="custom-control-label" for="cap5_modk_p536_a_1">Selección</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p536_a_2" value="2">
                                            <label class="custom-control-label" for="cap5_modk_p536_a_2">Cruzamiento</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p536_a_3" value="3">
                                            <label class="custom-control-label" for="cap5_modk_p536_a_3">Uso de Reproductores registrados en registros oficiales de razas</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p536_a_4" value="4">
                                            <label class="custom-control-label" for="cap5_modk_p536_a_4">Inseminación artificial con semen importado</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p536_a_5" value="5">
                                            <label class="custom-control-label" for="cap5_modk_p536_a_5"> Inseminación artificial con semen nacional</label>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p536_a_6" value="6">
                                            <label class="custom-control-label" for="cap5_modk_p536_a_6">Introducción de reproductores mejorados</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p536_a_7" value="7">
                                            <label class="custom-control-label" for="cap5_modk_p536_a_7">Transferencia de embriones importados</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p536_a_8" value="8">
                                            <label class="custom-control-label" for="cap5_modk_p536_a_8">Transferencia de embriones nacionales</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p536_a_9" value="9">
                                            <label class="custom-control-label" for="cap5_modk_p536_a_9">Otro:</label>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="row cap5_modk_p536_a_9" style="display:none">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">Especifique</label>
                                    <input type="text" class="form-control" id="cap5_modk_p536_a_9_especifique" placeholder="Especifique" name="Encuesta[cap5_modk_p536_a_9_especifique]" maxlength="250">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <label for="">537. Desde enero 2020 hasta el día de hoy,  ¿Ha tenido reproductores machos y hembras:     </label>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p537_1" value="1">
                                            <label class="custom-control-label" for="cap5_modk_p537_1">¿De raza pura?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p537_2" value="2">
                                            <label class="custom-control-label" for="cap5_modk_p537_2">¿Mejorados?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p537_3" value="3">
                                            <label class="custom-control-label" for="cap5_modk_p537_3">¿Criollos?</label>
                                        </div>
                                        
                                    </div>

                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">538. Desde enero 2020 hasta el dia de hoy  ¿Ud utilizó semen o embriones certificados para reproducir o mejorar los animales que cría? </label>
                                    <select class="form-control" name="Encuesta[cap5_modk_p538]" id="cap5_modk_p538">
                                        <option value>Seleccionar</option>
                                        <option value="1">Si</option>
                                        <option value="2">No</option>
                                    </select>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12">
                                <label for="">539. ¿Qué información analiza para la toma de decisiones del material genético que usa en su ganado? </label>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p539_1" value="1">
                                            <label class="custom-control-label" for="cap5_modk_p539_1">¿Registros productivos?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p539_2" value="2">
                                            <label class="custom-control-label" for="cap5_modk_p539_2">¿Costos de producción?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p539_3" value="3">
                                            <label class="custom-control-label" for="cap5_modk_p539_3">¿Información de los sistemas de productividad lechera?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p539_4" value="4">
                                            <label class="custom-control-label" for="cap5_modk_p539_4">¿Información de catálogo de reproductor?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p539_5" value="5">
                                            <label class="custom-control-label" for="cap5_modk_p539_5">¿Ninguno?</label>
                                        </div>
                                        
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">540. ¿Usted está afiliado a algún sistema de productividad?</label>
                                    <select class="form-control" name="Encuesta[cap5_modk_p540]" id="cap5_modk_p540">
                                        <option value>Seleccionar</option>
                                        <option value="1">Si</option>
                                        <option value="2">No</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <label for="">541. ¿Cuáles? son las razas que se crían y engordan en esta unidad agraria?    </label>
                                <label for="">BOVINOS:</label>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p541_a_1" value="1">
                                            <label class="custom-control-label" for="cap5_modk_p541_a_1">Holstein</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p541_a_2" value="2">
                                            <label class="custom-control-label" for="cap5_modk_p541_a_2">Jersey</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p541_a_3" value="3">
                                            <label class="custom-control-label" for="cap5_modk_p541_a_3">Brown swiss</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p541_a_4" value="4">
                                            <label class="custom-control-label" for="cap5_modk_p541_a_4">Brahman/cebú</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p541_a_5" value="5">
                                            <label class="custom-control-label" for="cap5_modk_p541_a_5">Gyr/cebú</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p541_a_6" value="6">
                                            <label class="custom-control-label" for="cap5_modk_p541_a_6">Guzerat/cebú</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p541_a_7" value="7">
                                            <label class="custom-control-label" for="cap5_modk_p541_a_7">Criollo</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p541_a_8" value="8">
                                            <label class="custom-control-label" for="cap5_modk_p541_a_8">Otra:</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row cap5_modk_p541_a_8" style="display:none">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">Especifique</label>
                                    <input type="text" class="form-control" id="cap5_modk_p541_a_8_especifique" placeholder="Especifique" name="Encuesta[cap5_modk_p541_a_8_especifique]" maxlength="250">
                                </div>
                            </div>
                        </div>



                        <div class="row">
                            <div class="col-md-12">
                                <label for="">541. ¿Cuáles? son las razas que se crían y engordan en esta unidad agraria?    </label>
                                <label for="">PORCINOS:</label>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p541_a_9" value="9">
                                            <label class="custom-control-label" for="cap5_modk_p541_a_9">Landrace</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p541_a_10" value="10">
                                            <label class="custom-control-label" for="cap5_modk_p541_a_10">Yorkshire/Large white</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p541_a_11" value="11">
                                            <label class="custom-control-label" for="cap5_modk_p541_a_11">Duroc</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p541_a_12" value="12">
                                            <label class="custom-control-label" for="cap5_modk_p541_a_12">Hampshire</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p541_a_13" value="13">
                                            <label class="custom-control-label" for="cap5_modk_p541_a_13">Pietrain</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p541_a_14" value="14">
                                            <label class="custom-control-label" for="cap5_modk_p541_a_14">Criollo</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p541_a_15" value="15">
                                            <label class="custom-control-label" for="cap5_modk_p541_a_15">Otra:</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row cap5_modk_p541_a_15" style="display:none">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">Especifique</label>
                                    <input type="text" class="form-control" id="cap5_modk_p541_a_15_especifique" placeholder="Especifique" name="Encuesta[cap5_modk_p541_a_15_especifique]" maxlength="250">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <label for="">541. ¿Cuáles? son las razas que se crían y engordan en esta unidad agraria?    </label>
                                <label for="">OVINOS:</label>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p541_a_16" value="16">
                                            <label class="custom-control-label" for="cap5_modk_p541_a_16">Corriedale</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p541_a_17" value="17">
                                            <label class="custom-control-label" for="cap5_modk_p541_a_17">Junín</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p541_a_18" value="18">
                                            <label class="custom-control-label" for="cap5_modk_p541_a_18">Hampshire Down</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p541_a_19" value="19">
                                            <label class="custom-control-label" for="cap5_modk_p541_a_19">Black Belly</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p541_a_20" value="20">
                                            <label class="custom-control-label" for="cap5_modk_p541_a_20">Assaf</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p541_a_21" value="21">
                                            <label class="custom-control-label" for="cap5_modk_p541_a_21">Asblack</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p541_a_22" value="22">
                                            <label class="custom-control-label" for="cap5_modk_p541_a_22">East Friesian</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p541_a_23" value="23">
                                            <label class="custom-control-label" for="cap5_modk_p541_a_23">Criollo</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p541_a_24" value="24">
                                            <label class="custom-control-label" for="cap5_modk_p541_a_24">Otra:</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row cap5_modk_p541_a_24" style="display:none">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">Especifique</label>
                                    <input type="text" class="form-control" id="cap5_modk_p541_a_24_especifique" placeholder="Especifique" name="Encuesta[cap5_modk_p541_a_24_especifique]" maxlength="250">
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12">
                                <label for="">541. ¿Cuáles? son las razas que se crían y engordan en esta unidad agraria?    </label>
                                <label for="">CAPRINOS:</label>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p541_a_25" value="25">
                                            <label class="custom-control-label" for="cap5_modk_p541_a_25">Nubiano</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p541_a_26" value="26">
                                            <label class="custom-control-label" for="cap5_modk_p541_a_26">Anglonubian</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p541_a_27" value="27">
                                            <label class="custom-control-label" for="cap5_modk_p541_a_27">Toggenberg</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p541_a_28" value="28">
                                            <label class="custom-control-label" for="cap5_modk_p541_a_28">Saanen</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p541_a_29" value="29">
                                            <label class="custom-control-label" for="cap5_modk_p541_a_29">Criollo</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p541_a_30" value="30">
                                            <label class="custom-control-label" for="cap5_modk_p541_a_30">Otro:</label>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row cap5_modk_p541_a_30" style="display:none">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">Especifique</label>
                                    <input type="text" class="form-control" id="cap5_modk_p541_a_30_especifique" placeholder="Especifique" name="Encuesta[cap5_modk_p541_a_30_especifique]" maxlength="250">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <label for="">541. ¿Cuáles? son las razas que se crían y engordan en esta unidad agraria?    </label>
                                <label for="">ALPACAS:</label>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p541_a_31" value="31">
                                            <label class="custom-control-label" for="cap5_modk_p541_a_31">Suri</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p541_a_32" value="32">
                                            <label class="custom-control-label" for="cap5_modk_p541_a_32">Huacaya</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p541_a_33" value="33">
                                            <label class="custom-control-label" for="cap5_modk_p541_a_33">Cruzados</label>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <label for="">541. ¿Cuáles? son las razas que se crían y engordan en esta unidad agraria?    </label>
                                <label for="">LLAMAS:</label>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p541_a_34" value="34">
                                            <label class="custom-control-label" for="cap5_modk_p541_a_34">Q´ara</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p541_a_35" value="35">
                                            <label class="custom-control-label" for="cap5_modk_p541_a_35">Ch´acu</label>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <label for="">541. ¿Cuáles? son las razas que se crían y engordan en esta unidad agraria?    </label>
                                <label for="">CUYES:</label>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p541_a_36" value="36">
                                            <label class="custom-control-label" for="cap5_modk_p541_a_36">Perú</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p541_a_37" value="37">
                                            <label class="custom-control-label" for="cap5_modk_p541_a_37">Inti</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p541_a_38" value="38">
                                            <label class="custom-control-label" for="cap5_modk_p541_a_38">Andina</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap5_modk_p541_a_39" value="39">
                                            <label class="custom-control-label" for="cap5_modk_p541_a_39">Otro:</label>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row cap5_modk_p541_a_39" style="display:none">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">Especifique</label>
                                    <input type="text" class="form-control" id="cap5_modk_p541_a_39_especifique" placeholder="Especifique" name="Encuesta[cap5_modk_p541_a_39_especifique]" maxlength="250">
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for="">Observación</label>
                                <textarea name="Encuesta[cap5_observacion]" id="cap5_observacion" cols="30" rows="5" class="form-control" maxlength="500"></textarea>
                            </div>
                        </div>





                    </div>

                    <div class="tab-pane" id="capitulo_vi" role="tabpanel">
                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for="">CAPÍTULO VI.  INOCUIDAD (PARA TODO PRODUCTOR/A AGRARIO/A) DESDE ENERO DE 2020 HASTA EL DÍA DE LA ENTREVISTA</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label for="">601. ¿Qué hace con los residuos de sus cultivos?  </label>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap6_p601_1" value="1">
                                            <label class="custom-control-label" for="cap6_p601_1">Quema/incinera</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap6_p601_2" value="2">
                                            <label class="custom-control-label" for="cap6_p601_2">Usa como alimento para animales de crianza</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap6_p601_3" value="3">
                                            <label class="custom-control-label" for="cap6_p601_3">Desecha</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap6_p601_4" value="4">
                                            <label class="custom-control-label" for="cap6_p601_4">Usa como abono (o compost)</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap6_p601_5" value="5">
                                            <label class="custom-control-label" for="cap6_p601_5">Alimento de maíz molido (Hominy Feed)</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap6_p601_6" value="6">
                                            <label class="custom-control-label" for="cap6_p601_6">Usa como combustible</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap6_p601_7" value="7">
                                            <label class="custom-control-label" for="cap6_p601_7">Otro:</label>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row cap6_p601_7" style="display:none">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">Especifique</label>
                                    <input type="text" class="form-control" id="cap6_p601_7_especifique" placeholder="Especifique" name="Encuesta[cap6_p601_7_especifique]" maxlength="250">
                                </div>
                            </div>
                        </div>



                        <div class="row">
                            <div class="col-md-12">
                                <label for="">602. ¿Qué hace con los residuos de sus animales de crianza?   </label>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap6_p602_1" value="1">
                                            <label class="custom-control-label" for="cap6_p602_1">Quema/incinera</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap6_p602_2" value="2">
                                            <label class="custom-control-label" for="cap6_p602_2">Entierra</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap6_p602_3" value="3">
                                            <label class="custom-control-label" for="cap6_p602_3">Desecha</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap6_p602_4" value="4">
                                            <label class="custom-control-label" for="cap6_p602_4">Usa como abono (o compost)</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap6_p602_5" value="5">
                                            <label class="custom-control-label" for="cap6_p602_5">Usa como combustible</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap6_p602_6" value="6">
                                            <label class="custom-control-label" for="cap6_p602_6">Usa como alimento para animales de crianza</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap6_p602_7" value="7">
                                            <label class="custom-control-label" for="cap6_p602_7">Otro:</label>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row cap6_p602_7" style="display:none">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">Especifique</label>
                                    <input type="text" class="form-control" id="cap6_p602_7_especifique" placeholder="Especifique" name="Encuesta[cap6_p602_7_especifique]" maxlength="250">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">603. ¿Existen minas de explotación cerca a sus cultivos y animales de crianza?</label>
                                    <select class="form-control" name="Encuesta[cap6_p603]" id="cap6_p603">
                                        <option value>Seleccionar</option>
                                        <option value="1">Si</option>
                                        <option value="2">No</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">604. ¿Alguna entidad del estado le ha informado sobre la contaminación de los alimentos?</label>
                                    <select class="form-control" name="Encuesta[cap6_p604]" id="cap6_p604">
                                        <option value>Seleccionar</option>
                                        <option value="1">Si</option>
                                        <option value="2">No</option>
                                    </select>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12">
                                <label for="">605. ¿Qué entidad del estado le informó? </label>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap6_p605_1" value="1">
                                            <label class="custom-control-label" for="cap6_p605_1">SENASA Servicio Nacional de Sanidad Agraria</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap6_p605_2" value="2">
                                            <label class="custom-control-label" for="cap6_p605_2">MINSA Ministerio de Salud</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap6_p605_3" value="3">
                                            <label class="custom-control-label" for="cap6_p605_3">MIDAGRI Ministerio de Desarrollo Agrario y Riego</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap6_p605_4" value="4">
                                            <label class="custom-control-label" for="cap6_p605_4">GORE Gobierno Regional</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap6_p605_5" value="5">
                                            <label class="custom-control-label" for="cap6_p605_5">Gobierno Local / Distrital</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap6_p605_6" value="6">
                                            <label class="custom-control-label" for="cap6_p605_6">Otro: </label>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row cap6_p605_6" style="display:none">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">Especifique</label>
                                    <input type="text" class="form-control" id="cap6_p605_6_especifique" placeholder="Especifique" name="Encuesta[cap6_p605_6_especifique]" maxlength="250">
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12">
                                <label for="">606. ¿En qué lugar conserva o guarda los alimentos que produce? (solo para productos primarios) (Nota: Selección múltiple)</label>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap6_p606_1" value="1">
                                            <label class="custom-control-label" for="cap6_p606_1">En un lugar refrigerado</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap6_p606_2" value="2">
                                            <label class="custom-control-label" for="cap6_p606_2">A la intemperie</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap6_p606_3" value="3">
                                            <label class="custom-control-label" for="cap6_p606_3">En cuarto seguro y ventilado</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap6_p606_4" value="4">
                                            <label class="custom-control-label" for="cap6_p606_4">En cuarto no adecuado</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap6_p606_5" value="5">
                                            <label class="custom-control-label" for="cap6_p606_5">Otro: </label>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row cap6_p606_5" style="display:none">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">Especifique</label>
                                    <input type="text" class="form-control" id="cap6_p606_5_especifique" placeholder="Especifique" name="Encuesta[cap6_p606_5_especifique]" maxlength="250">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">607. ¿Sabe Usted, que los alimentos de consumo humano que produce, deben estar identificados para ser consumidos?</label>
                                    <select class="form-control" name="Encuesta[cap6_p607]" id="cap6_p607">
                                        <option value>Seleccionar</option>
                                        <option value="1">Si</option>
                                        <option value="2">No</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">608. Sus productos agrarios, ¿Cuentan con algún tipo de certificación de calidad otorgada por una institución?</label>
                                    <select class="form-control" name="Encuesta[cap6_p608]" id="cap6_p608">
                                        <option value>Seleccionar</option>
                                        <option value="1">Si</option>
                                        <option value="2">No</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">609.  La certificación de calidad . ¿Fué otorgada como:</label>
                                    <select class="form-control" name="Encuesta[cap6_p609]" id="cap6_p609">
                                        <option value="99999">Seleccionar</option>
                                        <option value="1">Productor/a?</option>
                                        <option value="2">Asociación/cooperativa/comité (grupal)?</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <label for="">610. ¿Cuál es el nombre de la certificación?</label>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap6_p610_1" value="1">
                                            <label class="custom-control-label" for="cap6_p610_1">Buenas prácticas agrícolas (Global Gap, ETI, Rainforest Alliance, Friendly Bird)</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap6_p610_2" value="2">
                                            <label class="custom-control-label" for="cap6_p610_2">Comercio justo (Fair trade)</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap6_p610_3" value="3">
                                            <label class="custom-control-label" for="cap6_p610_3">TN (TESCO NURTURE)</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap6_p610_4" value="4">
                                            <label class="custom-control-label" for="cap6_p610_4">Certificación orgánica</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap6_p610_5" value="5">
                                            <label class="custom-control-label" for="cap6_p610_5">Certificación SGP (Sistema de Garantía Participativo)</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap6_p610_6" value="6">
                                            <label class="custom-control-label" for="cap6_p610_6">Certificados de orígen (ICO)</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap6_p610_7" value="7">
                                            <label class="custom-control-label" for="cap6_p610_7">Otra:</label>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row cap6_p610_7" style="display:none">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">Especifique</label>
                                    <input type="text" class="form-control" id="cap6_p610_7_especifique" placeholder="Especifique" name="Encuesta[cap6_p610_7_especifique]" maxlength="250">
                                </div>
                            </div>
                        </div>






                    </div>

                    <div class="tab-pane" id="capitulo_vii" role="tabpanel">
                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for="">Módulo VII.C.  ASOCIATIVIDAD (PARA TODO PRODUCTOR/A AGRARIO/A) DESDE ENERO DE 2020 HASTA EL DÍA DE LA ENTREVISTA</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">710. ¿Ud pertenece a alguna asociación, cooperativa, comité o comunidad de productores/as agrarios/as?</label>
                                    <select class="form-control" name="Encuesta[cap7_modc_p710_a]" id="cap7_modc_p710_a">
                                        <option value>Seleccionar</option>
                                        <option value="1">Si</option>
                                        <option value="2">No</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">710. ¿Ud pertenece a alguna asociación, cooperativa, comité o comunidad de productores/as agrarios/as? ¿A cuántas pertenece?</label>
                                    <input type="text" class="form-control numerico" name="Encuesta[cap7_modc_p710_b]" id="cap7_modc_p710_b" maxlength="3">
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12 mb-2" style="padding-left:0px">
                                <a class="btn btn-success" target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulovii/create-capitulo-vii-modulo-c-asociaciones?id_encuesta=<?= $id_encuesta ?>&geocodigo_fdo=<?= $geocodigo_fdo ?>">Agregar</a>
                            </div>
                            <div class="col-md-12" style="padding-left:0px">
                            
                                <table id="lista-cap7-modc-asociaciones" class="table">
                                    <thead>
                                        <th>711. ¿Cuál es el nombre de la asociación, cooperativa, comité o comunidad a la que pertenece?</th>
                                        <th>712. ¿Cuál es el tipo: </th>
                                        <th>713. Desde qué año pertenece a la /el?</th>
                                        <th>Acciones</th>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <label for="">714. Desde enero 2020 hasta hoy, su participación en esta asociación, cooperativa,comité y/o comunidad de productores/as agrarios/as. ¿Qué beneficios o servicios le brindó? Nota: Selección Mutiple</label>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_modc_p714_1" value="1">
                                            <label class="custom-control-label" for="cap7_modc_p714_1">Acceso o mercados locales/nacionales para la venta de las cosechas y/productos pecuarios</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_modc_p714_2" value="2">
                                            <label class="custom-control-label" for="cap7_modc_p714_2">Acceso al mercado exterior para la venta de las cosechas y/productos pecuarios</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_modc_p714_3" value="3">
                                            <label class="custom-control-label" for="cap7_modc_p714_3">Abastecimiento de insumos agrícolas y/o pecuarios</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_modc_p714_4" value="4">
                                            <label class="custom-control-label" for="cap7_modc_p714_4">Obtener asistencia técnica y/o capacitación</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_modc_p714_5" value="5">
                                            <label class="custom-control-label" for="cap7_modc_p714_5">Acceso a servicios financieros y/o créditos</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_modc_p714_6" value="6">
                                            <label class="custom-control-label" for="cap7_modc_p714_6">Otro:</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_modc_p714_7" value="7">
                                            <label class="custom-control-label" for="cap7_modc_p714_7">Ningún beneficio o servicio</label>
                                        </div>
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row cap7_modc_p714_6" style="display:none">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">Especifique</label>
                                    <input type="text" class="form-control" id="cap7_modc_p714_6_especifique" placeholder="Especifique" name="Encuesta[cap7_modc_p714_6_especifique]" maxlength="250">
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12">
                                <label for="">715. Desde enero de 2020 hasta hoy, ¿Usted a través de la asociación, cooperativa,  comité, comunidad de productores/as agrarios/as a la cual pertenece ha participado en: Nota: Selección Mutiple</label>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_modc_p715_1" value="1">
                                            <label class="custom-control-label" for="cap7_modc_p715_1">Ferias locales, regionales o nacionales?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_modc_p715_2" value="2">
                                            <label class="custom-control-label" for="cap7_modc_p715_2">Bioferias locales, regionales o nacionales?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_modc_p715_3" value="3">
                                            <label class="custom-control-label" for="cap7_modc_p715_3">Ferias internacionales?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_modc_p715_4" value="4">
                                            <label class="custom-control-label" for="cap7_modc_p715_4">Ruedas de negocios?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_modc_p715_5" value="5">
                                            <label class="custom-control-label" for="cap7_modc_p715_5">Misiones comerciales?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_modc_p715_6" value="6">
                                            <label class="custom-control-label" for="cap7_modc_p715_6">Pasantías?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_modc_p715_7" value="7">
                                            <label class="custom-control-label" for="cap7_modc_p715_7">Ninguno</label>
                                        </div>
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <label for="">716. Cuando usted participó a través de la organización a la que pertenece en Ferias, Ruedas de negocios, etc ¿logró? Nota: Selección Múltiple</label>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_modc_p716_1" value="1">
                                            <label class="custom-control-label" for="cap7_modc_p716_1">Vender al público?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_modc_p716_2" value="2">
                                            <label class="custom-control-label" for="cap7_modc_p716_2">Definir un contrato?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_modc_p716_3" value="3">
                                            <label class="custom-control-label" for="cap7_modc_p716_3">Obtener un pedido, cotización?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_modc_p716_4" value="4">
                                            <label class="custom-control-label" for="cap7_modc_p716_4">Coordinar un envío de muestras?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_modc_p716_5" value="5">
                                            <label class="custom-control-label" for="cap7_modc_p716_5">Compromiso verbal de ventas a futuro?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_modc_p716_6" value="6">
                                            <label class="custom-control-label" for="cap7_modc_p716_6">Otro:</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_modc_p716_7" value="7">
                                            <label class="custom-control-label" for="cap7_modc_p716_7">Ninguno</label>
                                        </div>
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row cap7_modc_p716_6" style="display:none">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">Especifique</label>
                                    <input type="text" class="form-control" id="cap7_modc_p716_6_especifique" placeholder="Especifique" name="Encuesta[cap7_modc_p716_6_especifique]" maxlength="250">
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12">
                                <label for="">717. Ud. como productor/a, a través de la organización a la que pertenece ¿Tuvo acceso a: Nota: Selección Multiple </label>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_modc_p717_1" value="1">
                                            <label class="custom-control-label" for="cap7_modc_p717_1">Centro de acopio o almacén para cosecha?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_modc_p717_2" value="2">
                                            <label class="custom-control-label" for="cap7_modc_p717_2">Almacén para granos y forrajes?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_modc_p717_3" value="3">
                                            <label class="custom-control-label" for="cap7_modc_p717_3">Secadores?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_modc_p717_4" value="4">
                                            <label class="custom-control-label" for="cap7_modc_p717_4">Seleccionador o equipo de clasificación?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_modc_p717_5" value="5">
                                            <label class="custom-control-label" for="cap7_modc_p717_5">Cámara de frío/tanque de enfriamento?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_modc_p717_6" value="6">
                                            <label class="custom-control-label" for="cap7_modc_p717_6">Despulpadora o fermentadores?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_modc_p717_7" value="7">
                                            <label class="custom-control-label" for="cap7_modc_p717_7">Otro:</label>
                                        </div>
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row cap7_modc_p717_7" style="display:none">
                            <div class="col-md-6" >
                                <div class="form-group">
                                    <label for="">Especifique</label>
                                    <input type="text" class="form-control" id="cap7_modc_p717_7_especifique" placeholder="Especifique" name="Encuesta[cap7_modc_p717_7_especifique]" maxlength="250">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <label for="">718. ¿El / La/Los es: Nota:Selección Múltiple (1. Propio de la organización? 2. Alquilado? 3. Cedido? 4. Otro?, especifique).</label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Centro de acopio o almacén para cosecha?</label>
                                            <select id="cap7_modc_p718_1" name="Encuesta[cap7_modc_p718_1]" class="form-control">
                                                <option value>Seleccionar</option>
                                                <option value="1">1. Propio de la organización?</option>
                                                <option value="2">2. Alquilado?</option>
                                                <option value="3">3. Cedido?</option>
                                                <option value="4">4. Otro?</option>
                                            </select>

                                            <div class="row cap7_modc_p718_1" style="display:none">
                                                <div class="col-md-6" >
                                                    <div class="form-group">
                                                        <label for="">Especifique</label>
                                                        <input type="text" class="form-control" id="cap7_modc_p718_1_especifique" placeholder="Especifique" name="Encuesta[cap7_modc_p718_1_especifique]" maxlength="250">
                                                    </div>
                                                </div>
                                            </div>
                        
                                        </div>
                                        <div class="form-group">
                                            <label for="">Almacén para granos y forrajes?</label>
                                            <select id="cap7_modc_p718_2" name="Encuesta[cap7_modc_p718_2]" class="form-control">
                                                <option value>Seleccionar</option>
                                                <option value="1">1. Propio de la organización?</option>
                                                <option value="2">2. Alquilado?</option>
                                                <option value="3">3. Cedido?</option>
                                                <option value="4">4. Otro?</option>
                                            </select>
                                            <div class="row cap7_modc_p718_2" style="display:none">
                                                <div class="col-md-6" >
                                                    <div class="form-group">
                                                        <label for="">Especifique</label>
                                                        <input type="text" class="form-control" id="cap7_modc_p718_2_especifique" placeholder="Especifique" name="Encuesta[cap7_modc_p718_2_especifique]" maxlength="250">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="">Secadores?</label>
                                            <select id="cap7_modc_p718_3" name="Encuesta[cap7_modc_p718_3]" class="form-control">
                                                <option value>Seleccionar</option>
                                                <option value="1">1. Propio de la organización?</option>
                                                <option value="2">2. Alquilado?</option>
                                                <option value="3">3. Cedido?</option>
                                                <option value="4">4. Otro?</option>
                                            </select>
                                            <div class="row cap7_modc_p718_3" style="display:none">
                                                <div class="col-md-6" >
                                                    <div class="form-group">
                                                        <label for="">Especifique</label>
                                                        <input type="text" class="form-control" id="cap7_modc_p718_3_especifique" placeholder="Especifique" name="Encuesta[cap7_modc_p718_3_especifique]" maxlength="250">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="">Seleccionador o equipo de clasificación?</label>
                                            <select id="cap7_modc_p718_4" name="Encuesta[cap7_modc_p718_4]" class="form-control">
                                                <option value>Seleccionar</option>
                                                <option value="1">1. Propio de la organización?</option>
                                                <option value="2">2. Alquilado?</option>
                                                <option value="3">3. Cedido?</option>
                                                <option value="4">4. Otro?</option>
                                            </select>
                                            <div class="row cap7_modc_p718_4" style="display:none">
                                                <div class="col-md-6" >
                                                    <div class="form-group">
                                                        <label for="">Especifique</label>
                                                        <input type="text" class="form-control" id="cap7_modc_p718_4_especifique" placeholder="Especifique" name="Encuesta[cap7_modc_p718_4_especifique]" maxlength="250">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="">Cámara de frío/tanque de enfriamento?</label>
                                            <select id="cap7_modc_p718_5" name="Encuesta[cap7_modc_p718_5]" class="form-control">
                                                <option value>Seleccionar</option>
                                                <option value="1">1. Propio de la organización?</option>
                                                <option value="2">2. Alquilado?</option>
                                                <option value="3">3. Cedido?</option>
                                                <option value="4">4. Otro?</option>
                                            </select>
                                            <div class="row cap7_modc_p718_5" style="display:none">
                                                <div class="col-md-6" >
                                                    <div class="form-group">
                                                        <label for="">Especifique</label>
                                                        <input type="text" class="form-control" id="cap7_modc_p718_5_especifique" placeholder="Especifique" name="Encuesta[cap7_modc_p718_5_especifique]" maxlength="250">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="">Despulpadora o fermentadores?</label>
                                            <select id="cap7_modc_p718_6" name="Encuesta[cap7_modc_p718_6]" class="form-control">
                                                <option value>Seleccionar</option>
                                                <option value="1">1. Propio de la organización?</option>
                                                <option value="2">2. Alquilado?</option>
                                                <option value="3">3. Cedido?</option>
                                                <option value="4">4. Otro?</option>
                                            </select>
                                            <div class="row cap7_modc_p718_6" style="display:none">
                                                <div class="col-md-6" >
                                                    <div class="form-group">
                                                        <label for="">Especifique</label>
                                                        <input type="text" class="form-control" id="cap7_modc_p718_6_especifique" placeholder="Especifique" name="Encuesta[cap7_modc_p718_6_especifique]" maxlength="250">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="">Otro:</label>
                                            <select id="cap7_modc_p718_7" name="Encuesta[cap7_modc_p718_7]" class="form-control">
                                                <option value>Seleccionar</option>
                                                <option value="1">1. Propio de la organización?</option>
                                                <option value="2">2. Alquilado?</option>
                                                <option value="3">3. Cedido?</option>
                                                <option value="4">4. Otro?</option>
                                            </select>
                                            <div class="row cap7_modc_p718_7" style="display:none">
                                                <div class="col-md-6" >
                                                    <div class="form-group">
                                                        <label for="">Especifique</label>
                                                        <input type="text" class="form-control" id="cap7_modc_p718_7_especifique" placeholder="Especifique" name="Encuesta[cap7_modc_p718_7_especifique]" maxlength="250">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>

                        <!-- <div class="row cap7_modc_p718_7" style="display:none">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">Especifique</label>
                                    <input type="text" class="form-control" id="cap7_modc_p718_7_especifique" placeholder="Especifique" name="Encuesta[cap7_modc_p718_7_especifique]" maxlength="250">
                                </div>
                            </div>
                        </div> -->


                        <div class="row">
                            <div class="col-md-12">
                                <label for="">719. ¿ Señor productor por qué no esta asociado?</label>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_modc_p719_1" value="1">
                                            <label class="custom-control-label" for="cap7_modc_p719_1">Por desconfianza respecto al modelo</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_modc_p719_2" value="2">
                                            <label class="custom-control-label" for="cap7_modc_p719_2">Por qué no hay disponibilidad de cooperativas en la zona</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_modc_p719_3" value="3">
                                            <label class="custom-control-label" for="cap7_modc_p719_3">Por desconocimiento</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_modc_p719_4" value="4">
                                            <label class="custom-control-label" for="cap7_modc_p719_4">No lo considera útil asociarse</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_modc_p719_5" value="5">
                                            <label class="custom-control-label" for="cap7_modc_p719_5">Por pérdida de ingresos por pago de impuestos</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_modc_p719_6" value="6">
                                            <label class="custom-control-label" for="cap7_modc_p719_6">Otro:</label>
                                        </div>
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row cap7_modc_p719_6" style="display:none">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">Especifique</label>
                                    <input type="text" class="form-control" id="cap7_modc_p719_6_especifique" placeholder="Especifique" name="Encuesta[cap7_modc_p719_6_especifique]" maxlength="250">
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for="">Módulo  VII.E.  SERVICIOS FINANCIEROS (PARA TODO PRODUCTOR/A AGRARIO/A) Desde Enero 2020 al día de entrevista</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">724. Desde enero de 2020 hasta la fecha ¿Ud. solicitó algún crédito o préstamo?</label>
                                    <select class="form-control" name="Encuesta[cap7_mode_p724]" id="cap7_mode_p724">
                                        <option value>Seleccionar</option>
                                        <option value="1">Si</option>
                                        <option value="2">No</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">725. ¿Obtuvo el crédito o préstamo que solicitó?</label>
                                    <select class="form-control" name="Encuesta[cap7_mode_p725]" id="cap7_mode_p725">
                                        <option value>Seleccionar</option>
                                        <option value="1">Si</option>
                                        <option value="2">No</option>
                                    </select>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12">
                                <label for="">726. ¿Quién le proporcionó el crédito o préstamo  que obtuvo? </label>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_mode_p726_1" value="1">
                                            <label class="custom-control-label" for="cap7_mode_p726_1">Agrobanco</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_mode_p726_2" value="2">
                                            <label class="custom-control-label" for="cap7_mode_p726_2">Caja municipal</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_mode_p726_3" value="3">
                                            <label class="custom-control-label" for="cap7_mode_p726_3">Caja rural</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_mode_p726_4" value="4">
                                            <label class="custom-control-label" for="cap7_mode_p726_4">Banca privada</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_mode_p726_5" value="5">
                                            <label class="custom-control-label" for="cap7_mode_p726_5">Financiera/EDPYME (Empresas de Desarrollo de Pequeña y Microempresa)</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_mode_p726_6" value="6">
                                            <label class="custom-control-label" for="cap7_mode_p726_6">Organismo No Gubernamental - ONG</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_mode_p726_7" value="7">
                                            <label class="custom-control-label" for="cap7_mode_p726_7">Cooperativa</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_mode_p726_8" value="8">
                                            <label class="custom-control-label" for="cap7_mode_p726_8">Establecimiento comercial</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_mode_p726_9" value="9">
                                            <label class="custom-control-label" for="cap7_mode_p726_9">Prestamista/habilitador</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_mode_p726_10" value="10">
                                            <label class="custom-control-label" for="cap7_mode_p726_10">Programas del estado</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_mode_p726_11" value="11">
                                            <label class="custom-control-label" for="cap7_mode_p726_11">Otro:</label>
                                        </div>
                                        
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row cap7_mode_p726_11" style="display:none">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">Especifique</label>
                                    <input type="text" class="form-control" id="cap7_mode_p726_11_especifique" placeholder="Especifique" name="Encuesta[cap7_mode_p726_11_especifique]" maxlength="250">
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12">
                                <label for=""> 727. ¿Utilizó el crédito o préstamo que obtuvo para:  </label>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_mode_p727_1" value="1">
                                            <label class="custom-control-label" for="cap7_mode_p727_1">Comprar insumos agrícolas?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_mode_p727_2" value="2">
                                            <label class="custom-control-label" for="cap7_mode_p727_2">Comprar insumos pecuario?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_mode_p727_3" value="3">
                                            <label class="custom-control-label" for="cap7_mode_p727_3">Pagar mano de obra (pago de jornaleros)?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_mode_p727_4" value="4">
                                            <label class="custom-control-label" for="cap7_mode_p727_4">Asistencia técnica?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_mode_p727_5" value="5">
                                            <label class="custom-control-label" for="cap7_mode_p727_5">Alquiler de maquinaria (agraria)?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_mode_p727_6" value="6">
                                            <label class="custom-control-label" for="cap7_mode_p727_6">Compra de terrenos agrícolas?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_mode_p727_7" value="7">
                                            <label class="custom-control-label" for="cap7_mode_p727_7">Compra de ganado?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_mode_p727_8" value="8">
                                            <label class="custom-control-label" for="cap7_mode_p727_8">Pago de deudas agrarias?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_mode_p727_9" value="9">
                                            <label class="custom-control-label" for="cap7_mode_p727_9">Otro:</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_mode_p727_10" value="10">
                                            <label class="custom-control-label" for="cap7_mode_p727_10">No utilizó para su actividad agraria?</label>
                                        </div>
                                        
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row cap7_mode_p727_9" style="display:none">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">Especifique</label>
                                    <input type="text" class="form-control" id="cap7_mode_p727_9_especifique" placeholder="Especifique" name="Encuesta[cap7_mode_p727_9_especifique]" maxlength="250">
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">728. Desde enero de 2020 a hoy, ¿Ud. ha sido beneficiario de algún seguro agrario??</label>
                                    <select class="form-control" name="Encuesta[cap7_mode_p728]" id="cap7_mode_p728">
                                        <option value>Seleccionar</option>
                                        <option value="1">Si</option>
                                        <option value="2">No</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <label for=""> 729. ¿Quién le proporcionó el seguro agrario?   </label>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_mode_p729_1" value="1">
                                            <label class="custom-control-label" for="cap7_mode_p729_1">MIDAGRI</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_mode_p729_2" value="2">
                                            <label class="custom-control-label" for="cap7_mode_p729_2">Empresa aseguradora</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_mode_p729_3" value="3">
                                            <label class="custom-control-label" for="cap7_mode_p729_3">Gobierno Regional (definir fondo de prevención)</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_mode_p729_4" value="4">
                                            <label class="custom-control-label" for="cap7_mode_p729_4">Agrobanco (Programa de rescate financiero agropecuario)</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_mode_p729_5" value="5">
                                            <label class="custom-control-label" for="cap7_mode_p729_5">Otro:</label>
                                        </div>
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">    730. Desde enero de 2020 hasta hoy. ¿Tiene alguna cuenta de ahorro?</label>
                                    <select class="form-control" name="Encuesta[cap7_mode_p730]" id="cap7_mode_p730">
                                        <option value>Seleccionar</option>
                                        <option value="1">Si</option>
                                        <option value="2">No</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        

                        <div class="row cap7_mode_p729_5" style="display:none">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">Especifique</label>
                                    <input type="text" class="form-control" id="cap7_mode_p729_5_especifique" placeholder="Especifique" name="Encuesta[cap7_mode_p729_5_especifique]" maxlength="250">
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12">
                                <label for="">731. ¿En que institución tiene cuenta de ahorro?:  </label>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_mode_p731_1" value="1">
                                            <label class="custom-control-label" for="cap7_mode_p731_1">Banco de la Nación</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_mode_p731_2" value="2">
                                            <label class="custom-control-label" for="cap7_mode_p731_2">Banca privada</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_mode_p731_3" value="3">
                                            <label class="custom-control-label" for="cap7_mode_p731_3">Caja municipal</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_mode_p731_4" value="4">
                                            <label class="custom-control-label" for="cap7_mode_p731_4">Caja rural</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_mode_p731_5" value="5">
                                            <label class="custom-control-label" for="cap7_mode_p731_5">Financiera / EDPYME (Empresas de Desarrollo de Pequeña y Microempresa)</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap7_mode_p731_6" value="6">
                                            <label class="custom-control-label" for="cap7_mode_p731_6">Otro:</label>
                                        </div>
                                        
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row cap7_mode_p731_6" style="display:none">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">Especifique</label>
                                    <input type="text" class="form-control" id="cap7_mode_p731_6_especifique" placeholder="Especifique" name="Encuesta[cap7_mode_p731_6_especifique]" maxlength="250">
                                </div>
                            </div>
                        </div>


                        


                    </div>

                    <div class="tab-pane" id="capitulo_viii" role="tabpanel">
                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for="">CAPÍTULO VIII.  AGUA Y ENERGÍA EN LA PARCELA AGRARIA</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <label for="">801. ¿Cuál es la fuente de abastecimiento de agua de la parcela? </label>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap8_p801_1" value="1">
                                            <label class="custom-control-label" for="cap8_p801_1">Reservorio</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap8_p801_2" value="2">
                                            <label class="custom-control-label" for="cap8_p801_2">Pozo</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap8_p801_3" value="3">
                                            <label class="custom-control-label" for="cap8_p801_3">Qocha</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap8_p801_4" value="4">
                                            <label class="custom-control-label" for="cap8_p801_4">Río, quebrada, acequia</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap8_p801_5" value="5">
                                            <label class="custom-control-label" for="cap8_p801_5">Lago, laguna</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap8_p801_6" value="6">
                                            <label class="custom-control-label" for="cap8_p801_6">Manantial o puquio</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap8_p801_7" value="7">
                                            <label class="custom-control-label" for="cap8_p801_7">Agua lluvia</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap8_p801_8" value="8">
                                            <label class="custom-control-label" for="cap8_p801_8">Otro</label>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row cap8_p801_8" style="display:none">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">Especifique</label>
                                    <input type="text" class="form-control" id="cap8_p801_8_especifique" placeholder="Especifique" name="Encuesta[cap8_p801_8_especifique]">
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12">
                                <label for="">802. En general, ¿Considera Ud. que el agua para riego está:</label>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap8_p802_1" value="1">
                                            <label class="custom-control-label" for="cap8_p802_1">Contaminada por explotaciones mineras?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap8_p802_2" value="2">
                                            <label class="custom-control-label" for="cap8_p802_2">Contaminada por explotaciones de gas?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap8_p802_3" value="3">
                                            <label class="custom-control-label" for="cap8_p802_3">Contaminada por desaguez industriales y domésticos?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap8_p802_4" value="4">
                                            <label class="custom-control-label" for="cap8_p802_4">Contaminada por agroquímicos de la actividad agrícola?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap8_p802_5" value="5">
                                            <label class="custom-control-label" for="cap8_p802_5">Contaminada por agroquímicos de la actividad pecuaria?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap8_p802_6" value="6">
                                            <label class="custom-control-label" for="cap8_p802_6">Otro</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap8_p802_7" value="7">
                                            <label class="custom-control-label" for="cap8_p802_7">Ninguno</label>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row cap8_p802_6" style="display:none">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">Especifique</label>
                                    <input type="text" class="form-control" id="cap8_p802_6_especifique" placeholder="Especifique" name="Encuesta[cap8_p802_6_especifique]" maxlength="250">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <label for="">803. ¿Para el desarrollo de las actividades agrarias la energía que utiliza es de:</label>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap8_p803_1" value="1">
                                            <label class="custom-control-label" for="cap8_p803_1">Red Pública?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap8_p803_2" value="2">
                                            <label class="custom-control-label" for="cap8_p803_2">Motor generador?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap8_p803_3" value="3">
                                            <label class="custom-control-label" for="cap8_p803_3">Molinos de viento?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap8_p803_4" value="4">
                                            <label class="custom-control-label" for="cap8_p803_4">Panel solar?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap8_p803_5" value="5">
                                            <label class="custom-control-label" for="cap8_p803_5">Combustible? (gasolina, ACPM, gas)</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap8_p803_6" value="6">
                                            <label class="custom-control-label" for="cap8_p803_6">Carbón mineral?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap8_p803_7" value="7">
                                            <label class="custom-control-label" for="cap8_p803_7">Biogas?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap8_p803_8" value="8">
                                            <label class="custom-control-label" for="cap8_p803_8">Quema desechos agrarios?</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                            <input type="checkbox" class="custom-control-input" id="cap8_p803_9" value="9">
                                            <label class="custom-control-label" for="cap8_p803_9">Otro:</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row cap8_p803_9" style="display:none">
                            <div class="col-md-6" >
                                <div class="form-group">
                                    <label for="">Especifique</label>
                                    <input type="text" class="form-control" id="cap8_p803_9_especifique" placeholder="Especifique" name="Encuesta[cap8_p803_9_especifique]" maxlength="250">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for="">Observación</label>
                                <textarea name="Encuesta[cap8_observacion]" id="cap8_observacion" cols="30" rows="5" class="form-control" maxlength="250"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="capitulo_ix" role="tabpanel">
                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for=""> CAPÍTULO IX.  MAQUINARIA Y EQUIPO DE USO AGRARIO EN LA UNIDAD PRODUCTORA AGRARIA (Para todo productor/a agrario/a)</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">901. ¿Desde enero 2020 hasta hoy, ha utilizado maquinaria y/o equipo para el desarrollo de las actividades agrarias?</label>
                                    <select class="form-control" name="Encuesta[cap9_p901]" id="cap9_p901">
                                        <option value>Seleccionar</option>
                                        <option value="1">Si</option>
                                        <option value="2">No</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 mb-2" style="padding-left:0px">
                                <a class="btn btn-success" target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capituloix/create-capitulo-ix-maquinarias?id_encuesta=<?= $id_encuesta ?>&geocodigo_fdo=<?= $geocodigo_fdo ?>">Agregar</a>
                            </div>
                            <div class="col-md-12" style="padding-left:0px">
                                <table id="lista-cap9-maquinaria" class="table">
                                    <thead>
                                        <th>902. ¿Qué tipo de maquinaria y/o equipo ha utilizado desde enero 2020 hasta hoy?</th>
                                        <th>903. ¿Cuál es la potencia de la maquinaria?</th>
                                        <th>904. ¿La maquinaria y/o equipo es :</th>
                                        <th>Acciones</th>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <!-- <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for="">Observación</label>
                                <textarea name="Encuesta[cap9_observacion]" id="cap9_observacion" cols="30" rows="5" class="form-control"></textarea>
                            </div>
                        </div> -->


                    </div>

                    <div class="tab-pane" id="capitulo_x" role="tabpanel">
                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for=""> CAPÍTULO X. INFRAESTRUCTURA E INSTALACIONES DE USO AGRARIO (Para todo productor/a agrario/a) EN LA PARCELA</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 mb-2" style="padding-left:0px">
                                <a class="btn btn-success" target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/capitulox/create-capitulo-x-infraestructuras?id_encuesta=<?= $id_encuesta ?>&geocodigo_fdo=<?= $geocodigo_fdo ?>">Agregar</a>
                            </div>

                            <div class="col-md-12" style="padding-left:0px">
                                <table id="lista-cap10-infraestructuras" class="table">
                                    <thead>
                                        <tr>
                                            <td>1001. ¿Hoy que tipo de instalaciones e infraestructura tiene la parcela?</td>
                                            <td>1002. ¿Cuál es el área de las instalaciones e infraestructura? (m2)</td>
                                            <td>1003. ¿Cuál es el estado de las instalaciones e infraestructura?</td>
                                            <td>Acciones</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for="">Observación</label>
                                <textarea name="Encuesta[cap10_observacion]" id="cap10_observacion" cols="30" rows="5" class="form-control"></textarea>
                            </div>
                        </div> -->


                    </div>

                    <!-- <div class="tab-pane" id="capitulo_xi" role="tabpanel">
                    
                    </div> -->

                    <div class="tab-pane" id="capitulo_xii" role="tabpanel">
                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for=""> CAPÍTULO XII.  EVALUACIÓN DE RESULTADOS</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for="">Módulo XII.A. RESULTADOS DE LA ENTREVISTA Y SUPERVISIÓN</label>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for="">RESULTADO FINAL DE LA ENCUESTA</label>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for="">6. Fecha</label>
                                <input type="date" name="Encuesta[cap12_moda_p1206]" id="cap12_moda_p1206" class="form-control">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">7. Resultado Final</label>
                                    <select name="Encuesta[cap12_moda_p1207]" id="cap12_moda_p1207"class="form-control">
                                        <option value>Seleccionar</option>
                                        <option value="1">Completa</option>
                                        <option value="2">Incompleta</option>
                                        <option value="3">Rechazo</option>
                                        <option value="4">Ausente</option>
                                        <option value="5">No se inició la entrevista</option>
                                        <option value="6">Otro</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row cap12_moda_p1207" style="display:none">
                            <div class="col-md-6" style="padding-left:0px">
                                <div class="form-group">
                                    <label for="">Especifique</label>
                                    <input type="text" class="form-control" id="cap12_moda_p1207_6_especifique" placeholder="Especifique" name="Encuesta[cap12_moda_p1207_6_especifique]">
                                </div>
                            </div>
                        </div>



                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for="">Módulo XII.B. OBSERVACIONES</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12" style="padding-left:0px">
                                <label for="">OBSERVACIONES</label>
                                <textarea name="Encuesta[cap12_observacion]" id="cap12_observacion" cols="30" rows="5" class="form-control"></textarea>
                            </div>
                        </div>


                    </div>


                </div>
                <hr>
                <button type="button" class="btn btn-success btn-grabar-encuesta">Guardar</button>
                <button type="button" class="btn btn-success btn-finalizar-encuesta">Finalizar</button>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<?php ActiveForm::end(); ?>

<div id="modal" class="fade modal" role="dialog"  tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        </div>
    </div>
</div>

<script>
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var loading =   $('#staticBackdrop');
var id_encuesta = "<?= $id_encuesta ?>";
$("body").on("click", ".btn-grabar-encuesta", function (e) {

    e.preventDefault();

    /* Seteando variables del capitulo I */
    var p01_1   = ($("[id=\"p01_1\"]:checked").val())?1:null;
    var p01_2   = ($("[id=\"p01_2\"]:checked").val())?2:null;
    var p01_3   = ($("[id=\"p01_3\"]:checked").val())?3:null;
    var p01_4   = ($("[id=\"p01_4\"]:checked").val())?4:null;
    var p01_5   = ($("[id=\"p01_5\"]:checked").val())?5:null;
    var p01_6   = ($("[id=\"p01_6\"]:checked").val())?6:null;
    var p01_7   = ($("[id=\"p01_7\"]:checked").val())?7:null;
    var p01_8   = ($("[id=\"p01_8\"]:checked").val())?8:null;
    var p01_9   = ($("[id=\"p01_9\"]:checked").val())?9:null;
    var p01_10   = ($("[id=\"p01_10\"]:checked").val())?10:null;
    var p01_11   = ($("[id=\"p01_11\"]:checked").val())?11:null;
    var p01_12   = ($("[id=\"p01_12\"]:checked").val())?12:null;
    /* Seteando variables del capitulo II */

    /* Seteando variables del capitulo III */
    var p03_c_int_tenencia = "";
    var p03_c_int_tenencia_27   = ($("[id=\"p03_c_int_tenencia_27\"]:checked").val())?27:0;
    var p03_c_int_tenencia_28   = ($("[id=\"p03_c_int_tenencia_28\"]:checked").val())?28:0;
    var p03_c_int_tenencia_29   = ($("[id=\"p03_c_int_tenencia_29\"]:checked").val())?29:0;
    var p03_c_int_tenencia_30   = ($("[id=\"p03_c_int_tenencia_30\"]:checked").val())?30:0;
    var p03_c_int_tenencia_31   = ($("[id=\"p03_c_int_tenencia_31\"]:checked").val())?31:0;

    var p03_c_int_tenencia_lista = [p03_c_int_tenencia_27,p03_c_int_tenencia_28,p03_c_int_tenencia_29,p03_c_int_tenencia_30,p03_c_int_tenencia_31];
    var p03_c_int_tenencia_contador = 0;
    $.each(p03_c_int_tenencia_lista, function( index, value ) {
        if(value!=0){
            if(p03_c_int_tenencia_contador==0){
                p03_c_int_tenencia = p03_c_int_tenencia + value;
            }else{
                p03_c_int_tenencia = p03_c_int_tenencia + "," + value;
            }
            p03_c_int_tenencia_contador++;
        }
    });

    /* Seteando variables del capitulo IV */
    var cap4_modd_p437 = "";
    var cap4_modd_p437_1   = ($("[id=\"cap4_modd_p437_1\"]:checked").val())?1:0;
    var cap4_modd_p437_2   = ($("[id=\"cap4_modd_p437_2\"]:checked").val())?2:0;
    var cap4_modd_p437_3   = ($("[id=\"cap4_modd_p437_3\"]:checked").val())?3:0;
    var cap4_modd_p437_4   = ($("[id=\"cap4_modd_p437_4\"]:checked").val())?4:0;
    var cap4_modd_p437_5   = ($("[id=\"cap4_modd_p437_5\"]:checked").val())?5:0;
    var cap4_modd_p437_6   = ($("[id=\"cap4_modd_p437_6\"]:checked").val())?6:0;
    var cap4_modd_p437_7   = ($("[id=\"cap4_modd_p437_7\"]:checked").val())?7:0;
    var cap4_modd_p437_8   = ($("[id=\"cap4_modd_p437_8\"]:checked").val())?8:0;
    var cap4_modd_p437_lista = [cap4_modd_p437_1,cap4_modd_p437_2,cap4_modd_p437_3,cap4_modd_p437_4,cap4_modd_p437_5,cap4_modd_p437_6,cap4_modd_p437_7,cap4_modd_p437_8];
    var cap4_modd_p437_contador = 0;
    $.each(cap4_modd_p437_lista, function( index, value ) {
        if(value!=0){
            if(cap4_modd_p437_contador==0){
                cap4_modd_p437 = cap4_modd_p437 + value;
            }else{
                cap4_modd_p437 = cap4_modd_p437 + "," + value;
            }
            cap4_modd_p437_contador++;
        }
    });


    var cap4_modd_p438 = "";
    var cap4_modd_p438_1   = ($("[id=\"cap4_modd_p438_1\"]:checked").val())?1:0;
    var cap4_modd_p438_2   = ($("[id=\"cap4_modd_p438_2\"]:checked").val())?2:0;
    var cap4_modd_p438_3   = ($("[id=\"cap4_modd_p438_3\"]:checked").val())?3:0;
    var cap4_modd_p438_4   = ($("[id=\"cap4_modd_p438_4\"]:checked").val())?4:0;
    var cap4_modd_p438_5   = ($("[id=\"cap4_modd_p438_5\"]:checked").val())?5:0;
    var cap4_modd_p438_6   = ($("[id=\"cap4_modd_p438_6\"]:checked").val())?6:0;
    var cap4_modd_p438_7   = ($("[id=\"cap4_modd_p438_7\"]:checked").val())?7:0;
    var cap4_modd_p438_8   = ($("[id=\"cap4_modd_p438_8\"]:checked").val())?8:0;
    var cap4_modd_p438_9   = ($("[id=\"cap4_modd_p438_9\"]:checked").val())?9:0;
    var cap4_modd_p438_10   = ($("[id=\"cap4_modd_p438_10\"]:checked").val())?10:0;
    var cap4_modd_p438_11   = ($("[id=\"cap4_modd_p438_11\"]:checked").val())?11:0;
    
    var cap4_modd_p438_lista = [cap4_modd_p438_1,cap4_modd_p438_2,cap4_modd_p438_3,cap4_modd_p438_4,cap4_modd_p438_5,cap4_modd_p438_6,cap4_modd_p438_7,cap4_modd_p438_8,cap4_modd_p438_9,cap4_modd_p438_10,cap4_modd_p438_11];
    var cap4_modd_p438_contador = 0;
    $.each(cap4_modd_p438_lista, function( index, value ) {
        if(value!=0){
            if(cap4_modd_p438_contador==0){
                cap4_modd_p438 = cap4_modd_p438 + value;
            }else{
                cap4_modd_p438 = cap4_modd_p438 + "," + value;
            }
            cap4_modd_p438_contador++;
        }
    });

    var cap4_mode_p440 = "";
    var cap4_mode_p440_1   = ($("[id=\"cap4_mode_p440_1\"]").val())?$("[id=\"cap4_mode_p440_1\"]").val():'';
    var cap4_mode_p440_2   = ($("[id=\"cap4_mode_p440_2\"]").val())?$("[id=\"cap4_mode_p440_2\"]").val():'';
    var cap4_mode_p440_3   = ($("[id=\"cap4_mode_p440_3\"]").val())?$("[id=\"cap4_mode_p440_3\"]").val():'';
    var cap4_mode_p440_4   = ($("[id=\"cap4_mode_p440_4\"]").val())?$("[id=\"cap4_mode_p440_4\"]").val():'';
    var cap4_mode_p440_5   = ($("[id=\"cap4_mode_p440_5\"]").val())?$("[id=\"cap4_mode_p440_5\"]").val():'';
    var cap4_mode_p440_6   = ($("[id=\"cap4_mode_p440_6\"]").val())?$("[id=\"cap4_mode_p440_6\"]").val():'';
    var cap4_mode_p440_7   = ($("[id=\"cap4_mode_p440_7\"]").val())?$("[id=\"cap4_mode_p440_7\"]").val():'';
    var cap4_mode_p440_8   = ($("[id=\"cap4_mode_p440_8\"]").val())?$("[id=\"cap4_mode_p440_8\"]").val():'';
    var cap4_mode_p440_9   = ($("[id=\"cap4_mode_p440_9\"]").val())?$("[id=\"cap4_mode_p440_9\"]").val():'';
    var cap4_mode_p440_10   = ($("[id=\"cap4_mode_p440_10\"]").val())?$("[id=\"cap4_mode_p440_10\"]").val():'';
    var cap4_mode_p440_11   = ($("[id=\"cap4_mode_p440_11\"]").val())?$("[id=\"cap4_mode_p440_11\"]").val():'';
    var cap4_mode_p440_12   = ($("[id=\"cap4_mode_p440_12\"]").val())?$("[id=\"cap4_mode_p440_12\"]").val():'';
    var cap4_mode_p440_13   = ($("[id=\"cap4_mode_p440_13\"]").val())?$("[id=\"cap4_mode_p440_13\"]").val():'';
    var cap4_mode_p440_14   = ($("[id=\"cap4_mode_p440_14\"]").val())?$("[id=\"cap4_mode_p440_14\"]").val():'';
    var cap4_mode_p440_15   = ($("[id=\"cap4_mode_p440_15\"]").val())?$("[id=\"cap4_mode_p440_15\"]").val():'';
    var cap4_mode_p440_16   = ($("[id=\"cap4_mode_p440_16\"]").val())?$("[id=\"cap4_mode_p440_16\"]").val():'';
    var cap4_mode_p440_17   = ($("[id=\"cap4_mode_p440_17\"]").val())?$("[id=\"cap4_mode_p440_17\"]").val():'';
    var cap4_mode_p440_18   = ($("[id=\"cap4_mode_p440_18\"]").val())?$("[id=\"cap4_mode_p440_18\"]").val():'';
    var cap4_mode_p440_19   = ($("[id=\"cap4_mode_p440_19\"]").val())?$("[id=\"cap4_mode_p440_19\"]").val():'';
    var cap4_mode_p440_20   = ($("[id=\"cap4_mode_p440_20\"]").val())?$("[id=\"cap4_mode_p440_20\"]").val():'';
    var cap4_mode_p440_21   = ($("[id=\"cap4_mode_p440_21\"]").val())?$("[id=\"cap4_mode_p440_21\"]").val():'';
    var cap4_mode_p440_22   = ($("[id=\"cap4_mode_p440_22\"]").val())?$("[id=\"cap4_mode_p440_22\"]").val():'';

    var cap4_mode_p441_1   = ($("[id=\"cap4_mode_p441_1\"]").val())?$("[id=\"cap4_mode_p441_1\"]").val():'';
    var cap4_mode_p441_2   = ($("[id=\"cap4_mode_p441_2\"]").val())?$("[id=\"cap4_mode_p441_2\"]").val():'';
    var cap4_mode_p441_3   = ($("[id=\"cap4_mode_p441_3\"]").val())?$("[id=\"cap4_mode_p441_3\"]").val():'';
    var cap4_mode_p441_4   = ($("[id=\"cap4_mode_p441_4\"]").val())?$("[id=\"cap4_mode_p441_4\"]").val():'';
    var cap4_mode_p441_5   = ($("[id=\"cap4_mode_p441_5\"]").val())?$("[id=\"cap4_mode_p441_5\"]").val():'';
    var cap4_mode_p441_6   = ($("[id=\"cap4_mode_p441_6\"]").val())?$("[id=\"cap4_mode_p441_6\"]").val():'';
    var cap4_mode_p441_7   = ($("[id=\"cap4_mode_p441_7\"]").val())?$("[id=\"cap4_mode_p441_7\"]").val():'';
    var cap4_mode_p441_8   = ($("[id=\"cap4_mode_p441_8\"]").val())?$("[id=\"cap4_mode_p441_8\"]").val():'';
    var cap4_mode_p441_9   = ($("[id=\"cap4_mode_p441_9\"]").val())?$("[id=\"cap4_mode_p441_9\"]").val():'';
    var cap4_mode_p441_10   = ($("[id=\"cap4_mode_p441_10\"]").val())?$("[id=\"cap4_mode_p441_10\"]").val():'';
    var cap4_mode_p441_11   = ($("[id=\"cap4_mode_p441_11\"]").val())?$("[id=\"cap4_mode_p441_11\"]").val():'';
    var cap4_mode_p441_12   = ($("[id=\"cap4_mode_p441_12\"]").val())?$("[id=\"cap4_mode_p441_12\"]").val():'';
    var cap4_mode_p441_13   = ($("[id=\"cap4_mode_p441_13\"]").val())?$("[id=\"cap4_mode_p441_13\"]").val():'';
    var cap4_mode_p441_14   = ($("[id=\"cap4_mode_p441_14\"]").val())?$("[id=\"cap4_mode_p441_14\"]").val():'';
    var cap4_mode_p441_15   = ($("[id=\"cap4_mode_p441_15\"]").val())?$("[id=\"cap4_mode_p441_15\"]").val():'';
    var cap4_mode_p441_16   = ($("[id=\"cap4_mode_p441_16\"]").val())?$("[id=\"cap4_mode_p441_16\"]").val():'';
    var cap4_mode_p441_17   = ($("[id=\"cap4_mode_p441_17\"]").val())?$("[id=\"cap4_mode_p441_17\"]").val():'';
    var cap4_mode_p441_18   = ($("[id=\"cap4_mode_p441_18\"]").val())?$("[id=\"cap4_mode_p441_18\"]").val():'';
    var cap4_mode_p441_19   = ($("[id=\"cap4_mode_p441_19\"]").val())?$("[id=\"cap4_mode_p441_19\"]").val():'';
    var cap4_mode_p441_20   = ($("[id=\"cap4_mode_p441_20\"]").val())?$("[id=\"cap4_mode_p441_20\"]").val():'';
    var cap4_mode_p441_21   = ($("[id=\"cap4_mode_p441_21\"]").val())?$("[id=\"cap4_mode_p441_21\"]").val():'';
    var cap4_mode_p441_22   = ($("[id=\"cap4_mode_p441_22\"]").val())?$("[id=\"cap4_mode_p441_22\"]").val():'';

    var cap4_mode_p440_lista =  [
                                    {"a":[
                                        {"nro_practica":1,"efectua_practica":cap4_mode_p440_1,"cuantos_anios":cap4_mode_p441_1},
                                        {"nro_practica":2,"efectua_practica":cap4_mode_p440_2,"cuantos_anios":cap4_mode_p441_2},
                                        {"nro_practica":3,"efectua_practica":cap4_mode_p440_3,"cuantos_anios":cap4_mode_p441_3},
                                        {"nro_practica":4,"efectua_practica":cap4_mode_p440_4,"cuantos_anios":cap4_mode_p441_4},
                                    ]},
                                    {"b":[
                                        {"nro_practica":5,"efectua_practica":cap4_mode_p440_5,"cuantos_anios":cap4_mode_p441_5},
                                        {"nro_practica":6,"efectua_practica":cap4_mode_p440_6,"cuantos_anios":cap4_mode_p441_6},
                                        {"nro_practica":7,"efectua_practica":cap4_mode_p440_7,"cuantos_anios":cap4_mode_p441_7},
                                        {"nro_practica":8,"efectua_practica":cap4_mode_p440_8,"cuantos_anios":cap4_mode_p441_8},
                                        {"nro_practica":9,"efectua_practica":cap4_mode_p440_9,"cuantos_anios":cap4_mode_p441_9},
                                        {"nro_practica":10,"efectua_practica":cap4_mode_p440_10,"cuantos_anios":cap4_mode_p441_10},
                                        {"nro_practica":11,"efectua_practica":cap4_mode_p440_11,"cuantos_anios":cap4_mode_p441_11},
                                        {"nro_practica":12,"efectua_practica":cap4_mode_p440_12,"cuantos_anios":cap4_mode_p441_12},
                                        {"nro_practica":13,"efectua_practica":cap4_mode_p440_13,"cuantos_anios":cap4_mode_p441_13},
                                    ]},
                                    {"c":[
                                        {"nro_practica":14,"efectua_practica":cap4_mode_p440_14,"cuantos_anios":cap4_mode_p441_14},
                                        {"nro_practica":15,"efectua_practica":cap4_mode_p440_15,"cuantos_anios":cap4_mode_p441_15},
                                        {"nro_practica":16,"efectua_practica":cap4_mode_p440_16,"cuantos_anios":cap4_mode_p441_16},
                                        {"nro_practica":17,"efectua_practica":cap4_mode_p440_17,"cuantos_anios":cap4_mode_p441_17},
                                    ]},
                                    {"d":[
                                        {"nro_practica":18,"efectua_practica":cap4_mode_p440_18,"cuantos_anios":cap4_mode_p441_18},
                                        {"nro_practica":19,"efectua_practica":cap4_mode_p440_19,"cuantos_anios":cap4_mode_p441_19},
                                        {"nro_practica":20,"efectua_practica":cap4_mode_p440_20,"cuantos_anios":cap4_mode_p441_20},
                                        {"nro_practica":21,"efectua_practica":cap4_mode_p440_21,"cuantos_anios":cap4_mode_p441_21},
                                        {"nro_practica":22,"efectua_practica":cap4_mode_p440_22,"cuantos_anios":cap4_mode_p441_22},
                                    ]},
                                ];
    
    //return false;

    var cap4_mode_p443 = "";
    var cap4_mode_p443_1   = ($("[id=\"cap4_mode_p443_1\"]:checked").val())?1:0;
    var cap4_mode_p443_2   = ($("[id=\"cap4_mode_p443_2\"]:checked").val())?2:0;
    var cap4_mode_p443_3   = ($("[id=\"cap4_mode_p443_3\"]:checked").val())?3:0;
    var cap4_mode_p443_4   = ($("[id=\"cap4_mode_p443_4\"]:checked").val())?4:0;
    var cap4_mode_p443_5   = ($("[id=\"cap4_mode_p443_5\"]:checked").val())?5:0;
    var cap4_mode_p443_6   = ($("[id=\"cap4_mode_p443_6\"]:checked").val())?6:0;
    var cap4_mode_p443_7   = ($("[id=\"cap4_mode_p443_7\"]:checked").val())?7:0;

    var cap4_mode_p443_lista = [cap4_mode_p443_1,cap4_mode_p443_2,cap4_mode_p443_3,cap4_mode_p443_4,cap4_mode_p443_5,cap4_mode_p443_6,cap4_mode_p443_7];
    var cap4_mode_p443_contador = 0;
    $.each(cap4_mode_p443_lista, function( index, value ) {
        if(value!=0){
            if(cap4_mode_p443_contador==0){
                cap4_mode_p443 = cap4_mode_p443 + value;
            }else{
                cap4_mode_p443 = cap4_mode_p443 + "," + value;
            }
            cap4_mode_p443_contador++;
        }
    });

    var cap4_mode_p444 = "";
    var cap4_mode_p444_1   = ($("[id=\"cap4_mode_p444_1\"]:checked").val())?1:0;
    var cap4_mode_p444_2   = ($("[id=\"cap4_mode_p444_2\"]:checked").val())?2:0;
    var cap4_mode_p444_3   = ($("[id=\"cap4_mode_p444_3\"]:checked").val())?3:0;
    var cap4_mode_p444_4   = ($("[id=\"cap4_mode_p444_4\"]:checked").val())?4:0;
    var cap4_mode_p444_5   = ($("[id=\"cap4_mode_p444_5\"]:checked").val())?5:0;
    var cap4_mode_p444_6   = ($("[id=\"cap4_mode_p444_6\"]:checked").val())?6:0;
    var cap4_mode_p444_7   = ($("[id=\"cap4_mode_p444_7\"]:checked").val())?7:0;
    var cap4_mode_p444_8   = ($("[id=\"cap4_mode_p444_8\"]:checked").val())?8:0;

    var cap4_mode_p444_lista = [cap4_mode_p444_1,cap4_mode_p444_2,cap4_mode_p444_3,cap4_mode_p444_4,cap4_mode_p444_5,cap4_mode_p444_6,cap4_mode_p444_7,cap4_mode_p444_8];
    var cap4_mode_p444_contador = 0;
    $.each(cap4_mode_p444_lista, function( index, value ) {
        if(value!=0){
            if(cap4_mode_p444_contador==0){
                cap4_mode_p444 = cap4_mode_p444 + value;
            }else{
                cap4_mode_p444 = cap4_mode_p444 + "," + value;
            }
            cap4_mode_p444_contador++;
        }
    });
    /* Seteando variables del capitulo V */
    var cap5_moda_p501_b = "";
    var cap5_moda_p501_b_1   = ($("[id=\"cap5_moda_p501_b_1\"]:checked").val())?1:0;
    var cap5_moda_p501_b_2   = ($("[id=\"cap5_moda_p501_b_2\"]:checked").val())?2:0;
    var cap5_moda_p501_b_3   = ($("[id=\"cap5_moda_p501_b_3\"]:checked").val())?3:0;
    var cap5_moda_p501_b_4   = ($("[id=\"cap5_moda_p501_b_4\"]:checked").val())?4:0;
    var cap5_moda_p501_b_5   = ($("[id=\"cap5_moda_p501_b_5\"]:checked").val())?5:0;
    var cap5_moda_p501_b_6   = ($("[id=\"cap5_moda_p501_b_6\"]:checked").val())?6:0;
    var cap5_moda_p501_b_7   = ($("[id=\"cap5_moda_p501_b_7\"]:checked").val())?7:0;
    var cap5_moda_p501_b_8   = ($("[id=\"cap5_moda_p501_b_8\"]:checked").val())?8:0;
    var cap5_moda_p501_b_9   = ($("[id=\"cap5_moda_p501_b_9\"]:checked").val())?9:0;
    var cap5_moda_p501_b_10   = ($("[id=\"cap5_moda_p501_b_10\"]:checked").val())?10:0;
    var cap5_moda_p501_b_11   = ($("[id=\"cap5_moda_p501_b_11\"]:checked").val())?11:0;
    var cap5_moda_p501_b_12   = ($("[id=\"cap5_moda_p501_b_12\"]:checked").val())?12:0;
    var cap5_moda_p501_b_13   = ($("[id=\"cap5_moda_p501_b_13\"]:checked").val())?13:0;
    var cap5_moda_p501_b_14   = ($("[id=\"cap5_moda_p501_b_14\"]:checked").val())?14:0;
    var cap5_moda_p501_b_15   = ($("[id=\"cap5_moda_p501_b_15\"]:checked").val())?15:0;
    var cap5_moda_p501_b_16   = ($("[id=\"cap5_moda_p501_b_16\"]:checked").val())?16:0;
    var cap5_moda_p501_b_17   = ($("[id=\"cap5_moda_p501_b_17\"]:checked").val())?17:0;
    var cap5_moda_p501_b_18   = ($("[id=\"cap5_moda_p501_b_18\"]:checked").val())?18:0;
    var cap5_moda_p501_b_19   = ($("[id=\"cap5_moda_p501_b_19\"]:checked").val())?19:0;
    var cap5_moda_p501_b_20   = ($("[id=\"cap5_moda_p501_b_20\"]:checked").val())?20:0;
    var cap5_moda_p501_b_21   = ($("[id=\"cap5_moda_p501_b_21\"]:checked").val())?21:0;
    var cap5_moda_p501_b_22   = ($("[id=\"cap5_moda_p501_b_22\"]:checked").val())?22:0;
    var cap5_moda_p501_b_23   = ($("[id=\"cap5_moda_p501_b_23\"]:checked").val())?23:0;
    var cap5_moda_p501_b_24   = ($("[id=\"cap5_moda_p501_b_24\"]:checked").val())?24:0;
    var cap5_moda_p501_b_25   = ($("[id=\"cap5_moda_p501_b_25\"]:checked").val())?25:0;
    var cap5_moda_p501_b_26   = ($("[id=\"cap5_moda_p501_b_26\"]:checked").val())?26:0;
    var cap5_moda_p501_b_27   = ($("[id=\"cap5_moda_p501_b_27\"]:checked").val())?27:0;
    var cap5_moda_p501_b_28   = ($("[id=\"cap5_moda_p501_b_28\"]:checked").val())?99999:0;


    var cap5_moda_p501_b_1_hembra   = ($("[id=\"cap5_moda_p501_b_1_hembra\"]").val())?$("[id=\"cap5_moda_p501_b_1_hembra\"]").val():'';
    var cap5_moda_p501_b_2_hembra   = ($("[id=\"cap5_moda_p501_b_2_hembra\"]").val())?$("[id=\"cap5_moda_p501_b_2_hembra\"]").val():'';
    var cap5_moda_p501_b_3_hembra   = ($("[id=\"cap5_moda_p501_b_3_hembra\"]").val())?$("[id=\"cap5_moda_p501_b_3_hembra\"]").val():'';
    var cap5_moda_p501_b_4_hembra   = ($("[id=\"cap5_moda_p501_b_4_hembra\"]").val())?$("[id=\"cap5_moda_p501_b_4_hembra\"]").val():'';
    var cap5_moda_p501_b_5_hembra   = ($("[id=\"cap5_moda_p501_b_5_hembra\"]").val())?$("[id=\"cap5_moda_p501_b_5_hembra\"]").val():'';
    var cap5_moda_p501_b_6_hembra   = ($("[id=\"cap5_moda_p501_b_6_hembra\"]").val())?$("[id=\"cap5_moda_p501_b_6_hembra\"]").val():'';
    var cap5_moda_p501_b_7_hembra   = ($("[id=\"cap5_moda_p501_b_7_hembra\"]").val())?$("[id=\"cap5_moda_p501_b_7_hembra\"]").val():'';
    var cap5_moda_p501_b_8_hembra   = ($("[id=\"cap5_moda_p501_b_8_hembra\"]").val())?$("[id=\"cap5_moda_p501_b_8_hembra\"]").val():'';
    var cap5_moda_p501_b_9_hembra   = ($("[id=\"cap5_moda_p501_b_9_hembra\"]").val())?$("[id=\"cap5_moda_p501_b_9_hembra\"]").val():'';
    var cap5_moda_p501_b_10_hembra   = ($("[id=\"cap5_moda_p501_b_10_hembra\"]").val())?$("[id=\"cap5_moda_p501_b_10_hembra\"]").val():'';
    var cap5_moda_p501_b_11_hembra   = ($("[id=\"cap5_moda_p501_b_11_hembra\"]").val())?$("[id=\"cap5_moda_p501_b_11_hembra\"]").val():'';
    var cap5_moda_p501_b_12_hembra   = ($("[id=\"cap5_moda_p501_b_12_hembra\"]").val())?$("[id=\"cap5_moda_p501_b_12_hembra\"]").val():'';
    var cap5_moda_p501_b_13_hembra   = ($("[id=\"cap5_moda_p501_b_13_hembra\"]").val())?$("[id=\"cap5_moda_p501_b_13_hembra\"]").val():'';
    var cap5_moda_p501_b_14_hembra   = ($("[id=\"cap5_moda_p501_b_14_hembra\"]").val())?$("[id=\"cap5_moda_p501_b_14_hembra\"]").val():'';
    var cap5_moda_p501_b_15_hembra   = ($("[id=\"cap5_moda_p501_b_15_hembra\"]").val())?$("[id=\"cap5_moda_p501_b_15_hembra\"]").val():'';
    var cap5_moda_p501_b_16_hembra   = ($("[id=\"cap5_moda_p501_b_16_hembra\"]").val())?$("[id=\"cap5_moda_p501_b_16_hembra\"]").val():'';
    var cap5_moda_p501_b_17_hembra   = ($("[id=\"cap5_moda_p501_b_17_hembra\"]").val())?$("[id=\"cap5_moda_p501_b_17_hembra\"]").val():'';
    var cap5_moda_p501_b_18_hembra   = ($("[id=\"cap5_moda_p501_b_18_hembra\"]").val())?$("[id=\"cap5_moda_p501_b_18_hembra\"]").val():'';
    var cap5_moda_p501_b_19_hembra   = ($("[id=\"cap5_moda_p501_b_19_hembra\"]").val())?$("[id=\"cap5_moda_p501_b_19_hembra\"]").val():'';
    var cap5_moda_p501_b_20_hembra   = ($("[id=\"cap5_moda_p501_b_20_hembra\"]").val())?$("[id=\"cap5_moda_p501_b_20_hembra\"]").val():'';
    var cap5_moda_p501_b_21_hembra   = ($("[id=\"cap5_moda_p501_b_21_hembra\"]").val())?$("[id=\"cap5_moda_p501_b_21_hembra\"]").val():'';
    var cap5_moda_p501_b_22_hembra   = ($("[id=\"cap5_moda_p501_b_22_hembra\"]").val())?$("[id=\"cap5_moda_p501_b_22_hembra\"]").val():'';
    var cap5_moda_p501_b_23_hembra   = ($("[id=\"cap5_moda_p501_b_23_hembra\"]").val())?$("[id=\"cap5_moda_p501_b_23_hembra\"]").val():'';
    var cap5_moda_p501_b_24_hembra   = ($("[id=\"cap5_moda_p501_b_24_hembra\"]").val())?$("[id=\"cap5_moda_p501_b_24_hembra\"]").val():'';
    var cap5_moda_p501_b_25_hembra   = ($("[id=\"cap5_moda_p501_b_25_hembra\"]").val())?$("[id=\"cap5_moda_p501_b_25_hembra\"]").val():'';
    var cap5_moda_p501_b_26_hembra   = ($("[id=\"cap5_moda_p501_b_26_hembra\"]").val())?$("[id=\"cap5_moda_p501_b_26_hembra\"]").val():'';
    var cap5_moda_p501_b_27_hembra   = ($("[id=\"cap5_moda_p501_b_27_hembra\"]").val())?$("[id=\"cap5_moda_p501_b_27_hembra\"]").val():'';
    var cap5_moda_p501_b_28_hembra   = ($("[id=\"cap5_moda_p501_b_28_hembra\"]").val())?$("[id=\"cap5_moda_p501_b_28_hembra\"]").val():'';

    var cap5_moda_p501_b_1_macho   = ($("[id=\"cap5_moda_p501_b_1_macho\"]").val())?$("[id=\"cap5_moda_p501_b_1_macho\"]").val():'';
    var cap5_moda_p501_b_2_macho   = ($("[id=\"cap5_moda_p501_b_2_macho\"]").val())?$("[id=\"cap5_moda_p501_b_2_macho\"]").val():'';
    var cap5_moda_p501_b_3_macho   = ($("[id=\"cap5_moda_p501_b_3_macho\"]").val())?$("[id=\"cap5_moda_p501_b_3_macho\"]").val():'';
    var cap5_moda_p501_b_4_macho   = ($("[id=\"cap5_moda_p501_b_4_macho\"]").val())?$("[id=\"cap5_moda_p501_b_4_macho\"]").val():'';
    var cap5_moda_p501_b_5_macho   = ($("[id=\"cap5_moda_p501_b_5_macho\"]").val())?$("[id=\"cap5_moda_p501_b_5_macho\"]").val():'';
    var cap5_moda_p501_b_6_macho   = ($("[id=\"cap5_moda_p501_b_6_macho\"]").val())?$("[id=\"cap5_moda_p501_b_6_macho\"]").val():'';
    var cap5_moda_p501_b_7_macho   = ($("[id=\"cap5_moda_p501_b_7_macho\"]").val())?$("[id=\"cap5_moda_p501_b_7_macho\"]").val():'';
    var cap5_moda_p501_b_8_macho   = ($("[id=\"cap5_moda_p501_b_8_macho\"]").val())?$("[id=\"cap5_moda_p501_b_8_macho\"]").val():'';
    var cap5_moda_p501_b_9_macho   = ($("[id=\"cap5_moda_p501_b_9_macho\"]").val())?$("[id=\"cap5_moda_p501_b_9_macho\"]").val():'';
    var cap5_moda_p501_b_10_macho   = ($("[id=\"cap5_moda_p501_b_10_macho\"]").val())?$("[id=\"cap5_moda_p501_b_10_macho\"]").val():'';
    var cap5_moda_p501_b_11_macho   = ($("[id=\"cap5_moda_p501_b_11_macho\"]").val())?$("[id=\"cap5_moda_p501_b_11_macho\"]").val():'';
    var cap5_moda_p501_b_12_macho   = ($("[id=\"cap5_moda_p501_b_12_macho\"]").val())?$("[id=\"cap5_moda_p501_b_12_macho\"]").val():'';
    var cap5_moda_p501_b_13_macho   = ($("[id=\"cap5_moda_p501_b_13_macho\"]").val())?$("[id=\"cap5_moda_p501_b_13_macho\"]").val():'';
    var cap5_moda_p501_b_14_macho   = ($("[id=\"cap5_moda_p501_b_14_macho\"]").val())?$("[id=\"cap5_moda_p501_b_14_macho\"]").val():'';
    var cap5_moda_p501_b_15_macho   = ($("[id=\"cap5_moda_p501_b_15_macho\"]").val())?$("[id=\"cap5_moda_p501_b_15_macho\"]").val():'';
    var cap5_moda_p501_b_16_macho   = ($("[id=\"cap5_moda_p501_b_16_macho\"]").val())?$("[id=\"cap5_moda_p501_b_16_macho\"]").val():'';
    var cap5_moda_p501_b_17_macho   = ($("[id=\"cap5_moda_p501_b_17_macho\"]").val())?$("[id=\"cap5_moda_p501_b_17_macho\"]").val():'';
    var cap5_moda_p501_b_18_macho   = ($("[id=\"cap5_moda_p501_b_18_macho\"]").val())?$("[id=\"cap5_moda_p501_b_18_macho\"]").val():'';
    var cap5_moda_p501_b_19_macho   = ($("[id=\"cap5_moda_p501_b_19_macho\"]").val())?$("[id=\"cap5_moda_p501_b_19_macho\"]").val():'';
    var cap5_moda_p501_b_20_macho   = ($("[id=\"cap5_moda_p501_b_20_macho\"]").val())?$("[id=\"cap5_moda_p501_b_20_macho\"]").val():'';
    var cap5_moda_p501_b_21_macho   = ($("[id=\"cap5_moda_p501_b_21_macho\"]").val())?$("[id=\"cap5_moda_p501_b_21_macho\"]").val():'';
    var cap5_moda_p501_b_22_macho   = ($("[id=\"cap5_moda_p501_b_22_macho\"]").val())?$("[id=\"cap5_moda_p501_b_22_macho\"]").val():'';
    var cap5_moda_p501_b_23_macho   = ($("[id=\"cap5_moda_p501_b_23_macho\"]").val())?$("[id=\"cap5_moda_p501_b_23_macho\"]").val():'';
    var cap5_moda_p501_b_24_macho   = ($("[id=\"cap5_moda_p501_b_24_macho\"]").val())?$("[id=\"cap5_moda_p501_b_24_macho\"]").val():'';
    var cap5_moda_p501_b_25_macho   = ($("[id=\"cap5_moda_p501_b_25_macho\"]").val())?$("[id=\"cap5_moda_p501_b_25_macho\"]").val():'';
    var cap5_moda_p501_b_26_macho   = ($("[id=\"cap5_moda_p501_b_26_macho\"]").val())?$("[id=\"cap5_moda_p501_b_26_macho\"]").val():'';
    var cap5_moda_p501_b_27_macho   = ($("[id=\"cap5_moda_p501_b_27_macho\"]").val())?$("[id=\"cap5_moda_p501_b_27_macho\"]").val():'';
    var cap5_moda_p501_b_28_macho   = ($("[id=\"cap5_moda_p501_b_28_macho\"]").val())?$("[id=\"cap5_moda_p501_b_28_macho\"]").val():'';

    var cap5_moda_p501_b_lista =  [
        {"cap5_moda_p501_b":cap5_moda_p501_b_1,"cap5_moda_p501_b_hembra":cap5_moda_p501_b_1_hembra,"cap5_moda_p501_b_macho":cap5_moda_p501_b_1_macho},
        {"cap5_moda_p501_b":cap5_moda_p501_b_2,"cap5_moda_p501_b_hembra":cap5_moda_p501_b_2_hembra,"cap5_moda_p501_b_macho":cap5_moda_p501_b_2_macho},
        {"cap5_moda_p501_b":cap5_moda_p501_b_3,"cap5_moda_p501_b_hembra":cap5_moda_p501_b_3_hembra,"cap5_moda_p501_b_macho":cap5_moda_p501_b_3_macho},
        {"cap5_moda_p501_b":cap5_moda_p501_b_4,"cap5_moda_p501_b_hembra":cap5_moda_p501_b_4_hembra,"cap5_moda_p501_b_macho":cap5_moda_p501_b_4_macho},
        {"cap5_moda_p501_b":cap5_moda_p501_b_5,"cap5_moda_p501_b_hembra":cap5_moda_p501_b_5_hembra,"cap5_moda_p501_b_macho":cap5_moda_p501_b_5_macho},
        {"cap5_moda_p501_b":cap5_moda_p501_b_6,"cap5_moda_p501_b_hembra":cap5_moda_p501_b_6_hembra,"cap5_moda_p501_b_macho":cap5_moda_p501_b_6_macho},
        {"cap5_moda_p501_b":cap5_moda_p501_b_7,"cap5_moda_p501_b_hembra":cap5_moda_p501_b_7_hembra,"cap5_moda_p501_b_macho":cap5_moda_p501_b_7_macho},
        {"cap5_moda_p501_b":cap5_moda_p501_b_8,"cap5_moda_p501_b_hembra":cap5_moda_p501_b_8_hembra,"cap5_moda_p501_b_macho":cap5_moda_p501_b_8_macho},
        {"cap5_moda_p501_b":cap5_moda_p501_b_9,"cap5_moda_p501_b_hembra":cap5_moda_p501_b_9_hembra,"cap5_moda_p501_b_macho":cap5_moda_p501_b_9_macho},
        {"cap5_moda_p501_b":cap5_moda_p501_b_10,"cap5_moda_p501_b_hembra":cap5_moda_p501_b_10_hembra,"cap5_moda_p501_b_macho":cap5_moda_p501_b_10_macho},
        {"cap5_moda_p501_b":cap5_moda_p501_b_11,"cap5_moda_p501_b_hembra":cap5_moda_p501_b_11_hembra,"cap5_moda_p501_b_macho":cap5_moda_p501_b_11_macho},
        {"cap5_moda_p501_b":cap5_moda_p501_b_12,"cap5_moda_p501_b_hembra":cap5_moda_p501_b_12_hembra,"cap5_moda_p501_b_macho":cap5_moda_p501_b_12_macho},
        {"cap5_moda_p501_b":cap5_moda_p501_b_13,"cap5_moda_p501_b_hembra":cap5_moda_p501_b_13_hembra,"cap5_moda_p501_b_macho":cap5_moda_p501_b_13_macho},
        {"cap5_moda_p501_b":cap5_moda_p501_b_14,"cap5_moda_p501_b_hembra":cap5_moda_p501_b_14_hembra,"cap5_moda_p501_b_macho":cap5_moda_p501_b_14_macho},
        {"cap5_moda_p501_b":cap5_moda_p501_b_15,"cap5_moda_p501_b_hembra":cap5_moda_p501_b_15_hembra,"cap5_moda_p501_b_macho":cap5_moda_p501_b_15_macho},
        {"cap5_moda_p501_b":cap5_moda_p501_b_16,"cap5_moda_p501_b_hembra":cap5_moda_p501_b_16_hembra,"cap5_moda_p501_b_macho":cap5_moda_p501_b_16_macho},
        {"cap5_moda_p501_b":cap5_moda_p501_b_17,"cap5_moda_p501_b_hembra":cap5_moda_p501_b_17_hembra,"cap5_moda_p501_b_macho":cap5_moda_p501_b_17_macho},
        {"cap5_moda_p501_b":cap5_moda_p501_b_18,"cap5_moda_p501_b_hembra":cap5_moda_p501_b_18_hembra,"cap5_moda_p501_b_macho":cap5_moda_p501_b_18_macho},
        {"cap5_moda_p501_b":cap5_moda_p501_b_19,"cap5_moda_p501_b_hembra":cap5_moda_p501_b_19_hembra,"cap5_moda_p501_b_macho":cap5_moda_p501_b_19_macho},
        {"cap5_moda_p501_b":cap5_moda_p501_b_20,"cap5_moda_p501_b_hembra":cap5_moda_p501_b_20_hembra,"cap5_moda_p501_b_macho":cap5_moda_p501_b_20_macho},
        {"cap5_moda_p501_b":cap5_moda_p501_b_21,"cap5_moda_p501_b_hembra":cap5_moda_p501_b_21_hembra,"cap5_moda_p501_b_macho":cap5_moda_p501_b_21_macho},
        {"cap5_moda_p501_b":cap5_moda_p501_b_22,"cap5_moda_p501_b_hembra":cap5_moda_p501_b_22_hembra,"cap5_moda_p501_b_macho":cap5_moda_p501_b_22_macho},
        {"cap5_moda_p501_b":cap5_moda_p501_b_23,"cap5_moda_p501_b_hembra":cap5_moda_p501_b_23_hembra,"cap5_moda_p501_b_macho":cap5_moda_p501_b_23_macho},
        {"cap5_moda_p501_b":cap5_moda_p501_b_24,"cap5_moda_p501_b_hembra":cap5_moda_p501_b_24_hembra,"cap5_moda_p501_b_macho":cap5_moda_p501_b_24_macho},
        {"cap5_moda_p501_b":cap5_moda_p501_b_25,"cap5_moda_p501_b_hembra":cap5_moda_p501_b_25_hembra,"cap5_moda_p501_b_macho":cap5_moda_p501_b_25_macho},
        {"cap5_moda_p501_b":cap5_moda_p501_b_26,"cap5_moda_p501_b_hembra":cap5_moda_p501_b_26_hembra,"cap5_moda_p501_b_macho":cap5_moda_p501_b_26_macho},
        {"cap5_moda_p501_b":cap5_moda_p501_b_27,"cap5_moda_p501_b_hembra":cap5_moda_p501_b_27_hembra,"cap5_moda_p501_b_macho":cap5_moda_p501_b_27_macho},
        {"cap5_moda_p501_b":cap5_moda_p501_b_28,"cap5_moda_p501_b_hembra":cap5_moda_p501_b_28_hembra,"cap5_moda_p501_b_macho":cap5_moda_p501_b_28_macho},
    ];

    var cap5_modi_p530 = "";
    var cap5_modi_p530_1   = ($("[id=\"cap5_modi_p530_1\"]:checked").val())?1:0;
    var cap5_modi_p530_2   = ($("[id=\"cap5_modi_p530_2\"]:checked").val())?2:0;
    var cap5_modi_p530_3   = ($("[id=\"cap5_modi_p530_3\"]:checked").val())?3:0;
    var cap5_modi_p530_4   = ($("[id=\"cap5_modi_p530_4\"]:checked").val())?4:0;
    var cap5_modi_p530_5   = ($("[id=\"cap5_modi_p530_5\"]:checked").val())?5:0;
    var cap5_modi_p530_6   = ($("[id=\"cap5_modi_p530_6\"]:checked").val())?6:0;
    var cap5_modi_p530_7   = ($("[id=\"cap5_modi_p530_7\"]:checked").val())?7:0;
    var cap5_modi_p530_8   = ($("[id=\"cap5_modi_p530_8\"]:checked").val())?8:0;
    var cap5_modi_p530_9   = ($("[id=\"cap5_modi_p530_9\"]:checked").val())?9:0;

    var cap5_modi_p530_lista = [cap5_modi_p530_1,cap5_modi_p530_2,cap5_modi_p530_3,cap5_modi_p530_4,cap5_modi_p530_5,cap5_modi_p530_6,cap5_modi_p530_7,cap5_modi_p530_8,cap5_modi_p530_9];
    var cap5_modi_p530_contador = 0;
    $.each(cap5_modi_p530_lista, function( index, value ) {
        if(value!=0){
            if(cap5_modi_p530_contador==0){
                cap5_modi_p530 = cap5_modi_p530 + value;
            }else{
                cap5_modi_p530 = cap5_modi_p530 + "," + value;
            }
            cap5_modi_p530_contador++;
        }
    });

    var cap5_modi_p531 = "";
    var cap5_modi_p531_1   = ($("[id=\"cap5_modi_p531_1\"]:checked").val())?1:0;
    var cap5_modi_p531_2   = ($("[id=\"cap5_modi_p531_2\"]:checked").val())?1:0;

    var cap5_modi_p531_lista = [cap5_modi_p531_1,cap5_modi_p531_2];
    var cap5_modi_p531_contador = 0;
    $.each(cap5_modi_p531_lista, function( index, value ) {
        
        if(cap5_modi_p531_contador==0){
            cap5_modi_p531 = cap5_modi_p531 + value;
        }else{
            cap5_modi_p531 = cap5_modi_p531 + "," + value;
        }
        cap5_modi_p531_contador++;
    });

    

    var cap5_modi_p532 = "";
    var cap5_modi_p532_1   = ($("[id=\"cap5_modi_p532_1\"]:checked").val())?1:0;
    var cap5_modi_p532_2   = ($("[id=\"cap5_modi_p532_2\"]:checked").val())?1:0;
    var cap5_modi_p532_3   = ($("[id=\"cap5_modi_p532_3\"]:checked").val())?1:0;

    var cap5_modi_p532_lista = [cap5_modi_p532_1,cap5_modi_p532_2,cap5_modi_p532_3];
    var cap5_modi_p532_contador = 0;
    $.each(cap5_modi_p532_lista, function( index, value ) {
        if(cap5_modi_p532_contador==0){
            cap5_modi_p532 = cap5_modi_p532 + value;
        }else{
            cap5_modi_p532 = cap5_modi_p532 + "," + value;
        }
        cap5_modi_p532_contador++;
    });


    var cap5_modi_p533 = "";
    var cap5_modi_p533_1   = ($("[id=\"cap5_modi_p533_1\"]:checked").val())?1:0;
    var cap5_modi_p533_2   = ($("[id=\"cap5_modi_p533_2\"]:checked").val())?2:0;
    var cap5_modi_p533_3   = ($("[id=\"cap5_modi_p533_3\"]:checked").val())?3:0;
    var cap5_modi_p533_4   = ($("[id=\"cap5_modi_p533_4\"]:checked").val())?4:0;
    var cap5_modi_p533_5   = ($("[id=\"cap5_modi_p533_5\"]:checked").val())?5:0;
    var cap5_modi_p533_6   = ($("[id=\"cap5_modi_p533_6\"]:checked").val())?6:0;
    var cap5_modi_p533_7   = ($("[id=\"cap5_modi_p533_7\"]:checked").val())?7:0;
    var cap5_modi_p533_8   = ($("[id=\"cap5_modi_p533_8\"]:checked").val())?8:0;
    var cap5_modi_p533_9   = ($("[id=\"cap5_modi_p533_9\"]:checked").val())?9:0;
    var cap5_modi_p533_10   = ($("[id=\"cap5_modi_p533_10\"]:checked").val())?10:0;
    var cap5_modi_p533_11   = ($("[id=\"cap5_modi_p533_11\"]:checked").val())?11:0;
    var cap5_modi_p533_12   = ($("[id=\"cap5_modi_p533_12\"]:checked").val())?12:0;
    var cap5_modi_p533_13   = ($("[id=\"cap5_modi_p533_13\"]:checked").val())?13:0;
    var cap5_modi_p533_14   = ($("[id=\"cap5_modi_p533_14\"]:checked").val())?14:0;
    var cap5_modi_p533_15   = ($("[id=\"cap5_modi_p533_15\"]:checked").val())?15:0;
    var cap5_modi_p533_16   = ($("[id=\"cap5_modi_p533_16\"]:checked").val())?16:0;
    var cap5_modi_p533_17   = ($("[id=\"cap5_modi_p533_17\"]:checked").val())?17:0;
    var cap5_modi_p533_18   = ($("[id=\"cap5_modi_p533_18\"]:checked").val())?18:0;
    var cap5_modi_p533_19   = ($("[id=\"cap5_modi_p533_19\"]:checked").val())?19:0;
    var cap5_modi_p533_20   = ($("[id=\"cap5_modi_p533_20\"]:checked").val())?20:0;
    var cap5_modi_p533_21   = ($("[id=\"cap5_modi_p533_21\"]:checked").val())?21:0;
    var cap5_modi_p533_22   = ($("[id=\"cap5_modi_p533_22\"]:checked").val())?22:0;
    var cap5_modi_p533_23   = ($("[id=\"cap5_modi_p533_23\"]:checked").val())?23:0;
    var cap5_modi_p533_24   = ($("[id=\"cap5_modi_p533_24\"]:checked").val())?24:0;


    var cap5_modi_p533_lista = [cap5_modi_p533_1,cap5_modi_p533_2,cap5_modi_p533_3,cap5_modi_p533_4,cap5_modi_p533_5,cap5_modi_p533_6,cap5_modi_p533_7,cap5_modi_p533_8,cap5_modi_p533_9,cap5_modi_p533_10,cap5_modi_p533_11,cap5_modi_p533_12,cap5_modi_p533_13,cap5_modi_p533_14,cap5_modi_p533_15,cap5_modi_p533_16,cap5_modi_p533_17,cap5_modi_p533_18,cap5_modi_p533_19,cap5_modi_p533_20,cap5_modi_p533_21,cap5_modi_p533_22,cap5_modi_p533_23,cap5_modi_p533_24];
    var cap5_modi_p533_contador = 0;
    $.each(cap5_modi_p533_lista, function( index, value ) {
        if(value!=0){
            if(cap5_modi_p533_contador==0){
                cap5_modi_p533 = cap5_modi_p533 + value;
            }else{
                cap5_modi_p533 = cap5_modi_p533 + "," + value;
            }
            cap5_modi_p533_contador++;
        }
    });


    var cap5_modj_p534_b = "";
    var cap5_modj_p534_b_1   = ($("[id=\"cap5_modj_p534_b_1\"]:checked").val())?1:0;
    var cap5_modj_p534_b_2   = ($("[id=\"cap5_modj_p534_b_2\"]:checked").val())?2:0;
    var cap5_modj_p534_b_3   = ($("[id=\"cap5_modj_p534_b_3\"]:checked").val())?3:0;
    var cap5_modj_p534_b_4   = ($("[id=\"cap5_modj_p534_b_4\"]:checked").val())?4:0;
    var cap5_modj_p534_b_5   = ($("[id=\"cap5_modj_p534_b_5\"]:checked").val())?5:0;
    var cap5_modj_p534_b_6   = ($("[id=\"cap5_modj_p534_b_6\"]:checked").val())?6:0;
    var cap5_modj_p534_b_7   = ($("[id=\"cap5_modj_p534_b_7\"]:checked").val())?7:0;
    var cap5_modj_p534_b_8   = ($("[id=\"cap5_modj_p534_b_8\"]:checked").val())?8:0;
    var cap5_modj_p534_b_9   = ($("[id=\"cap5_modj_p534_b_9\"]:checked").val())?9:0;
    var cap5_modj_p534_b_10   = ($("[id=\"cap5_modj_p534_b_10\"]:checked").val())?10:0;
    var cap5_modj_p534_b_11   = ($("[id=\"cap5_modj_p534_b_11\"]:checked").val())?11:0;
    var cap5_modj_p534_b_12   = ($("[id=\"cap5_modj_p534_b_12\"]:checked").val())?12:0;
    var cap5_modj_p534_b_13   = ($("[id=\"cap5_modj_p534_b_13\"]:checked").val())?13:0;
    var cap5_modj_p534_b_14   = ($("[id=\"cap5_modj_p534_b_14\"]:checked").val())?14:0;
    var cap5_modj_p534_b_15   = ($("[id=\"cap5_modj_p534_b_15\"]:checked").val())?15:0;
    var cap5_modj_p534_b_16   = ($("[id=\"cap5_modj_p534_b_16\"]:checked").val())?16:0;
    var cap5_modj_p534_b_17   = ($("[id=\"cap5_modj_p534_b_17\"]:checked").val())?17:0;
    var cap5_modj_p534_b_18   = ($("[id=\"cap5_modj_p534_b_18\"]:checked").val())?18:0;
    var cap5_modj_p534_b_19   = ($("[id=\"cap5_modj_p534_b_19\"]:checked").val())?19:0;
    var cap5_modj_p534_b_20   = ($("[id=\"cap5_modj_p534_b_20\"]:checked").val())?20:0;
    var cap5_modj_p534_b_21   = ($("[id=\"cap5_modj_p534_b_21\"]:checked").val())?21:0;
    var cap5_modj_p534_b_22   = ($("[id=\"cap5_modj_p534_b_22\"]:checked").val())?22:0;
    var cap5_modj_p534_b_23   = ($("[id=\"cap5_modj_p534_b_23\"]:checked").val())?23:0;
    var cap5_modj_p534_b_24   = ($("[id=\"cap5_modj_p534_b_24\"]:checked").val())?24:0;
    var cap5_modj_p534_b_25   = ($("[id=\"cap5_modj_p534_b_25\"]:checked").val())?25:0;
    var cap5_modj_p534_b_26   = ($("[id=\"cap5_modj_p534_b_26\"]:checked").val())?26:0;
    var cap5_modj_p534_b_27   = ($("[id=\"cap5_modj_p534_b_27\"]:checked").val())?27:0;
    var cap5_modj_p534_b_28   = ($("[id=\"cap5_modj_p534_b_28\"]:checked").val())?28:0;
    var cap5_modj_p534_b_29   = ($("[id=\"cap5_modj_p534_b_29\"]:checked").val())?29:0;
    var cap5_modj_p534_b_30   = ($("[id=\"cap5_modj_p534_b_30\"]:checked").val())?30:0;
    var cap5_modj_p534_b_31   = ($("[id=\"cap5_modj_p534_b_31\"]:checked").val())?31:0;
    var cap5_modj_p534_b_32   = ($("[id=\"cap5_modj_p534_b_32\"]:checked").val())?32:0;
    var cap5_modj_p534_b_33   = ($("[id=\"cap5_modj_p534_b_33\"]:checked").val())?33:0;
    var cap5_modj_p534_b_34   = ($("[id=\"cap5_modj_p534_b_34\"]:checked").val())?34:0;
    var cap5_modj_p534_b_35   = ($("[id=\"cap5_modj_p534_b_35\"]:checked").val())?35:0;
    var cap5_modj_p534_b_36   = ($("[id=\"cap5_modj_p534_b_36\"]:checked").val())?36:0;
    var cap5_modj_p534_b_37   = ($("[id=\"cap5_modj_p534_b_37\"]:checked").val())?37:0;
    var cap5_modj_p534_b_38   = ($("[id=\"cap5_modj_p534_b_38\"]:checked").val())?38:0;




    var cap5_modj_p534_b_lista = [cap5_modj_p534_b_1,cap5_modj_p534_b_2,cap5_modj_p534_b_3,cap5_modj_p534_b_4,cap5_modj_p534_b_5,cap5_modj_p534_b_6,cap5_modj_p534_b_7,
    cap5_modj_p534_b_8,cap5_modj_p534_b_9,cap5_modj_p534_b_10,cap5_modj_p534_b_11,cap5_modj_p534_b_12,cap5_modj_p534_b_13,cap5_modj_p534_b_14,cap5_modj_p534_b_15,
    cap5_modj_p534_b_16,cap5_modj_p534_b_17,cap5_modj_p534_b_18,cap5_modj_p534_b_19,cap5_modj_p534_b_20,cap5_modj_p534_b_21,cap5_modj_p534_b_22,cap5_modj_p534_b_23,cap5_modj_p534_b_24,
    cap5_modj_p534_b_25,cap5_modj_p534_b_26,cap5_modj_p534_b_27,cap5_modj_p534_b_28,cap5_modj_p534_b_29,cap5_modj_p534_b_30,cap5_modj_p534_b_31,cap5_modj_p534_b_32,cap5_modj_p534_b_33,
    cap5_modj_p534_b_34,cap5_modj_p534_b_35,cap5_modj_p534_b_36,cap5_modj_p534_b_37,cap5_modj_p534_b_38];
    var cap5_modj_p534_b_contador = 0;
    $.each(cap5_modj_p534_b_lista, function( index, value ) {
        if(value!=0){
            if(cap5_modj_p534_b_contador==0){
                cap5_modj_p534_b = cap5_modj_p534_b + value;
            }else{
                cap5_modj_p534_b = cap5_modj_p534_b + "," + value;
            }
            cap5_modj_p534_b_contador++;
        }
    });
    
    var cap5_modj_p535_a = "";
    var cap5_modj_p535_a_1   = ($("[id=\"cap5_modj_p535_a_1\"]:checked").val())?1:0;
    var cap5_modj_p535_a_2   = ($("[id=\"cap5_modj_p535_a_2\"]:checked").val())?2:0;
    var cap5_modj_p535_a_3   = ($("[id=\"cap5_modj_p535_a_3\"]:checked").val())?3:0;
    var cap5_modj_p535_a_4   = ($("[id=\"cap5_modj_p535_a_4\"]:checked").val())?4:0;
    var cap5_modj_p535_a_5   = ($("[id=\"cap5_modj_p535_a_5\"]:checked").val())?5:0;
    var cap5_modj_p535_a_6   = ($("[id=\"cap5_modj_p535_a_6\"]:checked").val())?6:0;
    var cap5_modj_p535_a_7   = ($("[id=\"cap5_modj_p535_a_7\"]:checked").val())?7:0;

    var cap5_modj_p535_a_lista = [cap5_modj_p535_a_1,cap5_modj_p535_a_2,cap5_modj_p535_a_3,cap5_modj_p535_a_4,cap5_modj_p535_a_5,cap5_modj_p535_a_6,cap5_modj_p535_a_7];
    var cap5_modj_p535_a_contador = 0;
    $.each(cap5_modj_p535_a_lista, function( index, value ) {
        if(value!=0){
            if(cap5_modj_p535_a_contador==0){
                cap5_modj_p535_a = cap5_modj_p535_a + value;
            }else{
                cap5_modj_p535_a = cap5_modj_p535_a + "," + value;
            }
            cap5_modj_p535_a_contador++;
        }
    });

    var cap5_modk_p537 = "";
    var cap5_modk_p537_1   = ($("[id=\"cap5_modk_p537_1\"]:checked").val())?1:0;
    var cap5_modk_p537_2   = ($("[id=\"cap5_modk_p537_2\"]:checked").val())?1:0;
    var cap5_modk_p537_3   = ($("[id=\"cap5_modk_p537_3\"]:checked").val())?1:0;

    var cap5_modk_p537_lista = [cap5_modk_p537_1,cap5_modk_p537_2,cap5_modk_p537_3];
    var cap5_modk_p537_contador = 0;
    $.each(cap5_modk_p537_lista, function( index, value ) {
        if(cap5_modk_p537_contador==0){
            cap5_modk_p537 = cap5_modk_p537 + value;
        }else{
            cap5_modk_p537 = cap5_modk_p537 + "," + value;
        }
        cap5_modk_p537_contador++;
    });

    var cap5_modk_p536_a = "";
    var cap5_modk_p536_a_1   = ($("[id=\"cap5_modk_p536_a_1\"]:checked").val())?1:0;
    var cap5_modk_p536_a_2   = ($("[id=\"cap5_modk_p536_a_2\"]:checked").val())?2:0;
    var cap5_modk_p536_a_3   = ($("[id=\"cap5_modk_p536_a_3\"]:checked").val())?3:0;
    var cap5_modk_p536_a_4   = ($("[id=\"cap5_modk_p536_a_4\"]:checked").val())?4:0;
    var cap5_modk_p536_a_5   = ($("[id=\"cap5_modk_p536_a_5\"]:checked").val())?5:0;
    var cap5_modk_p536_a_6   = ($("[id=\"cap5_modk_p536_a_6\"]:checked").val())?6:0;
    var cap5_modk_p536_a_7   = ($("[id=\"cap5_modk_p536_a_7\"]:checked").val())?7:0;
    var cap5_modk_p536_a_8   = ($("[id=\"cap5_modk_p536_a_8\"]:checked").val())?8:0;
    var cap5_modk_p536_a_9   = ($("[id=\"cap5_modk_p536_a_9\"]:checked").val())?9:0;

    var cap5_modk_p536_a_lista = [cap5_modk_p536_a_1,cap5_modk_p536_a_2,cap5_modk_p536_a_3,cap5_modk_p536_a_4,cap5_modk_p536_a_5,cap5_modk_p536_a_6,cap5_modk_p536_a_7,cap5_modk_p536_a_8,cap5_modk_p536_a_9];
    var cap5_modk_p536_a_contador = 0;
    $.each(cap5_modk_p536_a_lista, function( index, value ) {
        if(value!=0){
            if(cap5_modk_p536_a_contador==0){
                cap5_modk_p536_a = cap5_modk_p536_a + value;
            }else{
                cap5_modk_p536_a = cap5_modk_p536_a + "," + value;
            }
            cap5_modk_p536_a_contador++;
        }
    });

    var cap5_modk_p539_a = "";
    var cap5_modk_p539_a_1   = ($("[id=\"cap5_modk_p539_1\"]:checked").val())?1:0;
    var cap5_modk_p539_a_2   = ($("[id=\"cap5_modk_p539_2\"]:checked").val())?2:0;
    var cap5_modk_p539_a_3   = ($("[id=\"cap5_modk_p539_3\"]:checked").val())?3:0;
    var cap5_modk_p539_a_4   = ($("[id=\"cap5_modk_p539_4\"]:checked").val())?4:0;
    var cap5_modk_p539_a_5   = ($("[id=\"cap5_modk_p539_5\"]:checked").val())?5:0;

    var cap5_modk_p539_a_lista = [cap5_modk_p539_a_1,cap5_modk_p539_a_2,cap5_modk_p539_a_3,cap5_modk_p539_a_4,cap5_modk_p539_a_5];
    var cap5_modk_p539_a_contador = 0;
    $.each(cap5_modk_p539_a_lista, function( index, value ) {
        if(value!=0){
            if(cap5_modk_p539_a_contador==0){
                cap5_modk_p539_a = cap5_modk_p539_a + value;
            }else{
                cap5_modk_p539_a = cap5_modk_p539_a + "," + value;
            }
            cap5_modk_p539_a_contador++;
        }
    });



    var cap5_modk_p541_a = "";
    var cap5_modk_p541_a_1   = ($("[id=\"cap5_modk_p541_a_1\"]:checked").val())?1:0;
    var cap5_modk_p541_a_2   = ($("[id=\"cap5_modk_p541_a_2\"]:checked").val())?2:0;
    var cap5_modk_p541_a_3   = ($("[id=\"cap5_modk_p541_a_3\"]:checked").val())?3:0;
    var cap5_modk_p541_a_4   = ($("[id=\"cap5_modk_p541_a_4\"]:checked").val())?4:0;
    var cap5_modk_p541_a_5   = ($("[id=\"cap5_modk_p541_a_5\"]:checked").val())?5:0;
    var cap5_modk_p541_a_6   = ($("[id=\"cap5_modk_p541_a_6\"]:checked").val())?6:0;
    var cap5_modk_p541_a_7   = ($("[id=\"cap5_modk_p541_a_7\"]:checked").val())?7:0;
    var cap5_modk_p541_a_8   = ($("[id=\"cap5_modk_p541_a_8\"]:checked").val())?8:0;
    var cap5_modk_p541_a_9   = ($("[id=\"cap5_modk_p541_a_9\"]:checked").val())?9:0;
    var cap5_modk_p541_a_10   = ($("[id=\"cap5_modk_p541_a_10\"]:checked").val())?10:0;
    var cap5_modk_p541_a_11   = ($("[id=\"cap5_modk_p541_a_11\"]:checked").val())?11:0;
    var cap5_modk_p541_a_12   = ($("[id=\"cap5_modk_p541_a_12\"]:checked").val())?12:0;
    var cap5_modk_p541_a_13   = ($("[id=\"cap5_modk_p541_a_13\"]:checked").val())?13:0;
    var cap5_modk_p541_a_14   = ($("[id=\"cap5_modk_p541_a_14\"]:checked").val())?14:0;
    var cap5_modk_p541_a_15   = ($("[id=\"cap5_modk_p541_a_15\"]:checked").val())?15:0;
    var cap5_modk_p541_a_16   = ($("[id=\"cap5_modk_p541_a_16\"]:checked").val())?16:0;
    var cap5_modk_p541_a_17   = ($("[id=\"cap5_modk_p541_a_17\"]:checked").val())?17:0;
    var cap5_modk_p541_a_18   = ($("[id=\"cap5_modk_p541_a_18\"]:checked").val())?18:0;
    var cap5_modk_p541_a_19   = ($("[id=\"cap5_modk_p541_a_19\"]:checked").val())?19:0;
    var cap5_modk_p541_a_20   = ($("[id=\"cap5_modk_p541_a_20\"]:checked").val())?20:0;
    var cap5_modk_p541_a_21   = ($("[id=\"cap5_modk_p541_a_21\"]:checked").val())?21:0;
    var cap5_modk_p541_a_22   = ($("[id=\"cap5_modk_p541_a_22\"]:checked").val())?22:0;
    var cap5_modk_p541_a_23   = ($("[id=\"cap5_modk_p541_a_23\"]:checked").val())?23:0;
    var cap5_modk_p541_a_24   = ($("[id=\"cap5_modk_p541_a_24\"]:checked").val())?24:0;
    var cap5_modk_p541_a_25   = ($("[id=\"cap5_modk_p541_a_25\"]:checked").val())?25:0;
    var cap5_modk_p541_a_26   = ($("[id=\"cap5_modk_p541_a_26\"]:checked").val())?26:0;
    var cap5_modk_p541_a_27   = ($("[id=\"cap5_modk_p541_a_27\"]:checked").val())?27:0;
    var cap5_modk_p541_a_28   = ($("[id=\"cap5_modk_p541_a_28\"]:checked").val())?28:0;
    var cap5_modk_p541_a_29   = ($("[id=\"cap5_modk_p541_a_29\"]:checked").val())?29:0;
    var cap5_modk_p541_a_30   = ($("[id=\"cap5_modk_p541_a_30\"]:checked").val())?30:0;
    var cap5_modk_p541_a_31   = ($("[id=\"cap5_modk_p541_a_31\"]:checked").val())?31:0;
    var cap5_modk_p541_a_32   = ($("[id=\"cap5_modk_p541_a_32\"]:checked").val())?32:0;
    var cap5_modk_p541_a_33   = ($("[id=\"cap5_modk_p541_a_33\"]:checked").val())?33:0;
    var cap5_modk_p541_a_34   = ($("[id=\"cap5_modk_p541_a_34\"]:checked").val())?34:0;
    var cap5_modk_p541_a_35   = ($("[id=\"cap5_modk_p541_a_35\"]:checked").val())?35:0;
    var cap5_modk_p541_a_36   = ($("[id=\"cap5_modk_p541_a_36\"]:checked").val())?36:0;
    var cap5_modk_p541_a_37   = ($("[id=\"cap5_modk_p541_a_37\"]:checked").val())?37:0;
    var cap5_modk_p541_a_38   = ($("[id=\"cap5_modk_p541_a_38\"]:checked").val())?38:0;
    var cap5_modk_p541_a_39   = ($("[id=\"cap5_modk_p541_a_39\"]:checked").val())?39:0;




    var cap5_modk_p541_a_lista = [cap5_modk_p541_a_1,cap5_modk_p541_a_2,cap5_modk_p541_a_3,cap5_modk_p541_a_4,cap5_modk_p541_a_5,cap5_modk_p541_a_6,cap5_modk_p541_a_7,
    cap5_modk_p541_a_8,cap5_modk_p541_a_9,cap5_modk_p541_a_10,cap5_modk_p541_a_11,cap5_modk_p541_a_12,cap5_modk_p541_a_13,cap5_modk_p541_a_14,cap5_modk_p541_a_15,
    cap5_modk_p541_a_16,cap5_modk_p541_a_17,cap5_modk_p541_a_18,cap5_modk_p541_a_19,cap5_modk_p541_a_20,cap5_modk_p541_a_21,cap5_modk_p541_a_22,cap5_modk_p541_a_23,cap5_modk_p541_a_24,
    cap5_modk_p541_a_25,cap5_modk_p541_a_26,cap5_modk_p541_a_27,cap5_modk_p541_a_28,cap5_modk_p541_a_29,cap5_modk_p541_a_30,cap5_modk_p541_a_31,cap5_modk_p541_a_32,cap5_modk_p541_a_33,
    cap5_modk_p541_a_34,cap5_modk_p541_a_35,cap5_modk_p541_a_36,cap5_modk_p541_a_37,cap5_modk_p541_a_38,cap5_modk_p541_a_39];
    var cap5_modk_p541_a_contador = 0;
    $.each(cap5_modk_p541_a_lista, function( index, value ) {
        if(value!=0){
            if(cap5_modk_p541_a_contador==0){
                cap5_modk_p541_a = cap5_modk_p541_a + value;
            }else{
                cap5_modk_p541_a = cap5_modk_p541_a + "," + value;
            }
            cap5_modk_p541_a_contador++;
        }
    });
    /* Seteando variables del capitulo VI */

    var cap6_p601 = "";
    var cap6_p601_1   = ($("[id=\"cap6_p601_1\"]:checked").val())?1:0;
    var cap6_p601_2   = ($("[id=\"cap6_p601_2\"]:checked").val())?2:0;
    var cap6_p601_3   = ($("[id=\"cap6_p601_3\"]:checked").val())?3:0;
    var cap6_p601_4   = ($("[id=\"cap6_p601_4\"]:checked").val())?4:0;
    var cap6_p601_5   = ($("[id=\"cap6_p601_5\"]:checked").val())?5:0;
    var cap6_p601_6   = ($("[id=\"cap6_p601_6\"]:checked").val())?6:0;
    var cap6_p601_7   = ($("[id=\"cap6_p601_7\"]:checked").val())?7:0;

    var cap6_p601_lista = [cap6_p601_1,cap6_p601_2,cap6_p601_3,cap6_p601_4,cap6_p601_5,cap6_p601_6,cap6_p601_7];
    var cap6_p601_contador = 0;
    $.each(cap6_p601_lista, function( index, value ) {
        if(value!=0){
            if(cap6_p601_contador==0){
                cap6_p601 = cap6_p601 + value;
            }else{
                cap6_p601 = cap6_p601 + "," + value;
            }
            cap6_p601_contador++;
        }
    });


    var cap6_p602 = "";
    var cap6_p602_1   = ($("[id=\"cap6_p602_1\"]:checked").val())?1:0;
    var cap6_p602_2   = ($("[id=\"cap6_p602_2\"]:checked").val())?2:0;
    var cap6_p602_3   = ($("[id=\"cap6_p602_3\"]:checked").val())?3:0;
    var cap6_p602_4   = ($("[id=\"cap6_p602_4\"]:checked").val())?4:0;
    var cap6_p602_5   = ($("[id=\"cap6_p602_5\"]:checked").val())?5:0;
    var cap6_p602_6   = ($("[id=\"cap6_p602_6\"]:checked").val())?6:0;
    var cap6_p602_7   = ($("[id=\"cap6_p602_7\"]:checked").val())?7:0;

    var cap6_p602_lista = [cap6_p602_1,cap6_p602_2,cap6_p602_3,cap6_p602_4,cap6_p602_5,cap6_p602_6,cap6_p602_7];
    var cap6_p602_contador = 0;
    $.each(cap6_p602_lista, function( index, value ) {
        if(value!=0){
            if(cap6_p602_contador==0){
                cap6_p602 = cap6_p602 + value;
            }else{
                cap6_p602 = cap6_p602 + "," + value;
            }
            cap6_p602_contador++;
        }
    });

    var cap6_p605 = "";
    var cap6_p605_1   = ($("[id=\"cap6_p605_1\"]:checked").val())?1:0;
    var cap6_p605_2   = ($("[id=\"cap6_p605_2\"]:checked").val())?2:0;
    var cap6_p605_3   = ($("[id=\"cap6_p605_3\"]:checked").val())?3:0;
    var cap6_p605_4   = ($("[id=\"cap6_p605_4\"]:checked").val())?4:0;
    var cap6_p605_5   = ($("[id=\"cap6_p605_5\"]:checked").val())?5:0;
    var cap6_p605_6   = ($("[id=\"cap6_p605_6\"]:checked").val())?6:0;

    var cap6_p605_lista = [cap6_p605_1,cap6_p605_2,cap6_p605_3,cap6_p605_4,cap6_p605_5,cap6_p605_6];
    var cap6_p605_contador = 0;
    $.each(cap6_p605_lista, function( index, value ) {
        if(value!=0){
            if(cap6_p605_contador==0){
                cap6_p605 = cap6_p605 + value;
            }else{
                cap6_p605 = cap6_p605 + "," + value;
            }
            cap6_p605_contador++;
        }
    });


    var cap6_p606 = "";
    var cap6_p606_1   = ($("[id=\"cap6_p606_1\"]:checked").val())?1:0;
    var cap6_p606_2   = ($("[id=\"cap6_p606_2\"]:checked").val())?2:0;
    var cap6_p606_3   = ($("[id=\"cap6_p606_3\"]:checked").val())?3:0;
    var cap6_p606_4   = ($("[id=\"cap6_p606_4\"]:checked").val())?4:0;
    var cap6_p606_5   = ($("[id=\"cap6_p606_5\"]:checked").val())?5:0;

    var cap6_p606_lista = [cap6_p606_1,cap6_p606_2,cap6_p606_3,cap6_p606_4,cap6_p606_5];
    var cap6_p606_contador = 0;
    $.each(cap6_p606_lista, function( index, value ) {
        if(value!=0){
            if(cap6_p606_contador==0){
                cap6_p606 = cap6_p606 + value;
            }else{
                cap6_p606 = cap6_p606 + "," + value;
            }
            cap6_p606_contador++;
        }
    });

    var cap6_p610 = "";
    var cap6_p610_1   = ($("[id=\"cap6_p610_1\"]:checked").val())?1:0;
    var cap6_p610_2   = ($("[id=\"cap6_p610_2\"]:checked").val())?2:0;
    var cap6_p610_3   = ($("[id=\"cap6_p610_3\"]:checked").val())?3:0;
    var cap6_p610_4   = ($("[id=\"cap6_p610_4\"]:checked").val())?4:0;
    var cap6_p610_5   = ($("[id=\"cap6_p610_5\"]:checked").val())?5:0;
    var cap6_p610_6   = ($("[id=\"cap6_p610_6\"]:checked").val())?6:0;
    var cap6_p610_7   = ($("[id=\"cap6_p610_7\"]:checked").val())?7:0;

    var cap6_p610_lista = [cap6_p610_1,cap6_p610_2,cap6_p610_3,cap6_p610_4,cap6_p610_5,cap6_p610_6,cap6_p610_7];
    var cap6_p610_contador = 0;
    $.each(cap6_p610_lista, function( index, value ) {
        if(value!=0){
            if(cap6_p610_contador==0){
                cap6_p610 = cap6_p610 + value;
            }else{
                cap6_p610 = cap6_p610 + "," + value;
            }
            cap6_p610_contador++;
        }
    });


    /* Seteando variables del capitulo VII */
    var cap7_modc_p714 = "";
    var cap7_modc_p714_1   = ($("[id=\"cap7_modc_p714_1\"]:checked").val())?1:0;
    var cap7_modc_p714_2   = ($("[id=\"cap7_modc_p714_2\"]:checked").val())?2:0;
    var cap7_modc_p714_3   = ($("[id=\"cap7_modc_p714_3\"]:checked").val())?3:0;
    var cap7_modc_p714_4   = ($("[id=\"cap7_modc_p714_4\"]:checked").val())?4:0;
    var cap7_modc_p714_5   = ($("[id=\"cap7_modc_p714_5\"]:checked").val())?5:0;
    var cap7_modc_p714_6   = ($("[id=\"cap7_modc_p714_6\"]:checked").val())?6:0;
    var cap7_modc_p714_7   = ($("[id=\"cap7_modc_p714_7\"]:checked").val())?7:0;

    var cap7_modc_p714_lista = [cap7_modc_p714_1,cap7_modc_p714_2,cap7_modc_p714_3,cap7_modc_p714_4,cap7_modc_p714_5,cap7_modc_p714_6,cap7_modc_p714_7];
    var cap7_modc_p714_contador = 0;
    $.each(cap7_modc_p714_lista, function( index, value ) {
        if(value!=0){
            if(cap7_modc_p714_contador==0){
                cap7_modc_p714 = cap7_modc_p714 + value;
            }else{
                cap7_modc_p714 = cap7_modc_p714 + "," + value;
            }
            cap7_modc_p714_contador++;
        }
    });


    var cap7_modc_p715 = "";
    var cap7_modc_p715_1   = ($("[id=\"cap7_modc_p715_1\"]:checked").val())?1:0;
    var cap7_modc_p715_2   = ($("[id=\"cap7_modc_p715_2\"]:checked").val())?2:0;
    var cap7_modc_p715_3   = ($("[id=\"cap7_modc_p715_3\"]:checked").val())?3:0;
    var cap7_modc_p715_4   = ($("[id=\"cap7_modc_p715_4\"]:checked").val())?4:0;
    var cap7_modc_p715_5   = ($("[id=\"cap7_modc_p715_5\"]:checked").val())?5:0;
    var cap7_modc_p715_6   = ($("[id=\"cap7_modc_p715_6\"]:checked").val())?6:0;
    var cap7_modc_p715_7   = ($("[id=\"cap7_modc_p715_7\"]:checked").val())?7:0;

    var cap7_modc_p715_lista = [cap7_modc_p715_1,cap7_modc_p715_2,cap7_modc_p715_3,cap7_modc_p715_4,cap7_modc_p715_5,cap7_modc_p715_6,cap7_modc_p715_7];
    var cap7_modc_p715_contador = 0;
    $.each(cap7_modc_p715_lista, function( index, value ) {
        if(value!=0){
            if(cap7_modc_p715_contador==0){
                cap7_modc_p715 = cap7_modc_p715 + value;
            }else{
                cap7_modc_p715 = cap7_modc_p715 + "," + value;
            }
            cap7_modc_p715_contador++;
        }
    });

    var cap7_modc_p716 = "";
    var cap7_modc_p716_1   = ($("[id=\"cap7_modc_p716_1\"]:checked").val())?1:0;
    var cap7_modc_p716_2   = ($("[id=\"cap7_modc_p716_2\"]:checked").val())?2:0;
    var cap7_modc_p716_3   = ($("[id=\"cap7_modc_p716_3\"]:checked").val())?3:0;
    var cap7_modc_p716_4   = ($("[id=\"cap7_modc_p716_4\"]:checked").val())?4:0;
    var cap7_modc_p716_5   = ($("[id=\"cap7_modc_p716_5\"]:checked").val())?5:0;
    var cap7_modc_p716_6   = ($("[id=\"cap7_modc_p716_6\"]:checked").val())?6:0;
    var cap7_modc_p716_7   = ($("[id=\"cap7_modc_p716_7\"]:checked").val())?7:0;

    var cap7_modc_p716_lista = [cap7_modc_p716_1,cap7_modc_p716_2,cap7_modc_p716_3,cap7_modc_p716_4,cap7_modc_p716_5,cap7_modc_p716_6,cap7_modc_p716_7];
    var cap7_modc_p716_contador = 0;
    $.each(cap7_modc_p716_lista, function( index, value ) {
        if(value!=0){
            if(cap7_modc_p716_contador==0){
                cap7_modc_p716 = cap7_modc_p716 + value;
            }else{
                cap7_modc_p716 = cap7_modc_p716 + "," + value;
            }
            cap7_modc_p716_contador++;
        }
    });


    var cap7_modc_p717 = "";
    var cap7_modc_p717_1   = ($("[id=\"cap7_modc_p717_1\"]:checked").val())?1:0;
    var cap7_modc_p717_2   = ($("[id=\"cap7_modc_p717_2\"]:checked").val())?2:0;
    var cap7_modc_p717_3   = ($("[id=\"cap7_modc_p717_3\"]:checked").val())?3:0;
    var cap7_modc_p717_4   = ($("[id=\"cap7_modc_p717_4\"]:checked").val())?4:0;
    var cap7_modc_p717_5   = ($("[id=\"cap7_modc_p717_5\"]:checked").val())?5:0;
    var cap7_modc_p717_6   = ($("[id=\"cap7_modc_p717_6\"]:checked").val())?6:0;
    var cap7_modc_p717_7   = ($("[id=\"cap7_modc_p717_7\"]:checked").val())?7:0;

    var cap7_modc_p717_lista = [cap7_modc_p717_1,cap7_modc_p717_2,cap7_modc_p717_3,cap7_modc_p717_4,cap7_modc_p717_5,cap7_modc_p717_6,cap7_modc_p717_7];
    var cap7_modc_p717_contador = 0;
    $.each(cap7_modc_p717_lista, function( index, value ) {
        if(value!=0){
            if(cap7_modc_p717_contador==0){
                cap7_modc_p717 = cap7_modc_p717 + value;
            }else{
                cap7_modc_p717 = cap7_modc_p717 + "," + value;
            }
            cap7_modc_p717_contador++;
        }
    });
    /* 
    var cap7_modc_p718 = "";
    var cap7_modc_p718_1   = ($("[id=\"cap7_modc_p718_1\"]:checked").val())?1:0;
    var cap7_modc_p718_2   = ($("[id=\"cap7_modc_p718_2\"]:checked").val())?2:0;
    var cap7_modc_p718_3   = ($("[id=\"cap7_modc_p718_3\"]:checked").val())?3:0;
    var cap7_modc_p718_4   = ($("[id=\"cap7_modc_p718_4\"]:checked").val())?4:0;
    var cap7_modc_p718_5   = ($("[id=\"cap7_modc_p718_5\"]:checked").val())?5:0;
    var cap7_modc_p718_6   = ($("[id=\"cap7_modc_p718_6\"]:checked").val())?6:0;
    var cap7_modc_p718_7   = ($("[id=\"cap7_modc_p718_7\"]:checked").val())?7:0;

    var cap7_modc_p718_lista = [cap7_modc_p718_1,cap7_modc_p718_2,cap7_modc_p718_3,cap7_modc_p718_4,cap7_modc_p718_5,cap7_modc_p718_6,cap7_modc_p718_7];
    var cap7_modc_p718_contador = 0;
    $.each(cap7_modc_p718_lista, function( index, value ) {
        if(value!=0){
            if(cap7_modc_p718_contador==0){
                cap7_modc_p718 = cap7_modc_p718 + value;
            }else{
                cap7_modc_p718 = cap7_modc_p718 + "," + value;
            }
            cap7_modc_p718_contador++;
        }
    }); */


    var cap7_modc_p719 = "";
    var cap7_modc_p719_1   = ($("[id=\"cap7_modc_p719_1\"]:checked").val())?1:0;
    var cap7_modc_p719_2   = ($("[id=\"cap7_modc_p719_2\"]:checked").val())?2:0;
    var cap7_modc_p719_3   = ($("[id=\"cap7_modc_p719_3\"]:checked").val())?3:0;
    var cap7_modc_p719_4   = ($("[id=\"cap7_modc_p719_4\"]:checked").val())?4:0;
    var cap7_modc_p719_5   = ($("[id=\"cap7_modc_p719_5\"]:checked").val())?5:0;
    var cap7_modc_p719_6   = ($("[id=\"cap7_modc_p719_6\"]:checked").val())?6:0;

    var cap7_modc_p719_lista = [cap7_modc_p719_1,cap7_modc_p719_2,cap7_modc_p719_3,cap7_modc_p719_4,cap7_modc_p719_5,cap7_modc_p719_6];
    var cap7_modc_p719_contador = 0;
    $.each(cap7_modc_p719_lista, function( index, value ) {
        if(value!=0){
            if(cap7_modc_p719_contador==0){
                cap7_modc_p719 = cap7_modc_p719 + value;
            }else{
                cap7_modc_p719 = cap7_modc_p719 + "," + value;
            }
            cap7_modc_p719_contador++;
        }
    });


    var cap7_mode_p726 = "";
    var cap7_mode_p726_1   = ($("[id=\"cap7_mode_p726_1\"]:checked").val())?1:0;
    var cap7_mode_p726_2   = ($("[id=\"cap7_mode_p726_2\"]:checked").val())?2:0;
    var cap7_mode_p726_3   = ($("[id=\"cap7_mode_p726_3\"]:checked").val())?3:0;
    var cap7_mode_p726_4   = ($("[id=\"cap7_mode_p726_4\"]:checked").val())?4:0;
    var cap7_mode_p726_5   = ($("[id=\"cap7_mode_p726_5\"]:checked").val())?5:0;
    var cap7_mode_p726_6   = ($("[id=\"cap7_mode_p726_6\"]:checked").val())?6:0;
    var cap7_mode_p726_7   = ($("[id=\"cap7_mode_p726_7\"]:checked").val())?7:0;
    var cap7_mode_p726_8   = ($("[id=\"cap7_mode_p726_8\"]:checked").val())?8:0;
    var cap7_mode_p726_9   = ($("[id=\"cap7_mode_p726_9\"]:checked").val())?9:0;
    var cap7_mode_p726_10   = ($("[id=\"cap7_mode_p726_10\"]:checked").val())?10:0;
    var cap7_mode_p726_11   = ($("[id=\"cap7_mode_p726_11\"]:checked").val())?11:0;

    var cap7_mode_p726_lista = [cap7_mode_p726_1,cap7_mode_p726_2,cap7_mode_p726_3,cap7_mode_p726_4,cap7_mode_p726_5,cap7_mode_p726_6,cap7_mode_p726_7,cap7_mode_p726_8,cap7_mode_p726_9,cap7_mode_p726_10,cap7_mode_p726_11];
    var cap7_mode_p726_contador = 0;
    $.each(cap7_mode_p726_lista, function( index, value ) {
        if(value!=0){
            if(cap7_mode_p726_contador==0){
                cap7_mode_p726 = cap7_mode_p726 + value;
            }else{
                cap7_mode_p726 = cap7_mode_p726 + "," + value;
            }
            cap7_mode_p726_contador++;
        }
    });


    var cap7_mode_p727 = "";
    var cap7_mode_p727_1   = ($("[id=\"cap7_mode_p727_1\"]:checked").val())?1:0;
    var cap7_mode_p727_2   = ($("[id=\"cap7_mode_p727_2\"]:checked").val())?2:0;
    var cap7_mode_p727_3   = ($("[id=\"cap7_mode_p727_3\"]:checked").val())?3:0;
    var cap7_mode_p727_4   = ($("[id=\"cap7_mode_p727_4\"]:checked").val())?4:0;
    var cap7_mode_p727_5   = ($("[id=\"cap7_mode_p727_5\"]:checked").val())?5:0;
    var cap7_mode_p727_6   = ($("[id=\"cap7_mode_p727_6\"]:checked").val())?6:0;
    var cap7_mode_p727_7   = ($("[id=\"cap7_mode_p727_7\"]:checked").val())?7:0;
    var cap7_mode_p727_8   = ($("[id=\"cap7_mode_p727_8\"]:checked").val())?8:0;
    var cap7_mode_p727_9   = ($("[id=\"cap7_mode_p727_9\"]:checked").val())?9:0;
    var cap7_mode_p727_10   = ($("[id=\"cap7_mode_p727_10\"]:checked").val())?10:0;

    var cap7_mode_p727_lista = [cap7_mode_p727_1,cap7_mode_p727_2,cap7_mode_p727_3,cap7_mode_p727_4,cap7_mode_p727_5,cap7_mode_p727_6,cap7_mode_p727_7,cap7_mode_p727_8,cap7_mode_p727_9,cap7_mode_p727_10];
    var cap7_mode_p727_contador = 0;
    $.each(cap7_mode_p727_lista, function( index, value ) {
        if(value!=0){
            if(cap7_mode_p727_contador==0){
                cap7_mode_p727 = cap7_mode_p727 + value;
            }else{
                cap7_mode_p727 = cap7_mode_p727 + "," + value;
            }
            cap7_mode_p727_contador++;
        }
    });


    var cap7_mode_p729 = "";
    var cap7_mode_p729_1   = ($("[id=\"cap7_mode_p729_1\"]:checked").val())?1:0;
    var cap7_mode_p729_2   = ($("[id=\"cap7_mode_p729_2\"]:checked").val())?2:0;
    var cap7_mode_p729_3   = ($("[id=\"cap7_mode_p729_3\"]:checked").val())?3:0;
    var cap7_mode_p729_4   = ($("[id=\"cap7_mode_p729_4\"]:checked").val())?4:0;
    var cap7_mode_p729_5   = ($("[id=\"cap7_mode_p729_5\"]:checked").val())?5:0;

    var cap7_mode_p729_lista = [cap7_mode_p729_1,cap7_mode_p729_2,cap7_mode_p729_3,cap7_mode_p729_4,cap7_mode_p729_5];
    var cap7_mode_p729_contador = 0;
    $.each(cap7_mode_p729_lista, function( index, value ) {
        if(value!=0){
            if(cap7_mode_p729_contador==0){
                cap7_mode_p729 = cap7_mode_p729 + value;
            }else{
                cap7_mode_p729 = cap7_mode_p729 + "," + value;
            }
            cap7_mode_p729_contador++;
        }
    });


    var cap7_mode_p731 = "";
    var cap7_mode_p731_1   = ($("[id=\"cap7_mode_p731_1\"]:checked").val())?1:0;
    var cap7_mode_p731_2   = ($("[id=\"cap7_mode_p731_2\"]:checked").val())?2:0;
    var cap7_mode_p731_3   = ($("[id=\"cap7_mode_p731_3\"]:checked").val())?3:0;
    var cap7_mode_p731_4   = ($("[id=\"cap7_mode_p731_4\"]:checked").val())?4:0;
    var cap7_mode_p731_5   = ($("[id=\"cap7_mode_p731_5\"]:checked").val())?5:0;
    var cap7_mode_p731_6   = ($("[id=\"cap7_mode_p731_6\"]:checked").val())?6:0;

    var cap7_mode_p731_lista = [cap7_mode_p731_1,cap7_mode_p731_2,cap7_mode_p731_3,cap7_mode_p731_4,cap7_mode_p731_5,cap7_mode_p731_6];
    var cap7_mode_p731_contador = 0;
    $.each(cap7_mode_p731_lista, function( index, value ) {
        if(value!=0){
            if(cap7_mode_p731_contador==0){
                cap7_mode_p731 = cap7_mode_p731 + value;
            }else{
                cap7_mode_p731 = cap7_mode_p731 + "," + value;
            }
            cap7_mode_p731_contador++;
        }
    });

    /* Seteando variables del capitulo VIII */
    var cap8_p801 = "";
    var cap8_p801_1   = ($("[id=\"cap8_p801_1\"]:checked").val())?1:0;
    var cap8_p801_2   = ($("[id=\"cap8_p801_2\"]:checked").val())?2:0;
    var cap8_p801_3   = ($("[id=\"cap8_p801_3\"]:checked").val())?3:0;
    var cap8_p801_4   = ($("[id=\"cap8_p801_4\"]:checked").val())?4:0;
    var cap8_p801_5   = ($("[id=\"cap8_p801_5\"]:checked").val())?5:0;
    var cap8_p801_6   = ($("[id=\"cap8_p801_6\"]:checked").val())?6:0;
    var cap8_p801_7   = ($("[id=\"cap8_p801_7\"]:checked").val())?7:0;
    var cap8_p801_8   = ($("[id=\"cap8_p801_8\"]:checked").val())?8:0;

    var cap8_p801_lista = [cap8_p801_1,cap8_p801_2,cap8_p801_3,cap8_p801_4,cap8_p801_5,cap8_p801_6,cap8_p801_7,cap8_p801_8];
    var cap8_p801_contador = 0;
    $.each(cap8_p801_lista, function( index, value ) {
        if(value!=0){
            if(cap8_p801_contador==0){
                cap8_p801 = cap8_p801 + value;
            }else{
                cap8_p801 = cap8_p801 + "," + value;
            }
            cap8_p801_contador++;
        }
    });

    var cap8_p802 = "";
    var cap8_p802_1   = ($("[id=\"cap8_p802_1\"]:checked").val())?1:0;
    var cap8_p802_2   = ($("[id=\"cap8_p802_2\"]:checked").val())?2:0;
    var cap8_p802_3   = ($("[id=\"cap8_p802_3\"]:checked").val())?3:0;
    var cap8_p802_4   = ($("[id=\"cap8_p802_4\"]:checked").val())?4:0;
    var cap8_p802_5   = ($("[id=\"cap8_p802_5\"]:checked").val())?5:0;
    var cap8_p802_6   = ($("[id=\"cap8_p802_6\"]:checked").val())?6:0;
    var cap8_p802_7   = ($("[id=\"cap8_p802_7\"]:checked").val())?7:0;

    var cap8_p802_lista = [cap8_p802_1,cap8_p802_2,cap8_p802_3,cap8_p802_4,cap8_p802_5,cap8_p802_6,cap8_p802_7];
    var cap8_p802_contador = 0;
    $.each(cap8_p802_lista, function( index, value ) {
        if(value!=0){
            if(cap8_p802_contador==0){
                cap8_p802 = cap8_p802 + value;
            }else{
                cap8_p802 = cap8_p802 + "," + value;
            }
            cap8_p802_contador++;
        }
    });


    /* Seteando variables del capitulo VIII */
    var cap8_p803 = "";
    var cap8_p803_1   = ($("[id=\"cap8_p803_1\"]:checked").val())?1:0;
    var cap8_p803_2   = ($("[id=\"cap8_p803_2\"]:checked").val())?2:0;
    var cap8_p803_3   = ($("[id=\"cap8_p803_3\"]:checked").val())?3:0;
    var cap8_p803_4   = ($("[id=\"cap8_p803_4\"]:checked").val())?4:0;
    var cap8_p803_5   = ($("[id=\"cap8_p803_5\"]:checked").val())?5:0;
    var cap8_p803_6   = ($("[id=\"cap8_p803_6\"]:checked").val())?6:0;
    var cap8_p803_7   = ($("[id=\"cap8_p803_7\"]:checked").val())?7:0;
    var cap8_p803_8   = ($("[id=\"cap8_p803_8\"]:checked").val())?8:0;
    var cap8_p803_9   = ($("[id=\"cap8_p803_9\"]:checked").val())?9:0;

    var cap8_p803_lista = [cap8_p803_1,cap8_p803_2,cap8_p803_3,cap8_p803_4,cap8_p803_5,cap8_p803_6,cap8_p803_7,cap8_p803_8,cap8_p803_9];
    var cap8_p803_contador = 0;
    $.each(cap8_p803_lista, function( index, value ) {
        if(value!=0){
            if(cap8_p803_contador==0){
                cap8_p803 = cap8_p803 + value;
            }else{
                cap8_p803 = cap8_p803 + "," + value;
            }
            cap8_p803_contador++;
        }
    });




    var form = $("#formEncuesta");
    var formData = $("#formEncuesta").serializeArray();
    /* Agregando variables del capitulo I para el envio del formulario*/
    formData.push({name: "Encuesta[p01_1]", value: p01_1});
    formData.push({name: "Encuesta[p01_2]", value: p01_2});
    formData.push({name: "Encuesta[p01_3]", value: p01_3});
    formData.push({name: "Encuesta[p01_4]", value: p01_4});
    formData.push({name: "Encuesta[p01_5]", value: p01_5});
    formData.push({name: "Encuesta[p01_6]", value: p01_6});
    formData.push({name: "Encuesta[p01_7]", value: p01_7});
    formData.push({name: "Encuesta[p01_8]", value: p01_8});
    formData.push({name: "Encuesta[p01_9]", value: p01_9});
    formData.push({name: "Encuesta[p01_10]", value: p01_10});
    formData.push({name: "Encuesta[p01_11]", value: p01_11});
    formData.push({name: "Encuesta[p01_12]", value: p01_12});
    /* Agregando variables del capitulo III para el envio del formulario*/
    formData.push({name: "Encuesta[p03_c_int_tenencia]", value: p03_c_int_tenencia});

    /* Agregando variables del capitulo IV para el envio del formulario*/
    formData.push({name: "Encuesta[cap4_modd_p437]", value: cap4_modd_p437});
    formData.push({name: "Encuesta[cap4_modd_p438]", value: cap4_modd_p438});
    formData.push({name: "Encuesta[cap4_mode_p443]", value: cap4_mode_p443});
    formData.push({name: "Encuesta[cap4_mode_p440]", value: JSON.stringify(cap4_mode_p440_lista)});
    formData.push({name: "Encuesta[cap4_mode_p444]", value: cap4_mode_p444});
    /* Agregando variables del capitulo V para el envio del formulario*/
    formData.push({name: "Encuesta[cap5_moda_p501_b]", value: JSON.stringify(cap5_moda_p501_b_lista)});
    formData.push({name: "Encuesta[cap5_modi_p530]", value: cap5_modi_p530});
    formData.push({name: "Encuesta[cap5_modi_p531]", value: cap5_modi_p531});
    formData.push({name: "Encuesta[cap5_modi_p532]", value: cap5_modi_p532});
    formData.push({name: "Encuesta[cap5_modi_p533]", value: cap5_modi_p533});

    formData.push({name: "Encuesta[cap5_modj_p534_b]", value: cap5_modj_p534_b});
    formData.push({name: "Encuesta[cap5_modj_p535_a]", value: cap5_modj_p535_a});

    formData.push({name: "Encuesta[cap5_modk_p537]", value: cap5_modk_p537});
    formData.push({name: "Encuesta[cap5_modk_p536_a]", value: cap5_modk_p536_a});
    formData.push({name: "Encuesta[cap5_modk_p539_a]", value: cap5_modk_p539_a});
    formData.push({name: "Encuesta[cap5_modk_p541_a]", value: cap5_modk_p541_a});

    
    
    /* Agregando variables del capitulo VI para el envio del formulario*/
    formData.push({name: "Encuesta[cap6_p601]", value: cap6_p601});
    formData.push({name: "Encuesta[cap6_p602]", value: cap6_p602});
    formData.push({name: "Encuesta[cap6_p605]", value: cap6_p605});
    formData.push({name: "Encuesta[cap6_p606]", value: cap6_p606});
    formData.push({name: "Encuesta[cap6_p610]", value: cap6_p610});

    /* Agregando variables del capitulo VII para el envio del formulario*/
    formData.push({name: "Encuesta[cap7_modc_p714]", value: cap7_modc_p714});
    formData.push({name: "Encuesta[cap7_modc_p715]", value: cap7_modc_p715});
    formData.push({name: "Encuesta[cap7_modc_p716]", value: cap7_modc_p716});
    formData.push({name: "Encuesta[cap7_modc_p717]", value: cap7_modc_p717});
    //formData.push({name: "Encuesta[cap7_modc_p718]", value: cap7_modc_p718});
    formData.push({name: "Encuesta[cap7_modc_p719]", value: cap7_modc_p719});
    formData.push({name: "Encuesta[cap7_mode_p726]", value: cap7_mode_p726});
    formData.push({name: "Encuesta[cap7_mode_p727]", value: cap7_mode_p727});
    formData.push({name: "Encuesta[cap7_mode_p729]", value: cap7_mode_p729});
    formData.push({name: "Encuesta[cap7_mode_p731]", value: cap7_mode_p731});

    /* Agregando variables del capitulo VIII para el envio del formulario*/
    formData.push({name: "Encuesta[cap8_p801]", value: cap8_p801});
    formData.push({name: "Encuesta[cap8_p802]", value: cap8_p802});
    formData.push({name: "Encuesta[cap8_p803]", value: cap8_p803});


    if (form.find(".has-error").length) {
        return false;
    }

    $.ajax({
        url:form.attr("action"),
        type: form.attr("method"),
        data: formData,
        dataType: "json",
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        beforeSend:function()
        {
            loading.modal("show");
        },
        success: function (results) {
            if(results.success){
                if(results.ban && results.ban==1){
                    window.location.href = "<?= \Yii::$app->request->BaseUrl ?>/parcela";
                }else{
                    setTimeout(function(){ loading.modal("hide"); }, 2000);
                }
                
            }
        },
    });
});

$("body").on("click", ".btn-finalizar-encuesta", function (e) {
    Swal.fire({
        title: '¿Está seguro de finalizar la encuesta?',
        text: "Una vez finalizado la encuesta no se podrá editar ni modificar la información",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, finalizar encuesta',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
    if (result.isConfirmed) {

        $.ajax({
                url: "<?= \Yii::$app->request->BaseUrl ?>/encuesta/finalizar",
                method: "POST",
                data:{_csrf:csrf,id_encuesta:id_encuesta},
                dataType:"Json",
                beforeSend:function(xhr, settings)
                {
                    loading.modal("show");
                },
                success:function(results)
                {
                    if(results.success){
                        window.location.href = "<?= \Yii::$app->request->BaseUrl ?>/encuesta/view?id_encuesta="+id_encuesta;
                        /* Swal.fire(
                            'Encuesta',
                            'La encuesta ha finalizado correctamente',
                            'success'
                        ); */
                    }
                    
                },
                error:function(){
                    alert("Error al realizar el proceso.");
                }
            });
        
    }
    })
});
/* Carga de Encuesta */
Encuesta();
async function Encuesta(){
    await   $.ajax({
                url: "<?= \Yii::$app->request->BaseUrl ?>/encuesta/get-encuesta",
                method: "POST",
                data:{_csrf:csrf,id_encuesta:id_encuesta},
                dataType:"Json",
                beforeSend:function(xhr, settings)
                {
                    loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){

                        if(results.encuesta.infoGeneral){
                            let infoGeneral = results.encuesta.infoGeneral;
                            $("#general_ubigeo").val(infoGeneral.COD_UBIGEO);
                            $("#general_region_natural").val(infoGeneral.COD_REGION_NATURAL);
                            $("#general_piso_ecologico").val(infoGeneral.TXT_PISO_ECOLOGICO);
                            $("#general_subestrato").val(infoGeneral.COD_SUBESTRATO);
                            $("#general_comunidad_campesina").val(infoGeneral.TXT_COMUNIDAD_CAMPESINA);
                            $("#general_codigo").val(infoGeneral.COD_CC);
                            $("#general_numero_segmento").val(infoGeneral.COD_SEGMENTO_EMPRESA);
                            $("#general_numero_parcela").val(infoGeneral.NUM_PARCELA_SM);
                            $("#general_total_parcela").val(infoGeneral.NUM_TOTAL_PARCELAS_SM);
                            $("#general_region").val(infoGeneral.TXT_DEPARTAMENTO);
/* 
                            $("#general_provincia").val(infoGeneral.TXT_PROVINCIA);
                            $("#general_distrito").val(infoGeneral.COD_UBIGEO); */

                            $("#general_centro_poblado").val(infoGeneral.COD_CCPP);

                            codUbigeo = infoGeneral.COD_UBIGEO;
                            
                            if(codUbigeo){
                                cod_distrito = codUbigeo.substring(0, 6);
                                cod_provincia = codUbigeo.substring(0, 4);
                                cod_departamento = codUbigeo.substring(0, 2);
                                //console.log(cod_distrito,cod_provincia,cod_departamento)
                                ProvinciasGeneral(cod_departamento,cod_provincia);
                                DistritosGeneral(cod_provincia,cod_distrito);
                                CentrosPobladosGeneral(cod_distrito,infoGeneral.COD_CCPP);
                            }else{
                                cod_departamento = infoGeneral.IDDPTO;
                                ProvinciasGeneral(cod_departamento);
                            }
                        }

                        if(results.encuesta.capitulo_i){
                            let capitulo_i_cabecera = results.encuesta.capitulo_i.cabecera;
                            let capitulo_i_detalle = results.encuesta.capitulo_i.detalle;
                            /* Seteando valor de cabecera p01 */
                            $("#p01_observacion").val(capitulo_i_cabecera.TXT_OBSERVACION);

                            /* Seteando valores de detalle p01 */
                            $.each(capitulo_i_detalle, function( index, value ) {
                                if(value.ID_ACT_PARCELA){
                                    $("#p01_"+value.ID_ACT_PARCELA).prop("checked", true);
                                }else{
                                    $("#p01_"+value.ID_ACT_PARCELA).prop("checked", false);
                                }
                            });
                        }


                        if(results.encuesta.capitulo_ii){
                            let capitulo_ii_cabecera = results.encuesta.capitulo_ii.cabecera;
                            let capitulo_ii_detalle = results.encuesta.capitulo_ii.detalle;
                            /* Seteando valor de cabecera p02 */
                            if(capitulo_ii_cabecera.COD_TIPO_PRODUCTOR && capitulo_ii_cabecera.COD_TIPO_PRODUCTOR=="1"){
                                $("#p02_tipo_productor_"+capitulo_ii_cabecera.COD_TIPO_PRODUCTOR).prop("checked", true);
                            }else if(capitulo_ii_cabecera.COD_TIPO_PRODUCTOR && capitulo_ii_cabecera.COD_TIPO_PRODUCTOR=="2"){
                                $("#p02_tipo_productor_"+capitulo_ii_cabecera.COD_TIPO_PRODUCTOR).prop("checked", true);
                            }
                                
                            $("#p02_observacion").val(capitulo_ii_cabecera.TXT_OBSERVACIONES);

                            if(capitulo_ii_cabecera.COD_TIPO_PRODUCTOR && capitulo_ii_cabecera.COD_TIPO_PRODUCTOR=="1"){
                                $(".capitulo_ii_modulo_a").show();
                               
                            }else if(capitulo_ii_cabecera.COD_TIPO_PRODUCTOR && capitulo_ii_cabecera.COD_TIPO_PRODUCTOR=="2"){
                                $(".capitulo_ii_modulo_b").show();
                                
                            }
                            
                            
                            /* Seteando valores de detalle p02 */
                            $.each(capitulo_ii_detalle, function( index, value ) {

                                if(capitulo_ii_cabecera.COD_TIPO_PRODUCTOR && capitulo_ii_cabecera.COD_TIPO_PRODUCTOR=="1"){
                                    if(capitulo_ii_detalle.length==2){
                                        if(value.FLG_INFO=="0"){
                                            if(capitulo_ii_cabecera.COD_TOMA_DECISIONES_N){
                                                $("#p02_a_toma_decision_"+capitulo_ii_cabecera.COD_TOMA_DECISIONES_N).prop("checked", true);
                                            }

                                            $("#p02_a_representante_nombres").val(value.TXT_NOMBRES);
                                            $("#p02_a_representante_apellidos").val(value.TXT_APELLIDOS);
                                            $("#p02_a_txt_dni").val(value.TXT_DNI_RUC);

                                            $("#p02_a_representante_tiene_numero").val(value.FLG_NUMERO);
                                            if(value.FLG_NUMERO=="1"){
                                                $(".p02_a_representante_tiene_numero").show();
                                            }

                                            $("#p02_a_representante_telefono_fijo").val(value.TXT_FIJO);
                                            $("#p02_a_representante_telefono_celular").val(value.TXT_CELULAR);
                                            $("#p02_a_tiene_correo_electronico").val(value.FLG_CORREO_ELECTRONICO);
                                            if(value.FLG_CORREO_ELECTRONICO=="1"){
                                                $(".p02_a_tiene_correo_electronico").show();
                                            }
                                            
                                            $("#p02_a_correo_electronico").val(value.TXT_CORREO);

                                            if(value.COD_PERFIL){
                                                $("#p02_a_informacion_proporcionada_"+value.COD_PERFIL).prop("checked", true);
                                                if(value.COD_PERFIL=="12"){
                                                    $(".p02_a_informacion_proporcionada_12").show();
                                                    $("#p02_a_informacion_proporcionada_especifique").val(value.TXT_OTRO_PERFIL);
                                                }
                                            }

                                            
                                            $("#p02_a_es_productor").val(value.FLG_INFO);

                                            $(".p02_a_es_productor").show();
                                        }else if(value.FLG_INFO=="1"){

                                            cen = value.COD_CENTRO_POBLADO;
                                            if(cen){
                                                dis = cen.substring(0, 6);
                                                pro = cen.substring(0, 4);
                                                dep = cen.substring(0, 2);
                                                Regiones("2","a",dep,"#p02_a_informante_departamento");
                                                Provincias("2","a",dep,pro,"#p02_a_informante_provincia");
                                                Distritos("2","a",pro,dis,"#p02_a_informante_distrito")
                                                CentrosPoblados("2","a",dis,cen,"#p02_a_informante_centro_poblado");
                                            }
                                            

                                            $("#p02_a_informante_nombres").val(value.TXT_NOMBRES);
                                            $("#p02_a_informante_apellidos").val(value.TXT_APELLIDOS);
                                            $("#p02_a_informante_tiene_numero").val(value.FLG_NUMERO);
                                            if(value.FLG_NUMERO=="1"){
                                                $(".p02_a_informante_tiene_numero").show();
                                            }
                                            $("#p02_a_informante_telefono_fijo").val(value.TXT_FIJO);
                                            $("#p02_a_informante_telefono_celular").val(value.TXT_CELULAR);

                                            if(value.COD_VIVE_PRODUCTOR){
                                                $("#p02_a_productor_agrario_vive_"+value.COD_VIVE_PRODUCTOR).prop("checked", true);
                                                if(value.COD_VIVE_PRODUCTOR=="15"){
                                                    $(".p02_a_productor_agrario_vive_15").show();
                                                    $("#p02_a_productor_agrario_vive_especifique").val(value.TXT_OTRO_VIVE_PRODUCTOR);
                                                }
                                                
                                            }
                                            $("#p02_a_pertenece_comunidad").val(capitulo_ii_cabecera.TIP_PERT_COMU_CAMP_N);
                                            

                                            
                                        }
                                    }else{
                                        Regiones("2","a",null,"#p02_a_informante_departamento");
                                        if(capitulo_ii_cabecera.COD_TOMA_DECISIONES_N){
                                            $("#p02_a_toma_decision_"+capitulo_ii_cabecera.COD_TOMA_DECISIONES_N).prop("checked", true);
                                        }

                                        $("#p02_a_representante_nombres").val(value.TXT_NOMBRES);
                                        $("#p02_a_representante_apellidos").val(value.TXT_APELLIDOS);
                                        $("#p02_a_txt_dni").val(value.TXT_DNI_RUC);

                                        $("#p02_a_representante_tiene_numero").val(value.FLG_NUMERO);
                                        if(value.FLG_NUMERO=="1"){
                                            $(".p02_a_representante_tiene_numero").show();
                                        }

                                        $("#p02_a_representante_telefono_fijo").val(value.TXT_FIJO);
                                        $("#p02_a_representante_telefono_celular").val(value.TXT_CELULAR);
                                        $("#p02_a_tiene_correo_electronico").val(value.FLG_CORREO_ELECTRONICO);
                                        if(value.FLG_CORREO_ELECTRONICO=="1"){
                                            $(".p02_a_tiene_correo_electronico").show();
                                        }
                                        
                                        $("#p02_a_correo_electronico").val(value.TXT_CORREO);

                                        if(value.COD_PERFIL){
                                            $("#p02_a_informacion_proporcionada_"+value.COD_PERFIL).prop("checked", true);
                                            if(value.COD_PERFIL=="12"){
                                                $(".p02_a_informacion_proporcionada_12").show();
                                                $("#p02_a_informacion_proporcionada_especifique").val(value.TXT_OTRO_PERFIL);
                                            }
                                        }

                                        
                                        $("#p02_a_es_productor").val(value.FLG_INFO);
                                        $("#p02_a_pertenece_comunidad").val(capitulo_ii_cabecera.TIP_PERT_COMU_CAMP_N);
                                    }
                                    

                                }else if(capitulo_ii_cabecera.COD_TIPO_PRODUCTOR && capitulo_ii_cabecera.COD_TIPO_PRODUCTOR=="2"){
                                    if(value.FLG_INFO=="0"){
                                        cen = value.COD_CENTRO_POBLADO;
                                        if(cen){
                                            dis = cen.substring(0, 6);
                                            pro = cen.substring(0, 4);
                                            dep = cen.substring(0, 2);
                                            Regiones("2","b",dep,"#p02_b_representante_departamento");
                                            Provincias("2","b",dep,pro,"#p02_b_representante_provincia");
                                            Distritos("2","b",pro,dis,"#p02_b_representante_distrito");
                                            CentrosPoblados("2","b",dis,cen,"#p02_b_representante_centro_poblado");
                                            $("#p02_b_representante_codigo_centro_poblado").val(cen);
                                        }

                                        $("#p02_b_razon_social").val(value.TXT_RAZON_SOCIAL);
                                        $("#p02_b_ruc").val(value.TXT_DNI_RUC);
                                        $("#p02_b_representante_legal_nombres").val(value.TXT_NOMBRES);
                                        $("#p02_b_representante_legal_apellidos").val(value.TXT_APELLIDOS);
                                        if(value.COD_TIPO_VIA){
                                            $("#p02_b_tipo_via_"+value.COD_TIPO_VIA).prop("checked", true);
                                            if(value.COD_TIPO_VIA=="21"){
                                                $('.p02_b_tipo_via_21').show();
                                                $("#p02_b_tipo_via_especifique").val(value.TXT_OTRO_VIA);
                                            }
                                        }
                                        

                                        $("#p02_b_avenida").val(value.TXT_AVENIDA);
                                        $("#p02_b_nro_puerta").val(value.TXT_PUERTA);
                                        $("#p02_b_block").val(value.TXT_BLOCK);
                                        $("#p02_b_interior").val(value.TXT_INTERIOR);
                                        $("#p02_b_piso").val(value.TXT_PISO);
                                        $("#p02_b_manzana").val(value.TXT_MANZANA);
                                        $("#p02_b_lote").val(value.TXT_LOTE);
                                    }else if(value.FLG_INFO=="1"){
                                        

                                        $("#p02_b_informante_nombres").val(value.TXT_NOMBRES);
                                        $("#p02_b_informante_apellidos").val(value.TXT_APELLIDOS);

                                        if(value.COD_PERFIL){
                                            $("#p02_b_cargo_"+value.COD_PERFIL).prop("checked", true);
                                            if(value.COD_PERFIL=="26"){
                                                $('.p02_b_cargo_26').show();
                                                $("#p02_b_cargo_especifique").val(value.TXT_OTRO_PERFIL);
                                            }
                                        }
                                        
                                        

                                        $("#p02_b_informante_tiene_numero").val(value.FLG_NUMERO);
                                        if(value.FLG_NUMERO=="1"){
                                            $(".p02_b_informante_tiene_numero").show();
                                        }

                                        $("#p02_b_informante_telefono_fijo").val(value.TXT_FIJO);
                                        $("#p02_b_informante_telefono_celular").val(value.TXT_CELULAR);

                                        $("#p02_b_informante_tiene_correo_electronico").val(value.FLG_CORREO_ELECTRONICO);
                                        if(value.FLG_CORREO_ELECTRONICO=="1"){
                                            $(".p02_b_informante_tiene_correo_electronico").show();
                                        }
                                        
                                        $("#p02_b_informante_correo_electronico").val(value.TXT_CORREO);
                                        $("#p02_b_nombre_fundo").val(capitulo_ii_cabecera.TXT_NOM_PARCELA_J);
                                        


                                    }
                                    //Regiones("2","b");
                                }
                            });


                        }

                        if(results.encuesta.capitulo_iii){
                            let capitulo_iii_cabecera = results.encuesta.capitulo_iii.cabecera;
                            let capitulo_iii_detalle = results.encuesta.capitulo_iii.detalle;
                            let capitulo_iii_detalle_modulo_a = results.encuesta.capitulo_iii.capitulo_iii_detalle_modulo_a;
                            let capitulo_iii_detalle_modulo_b = results.encuesta.capitulo_iii.capitulo_iii_detalle_modulo_b;
                            
                            $("#p03_observacion").val(capitulo_iii_cabecera.TXT_OBSERVACIONES);
                            $("#p03_c_int_trabaja_otra_parcela").val(capitulo_iii_cabecera.FLG_CONDUCE_PARCELA);
                            $("#p03_c_int_cantidad_parcelas").val(capitulo_iii_cabecera.NUM_PARCELAS_CONDUCE);

                            /* Seteando valores de detalle p03 */
                            $.each(capitulo_iii_detalle, function( index, value ) {
                                if(value.ID_TENENCIA){
                                    $("#p03_c_int_tenencia_"+value.ID_TENENCIA).prop("checked", true);
                                }else{
                                    $("#p03_c_int_tenencia_"+value.ID_TENENCIA).prop("checked", false);
                                }

                                if(value.ID_TENENCIA=="31"){
                                    $("#p03_c_int_tenencia_31_especifique").val(value.TXT_OTRO_TENENCIA);
                                    $(".p03_c_int_tenencia_31").show()
                                }
                            });

                            /* Seteando valores de detalle del modulo a */
                           
                            var lote_capitulo_iii_modulo_a = "";
                            $('#lista-usos-coberturas').DataTable().destroy();
                            $.each(capitulo_iii_detalle_modulo_a, function( index, value ) {
                                lote_capitulo_iii_modulo_a = lote_capitulo_iii_modulo_a + "<tr>";
                                    lote_capitulo_iii_modulo_a = lote_capitulo_iii_modulo_a + "<td>" + ((value.NUM_LOTE)?value.NUM_LOTE:"") + "</td>";
                                    lote_capitulo_iii_modulo_a = lote_capitulo_iii_modulo_a + "<td>" + ((value.TXT_USO_COBERT_TIERRA)?value.TXT_USO_COBERT_TIERRA:"") + "</td>";
                                    lote_capitulo_iii_modulo_a = lote_capitulo_iii_modulo_a + "<td>" + ((value.NUM_CANT_LOTE_PARCELA)?value.NUM_CANT_LOTE_PARCELA:"") + "</td>";
                                    lote_capitulo_iii_modulo_a = lote_capitulo_iii_modulo_a + "<td>" + ((value.TXT_UNIDAD_MEDIDA)?value.TXT_UNIDAD_MEDIDA:"") + "</td>";
                                    lote_capitulo_iii_modulo_a = lote_capitulo_iii_modulo_a + "<td>" + ((value.NUM_EQ_LOTE_PARCELA)?value.NUM_EQ_LOTE_PARCELA:"") + "</td>";


                                    lote_capitulo_iii_modulo_a = lote_capitulo_iii_modulo_a + "<td>";
                                        lote_capitulo_iii_modulo_a = lote_capitulo_iii_modulo_a + '<a href="<?= \Yii::$app->request->BaseUrl ?>/lote/update-capitulo-iii-modulo-a?id_cap_iii_lote=' + value.ID_CAP_III_LOTE + '&geocodigo_fdo=<?= $geocodigo_fdo ?>" target="_blank"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> ';
                                        lote_capitulo_iii_modulo_a = lote_capitulo_iii_modulo_a + '<button type="button" data-id_lote="' + value.ID_CAP_III_LOTE + '" class="btn btn-danger btn-eliminar-cap-iii-modulo-a"><i class="fas fa-trash"></i></button>'
                                    
                                    lote_capitulo_iii_modulo_a = lote_capitulo_iii_modulo_a +"</td>";
                                lote_capitulo_iii_modulo_a = lote_capitulo_iii_modulo_a + "</tr>";
                            });
                            $('#lista-usos-coberturas tbody').html(lote_capitulo_iii_modulo_a);
                            $('#lista-usos-coberturas').DataTable({
                                "paging": true,
                                "lengthChange": true,
                                "searching": false,
                                "ordering": false,
                                "info": true,
                                "autoWidth": false,
                                "pageLength" : 10,
                                "language": {
                                    "sProcessing":    "Procesando...",
                                    "sLengthMenu":    "Mostrar _MENU_ registros",
                                    "sZeroRecords":   "No se encontraron resultados",
                                    "sEmptyTable":    "Ningun dato disponible en esta lista",
                                    "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                    "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                    "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                    "sInfoPostFix":   "",
                                    "sSearch":        "Buscar:",
                                    "sUrl":           "",
                                    "sInfoThousands":  ",",
                                    "sLoadingRecords": "Cargando...",
                                    "oPaginate": {
                                        "sFirst":    "Primero",
                                        "sLast":    "Último",
                                        "sNext":    "Siguiente",
                                        "sPrevious": "Anterior"
                                    },
                                    "oAria": {
                                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                    }
                                },
                            });

                            
                            /* Seteando valores de detalle del modulo b */
                                                    
                            var lote_capitulo_iii_modulo_b = "";
                            $('#lista-parcelas-no-establecidas').DataTable().destroy();
                            $.each(capitulo_iii_detalle_modulo_b, function( index, value ) {
                                lote_capitulo_iii_modulo_b = lote_capitulo_iii_modulo_b + "<tr>";
                                    lote_capitulo_iii_modulo_b = lote_capitulo_iii_modulo_b + "<td>" + ((value.NUM_PARCELA)?value.NUM_PARCELA:"") + "</td>";
                                    lote_capitulo_iii_modulo_b = lote_capitulo_iii_modulo_b + "<td>" + ((value.NUM_SUP_TOTAL)?value.NUM_SUP_TOTAL:"") + "</td>";
                                    lote_capitulo_iii_modulo_b = lote_capitulo_iii_modulo_b + "<td>" + ((value.TXT_UNIDAD_MEDIDA)?value.TXT_UNIDAD_MEDIDA:"") + "</td>";
                                    lote_capitulo_iii_modulo_b = lote_capitulo_iii_modulo_b + "<td>" + ((value.NUM_EQ_PARCELA)?value.NUM_EQ_PARCELA:"") + "</td>";

                                    lote_capitulo_iii_modulo_b = lote_capitulo_iii_modulo_b + "<td>";
                                        lote_capitulo_iii_modulo_b = lote_capitulo_iii_modulo_b + '<a href="<?= \Yii::$app->request->BaseUrl ?>/lote/update-capitulo-iii-modulo-b?id_otra_parcela=' + value.ID_OTRA_PARCELA + '&geocodigo_fdo=<?= $geocodigo_fdo ?>" target="_blank"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> ';
                                        lote_capitulo_iii_modulo_b = lote_capitulo_iii_modulo_b + '<button type="button" data-id_otra_parcela="' + value.ID_OTRA_PARCELA + '"  class="btn btn-danger btn-eliminar-cap-iii-modulo-b"><i class="fas fa-trash"></i></button>'
                                    
                                    lote_capitulo_iii_modulo_b = lote_capitulo_iii_modulo_b +"</td>";
                                lote_capitulo_iii_modulo_b = lote_capitulo_iii_modulo_b + "</tr>";
                            });
                            $('#lista-parcelas-no-establecidas tbody').html(lote_capitulo_iii_modulo_b);
                            $('#lista-parcelas-no-establecidas').DataTable({
                                "paging": true,
                                "lengthChange": true,
                                "searching": false,
                                "ordering": false,
                                "info": true,
                                "autoWidth": false,
                                "pageLength" : 10,
                                "language": {
                                    "sProcessing":    "Procesando...",
                                    "sLengthMenu":    "Mostrar _MENU_ registros",
                                    "sZeroRecords":   "No se encontraron resultados",
                                    "sEmptyTable":    "Ningun dato disponible en esta lista",
                                    "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                    "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                    "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                    "sInfoPostFix":   "",
                                    "sSearch":        "Buscar:",
                                    "sUrl":           "",
                                    "sInfoThousands":  ",",
                                    "sLoadingRecords": "Cargando...",
                                    "oPaginate": {
                                        "sFirst":    "Primero",
                                        "sLast":    "Último",
                                        "sNext":    "Siguiente",
                                        "sPrevious": "Anterior"
                                    },
                                    "oAria": {
                                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                    }
                                },
                            });
                            
                        }

                        if(results.encuesta.capitulo_iv){
                            let capitulo_iv_cabecera_modulo_a = results.encuesta.capitulo_iv.cabecera_modulo_a;
                            let capitulo_iv_cabecera_modulo_b = results.encuesta.capitulo_iv.cabecera_modulo_b;
                            let capitulo_iv_cabecera_modulo_c = results.encuesta.capitulo_iv.cabecera_modulo_c;

                            let capitulo_iv_cabecera_modulo_d = results.encuesta.capitulo_iv.cabecera_modulo_d;
                            let capitulo_iv_modulo_d_p437 = results.encuesta.capitulo_iv.detalle_p437;
                            let capitulo_iv_modulo_d_p438 = results.encuesta.capitulo_iv.detalle_p438;

                            let capitulo_iv_cabecera_modulo_e = results.encuesta.capitulo_iv.cabecera_modulo_e;
                            let capitulo_iv_modulo_e_p440 =results.encuesta.capitulo_iv.detalle_p440;
                            let capitulo_iv_modulo_e_p443 =results.encuesta.capitulo_iv.detalle_p443;
                            let capitulo_iv_modulo_e_p444 =results.encuesta.capitulo_iv.detalle_p444;

                            /* Seteando para el modulo a */
                            var registro_capitulo_iv_cabecera_modulo_a = "";
                            $('#lista-cap4-moda-cultivos-transitorias').DataTable().destroy();
                            $.each(capitulo_iv_cabecera_modulo_a, function( index, value ) {
                                
                                registro_capitulo_iv_cabecera_modulo_a = registro_capitulo_iv_cabecera_modulo_a + "<tr>";
                                    registro_capitulo_iv_cabecera_modulo_a = registro_capitulo_iv_cabecera_modulo_a + "<td>" + ((value.NUM_LOTE)?value.NUM_LOTE:"") + "</td>";
                                    registro_capitulo_iv_cabecera_modulo_a = registro_capitulo_iv_cabecera_modulo_a + "<td>" + ((value.CULTIVO_TIEMPO)?value.CULTIVO_TIEMPO:"") + "</td>";
                                    registro_capitulo_iv_cabecera_modulo_a = registro_capitulo_iv_cabecera_modulo_a + "<td>" + ((value.TXT_CULTIVO)?value.TXT_CULTIVO:"") + "</td>";

                                    registro_capitulo_iv_cabecera_modulo_a = registro_capitulo_iv_cabecera_modulo_a + "<td>";
                                        registro_capitulo_iv_cabecera_modulo_a = registro_capitulo_iv_cabecera_modulo_a + '<a href="<?= \Yii::$app->request->BaseUrl ?>/capituloiv/update-capitulo-iv-modulo-a?id_cap_iv_a=' + value.ID_CAP_IV_A + '&geocodigo_fdo=<?= $geocodigo_fdo ?>"  target="_blank" class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> ';
                                        registro_capitulo_iv_cabecera_modulo_a = registro_capitulo_iv_cabecera_modulo_a + '<button type="button" data-id_cap_iv_a="' + value.ID_CAP_IV_A + '" class="btn btn-danger btn-eliminar-capitulo-iv-modulo-a"><i class="fas fa-trash"></i></button>'
                                    
                                    registro_capitulo_iv_cabecera_modulo_a = registro_capitulo_iv_cabecera_modulo_a +"</td>";
                                registro_capitulo_iv_cabecera_modulo_a = registro_capitulo_iv_cabecera_modulo_a + "</tr>";
                            });
                            $('#lista-cap4-moda-cultivos-transitorias tbody').html(registro_capitulo_iv_cabecera_modulo_a);
                            $('#lista-cap4-moda-cultivos-transitorias').DataTable({
                                "paging": true,
                                "lengthChange": true,
                                "searching": false,
                                "ordering": false,
                                "info": true,
                                "autoWidth": false,
                                "pageLength" : 10,
                                "language": {
                                    "sProcessing":    "Procesando...",
                                    "sLengthMenu":    "Mostrar _MENU_ registros",
                                    "sZeroRecords":   "No se encontraron resultados",
                                    "sEmptyTable":    "Ningun dato disponible en esta lista",
                                    "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                    "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                    "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                    "sInfoPostFix":   "",
                                    "sSearch":        "Buscar:",
                                    "sUrl":           "",
                                    "sInfoThousands":  ",",
                                    "sLoadingRecords": "Cargando...",
                                    "oPaginate": {
                                        "sFirst":    "Primero",
                                        "sLast":    "Último",
                                        "sNext":    "Siguiente",
                                        "sPrevious": "Anterior"
                                    },
                                    "oAria": {
                                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                    }
                                },
                            });

                            /* Seteando para el modulo b */
                            var registro_capitulo_iv_cabecera_modulo_b = "";
                            $('#lista-cap4-modb-arboles-frutales').DataTable().destroy();
                            $.each(capitulo_iv_cabecera_modulo_b, function( index, value ) {
                                registro_capitulo_iv_cabecera_modulo_b = registro_capitulo_iv_cabecera_modulo_b + "<tr>";
                                    registro_capitulo_iv_cabecera_modulo_b = registro_capitulo_iv_cabecera_modulo_b + "<td>" + ((value.ID_ARBOLES_FRUTALES)?value.ID_ARBOLES_FRUTALES:"") + "</td>";
                                    registro_capitulo_iv_cabecera_modulo_b = registro_capitulo_iv_cabecera_modulo_b + "<td>" + ((value.NUM_PLANTAS)?value.NUM_PLANTAS:"") + "</td>";
                                    registro_capitulo_iv_cabecera_modulo_b = registro_capitulo_iv_cabecera_modulo_b + "<td>" + ((value.NUM_PLANTAS_PROD)?value.NUM_PLANTAS_PROD:"") + "</td>";

                                    registro_capitulo_iv_cabecera_modulo_b = registro_capitulo_iv_cabecera_modulo_b + "<td>";
                                        registro_capitulo_iv_cabecera_modulo_b = registro_capitulo_iv_cabecera_modulo_b + '<a href="<?= \Yii::$app->request->BaseUrl ?>/capituloiv/update-capitulo-iv-modulo-b?num_orden=' + value.NUM_ORDEN + '&id_encuesta=<?= $id_encuesta ?>&geocodigo_fdo=<?= $geocodigo_fdo ?>" target="_blank"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> ';
                                        registro_capitulo_iv_cabecera_modulo_b = registro_capitulo_iv_cabecera_modulo_b + '<button type="button" data-num_orden="' + value.NUM_ORDEN + '" data-id_encuesta="<?= $id_encuesta ?>" class="btn btn-danger btn-eliminar-capitulo-iv-modulo-b"><i class="fas fa-trash"></i></button>'
                                    
                                    registro_capitulo_iv_cabecera_modulo_b = registro_capitulo_iv_cabecera_modulo_b +"</td>";
                                registro_capitulo_iv_cabecera_modulo_b = registro_capitulo_iv_cabecera_modulo_b + "</tr>";
                            });
                            $('#lista-cap4-modb-arboles-frutales tbody').html(registro_capitulo_iv_cabecera_modulo_b);
                            $('#lista-cap4-modb-arboles-frutales').DataTable({
                                "paging": true,
                                "lengthChange": true,
                                "searching": false,
                                "ordering": false,
                                "info": true,
                                "autoWidth": false,
                                "pageLength" : 10,
                                "language": {
                                    "sProcessing":    "Procesando...",
                                    "sLengthMenu":    "Mostrar _MENU_ registros",
                                    "sZeroRecords":   "No se encontraron resultados",
                                    "sEmptyTable":    "Ningun dato disponible en esta lista",
                                    "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                    "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                    "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                    "sInfoPostFix":   "",
                                    "sSearch":        "Buscar:",
                                    "sUrl":           "",
                                    "sInfoThousands":  ",",
                                    "sLoadingRecords": "Cargando...",
                                    "oPaginate": {
                                        "sFirst":    "Primero",
                                        "sLast":    "Último",
                                        "sNext":    "Siguiente",
                                        "sPrevious": "Anterior"
                                    },
                                    "oAria": {
                                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                    }
                                },
                            });

                            /* Seteando para el modulo c */
                            var registro_capitulo_iv_cabecera_modulo_c = "";
                            $('#lista-cap4-modc-viveros').DataTable().destroy();
                            $.each(capitulo_iv_cabecera_modulo_c, function( index, value ) {
                                registro_capitulo_iv_cabecera_modulo_c = registro_capitulo_iv_cabecera_modulo_c + "<tr>";
                                    registro_capitulo_iv_cabecera_modulo_c = registro_capitulo_iv_cabecera_modulo_c + "<td>" + ((value.NUM_ORDEN_SEMILLERO)?value.NUM_ORDEN_SEMILLERO:"") + "</td>";
                                    registro_capitulo_iv_cabecera_modulo_c = registro_capitulo_iv_cabecera_modulo_c + "<td>" + ((value.NUM_CANT_PLANTONES)?value.NUM_CANT_PLANTONES:"") + "</td>";
                                    registro_capitulo_iv_cabecera_modulo_c = registro_capitulo_iv_cabecera_modulo_c + "<td>" + ((value.NUM_EQ_PLANTONES)?value.NUM_EQ_PLANTONES:"") + "</td>";

                                    registro_capitulo_iv_cabecera_modulo_c = registro_capitulo_iv_cabecera_modulo_c + "<td>";
                                        registro_capitulo_iv_cabecera_modulo_c = registro_capitulo_iv_cabecera_modulo_c + '<a href="<?= \Yii::$app->request->BaseUrl ?>/capituloiv/update-capitulo-iv-modulo-c?num_orden_semillero=' + value.NUM_ORDEN_SEMILLERO + '&id_encuesta=<?= $id_encuesta ?>&geocodigo_fdo=<?= $geocodigo_fdo ?>" target="_blank"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> ';
                                        registro_capitulo_iv_cabecera_modulo_c = registro_capitulo_iv_cabecera_modulo_c + '<button type="button" data-num_orden_semillero="' + value.NUM_ORDEN_SEMILLERO + '" data-id_encuesta="<?= $id_encuesta ?>" class="btn btn-danger btn-eliminar-capitulo-iv-modulo-c"><i class="fas fa-trash"></i></button>'
                                    
                                    registro_capitulo_iv_cabecera_modulo_c = registro_capitulo_iv_cabecera_modulo_c +"</td>";
                                registro_capitulo_iv_cabecera_modulo_c = registro_capitulo_iv_cabecera_modulo_c + "</tr>";
                            });
                            $('#lista-cap4-modc-viveros tbody').html(registro_capitulo_iv_cabecera_modulo_c);
                            $('#lista-cap4-modc-viveros').DataTable({
                                "paging": true,
                                "lengthChange": true,
                                "searching": false,
                                "ordering": false,
                                "info": true,
                                "autoWidth": false,
                                "pageLength" : 10,
                                "language": {
                                    "sProcessing":    "Procesando...",
                                    "sLengthMenu":    "Mostrar _MENU_ registros",
                                    "sZeroRecords":   "No se encontraron resultados",
                                    "sEmptyTable":    "Ningun dato disponible en esta lista",
                                    "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                    "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                    "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                    "sInfoPostFix":   "",
                                    "sSearch":        "Buscar:",
                                    "sUrl":           "",
                                    "sInfoThousands":  ",",
                                    "sLoadingRecords": "Cargando...",
                                    "oPaginate": {
                                        "sFirst":    "Primero",
                                        "sLast":    "Último",
                                        "sNext":    "Siguiente",
                                        "sPrevious": "Anterior"
                                    },
                                    "oAria": {
                                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                    }
                                },
                            });

                            /* Seteando valor de cabecera */
                            $("#cap4_modd_p439").val(capitulo_iv_cabecera_modulo_d.ID_CONDICION_PASTOS_NAT);

                            /* Seteando valores de detalle p437 */
                            $.each(capitulo_iv_modulo_d_p437, function( index, value ) {
                                if(value.ID_PASTOS_NATURALES){
                                    $("#cap4_modd_p437_"+value.ID_PASTOS_NATURALES).prop("checked", true);
                                }else{
                                    $("#cap4_modd_p437_"+value.ID_PASTOS_NATURALES).prop("checked", false);
                                }

                                if(value.ID_PASTOS_NATURALES=="8"){
                                    $("#cap4_modd_p437_8_especifique").val(value.TXT_OTRO);
                                    $(".cap4_modd_p437_8").show()
                                }
                            });

                            /* Seteando valores de detalle p438 */
                            $.each(capitulo_iv_modulo_d_p438, function( index, value ) {
                                if(value.ID_LABORES_CULTURALES){
                                    $("#cap4_modd_p438_"+value.ID_LABORES_CULTURALES).prop("checked", true);
                                }else{
                                    $("#cap4_modd_p438_"+value.ID_LABORES_CULTURALES).prop("checked", false);
                                }

                                if(value.ID_LABORES_CULTURALES=="10"){
                                    $("#cap4_modd_p438_10_especifique").val(value.TXT_OTRO);
                                    $(".cap4_modd_p438_10").show()
                                }
                            });

                            /* Seteando valor de cabecera */
                            $("#cap4_mode_p442").val(capitulo_iv_cabecera_modulo_e.TIP_EQUIPO_PROTECCION);
                            $("#cap4_mode_p445").val(capitulo_iv_cabecera_modulo_e.TIP_EVALUA_PLAGAS);
                            $("#cap4_mode_p446").val(capitulo_iv_cabecera_modulo_e.TIP_CONTROL_BIOLOGICO);
                            $("#cap4_mode_observacion").val(capitulo_iv_cabecera_modulo_e.TXT_OBSERVACIONES);

                            $.each(capitulo_iv_modulo_e_p440, function( index, value ) {
                                $("#cap4_mode_p440_"+value.ID_PRACTICA_AGRICOLA).val(value.TIP_PRACTICA);
                                $("#cap4_mode_p441_"+value.ID_PRACTICA_AGRICOLA).val(value.NUM_ANIOS);
                            });


                            $.each(capitulo_iv_modulo_e_p443, function( index, value ) {
                                if(value.ID_ESTADO_ENVASE){
                                    $("#cap4_mode_p443_"+value.ID_ESTADO_ENVASE).prop("checked", true);
                                }else{
                                    $("#cap4_mode_p443_"+value.ID_ESTADO_ENVASE).prop("checked", false);
                                }

                                if(value.ID_ESTADO_ENVASE=="7"){
                                    $("#cap4_mode_p443_7_especifique").val(value.TXT_OTRO);
                                    $(".cap4_mode_p443_7").show()
                                }
                            });

                            $.each(capitulo_iv_modulo_e_p444, function( index, value ) {
                                if(value.ID_TIPO_CONTROL_PLAGA){
                                    $("#cap4_mode_p444_"+value.ID_TIPO_CONTROL_PLAGA).prop("checked", true);
                                }else{
                                    $("#cap4_mode_p444_"+value.ID_TIPO_CONTROL_PLAGA).prop("checked", false);
                                }

                                if(value.ID_TIPO_CONTROL_PLAGA=="8"){
                                    $("#cap4_mode_p444_8_especifique").val(value.TXT_OTRO);
                                    $(".cap4_mode_p444_8").show()
                                }
                            });

                        }

                        if(results.encuesta.capitulo_v){
                            let capitulo_v_cabecera_modulo_a = results.encuesta.capitulo_v.cabecera_modulo_a;
                            let capitulo_v_cabecera_modulo_b = results.encuesta.capitulo_v.cabecera_modulo_b;
                            let capitulo_v_cabecera_modulo_h = results.encuesta.capitulo_v.cabecera_modulo_h;
                            let capitulo_v_cabecera_modulo_i = results.encuesta.capitulo_v.cabecera_modulo_i;
                            let capitulo_v_cabecera_modulo_j = results.encuesta.capitulo_v.cabecera_modulo_j;
                            let capitulo_v_cabecera_modulo_k = results.encuesta.capitulo_v.cabecera_modulo_k;

                            let capitulo_v_p502 = results.encuesta.capitulo_v.detalle_p502;
                            let capitulo_v_p509 = results.encuesta.capitulo_v.detalle_p509;
                            let capitulo_v_p516 = results.encuesta.capitulo_v.detalle_p516;
                            let capitulo_v_p524 = results.encuesta.capitulo_v.detalle_p524;

                            let capitulo_v_p530 = results.encuesta.capitulo_v.detalle_p530;
                            let capitulo_v_p533 = results.encuesta.capitulo_v.detalle_p533;
                            let capitulo_v_p534 = results.encuesta.capitulo_v.detalle_p534;
                            let capitulo_v_p535 = results.encuesta.capitulo_v.detalle_p535;
                            let capitulo_v_p536 = results.encuesta.capitulo_v.detalle_p536;
                            let capitulo_v_p539 = results.encuesta.capitulo_v.detalle_p539;
                            let capitulo_v_p541 = results.encuesta.capitulo_v.detalle_p541;

                            $("#cap5_modb_p502_flag").val(capitulo_v_cabecera_modulo_b.FLG_VACUNO);
                            $("#cap5_modc_p502_flag").val(capitulo_v_cabecera_modulo_b.FLG_PORCINO);
                            $("#cap5_modd_p502_flag").val(capitulo_v_cabecera_modulo_b.FLG_CUY);
                            $("#cap5_mode_p502_a_flag").val(capitulo_v_cabecera_modulo_b.FLG_OVINO);
                            $("#cap5_mode_p502_b_flag").val(capitulo_v_cabecera_modulo_b.FLG_CAPRINO);
                            $("#cap5_modf_p502_a_flag").val(capitulo_v_cabecera_modulo_b.FLG_ALPACA);
                            $("#cap5_modf_p502_b_flag").val(capitulo_v_cabecera_modulo_b.FLG_LLAMA);
                            $("#cap5_modf_p502_c_flag").val(capitulo_v_cabecera_modulo_b.FLG_VICUNIA);

                            $.each(capitulo_v_cabecera_modulo_a, function( index, value ) {
                                if(value.ID_ESPECIE){
                                    $("#cap5_moda_p501_b_"+value.ID_ESPECIE).prop("checked", true);
                                }else{
                                    $("#cap5_moda_p501_b_"+value.ID_ESPECIE).prop("checked", false);
                                }
                                $("#cap5_moda_p501_b_"+value.ID_ESPECIE+"_hembra").val(value.NUM_HEMBRA);
                                $("#cap5_moda_p501_b_"+value.ID_ESPECIE+"_macho").val(value.NUM_MACHO);

                                if(value.ID_ESPECIE=="27"){
                                    $("#cap5_moda_p501_b_27_especifique").val(value.TXT_OTRO);
                                    $(".cap5_moda_p501_b_27").show()
                                }

                            });
                            /* modulo h */
                                
                            $("#cap5_modh_p502_flag").val(capitulo_v_cabecera_modulo_h.TIP_PRODUCTOS_PECUARIOS);
                            /* modulo i */

                            if(capitulo_v_cabecera_modulo_i.FLG_PROCEDENCIA_COMERCIAL=="1"){
                                $("#cap5_modi_p531_1").prop("checked", true);
                            }
                            if(capitulo_v_cabecera_modulo_i.FLG_PROCEDENCIA_PROPIA=="1"){
                                $("#cap5_modi_p531_2").prop("checked", true);
                            }

                            if(capitulo_v_cabecera_modulo_i.FLG_ALIMENTO_PELETIZADO=="1"){
                                $("#cap5_modi_p532_1").prop("checked", true);
                            }

                            if(capitulo_v_cabecera_modulo_i.FLG_ALIMENTO_HARINA=="1"){
                                $("#cap5_modi_p532_2").prop("checked", true);
                            }

                            if(capitulo_v_cabecera_modulo_i.FLG_ALIMENTO_OTRO=="1"){
                                $("#cap5_modi_p532_3").prop("checked", true);
                            }

                            /* Seteando valores de detalle p502 */
                            $.each(capitulo_v_p502, function( index, value ) {
                                if(value.ID_ESPECIE_CATEGORIA){
                                    $("#cap5_modb_p502_"+value.ID_ESPECIE_CATEGORIA).prop("checked", true);
                                }
                            });

                            /* Seteando valores de detalle p509 */
                            $.each(capitulo_v_p509, function( index, value ) {
                                if(value.ID_AVICOLA){
                                    $("#cap5_modg_p509_"+value.ID_AVICOLA).prop("checked", true);
                                }
                            });

                            /* Seteando valores de detalle p516 */
                            $.each(capitulo_v_p516, function( index, value ) {
                                if(value.ID_AVICOLA){
                                    $("#cap5_modg_p516_"+value.ID_AVICOLA).prop("checked", true);
                                }
                            });

                            /* Seteando valores de detalle p524 */
                            $.each(capitulo_v_p524, function( index, value ) {
                                if(value.ID_PRODUCTO){
                                    $("#cap5_modh_p524_"+value.ID_PRODUCTO).prop("checked", true);
                                }
                            });
                            


                            /* Seteando valores de detalle p530 */
                            $.each(capitulo_v_p530, function( index, value ) {
                                if(value.ID_TIPO_ALIMENTACION){
                                    $("#cap5_modi_p530_"+value.ID_TIPO_ALIMENTACION).prop("checked", true);
                                }else{
                                    $("#cap5_modi_p530_"+value.ID_TIPO_ALIMENTACION).prop("checked", false);
                                }

                                if(value.ID_TIPO_ALIMENTACION=="9"){
                                    $("#cap5_modi_p530_9_especifique").val(value.TXT_OTRO);
                                    $(".cap5_modi_p530_9").show()
                                }
                            });

                            /* Seteando valores de detalle p533 */
                            $.each(capitulo_v_p533, function( index, value ) {
                                if(value.ID_INSUMO){
                                    $("#cap5_modi_p533_"+value.ID_INSUMO).prop("checked", true);
                                }else{
                                    $("#cap5_modi_p533_"+value.ID_INSUMO).prop("checked", true);
                                }

                                if(value.ID_INSUMO=="24"){
                                    $("#cap5_modi_p533_24_especifique").val(value.TXT_OTRO);
                                    $(".cap5_modi_p533_24").show()
                                }
                            });

                            /* modulo j */
                            $("#cap5_modj_p534_a").val(capitulo_v_cabecera_modulo_j.FLG_APLICO_VACUNA);
                            $("#cap5_modj_p535").val(capitulo_v_cabecera_modulo_j.FLG_DESPARASITACION);

                            $.each(capitulo_v_p534, function( index, value ) {
                                if(value.ID_VACUNAS){
                                    $("#cap5_modj_p534_b_"+value.ID_VACUNAS).prop("checked", true);
                                }else{
                                    $("#cap5_modj_p534_b_"+value.ID_VACUNAS).prop("checked", true);
                                }

                                if(value.ID_VACUNAS=="5"){
                                    $("#cap5_modj_p534_b_5_especifique").val(value.TXT_OTRO);
                                    $(".cap5_modj_p534_b_5").show()
                                }

                                if(value.ID_VACUNAS=="11"){
                                    $("#cap5_modj_p534_b_11_especifique").val(value.TXT_OTRO);
                                    $(".cap5_modj_p534_b_11").show()
                                }

                                if(value.ID_VACUNAS=="18"){
                                    $("#cap5_modj_p534_b_18_especifique").val(value.TXT_OTRO);
                                    $(".cap5_modj_p534_b_18").show()
                                }

                                if(value.ID_VACUNAS=="27"){
                                    $("#cap5_modj_p534_b_27_especifique").val(value.TXT_OTRO);
                                    $(".cap5_modj_p534_b_27").show()
                                }

                                if(value.ID_VACUNAS=="37"){
                                    $("#cap5_modj_p534_b_37_especifique").val(value.TXT_OTRO);
                                    $(".cap5_modj_p534_b_37").show()
                                }
                            });

                            $.each(capitulo_v_p535, function( index, value ) {
                                if(value.ID_PARASITO){
                                    $("#cap5_modj_p535_a_"+value.ID_PARASITO).prop("checked", true);
                                }else{
                                    $("#cap5_modj_p535_a_"+value.ID_PARASITO).prop("checked", true);
                                }
                            });

                            /* modulo k */
                            $("#cap5_modk_p536").val(capitulo_v_cabecera_modulo_k.FLG_MEJORAMIENTO_GENETICO);

                            if(capitulo_v_cabecera_modulo_k.FLG_REPRODUCTORES_RAZA=="1"){
                                $("#cap5_modk_p537_1").prop("checked", true);
                            }

                            if(capitulo_v_cabecera_modulo_k.FLG_REPRODUCTORES_MEJORADO=="1"){
                                $("#cap5_modk_p537_2").prop("checked", true);
                            }

                            if(capitulo_v_cabecera_modulo_k.FLG_REPRODUCTORES_CRIOLLO=="1"){
                                $("#cap5_modk_p537_3").prop("checked", true);
                            }

                            $("#cap5_modk_p538").val(capitulo_v_cabecera_modulo_k.FLG_EMBRION_CERTIFICADO);
                            $("#cap5_modk_p540").val(capitulo_v_cabecera_modulo_k.FLG_AFILIADO_SIST_PROD);
                            $("#cap5_observacion").val(capitulo_v_cabecera_modulo_k.TXT_OBSERVACIONES);

                            /* Seteando valores de detalle p536 */
                            $.each(capitulo_v_p536, function( index, value ) {
                                if(value.ID_MEJORAMIENTO_GENETICO){
                                    $("#cap5_modk_p536_a_"+value.ID_MEJORAMIENTO_GENETICO).prop("checked", true);
                                }else{
                                    $("#cap5_modk_p536_a_"+value.ID_MEJORAMIENTO_GENETICO).prop("checked", true);
                                }

                                if(value.ID_MEJORAMIENTO_GENETICO=="9"){
                                    $("#cap5_modk_p536_a_9_especifique").val(value.TXT_OTRO);
                                    $(".cap5_modk_p536_a_9").show()
                                }
                            });

                            /* Seteando valores de detalle p539 */
                            $.each(capitulo_v_p539, function( index, value ) {
                                if(value.ID_INFO_GENETICO){
                                    $("#cap5_modk_p539_"+value.ID_INFO_GENETICO).prop("checked", true);
                                }else{
                                    $("#cap5_modk_p539_"+value.ID_INFO_GENETICO).prop("checked", true);
                                }
                            });

                            /* Seteando valores de detalle p541 */
                            $.each(capitulo_v_p541, function( index, value ) {
                                if(value.ID_RAZA){
                                    $("#cap5_modk_p541_a_"+value.ID_RAZA).prop("checked", true);
                                }else{
                                    $("#cap5_modk_p541_a_"+value.ID_RAZA).prop("checked", true);
                                }

                                if(value.ID_RAZA=="8"){
                                    $("#cap5_modk_p541_a_8_especifique").val(value.TXT_OTRO);
                                    $(".cap5_modk_p541_a_8").show()
                                }

                                if(value.ID_RAZA=="15"){
                                    $("#cap5_modk_p541_a_15_especifique").val(value.TXT_OTRO);
                                    $(".cap5_modk_p541_a_15").show()
                                }

                                if(value.ID_RAZA=="24"){
                                    $("#cap5_modk_p541_a_24_especifique").val(value.TXT_OTRO);
                                    $(".cap5_modk_p541_a_24").show()
                                }

                                if(value.ID_RAZA=="30"){
                                    $("#cap5_modk_p541_a_30_especifique").val(value.TXT_OTRO);
                                    $(".cap5_modk_p541_a_30").show()
                                }

                                if(value.ID_RAZA=="39"){
                                    $("#cap5_modk_p541_a_39_especifique").val(value.TXT_OTRO);
                                    $(".cap5_modk_p541_a_39").show()
                                }
                            });

                        }

                        if(results.encuesta.capitulo_vi){
                            let capitulo_vi_cabecera = results.encuesta.capitulo_vi.cabecera;
                            let capitulo_vi_p601 = results.encuesta.capitulo_vi.detalle_p601;
                            let capitulo_vi_p602 = results.encuesta.capitulo_vi.detalle_p602;
                            let capitulo_vi_p605 = results.encuesta.capitulo_vi.detalle_p605;
                            let capitulo_vi_p606 = results.encuesta.capitulo_vi.detalle_p606;
                            let capitulo_vi_p610 = results.encuesta.capitulo_vi.detalle_p610;

                            /* Seteando valor de cabecera */
                            $("#cap6_p603").val(capitulo_vi_cabecera.TIP_MINAS_EXPLOTACION);
                            $("#cap6_p604").val(capitulo_vi_cabecera.TIP_INFO_CONTAMINACION);
                            $("#cap6_p607").val(capitulo_vi_cabecera.TIP_PRODUCTOS_IDENTIFICADOS);
                            $("#cap6_p608").val(capitulo_vi_cabecera.TIP_TIPO_CERTIFICACION);
                            if(capitulo_vi_cabecera.ID_CERTIFICACION_OTORGADA){
                                $("#cap6_p609").val(capitulo_vi_cabecera.ID_CERTIFICACION_OTORGADA);
                            }
                            


                            /* Seteando valores de detalle p601 */
                            $.each(capitulo_vi_p601, function( index, value ) {
                                if(value.ID_RESIDUO){
                                    $("#cap6_p601_"+value.ID_RESIDUO).prop("checked", true);
                                }else{
                                    $("#cap6_p601_"+value.ID_RESIDUO).prop("checked", true);
                                }

                                if(value.ID_RESIDUO=="7"){
                                    $("#cap6_p601_7_especifique").val(value.TXT_OTRO);
                                    $(".cap6_p601_7").show()
                                }
                            });

                            /* Seteando valores de detalle p602 */
                            $.each(capitulo_vi_p602, function( index, value ) {
                                if(value.ID_RESIDUO){
                                    $("#cap6_p602_"+value.ID_RESIDUO).prop("checked", true);
                                }else{
                                    $("#cap6_p602_"+value.ID_RESIDUO).prop("checked", true);
                                }

                                if(value.ID_RESIDUO=="7"){
                                    $("#cap6_p602_7_especifique").val(value.TXT_OTRO);
                                    $(".cap6_p602_7").show()
                                }
                            });

                            
                            /* Seteando valores de detalle p605 */
                            $.each(capitulo_vi_p605, function( index, value ) {
                                if(value.ID_ENTIDAD_ESTADO){
                                    $("#cap6_p605_"+value.ID_ENTIDAD_ESTADO).prop("checked", true);
                                }else{
                                    $("#cap6_p605_"+value.ID_ENTIDAD_ESTADO).prop("checked", true);
                                }

                                if(value.ID_ENTIDAD_ESTADO=="6"){
                                    $("#cap6_p605_6_especifique").val(value.TXT_OTRO);
                                    $(".cap6_p605_6").show()
                                }
                            });

                            /* Seteando valores de detalle p606 */
                            $.each(capitulo_vi_p606, function( index, value ) {
                                if(value.ID_CONSERVA_ALIMENTOS){
                                    $("#cap6_p606_"+value.ID_CONSERVA_ALIMENTOS).prop("checked", true);
                                }else{
                                    $("#cap6_p606_"+value.ID_CONSERVA_ALIMENTOS).prop("checked", true);
                                }

                                if(value.ID_CONSERVA_ALIMENTOS=="5"){
                                    $("#cap6_p606_5_especifique").val(value.TXT_OTRO);
                                    $(".cap6_p606_5").show()
                                }
                            });

                            /* Seteando valores de detalle p610 */
                            $.each(capitulo_vi_p610, function( index, value ) {
                                if(value.ID_CERTIFICADO){
                                    $("#cap6_p610_"+value.ID_CERTIFICADO).prop("checked", true);
                                }else{
                                    $("#cap6_p610_"+value.ID_CERTIFICADO).prop("checked", true);
                                }

                                if(value.ID_CERTIFICADO=="7"){
                                    $("#cap6_p610_7_especifique").val(value.TXT_OTRO);
                                    $(".cap6_p610_7").show()
                                }
                            });

                            


                        }

                        if(results.encuesta.capitulo_vii){
                            let capitulo_vii_cabecera_modulo_c = results.encuesta.capitulo_vii.cabecera_modulo_c;
                            let capitulo_vii_cabecera_modulo_e = results.encuesta.capitulo_vii.cabecera_modulo_e;
                            let capitulo_vii_modulo_c_asociaciones = results.encuesta.capitulo_vii.detalle_asociaciones;
                            
                            let capitulo_vii_modulo_c_p714 = results.encuesta.capitulo_vii.detalle_p714;
                            let capitulo_vii_modulo_c_p715 = results.encuesta.capitulo_vii.detalle_p715;
                            let capitulo_vii_modulo_c_p716 = results.encuesta.capitulo_vii.detalle_p716;
                            let capitulo_vii_modulo_c_p717 = results.encuesta.capitulo_vii.detalle_p717;
                            //let capitulo_vii_modulo_c_p718 = results.encuesta.capitulo_vii.detalle_p718;
                            let capitulo_vii_modulo_c_p719 = results.encuesta.capitulo_vii.detalle_p719;
                            let capitulo_vii_modulo_e_p726 = results.encuesta.capitulo_vii.detalle_p726;
                            let capitulo_vii_modulo_e_p727 = results.encuesta.capitulo_vii.detalle_p727;
                            let capitulo_vii_modulo_e_p729 = results.encuesta.capitulo_vii.detalle_p729;
                            let capitulo_vii_modulo_e_p731 = results.encuesta.capitulo_vii.detalle_p731;

                            /* Seteando valor de cabecera */
                            $("#cap7_modc_p710_a").val(capitulo_vii_cabecera_modulo_c.TIP_PERT_ASO_COOP_COM);
                            $("#cap7_modc_p710_b").val(capitulo_vii_cabecera_modulo_c.NUM_CUANTAS_PERT);
                            $("#cap7_mode_p724").val(capitulo_vii_cabecera_modulo_e.TIP_SOLICITO_CREDITO);
                            $("#cap7_mode_p725").val(capitulo_vii_cabecera_modulo_e.TIP_OBTUVO_CREDITO);
                            $("#cap7_mode_p728").val(capitulo_vii_cabecera_modulo_e.TIP_FUE_BENEFICIARIO);
                            $("#cap7_mode_p730").val(capitulo_vii_cabecera_modulo_e.TIP_TIENE_CTA_AHORRO);

                            

                            /* Seteando para el modulo a */
                            var lista_capitulo_vii_modulo_c_asociaciones = "";
                            $('#lista-cap7-modc-asociaciones').DataTable().destroy();
                            $.each(capitulo_vii_modulo_c_asociaciones, function( index, value ) {
                                lista_capitulo_vii_modulo_c_asociaciones = lista_capitulo_vii_modulo_c_asociaciones + "<tr>";
                                    lista_capitulo_vii_modulo_c_asociaciones = lista_capitulo_vii_modulo_c_asociaciones + "<td>" + ((value.TXT_NOMBRE)?value.TXT_NOMBRE:"") + "</td>";
                                    lista_capitulo_vii_modulo_c_asociaciones = lista_capitulo_vii_modulo_c_asociaciones + "<td>" + ((value.TXT_TIPO)?value.TXT_TIPO:"") + "</td>";
                                    lista_capitulo_vii_modulo_c_asociaciones = lista_capitulo_vii_modulo_c_asociaciones + "<td>" + ((value.TXT_ANIO)?value.TXT_ANIO:"") + "</td>";

                                    lista_capitulo_vii_modulo_c_asociaciones = lista_capitulo_vii_modulo_c_asociaciones + "<td>";
                                        lista_capitulo_vii_modulo_c_asociaciones = lista_capitulo_vii_modulo_c_asociaciones + '<a href="<?= \Yii::$app->request->BaseUrl ?>/capitulovii/update-capitulo-vii-modulo-c-asociaciones?id_cap_vii_c_asociacion=' + value.ID_ASO_COOP_COM + '&geocodigo_fdo=<?= $geocodigo_fdo ?>" target="_blank" class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> ';
                                        lista_capitulo_vii_modulo_c_asociaciones = lista_capitulo_vii_modulo_c_asociaciones + '<button type="button" data-id_cap_vii_c_asociacion="' + value.ID_ASO_COOP_COM + '" class="btn btn-danger btn-eliminar-capvii-modulo-c-asociaciones"><i class="fas fa-trash"></i></button>'
                                    
                                    lista_capitulo_vii_modulo_c_asociaciones = lista_capitulo_vii_modulo_c_asociaciones +"</td>";
                                lista_capitulo_vii_modulo_c_asociaciones = lista_capitulo_vii_modulo_c_asociaciones + "</tr>";
                            });
                            $('#lista-cap7-modc-asociaciones tbody').html(lista_capitulo_vii_modulo_c_asociaciones);
                            $('#lista-cap7-modc-asociaciones').DataTable({
                                "paging": true,
                                "lengthChange": true,
                                "searching": false,
                                "ordering": false,
                                "info": true,
                                "autoWidth": false,
                                "pageLength" : 10,
                                "language": {
                                    "sProcessing":    "Procesando...",
                                    "sLengthMenu":    "Mostrar _MENU_ registros",
                                    "sZeroRecords":   "No se encontraron resultados",
                                    "sEmptyTable":    "Ningun dato disponible en esta lista",
                                    "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                    "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                    "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                    "sInfoPostFix":   "",
                                    "sSearch":        "Buscar:",
                                    "sUrl":           "",
                                    "sInfoThousands":  ",",
                                    "sLoadingRecords": "Cargando...",
                                    "oPaginate": {
                                        "sFirst":    "Primero",
                                        "sLast":    "Último",
                                        "sNext":    "Siguiente",
                                        "sPrevious": "Anterior"
                                    },
                                    "oAria": {
                                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                    }
                                },
                            });


                            /* Seteando valores de detalle p714 */
                            $.each(capitulo_vii_modulo_c_p714, function( index, value ) {
                                if(value.ID_BENEFICIO){
                                    $("#cap7_modc_p714_"+value.ID_BENEFICIO).prop("checked", true);
                                }else{
                                    $("#cap7_modc_p714_"+value.ID_BENEFICIO).prop("checked", true);
                                }

                                if(value.ID_BENEFICIO=="6"){
                                    $("#cap7_modc_p714_6_especifique").val(value.TXT_OTRO);
                                    $(".cap7_modc_p714_6").show()
                                }
                            });

                            /* Seteando valores de detalle p715 */
                            $.each(capitulo_vii_modulo_c_p715, function( index, value ) {
                                if(value.ID_ACTIVIDADES){
                                    $("#cap7_modc_p715_"+value.ID_ACTIVIDADES).prop("checked", true);
                                }else{
                                    $("#cap7_modc_p715_"+value.ID_ACTIVIDADES).prop("checked", true);
                                }
                            });

                            /* Seteando valores de detalle p716 */
                            $.each(capitulo_vii_modulo_c_p716, function( index, value ) {
                                if(value.ID_LOGRO){
                                    $("#cap7_modc_p716_"+value.ID_LOGRO).prop("checked", true);
                                }else{
                                    $("#cap7_modc_p716_"+value.ID_LOGRO).prop("checked", true);
                                }

                                if(value.ID_LOGRO=="6"){
                                    $("#cap7_modc_p716_6_especifique").val(value.TXT_OTRO);
                                    $(".cap7_modc_p716_6").show()
                                }
                            });

                            /* Seteando valores de detalle p717 */
                            $.each(capitulo_vii_modulo_c_p717, function( index, value ) {
                                if(value.ID_EQUIPO){
                                    $("#cap7_modc_p717_"+value.ID_EQUIPO).prop("checked", true);
                                    $("#cap7_modc_p718_"+value.ID_EQUIPO).val(value.TIP_RESPUESTA);
                                    if(value.TIP_RESPUESTA=="4"){
                                        $("#cap7_modc_p718_" + value.ID_EQUIPO +"_especifique").val(value.TXT_OTRO_RESPUESTA);
                                        $(".cap7_modc_p718_" + value.ID_EQUIPO).show();

                                    }
                                }

                                if(value.ID_EQUIPO=="7"){
                                    $("#cap7_modc_p717_7_especifique").val(value.TXT_OTRO);
                                    $(".cap7_modc_p717_7").show()
                                }
                            });

                            /* Seteando valores de detalle p718 */
                            /*$.each(capitulo_vii_modulo_c_p718, function( index, value ) {
                                if(value.ID_EQUIPO){
                                    $("#cap7_modc_p718_"+value.ID_EQUIPO).prop("checked", true);
                                }else{
                                    $("#cap7_modc_p718_"+value.ID_EQUIPO).prop("checked", true);
                                }

                                if(value.ID_EQUIPO=="7"){
                                    $("#cap7_modc_p718_7_especifique").val(value.TXT_OTRO);
                                    $(".cap7_modc_p718_7").show()
                                }
                            });*/

                            /* Seteando valores de detalle p719 */
                            $.each(capitulo_vii_modulo_c_p719, function( index, value ) {
                                if(value.ID_MOTIVO_ASO){
                                    $("#cap7_modc_p719_"+value.ID_MOTIVO_ASO).prop("checked", true);
                                }else{
                                    $("#cap7_modc_p719_"+value.ID_MOTIVO_ASO).prop("checked", true);
                                }

                                if(value.ID_MOTIVO_ASO=="6"){
                                    $("#cap7_modc_p719_6_especifique").val(value.TXT_OTRO);
                                    $(".cap7_modc_p719_6").show()
                                }
                            });

                            /* Seteando valores de detalle p726 */
                            $.each(capitulo_vii_modulo_e_p726, function( index, value ) {
                                if(value.ID_ENTIDAD_CREDITO){
                                    $("#cap7_mode_p726_"+value.ID_ENTIDAD_CREDITO).prop("checked", true);
                                }else{
                                    $("#cap7_mode_p726_"+value.ID_ENTIDAD_CREDITO).prop("checked", true);
                                }

                                if(value.ID_ENTIDAD_CREDITO=="11"){
                                    $("#cap7_mode_p726_11_especifique").val(value.TXT_OTRO);
                                    $(".cap7_mode_p726_11").show()
                                }
                            });

                            /* Seteando valores de detalle p727 */
                            $.each(capitulo_vii_modulo_e_p727, function( index, value ) {
                                if(value.ID_UTILIDAD_PRESTAMO){
                                    $("#cap7_mode_p727_"+value.ID_UTILIDAD_PRESTAMO).prop("checked", true);
                                }else{
                                    $("#cap7_mode_p727_"+value.ID_UTILIDAD_PRESTAMO).prop("checked", true);
                                }

                                if(value.ID_UTILIDAD_PRESTAMO=="9"){
                                    $("#cap7_mode_p727_9_especifique").val(value.TXT_OTRO);
                                    $(".cap7_mode_p727_9").show()
                                }
                            });

                            /* Seteando valores de detalle p729 */
                            $.each(capitulo_vii_modulo_e_p729, function( index, value ) {
                                if(value.ID_ENTIDAD){
                                    $("#cap7_mode_p729_"+value.ID_ENTIDAD).prop("checked", true);
                                }else{
                                    $("#cap7_mode_p729_"+value.ID_ENTIDAD).prop("checked", true);
                                }

                                if(value.ID_ENTIDAD=="5"){
                                    $("#cap7_mode_p729_5_especifique").val(value.TXT_OTRO);
                                    $(".cap7_mode_p729_5").show()
                                }
                            });

                            /* Seteando valores de detalle p731 */
                            $.each(capitulo_vii_modulo_e_p731, function( index, value ) {
                                if(value.ID_ENTIDAD_BANCARIA){
                                    $("#cap7_mode_p731_"+value.ID_ENTIDAD_BANCARIA).prop("checked", true);
                                }else{
                                    $("#cap7_mode_p731_"+value.ID_ENTIDAD_BANCARIA).prop("checked", true);
                                }

                                if(value.ID_ENTIDAD_BANCARIA=="6"){
                                    $("#cap7_mode_p731_6_especifique").val(value.TXT_OTRO);
                                    $(".cap7_mode_p731_6").show()
                                }
                            });


                        }

                        if(results.encuesta.capitulo_viii){
                            let capitulo_viii_cabecera = results.encuesta.capitulo_viii.cabecera;
                            let capitulo_viii_p801 = results.encuesta.capitulo_viii.detalle_p801;
                            let capitulo_viii_p802 = results.encuesta.capitulo_viii.detalle_p802;
                            let capitulo_viii_p803 = results.encuesta.capitulo_viii.detalle_p803;

                            /* Seteando valor de cabecera */
                            $("#cap8_observacion").val(capitulo_viii_cabecera.TXT_OBSERVACION);

                            /* Seteando valores de detalle p801 */
                            $.each(capitulo_viii_p801, function( index, value ) {
                                if(value.ID_FUENTE_ABAST){
                                    $("#cap8_p801_"+value.ID_FUENTE_ABAST).prop("checked", true);
                                }else{
                                    $("#cap8_p801_"+value.ID_FUENTE_ABAST).prop("checked", true);
                                }

                                if(value.ID_FUENTE_ABAST=="8"){
                                    $("#cap8_p801_8_especifique").val(value.TXT_OTRO);
                                    $(".cap8_p801_8").show()
                                }

                            });




                            /* Seteando valores de detalle p802 */
                            $.each(capitulo_viii_p802, function( index, value ) {
                                if(value.ID_CLASE_AGUA){
                                    $("#cap8_p802_"+value.ID_CLASE_AGUA).prop("checked", true);
                                }else{
                                    $("#cap8_p802_"+value.ID_CLASE_AGUA).prop("checked", true);
                                }

                                if(value.ID_CLASE_AGUA=="6"){
                                    $("#cap8_p802_6_especifique").val(value.TXT_OTRO);
                                    $(".cap8_p802_6").show()
                                }
                            });

                            /* Seteando valores de detalle p803 */
                            $.each(capitulo_viii_p803, function( index, value ) {
                                if(value.ID_ENERGIA){
                                    $("#cap8_p803_"+value.ID_ENERGIA).prop("checked", true);
                                }else{
                                    $("#cap8_p803_"+value.ID_ENERGIA).prop("checked", true);
                                }

                                if(value.ID_ENERGIA=="9"){
                                    $("#cap8_p803_9_especifique").val(value.TXT_OTRO);
                                    $(".cap8_p803_9").show()
                                }
                            });


                        }

                        if(results.encuesta.capitulo_ix){
                            let capitulo_ix_cabecera = results.encuesta.capitulo_ix.cabecera;
                            let capitulo_ix_maquinarias = results.encuesta.capitulo_ix.detalle;
                            /* Seteando valor de cabecera */
                            $("#cap9_p901").val(capitulo_ix_cabecera.TIP_USO_MAQUINARIA);

                            /* Seteando para la lista de maquinas */
                            var lista_capitulo_ix_maquinarias = "";
                            $('#lista-cap9-maquinaria').DataTable().destroy();
                            $.each(capitulo_ix_maquinarias, function( index, value ) {
                                lista_capitulo_ix_maquinarias = lista_capitulo_ix_maquinarias + "<tr>";
                                    lista_capitulo_ix_maquinarias = lista_capitulo_ix_maquinarias + "<td>" + ((value.MAQ_EQUIP)?value.MAQ_EQUIP:"") + "</td>";
                                    lista_capitulo_ix_maquinarias = lista_capitulo_ix_maquinarias + "<td>" + ((value.NUM_POTENCIA)?value.NUM_POTENCIA:"") + "</td>";
                                    lista_capitulo_ix_maquinarias = lista_capitulo_ix_maquinarias + "<td>" + ((value.MAQUINARIA_EQUIPO_ES)?value.MAQUINARIA_EQUIPO_ES:"") + "</td>";

                                    lista_capitulo_ix_maquinarias = lista_capitulo_ix_maquinarias + "<td>";
                                        lista_capitulo_ix_maquinarias = lista_capitulo_ix_maquinarias + '<a href="<?= \Yii::$app->request->BaseUrl ?>/capituloix/update-capitulo-ix-maquinarias?id_maquinaria_equipo=' + value.ID_MAQUINARIA_EQUIPO + '&geocodigo_fdo=<?= $geocodigo_fdo ?>" target="_blank"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> ';
                                        lista_capitulo_ix_maquinarias = lista_capitulo_ix_maquinarias + '<button type="button" data-id_maquinaria_equipo="' + value.ID_MAQUINARIA_EQUIPO + '" class="btn btn-danger btn-eliminar-capix-maquinaria_equipo"><i class="fas fa-trash"></i></button>'
                                    
                                    lista_capitulo_ix_maquinarias = lista_capitulo_ix_maquinarias +"</td>";
                                lista_capitulo_ix_maquinarias = lista_capitulo_ix_maquinarias + "</tr>";
                            });
                            $('#lista-cap9-maquinaria tbody').html(lista_capitulo_ix_maquinarias);
                            $('#lista-cap9-maquinaria').DataTable({
                                "paging": true,
                                "lengthChange": true,
                                "searching": false,
                                "ordering": false,
                                "info": true,
                                "autoWidth": false,
                                "pageLength" : 10,
                                "language": {
                                    "sProcessing":    "Procesando...",
                                    "sLengthMenu":    "Mostrar _MENU_ registros",
                                    "sZeroRecords":   "No se encontraron resultados",
                                    "sEmptyTable":    "Ningun dato disponible en esta lista",
                                    "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                    "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                    "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                    "sInfoPostFix":   "",
                                    "sSearch":        "Buscar:",
                                    "sUrl":           "",
                                    "sInfoThousands":  ",",
                                    "sLoadingRecords": "Cargando...",
                                    "oPaginate": {
                                        "sFirst":    "Primero",
                                        "sLast":    "Último",
                                        "sNext":    "Siguiente",
                                        "sPrevious": "Anterior"
                                    },
                                    "oAria": {
                                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                    }
                                },
                            });

                        }

                        if(results.encuesta.capitulo_x){
                            let capitulo_x_infraestructuras = results.encuesta.capitulo_x.detalle;
                           
                            /* Seteando para la lista de maquinas */
                            var lista_capitulo_x_infraestructuras = "";
                            $('#lista-cap10-infraestructuras').DataTable().destroy();
                            $.each(capitulo_x_infraestructuras, function( index, value ) {
                                lista_capitulo_x_infraestructuras = lista_capitulo_x_infraestructuras + "<tr>";
                                    lista_capitulo_x_infraestructuras = lista_capitulo_x_infraestructuras + "<td>" + ((value.TIPO_INFRAESTRUCTURA)?value.TIPO_INFRAESTRUCTURA:"") + "</td>";
                                    lista_capitulo_x_infraestructuras = lista_capitulo_x_infraestructuras + "<td>" + ((value.NUM_AREA_INFRAESTRUCTURA)?value.NUM_AREA_INFRAESTRUCTURA:"") + "</td>";
                                    lista_capitulo_x_infraestructuras = lista_capitulo_x_infraestructuras + "<td>" + ((value.ESTADO)?value.ESTADO:"") + "</td>";

                                    lista_capitulo_x_infraestructuras = lista_capitulo_x_infraestructuras + "<td>";
                                        lista_capitulo_x_infraestructuras = lista_capitulo_x_infraestructuras + '<a href="<?= \Yii::$app->request->BaseUrl ?>/capitulox/update-capitulo-x-infraestructuras?id_cap_x=' + value.ID_CAP_X + '&geocodigo_fdo=<?= $geocodigo_fdo ?>" target="_blank"  class="btn btn-primary" > <i class="fas fa-pen-square"></i> </a> ';
                                        lista_capitulo_x_infraestructuras = lista_capitulo_x_infraestructuras + '<button type="button" data-id_cap_x="' + value.ID_CAP_X + '" class="btn btn-danger btn-eliminar-cap_x"><i class="fas fa-trash"></i></button>'
                                    
                                    lista_capitulo_x_infraestructuras = lista_capitulo_x_infraestructuras +"</td>";
                                lista_capitulo_x_infraestructuras = lista_capitulo_x_infraestructuras + "</tr>";
                            });
                            $('#lista-cap10-infraestructuras tbody').html(lista_capitulo_x_infraestructuras);
                            $('#lista-cap10-infraestructuras').DataTable({
                                "paging": true,
                                "lengthChange": true,
                                "searching": false,
                                "ordering": false,
                                "info": true,
                                "autoWidth": false,
                                "pageLength" : 10,
                                "language": {
                                    "sProcessing":    "Procesando...",
                                    "sLengthMenu":    "Mostrar _MENU_ registros",
                                    "sZeroRecords":   "No se encontraron resultados",
                                    "sEmptyTable":    "Ningun dato disponible en esta lista",
                                    "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                    "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                    "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                    "sInfoPostFix":   "",
                                    "sSearch":        "Buscar:",
                                    "sUrl":           "",
                                    "sInfoThousands":  ",",
                                    "sLoadingRecords": "Cargando...",
                                    "oPaginate": {
                                        "sFirst":    "Primero",
                                        "sLast":    "Último",
                                        "sNext":    "Siguiente",
                                        "sPrevious": "Anterior"
                                    },
                                    "oAria": {
                                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                    }
                                },
                            });

                        }

                        if(results.encuesta.capitulo_xii){
                            let capitulo_xii_cabecera = results.encuesta.capitulo_xii.cabecera;
                            /* Seteando valor de cabecera */
                            //'2021-02-01'
                            $("#cap12_moda_p1206").val(capitulo_xii_cabecera.TXT_FECHA_FINAL_F);
                            $("#cap12_moda_p1207").val(capitulo_xii_cabecera.FLG_RESULTADO_FINAL);
                            $("#cap12_moda_p1207_6_especifique").val(capitulo_xii_cabecera.TXT_OTRO_RESULTADO_FINAL);
                            $("#cap12_observacion").val(capitulo_xii_cabecera.TXT_OBSERVACION);
                        }
                        

                        setTimeout(function(){ loading.modal("hide"); }, 2000);
                    }
                },
                error:function(){
                    alert("Error al realizar el proceso.");
                }
            });
}
/* Informacion General */

function ProvinciasGeneral(cod_departamento,cod_provincia){
    $.ajax({
        url: "<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-provincias",
        method: "POST",
        data:{_csrf:csrf,cod_dep:cod_departamento},
        dataType:"Json",
        beforeSend:function(xhr, settings)
        {
            loading.modal("show");
        },
        success:function(results)
        {   
            if(results && results.success){
                var options_provincias = "<option value>Seleccionar Provincia</option>";
                $.each(results.provincias, function( index, value ) {
                    options_provincias = options_provincias + `<option value='${value.COD_PROV}'>${value.TXT_PROVINCIA}</option>`;
                });
                $("#general_provincia").html(options_provincias);
                if(cod_provincia){
                    $("#general_provincia").val(cod_provincia);
                }
                
                setTimeout(function(){ loading.modal("hide"); }, 2000);
            }
        },
        error:function(){
            alert("Error al realizar el proceso.");
        }
    });
}

function DistritosGeneral(cod_provincia,cod_distrito){
    $.ajax({
        url: "<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-distritos",
        method: "POST",
        data:{_csrf:csrf,cod_prov:cod_provincia},
        dataType:"Json",
        beforeSend:function(xhr, settings)
        {
            loading.modal("show");
        },
        success:function(results)
        {   
            if(results && results.success){
                var options_distritos = "<option value>Seleccionar Distrito</option>";
                $.each(results.distritos, function( index, value ) {
                    options_distritos = options_distritos + `<option value='${value.COD_UBIGEO}'>${value.TXT_DISTRITO}</option>`;
                });
                
                $("#general_distrito").html(options_distritos);
                if(cod_distrito){
                    console.log(cod_distrito)
                    $("#general_distrito").val(cod_distrito);
                }
                
                setTimeout(function(){ loading.modal("hide"); }, 2000);
            }
        },
        error:function(){
            alert("Error al realizar el proceso.");
        }
    });
}

function CentrosPobladosGeneral(cod_ubigeo,centro_poblado){
    $.ajax({
        url: "<?= \Yii::$app->request->BaseUrl ?>/centro-poblado/get-centros-poblados",
        method: "POST",
        data:{_csrf:csrf,cod_ubigeo:cod_ubigeo},
        dataType:"Json",
        beforeSend:function(xhr, settings)
        {
            loading.modal("show");
        },
        success:function(results)
        {   
            if(results && results.success){
                var options_centros_poblados = "<option value='99999'>Seleccionar Centro poblado</option>";
                $.each(results.centros_poblados, function( index, value ) {
                    options_centros_poblados = options_centros_poblados + `<option value='${value.COD_CCPP}'>${value.TXT_CCPP}</option>`;
                });
                $("#general_centro_poblado").html(options_centros_poblados);
                if(centro_poblado){
                    $("#general_centro_poblado").val(centro_poblado);
                }
                
                setTimeout(function(){ loading.modal("hide"); }, 2000);
            }
        },
        error:function(){
            alert("Error al realizar el proceso.");
        }
    });
}


$("body").on("change", "#general_provincia", function (e) {
    e.preventDefault();
    $("#general_distrito").html("<option value>Seleccionar Distrito</option>");
    $("#general_centro_poblado").html("<option value>Seleccionar Centro poblado</option>");
    if($(this).val()){
        cod_provincia = $(this).val();
        DistritosGeneral(cod_provincia);
    }
    
});

$("body").on("change", "#general_distrito", function (e) {
    e.preventDefault();
    $("#general_centro_poblado").html("<option value>Seleccionar Centro poblado</option>");
    
    if($(this).val()){
        cod_ubigeo = $(this).val();
        CentrosPobladosGeneral(cod_ubigeo);
    }

});

/* ####################################################################################################################################Capitulo II */
/* Variables globales */

/* Carga Maestras */

async function Regiones(capitulo,modulo,valor,identificador){
    await   $.ajax({
                url: "<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-regiones",
                method: "POST",
                data:{_csrf:csrf},
                dataType:"Json",
                beforeSend:function(xhr, settings)
                {
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var options_departamentos = "<option value='99999'>Seleccionar Departamento</option>";
                        /* Seteando valores de detalle p01 */
                        $.each(results.regiones, function( index, value ) {
                            options_departamentos = options_departamentos + `<option value='${value.COD_DEP}'>${value.TXT_DEPARTAMENTO}</option>`;
                        });

                        if(capitulo=="2" && modulo=="a"){
                            $("#p02_a_informante_departamento").html(options_departamentos);
                        }

                        if(capitulo=="2" && modulo=="b"){
                            $("#p02_b_representante_departamento").html(options_departamentos);
                        }

                        if(valor){
                            $(identificador).val(valor);
                        }
                    }
                },
                error:function(){
                    alert("Error al realizar el proceso.");
                }
            });
}

function Provincias(capitulo,modulo,cod_dep,valor,identificador){
    $.ajax({
        url: "<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-provincias",
        method: "POST",
        data:{_csrf:csrf,cod_dep:cod_dep},
        dataType:"Json",
        beforeSend:function(xhr, settings)
        {
            loading.modal("show");
        },
        success:function(results)
        {   
            if(results && results.success){
                var options_provincias = "<option value='99999'>Seleccionar Provincia</option>";
                $.each(results.provincias, function( index, value ) {
                    options_provincias = options_provincias + `<option value='${value.COD_PROV}'>${value.TXT_PROVINCIA}</option>`;
                });
                if(capitulo=="2" && modulo=="a"){
                    $("#p02_a_informante_provincia").html(options_provincias);
                }
                if(capitulo=="2" && modulo=="b"){
                    $("#p02_b_representante_provincia").html(options_provincias);
                }

                if(valor){
                    $(identificador).val(valor);
                }
                
                setTimeout(function(){ loading.modal("hide"); }, 2000);
            }
        },
        error:function(){
            alert("Error al realizar el proceso.");
        }
    });
}

function Distritos(capitulo,modulo,cod_prov,valor,identificador){
    $.ajax({
        url: "<?= \Yii::$app->request->BaseUrl ?>/ubigeo/get-distritos",
        method: "POST",
        data:{_csrf:csrf,cod_prov:cod_prov},
        dataType:"Json",
        beforeSend:function(xhr, settings)
        {
            loading.modal("show");
        },
        success:function(results)
        {   
            if(results && results.success){
                var options_distritos = "<option value='99999'>Seleccionar Distrito</option>";
                $.each(results.distritos, function( index, value ) {
                    options_distritos = options_distritos + `<option value='${value.COD_UBIGEO}'>${value.TXT_DISTRITO}</option>`;
                });
                if(capitulo=="2" && modulo=="a"){
                    $("#p02_a_informante_distrito").html(options_distritos);
                }
                if(capitulo=="2" && modulo=="b"){
                    $("#p02_b_representante_distrito").html(options_distritos);
                }

                if(valor){
                    $(identificador).val(valor);
                }
                
                setTimeout(function(){ loading.modal("hide"); }, 2000);
            }
        },
        error:function(){
            alert("Error al realizar el proceso.");
        }
    });
}

function CentrosPoblados(capitulo,modulo,cod_ubigeo,valor,identificador){
    $.ajax({
        url: "<?= \Yii::$app->request->BaseUrl ?>/centro-poblado/get-centros-poblados",
        method: "POST",
        data:{_csrf:csrf,cod_ubigeo:cod_ubigeo},
        dataType:"Json",
        beforeSend:function(xhr, settings)
        {
            loading.modal("show");
        },
        success:function(results)
        {   
            if(results && results.success){
                var options_centros_poblados = "<option value='99999'>Seleccionar Centro poblado</option>";
                $.each(results.centros_poblados, function( index, value ) {
                    options_centros_poblados = options_centros_poblados + `<option value='${value.COD_CCPP}'>${value.TXT_CCPP}</option>`;
                });
                if(capitulo=="2" && modulo=="a"){
                    $("#p02_a_informante_centro_poblado").html(options_centros_poblados);
                }

                if(capitulo=="2" && modulo=="b"){
                    $("#p02_b_representante_centro_poblado").html(options_centros_poblados);
                }

                if(valor){
                    $(identificador).val(valor);
                }
                
                setTimeout(function(){ loading.modal("hide"); }, 2000);
            }
        },
        error:function(){
            alert("Error al realizar el proceso.");
        }
    });
}



/* Acciones */

$("body").on("change", "#p02_tipo_productor_1,#p02_tipo_productor_2", function (e) {
    e.preventDefault();
    $('.capitulo_ii_modulo_a').hide();
    $('.capitulo_ii_modulo_b').hide();

    if($(this).val()=="1"){
        $('.capitulo_ii_modulo_a').show();
        Regiones("2","a");
    }else if($(this).val()=="2"){
        $('.capitulo_ii_modulo_b').show();
        Regiones("2","b");
    }
});


/* modulo a */

$("body").on("change", "#p02_a_representante_tiene_numero", function (e) {
    e.preventDefault();
    $(".p02_a_representante_tiene_numero").hide();
    $("#p02_a_representante_telefono_fijo").val("");
    $("#p02_a_representante_telefono_celular").val("");
    if($(this).val()=="1"){
        $(".p02_a_representante_tiene_numero").show()
    }
});

$("body").on("change", "#p02_a_tiene_correo_electronico", function (e) {
    e.preventDefault();
    $(".p02_a_tiene_correo_electronico").hide();
    $("#p02_a_correo_electronico").val("");
    if($(this).val()=="1"){
        $(".p02_a_tiene_correo_electronico").show()
    }
});



$("body").on("change", "#p02_a_informacion_proporcionada_7,#p02_a_informacion_proporcionada_8,#p02_a_informacion_proporcionada_9,#p02_a_informacion_proporcionada_10,#p02_a_informacion_proporcionada_11,#p02_a_informacion_proporcionada_12", function (e) {
    e.preventDefault();
    $(".p02_a_informacion_proporcionada_12").hide();
    $("#p02_a_informacion_proporcionada_especifique").val("");
    if($(this).val()=="12"){
        $(".p02_a_informacion_proporcionada_12").show()
    }
});

$("body").on("change", "#p02_a_productor_agrario_vive_13,#p02_a_productor_agrario_vive_14,#p02_a_productor_agrario_vive_15", function (e) {
    e.preventDefault();
    $(".p02_a_productor_agrario_vive_15").hide();
    $("#p02_a_productor_agrario_vive_especifique").val("");
    if($(this).val()=="15"){
        $(".p02_a_productor_agrario_vive_15").show()
    }
});

$("body").on("change", "#p02_a_es_productor", function (e) {
    e.preventDefault();
    $(".p02_a_es_productor").hide();
    if($(this).val()=="0"){
        $(".p02_a_es_productor").show()
    }
});

$("body").on("change", "#p02_a_informante_tiene_numero", function (e) {
    e.preventDefault();
    $(".p02_a_informante_tiene_numero").hide();
    $("#p02_a_informante_telefono_fijo").val("");
    $("#p02_a_informante_telefono_celular").val("");
    if($(this).val()=="1"){
        $(".p02_a_informante_tiene_numero").show()
    }
});




/* modulo b */
$("body").on("change", "#p02_b_representante_departamento,#p02_a_informante_departamento", function (e) {
    e.preventDefault();
    capitulo = $(this).attr('data-capitulo');
    modulo = $(this).attr('data-modulo');

    if(capitulo=="2" && modulo=="a"){
        $("#p02_a_informante_provincia").html("<option value='99999'>Seleccionar Provincia</option>");
        $("#p02_a_informante_distrito").html("<option value='99999'>Seleccionar Distrito</option>");
        $("#p02_a_informante_centro_poblado").html("<option value='99999'>Seleccionar Centro poblado</option>");
        if($(this).val()){
            p02_a_cod_dep = $(this).val();
            Provincias(capitulo,modulo,p02_a_cod_dep);
        }
    }

    if(capitulo=="2" && modulo=="b"){
        $("#p02_b_representante_provincia").html("<option value='99999'>Seleccionar Provincia</option>");
        $("#p02_b_representante_distrito").html("<option value='99999'>Seleccionar Distrito</option>");
        $("#p02_b_representante_centro_poblado").html("<option value='99999'>Seleccionar Centro poblado</option>");
        if($(this).val()){
            p02_b_cod_dep = $(this).val();
            Provincias(capitulo,modulo,p02_b_cod_dep);
        }
    }
    
});

$("body").on("change", "#p02_b_representante_provincia,#p02_a_informante_provincia", function (e) {
    e.preventDefault();
    capitulo = $(this).attr('data-capitulo');
    modulo = $(this).attr('data-modulo');

    if(capitulo=="2" && modulo=="a"){
        $("#p02_a_informante_distrito").html("<option value='99999'>Seleccionar Distrito</option>");
        $("#p02_a_informante_centro_poblado").html("<option value='99999'>Seleccionar Centro poblado</option>");
        if($(this).val()){
            p02_a_cod_prov = $(this).val();
            Distritos(capitulo,modulo,p02_a_cod_prov);
        }
    }

    if(capitulo=="2" && modulo=="b"){
        $("#p02_b_representante_distrito").html("<option value='99999'>Seleccionar Distrito</option>");
        $("#p02_b_representante_centro_poblado").html("<option value='99999'>Seleccionar Centro poblado</option>");
        if($(this).val()){
            p02_b_cod_prov = $(this).val();
            Distritos(capitulo,modulo,p02_b_cod_prov);
        }
    }
    
});

$("body").on("change", "#p02_b_representante_distrito,#p02_a_informante_distrito", function (e) {
    e.preventDefault();
    capitulo = $(this).attr('data-capitulo');
    modulo = $(this).attr('data-modulo');

    if(capitulo=="2" && modulo=="a"){
        $("#p02_a_informante_centro_poblado").html("<option value='99999'>Seleccionar Centro poblado</option>");
        if($(this).val()){
            p02_cod_ubigeo = $(this).val();
            CentrosPoblados(capitulo,modulo,p02_cod_ubigeo);
        }
    }


    if(capitulo=="2" && modulo=="b"){
        $("#p02_b_representante_centro_poblado").html("<option value='99999'>Seleccionar Centro poblado</option>");
        if($(this).val()){
            p02_cod_ubigeo = $(this).val();
            CentrosPoblados(capitulo,modulo,p02_cod_ubigeo);
        }
    }
});

$("body").on("change", "#p02_b_representante_centro_poblado", function (e) {
    e.preventDefault();
    capitulo = $(this).attr('data-capitulo');
    modulo = $(this).attr('data-modulo');
    if(capitulo=="2" && modulo=="b"){

        if($(this).val()){
            $("#p02_b_representante_codigo_centro_poblado").val($(this).val());
        }
    }
});

$("body").on("change", "#p02_b_tipo_via_16,#p02_b_tipo_via_17,#p02_b_tipo_via_18,#p02_b_tipo_via_19,#p02_b_tipo_via_20,#p02_b_tipo_via_21", function (e) {
    e.preventDefault();
    $(".p02_b_tipo_via_21").hide();
    $("#p02_b_tipo_via_especifique").val("");
    if($(this).val()=="21"){
        $(".p02_b_tipo_via_21").show()
    }
});

$("body").on("change", "#p02_b_cargo_22,#p02_b_cargo_23,#p02_b_cargo_24,#p02_b_cargo_25,#p02_b_cargo_26", function (e) {
    e.preventDefault();
    $(".p02_b_cargo_26").hide();
    $("#p02_b_cargo_especifique").val("");
    if($(this).val()=="26"){
        $(".p02_b_cargo_26").show()
    }
});


$("body").on("change", "#p02_b_informante_tiene_numero", function (e) {
    e.preventDefault();
    $(".p02_b_informante_tiene_numero").hide();
    $("#p02_b_informante_telefono_fijo").val("");
    $("#p02_b_informante_telefono_celular").val("");
    if($(this).val()=="1"){
        $(".p02_b_informante_tiene_numero").show()
    }
});

$("body").on("change", "#p02_b_informante_tiene_correo_electronico", function (e) {
    e.preventDefault();
    $(".p02_b_informante_tiene_correo_electronico").hide();
    $("#p02_b_informante_correo_electronico").val("");
    if($(this).val()=="1"){
        $(".p02_b_informante_tiene_correo_electronico").show()
    }
});

/* ####################################################################################################################################Capitulo III */
/* Variables globales */
$('#lista-usos-coberturas').DataTable();
$('#lista-parcelas-no-establecidas').DataTable();
/* Carga Maestras */

/* Acciones */
$("body").on("change", "#p03_c_int_tenencia_31", function (e) {
    e.preventDefault();
    $(".p03_c_int_tenencia_31").hide();
    $("#p03_c_int_tenencia_31_especifique").val("");
    if($("[id=\"p03_c_int_tenencia_31\"]:checked").val()){
        $(".p03_c_int_tenencia_31").show()
    }
});

$("body").on("click", ".btn-eliminar-cap-iii-modulo-a", function (e) {
    e.preventDefault();
    var id_lote = $(this).attr('data-id_lote');
    $.ajax({
        url: "<?= \Yii::$app->request->BaseUrl ?>/lote/eliminar-lote",
        method: "POST",
        data:{_csrf:csrf,id_lote:id_lote},
        dataType:"Json",
        beforeSend:function(xhr, settings)
        {
            loading.modal("show");
        },
        success:function(results)
        {   
            if(results && results.success){
                location.reload();
            }
        },
        error:function(){
            alert("Error al realizar el proceso.");
        }
    });

});

$("body").on("click", ".btn-eliminar-cap-iii-modulo-b", function (e) {
    e.preventDefault();
    var id_otra_parcela = $(this).attr('data-id_otra_parcela');
    $.ajax({
        url: "<?= \Yii::$app->request->BaseUrl ?>/lote/eliminar-otra-parcela",
        method: "POST",
        data:{_csrf:csrf,id_otra_parcela:id_otra_parcela},
        dataType:"Json",
        beforeSend:function(xhr, settings)
        {
            loading.modal("show");
        },
        success:function(results)
        {   
            if(results && results.success){
                location.reload();
            }
        },
        error:function(){
            alert("Error al realizar el proceso.");
        }
    });

});


/* ####################################################################################################################################Capitulo IV */
/* Variables globales */
$('#lista-cap4-moda-cultivos-transitorias').DataTable();
$('#lista-cap4-modb-arboles-frutales').DataTable();
$('#lista-cap4-modc-viveros').DataTable();

/* Carga Maestras */

/* Acciones */
$("body").on("click", ".btn-eliminar-capitulo-iv-modulo-a", function (e) {
    e.preventDefault();
    var id_cap_iv_a = $(this).attr('data-id_cap_iv_a');

    $.ajax({
        url: "<?= \Yii::$app->request->BaseUrl ?>/capituloiv/eliminar-modulo-a",
        method: "POST",
        data:{_csrf:csrf,id_cap_iv_a:id_cap_iv_a},
        dataType:"Json",
        beforeSend:function(xhr, settings)
        {
            loading.modal("show");
        },
        success:function(results)
        {   
            if(results && results.success){
                location.reload();
                //setTimeout(function(){ loading.modal("hide"); }, 2000);
            }
        },
        error:function(){
            alert("Error al realizar el proceso.");
        }
    });
});


$("body").on("click", ".btn-eliminar-capitulo-iv-modulo-b", function (e) {
    e.preventDefault();
    var num_orden = $(this).attr('data-num_orden');
    var id_encuesta = $(this).attr('data-id_encuesta');

    $.ajax({
        url: "<?= \Yii::$app->request->BaseUrl ?>/capituloiv/eliminar-modulo-b",
        method: "POST",
        data:{_csrf:csrf,num_orden:num_orden,id_encuesta:id_encuesta},
        dataType:"Json",
        beforeSend:function(xhr, settings)
        {
            loading.modal("show");
        },
        success:function(results)
        {   
            if(results && results.success){
                location.reload();
                //setTimeout(function(){ loading.modal("hide"); }, 2000);
            }
        },
        error:function(){
            alert("Error al realizar el proceso.");
        }
    });
});

$("body").on("click", ".btn-eliminar-capitulo-iv-modulo-c", function (e) {
    e.preventDefault();
    var num_orden_semillero = $(this).attr('data-num_orden_semillero');
    var id_encuesta = $(this).attr('data-id_encuesta');

    $.ajax({
        url: "<?= \Yii::$app->request->BaseUrl ?>/capituloiv/eliminar-modulo-c",
        method: "POST",
        data:{_csrf:csrf,num_orden_semillero:num_orden_semillero,id_encuesta:id_encuesta},
        dataType:"Json",
        beforeSend:function(xhr, settings)
        {
            loading.modal("show");
        },
        success:function(results)
        {   
            if(results && results.success){
                location.reload();
                //setTimeout(function(){ loading.modal("hide"); }, 2000);
            }
        },
        error:function(){
            alert("Error al realizar el proceso.");
        }
    });
});

$("body").on("change", "#cap4_modd_p437_8", function (e) {
    e.preventDefault();
    $(".cap4_modd_p437_8").hide();
    $("#cap4_modd_p437_8_especifique").val("");
    if($("[id=\"cap4_modd_p437_8\"]:checked").val()){
        $(".cap4_modd_p437_8").show()
    }
});


$("body").on("change", "#cap4_modd_p438_10", function (e) {
    e.preventDefault();
    $(".cap4_modd_p438_10").hide();
    $("#cap4_modd_p438_10_especifique").val("");
    if($("[id=\"cap4_modd_p438_10\"]:checked").val()){
        $(".cap4_modd_p438_10").show()
    }
});

$("body").on("change", "#cap4_mode_p443_7", function (e) {
    e.preventDefault();
    $(".cap4_mode_p443_7").hide();
    $("#cap4_mode_p443_7_especifique").val("");
    if($("[id=\"cap4_mode_p443_7\"]:checked").val()){
        $(".cap4_mode_p443_7").show()
    }
});

$("body").on("change", "#cap4_mode_p444_8", function (e) {
    e.preventDefault();
    $(".cap4_mode_p444_8").hide();
    $("#cap4_mode_p444_8_especifique").val("");
    if($("[id=\"cap4_mode_p444_8\"]:checked").val()){
        $(".cap4_mode_p444_8").show()
    }
});

/* ####################################################################################################################################Capitulo V */

$("body").on("change", "#cap5_moda_p501_b_27", function (e) {
    e.preventDefault();
    $(".cap5_moda_p501_b_27").hide();
    $("#cap5_moda_p501_b_27_especifique").val("");
    if($("[id=\"cap5_moda_p501_b_27\"]:checked").val()){
        $(".cap5_moda_p501_b_27").show()
    }
});

/* Acciones */
$("body").on("change", "#cap5_modi_p530_9", function (e) {
    e.preventDefault();
    $(".cap5_modi_p530_9").hide();
    $("#cap5_modi_p530_9_especifique").val("");
    if($("[id=\"cap5_modi_p530_9\"]:checked").val()){
        $(".cap5_modi_p530_9").show()
    }
});

$("body").on("change", "#cap5_modi_p533_24", function (e) {
    e.preventDefault();
    $(".cap5_modi_p533_24").hide();
    $("#cap5_modi_p533_24_especifique").val("");
    if($("[id=\"cap5_modi_p533_24\"]:checked").val()){
        $(".cap5_modi_p533_24").show()
    }
});

$("body").on("change", "#cap5_modj_p534_b_5", function (e) {
    e.preventDefault();
    $(".cap5_modj_p534_b_5").hide();
    $("#cap5_modj_p534_b_5_especifique").val("");
    if($("[id=\"cap5_modj_p534_b_5\"]:checked").val()){
        $(".cap5_modj_p534_b_5").show()
    }
});

$("body").on("change", "#cap5_modj_p534_b_11", function (e) {
    e.preventDefault();
    $(".cap5_modj_p534_b_11").hide();
    $("#cap5_modj_p534_b_11_especifique").val("");
    if($("[id=\"cap5_modj_p534_b_11\"]:checked").val()){
        $(".cap5_modj_p534_b_11").show()
    }
});

$("body").on("change", "#cap5_modj_p534_b_18", function (e) {
    e.preventDefault();
    $(".cap5_modj_p534_b_18").hide();
    $("#cap5_modj_p534_b_18_especifique").val("");
    if($("[id=\"cap5_modj_p534_b_18\"]:checked").val()){
        $(".cap5_modj_p534_b_18").show()
    }
});

$("body").on("change", "#cap5_modj_p534_b_27", function (e) {
    e.preventDefault();
    $(".cap5_modj_p534_b_27").hide();
    $("#cap5_modj_p534_b_27_especifique").val("");
    if($("[id=\"cap5_modj_p534_b_27\"]:checked").val()){
        $(".cap5_modj_p534_b_27").show()
    }
});

$("body").on("change", "#cap5_modj_p534_b_37", function (e) {
    e.preventDefault();
    $(".cap5_modj_p534_b_37").hide();
    $("#cap5_modj_p534_b_37_especifique").val("");
    if($("[id=\"cap5_modj_p534_b_37\"]:checked").val()){
        $(".cap5_modj_p534_b_37").show()
    }
});

$("body").on("change", "#cap5_modk_p536_a_9", function (e) {
    e.preventDefault();
    $(".cap5_modk_p536_a_9").hide();
    $("#cap5_modk_p536_a_9_especifique").val("");
    if($("[id=\"cap5_modk_p536_a_9\"]:checked").val()){
        $(".cap5_modk_p536_a_9").show()
    }
});

$("body").on("change", "#cap5_modk_p541_a_8", function (e) {
    e.preventDefault();
    $(".cap5_modk_p541_a_8").hide();
    $("#cap5_modk_p541_a_8_especifique").val("");
    if($("[id=\"cap5_modk_p541_a_8\"]:checked").val()){
        $(".cap5_modk_p541_a_8").show()
    }
});

$("body").on("change", "#cap5_modk_p541_a_15", function (e) {
    e.preventDefault();
    $(".cap5_modk_p541_a_15").hide();
    $("#cap5_modk_p541_a_15_especifique").val("");
    if($("[id=\"cap5_modk_p541_a_15\"]:checked").val()){
        $(".cap5_modk_p541_a_15").show()
    }
});

$("body").on("change", "#cap5_modk_p541_a_24", function (e) {
    e.preventDefault();
    $(".cap5_modk_p541_a_24").hide();
    $("#cap5_modk_p541_a_24_especifique").val("");
    if($("[id=\"cap5_modk_p541_a_24\"]:checked").val()){
        $(".cap5_modk_p541_a_24").show()
    }
});

$("body").on("change", "#cap5_modk_p541_a_30", function (e) {
    e.preventDefault();
    $(".cap5_modk_p541_a_30").hide();
    $("#cap5_modk_p541_a_30_especifique").val("");
    if($("[id=\"cap5_modk_p541_a_30\"]:checked").val()){
        $(".cap5_modk_p541_a_30").show()
    }
});

$("body").on("change", "#cap5_modk_p541_a_39", function (e) {
    e.preventDefault();
    $(".cap5_modk_p541_a_39").hide();
    $("#cap5_modk_p541_a_39_especifique").val("");
    if($("[id=\"cap5_modk_p541_a_39\"]:checked").val()){
        $(".cap5_modk_p541_a_39").show()
    }
});


/* ####################################################################################################################################Capitulo VI */

/* Acciones */
$("body").on("change", "#cap6_p601_7", function (e) {
    e.preventDefault();
    $(".cap6_p601_7").hide();
    $("#cap6_p601_7_especifique").val("");
    if($("[id=\"cap6_p601_7\"]:checked").val()){
        $(".cap6_p601_7").show()
    }
});

$("body").on("change", "#cap6_p602_7", function (e) {
    e.preventDefault();
    $(".cap6_p602_7").hide();
    $("#cap6_p602_7_especifique").val("");
    if($("[id=\"cap6_p602_7\"]:checked").val()){
        $(".cap6_p602_7").show()
    }
});

$("body").on("change", "#cap6_p605_6", function (e) {
    e.preventDefault();
    $(".cap6_p605_6").hide();
    $("#cap6_p605_6_especifique").val("");
    if($("[id=\"cap6_p605_6\"]:checked").val()){
        $(".cap6_p605_6").show()
    }
});

$("body").on("change", "#cap6_p606_5", function (e) {
    e.preventDefault();
    $(".cap6_p606_5").hide();
    $("#cap6_p606_5_especifique").val("");
    if($("[id=\"cap6_p606_5\"]:checked").val()){
        $(".cap6_p606_5").show()
    }
});

$("body").on("change", "#cap6_p610_7", function (e) {
    e.preventDefault();
    $(".cap6_p610_7").hide();
    $("#cap6_p610_7_especifique").val("");
    if($("[id=\"cap6_p610_7\"]:checked").val()){
        $(".cap6_p610_7").show()
    }
});


/* ####################################################################################################################################Capitulo VII */

/* Acciones */

$("body").on("click", ".btn-eliminar-capvii-modulo-c-asociaciones", function (e) {
    e.preventDefault();
    var id_cap_vii_c_asociacion = $(this).attr('data-id_cap_vii_c_asociacion');
    $.ajax({
        url: "<?= \Yii::$app->request->BaseUrl ?>/capitulovii/eliminar-modulo-c",
        method: "POST",
        data:{_csrf:csrf,id_cap_vii_c_asociacion:id_cap_vii_c_asociacion},
        dataType:"Json",
        beforeSend:function(xhr, settings)
        {
            loading.modal("show");
        },
        success:function(results)
        {   
            if(results && results.success){
                location.reload();
            }
        },
        error:function(){
            alert("Error al realizar el proceso.");
        }
    });

});


$("body").on("change", "#cap7_modc_p714_6", function (e) {
    e.preventDefault();
    $(".cap7_modc_p714_6").hide();
    $("#cap7_modc_p714_6_especifique").val("");
    if($("[id=\"cap7_modc_p714_6\"]:checked").val()){
        $(".cap7_modc_p714_6").show()
    }
});


$("body").on("change", "#cap7_modc_p716_6", function (e) {
    e.preventDefault();
    $(".cap7_modc_p716_6").hide();
    $("#cap7_modc_p716_6_especifique").val("");
    if($("[id=\"cap7_modc_p716_6\"]:checked").val()){
        $(".cap7_modc_p716_6").show()
    }
});

$("body").on("change", "#cap7_modc_p717_7", function (e) {
    e.preventDefault();
    $(".cap7_modc_p717_7").hide();
    $("#cap7_modc_p717_7_especifique").val("");
    if($("[id=\"cap7_modc_p717_7\"]:checked").val()){
        $(".cap7_modc_p717_7").show()
    }
});

$("body").on("change", "#cap7_modc_p718_1", function (e) {
    e.preventDefault();
    $(".cap7_modc_p718_1").hide();
    $("#cap7_modc_p718_1_especifique").val("");
    if($("[id=\"cap7_modc_p718_1\"]").val()=="4"){
        $(".cap7_modc_p718_1").show()
    }
});

$("body").on("change", "#cap7_modc_p718_2", function (e) {
    e.preventDefault();
    $(".cap7_modc_p718_2").hide();
    $("#cap7_modc_p718_2_especifique").val("");
    if($("[id=\"cap7_modc_p718_2\"]").val()=="4"){
        $(".cap7_modc_p718_2").show()
    }
});

$("body").on("change", "#cap7_modc_p718_3", function (e) {
    e.preventDefault();
    $(".cap7_modc_p718_3").hide();
    $("#cap7_modc_p718_3_especifique").val("");
    if($("[id=\"cap7_modc_p718_3\"]").val()=="4"){
        $(".cap7_modc_p718_3").show()
    }
});

$("body").on("change", "#cap7_modc_p718_4", function (e) {
    e.preventDefault();
    $(".cap7_modc_p718_4").hide();
    $("#cap7_modc_p718_4_especifique").val("");
    if($("[id=\"cap7_modc_p718_4\"]").val()=="4"){
        $(".cap7_modc_p718_4").show()
    }
});

$("body").on("change", "#cap7_modc_p718_5", function (e) {
    e.preventDefault();
    $(".cap7_modc_p718_5").hide();
    $("#cap7_modc_p718_5_especifique").val("");
    if($("[id=\"cap7_modc_p718_5\"]").val()=="4"){
        $(".cap7_modc_p718_5").show()
    }
});

$("body").on("change", "#cap7_modc_p718_6", function (e) {
    e.preventDefault();
    $(".cap7_modc_p718_6").hide();
    $("#cap7_modc_p718_6_especifique").val("");
    if($("[id=\"cap7_modc_p718_6\"]").val()=="4"){
        $(".cap7_modc_p718_6").show()
    }
});

$("body").on("change", "#cap7_modc_p718_7", function (e) {
    e.preventDefault();
    $(".cap7_modc_p718_7").hide();
    $("#cap7_modc_p718_7_especifique").val("");
    if($("[id=\"cap7_modc_p718_7\"]").val()=="4"){
        $(".cap7_modc_p718_7").show()
    }
});


$("body").on("change", "#cap7_modc_p719_6", function (e) {
    e.preventDefault();
    $(".cap7_modc_p719_6").hide();
    $("#cap7_modc_p719_6_especifique").val("");
    if($("[id=\"cap7_modc_p719_6\"]:checked").val()){
        $(".cap7_modc_p719_6").show()
    }
});

$("body").on("change", "#cap7_mode_p726_11", function (e) {
    e.preventDefault();
    $(".cap7_mode_p726_11").hide();
    $("#cap7_mode_p726_11_especifique").val("");
    if($("[id=\"cap7_mode_p726_11\"]:checked").val()){
        $(".cap7_mode_p726_11").show()
    }
});

$("body").on("change", "#cap7_mode_p727_9", function (e) {
    e.preventDefault();
    $(".cap7_mode_p727_9").hide();
    $("#cap7_mode_p727_9_especifique").val("");
    if($("[id=\"cap7_mode_p727_9\"]:checked").val()){
        $(".cap7_mode_p727_9").show()
    }
});

$("body").on("change", "#cap7_mode_p729_5", function (e) {
    e.preventDefault();
    $(".cap7_mode_p729_5").hide();
    $("#cap7_mode_p729_5_especifique").val("");
    if($("[id=\"cap7_mode_p729_5\"]:checked").val()){
        $(".cap7_mode_p729_5").show()
    }
});

$("body").on("change", "#cap7_mode_p731_6", function (e) {
    e.preventDefault();
    $(".cap7_mode_p731_6").hide();
    $("#cap7_mode_p731_6_especifique").val("");
    if($("[id=\"cap7_mode_p731_6\"]:checked").val()){
        $(".cap7_mode_p731_6").show()
    }
});


/* ####################################################################################################################################Capitulo VIII */

/* Acciones */
$("body").on("change", "#cap8_p801_8", function (e) {
     e.preventDefault();
     $(".cap8_p801_8").hide();
     $("#cap8_p801_8_especifique").val("");
     if($("[id=\"cap8_p801_8\"]:checked").val()){
         $(".cap8_p801_8").show()
     }
});

$("body").on("change", "#cap8_p802_6", function (e) {
    e.preventDefault();
    $(".cap8_p802_6").hide();
    $("#cap8_p802_6_especifique").val("");
    if($("[id=\"cap8_p802_6\"]:checked").val()){
        $(".cap8_p802_6").show()
    }
});

$("body").on("change", "#cap8_p803_9", function (e) {
    e.preventDefault();
    $(".cap8_p803_9").hide();
    $("#cap8_p803_9_especifique").val("");
    if($("[id=\"cap8_p803_9\"]:checked").val()){
        $(".cap8_p803_9").show()
    }
});

/* ####################################################################################################################################Capitulo IX */

$("body").on("click", ".btn-eliminar-capix-maquinaria_equipo", function (e) {
    e.preventDefault();
    var id_maquinaria_equipo = $(this).attr('data-id_maquinaria_equipo');
    $.ajax({
        url: "<?= \Yii::$app->request->BaseUrl ?>/capituloix/eliminar-maquinaria-equipo",
        method: "POST",
        data:{_csrf:csrf,id_maquinaria_equipo:id_maquinaria_equipo},
        dataType:"Json",
        beforeSend:function(xhr, settings)
        {
            loading.modal("show");
        },
        success:function(results)
        {   
            if(results && results.success){
                location.reload();
            }
        },
        error:function(){
            alert("Error al realizar el proceso.");
        }
    });

});
/* ####################################################################################################################################Capitulo X */

$("body").on("click", ".btn-eliminar-cap_x", function (e) {
    e.preventDefault();
    var id_cap_x = $(this).attr('data-id_cap_x');
    $.ajax({
        url: "<?= \Yii::$app->request->BaseUrl ?>/capitulox/eliminar-capx",
        method: "POST",
        data:{_csrf:csrf,id_cap_x:id_cap_x},
        dataType:"Json",
        beforeSend:function(xhr, settings)
        {
            loading.modal("show");
        },
        success:function(results)
        {   
            if(results && results.success){
                location.reload();
            }
        },
        error:function(){
            alert("Error al realizar el proceso.");
        }
    });

});


/* ####################################################################################################################################Capitulo XII */
$("body").on("change", "#cap12_moda_p1207", function (e) {
    e.preventDefault();
    $(".cap12_moda_p1207").hide();
    $("#cap12_moda_p1207_6_especifique").val("");
    if($(this).val()=="6"){
        $(".cap12_moda_p1207").show()
    }
});

</script>

