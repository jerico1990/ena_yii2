

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Encuesta</h4>
                    <a class="btn btn-primary" href="<?= \Yii::$app->request->BaseUrl ?>/encuesta/index?geocodigo_fdo=<?= $geocodigo_fdo ?>">< Regresar</a>
                </div>
            </div>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Parcelas</li>
                </ol>
            </div>

        </div>
    </div>
</div>

<?php $form = ActiveForm::begin(['options' => ['id' => 'formCapituloIxMaquinarias']]); ?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
            <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">902a. ¿Qué tipo de maquinaria y/o equipo ha utilizado desde enero 2020 hasta hoy? - Maquinaria y/o equipo</label>
                            <select name="CapituloIXMaquinarias[cap9_p902]" id="cap9_p902"class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row cap9_p902" style="display:none">
                    <div class="col-md-6" >
                        <div class="form-group">
                            <label for="">Especifique</label>
                            <input type="text" class="form-control" id="cap9_p902_888_889_especifique" placeholder="Especifique" name="CapituloIXMaquinarias[cap9_p902_888_889_especifique]" maxlength="250">
                        </div>
                    </div>
                </div>

                <!-- <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">902b. ¿Qué tipo de maquinaria y/o equipo ha utilizado desde enero 2020 hasta hoy? - Tipo</label>
                            <select name="CapituloIXMaquinarias[cap9_p901_b]" id="cap9_p901_b"class="form-control">
                                <option value>Seleccionar</option>
                                <option value="1">Maquinaria</option>
                                <option value="2">Equipo</option>
                            </select>
                        </div>
                    </div>
                </div> -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">903. ¿Cuál es la potencia de la maquinaria?</label>
                            <input type="text" class="form-control numerico" name="CapituloIXMaquinarias[cap9_p903]" id="cap9_p903" maxlength="12">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">904. ¿La maquinaria y/o equipo es</label>
                            <select name="CapituloIXMaquinarias[cap9_p904]" id="cap9_p904"class="form-control">
                                <option value="99999">Seleccionar</option>
                                <option value="106">Propia?</option>
                                <option value="107">Propiedad de la organización?</option>
                                <option value="108">Alquilada?</option>
                                <option value="109">Alquilada de la DRA?</option>
                                <option value="110">Otro</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row cap9_p904" style="display:none">
                    <div class="col-md-6" >
                        <div class="form-group">
                            <label for="">Especifique</label>
                            <input type="text" class="form-control" id="cap9_p904_5_especifique" placeholder="Especifique" name="CapituloIXMaquinarias[cap9_p904_5_especifique]" maxlength="250">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">905a. ¿Cual es valor de alquiler de la maquinaria y equipo ?  (Soles/hora)</label>
                            <input type="text" class="form-control numerico" name="CapituloIXMaquinarias[cap9_p905_a]" id="cap9_p905_a" maxlength="12">
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">905b. ¿Cual es valor de alquiler de la maquinaria y equipo ?  (Soles/ha)</label>
                            <input type="text" class="form-control numerico" name="CapituloIXMaquinarias[cap9_p905_b]" id="cap9_p905_b" maxlength="12">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">906. ¿Antigüedad de ?</label>
                            <input type="text" class="form-control numerico" name="CapituloIXMaquinarias[cap9_p906]" id="cap9_p906" maxlength="12">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">907. ¿Cuál es el estado del….....?</label>
                            <select name="CapituloIXMaquinarias[cap9_p907]" id="cap9_p907"class="form-control">
                                <option value>Seleccionar</option>
                                <option value="1">Buena</option>
                                <option value="2">Regular</option>
                                <option value="3">Mala</option>
                            </select>
                        </div>
                    </div>
                </div>

                
                <hr>
                <button class="btn btn-success btn-grabar-capitulo-ix-maquinarias">Grabar</button>

            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<?php ActiveForm::end(); ?>

<script>

var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var loading =   $('#staticBackdrop');

$("body").on("click", ".btn-grabar-capitulo-ix-maquinarias", function (e) {

    e.preventDefault();

    var form = $("#formCapituloIxMaquinarias");
    var formData = $("#formCapituloIxMaquinarias").serializeArray();

    if (form.find(".has-error").length) {
        return false;
    }

    $.ajax({
        url:form.attr("action"),
        type: form.attr("method"),
        data: formData,
        dataType: "json",
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        beforeSend:function()
        {
            loading.modal("show");
        },
        success: function (results) {
            if(results.success){
                window.location.href = "<?= \Yii::$app->request->BaseUrl ?>/capituloix/update-capitulo-ix-maquinarias?id_maquinaria_equipo="+results.id_maquinaria_equipo+"&geocodigo_fdo=<?= $geocodigo_fdo ?>";

                //loading.modal("hide");
            }
        },
    });
});

Maquinarias()
async function Maquinarias(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/capituloix/get-lista-maquinarias',
                method: 'POST',
                data:{_csrf:csrf},
                dataType:'Json',
                beforeSend:function(){
                    loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var opciones_maquinarias ="<option value>Seleccionar</option>";
                        $.each(results.maquinarias, function( index, value ) {
                            
                            opciones_maquinarias = opciones_maquinarias + "<option value='" + value.ID_MAQ_EQUIP + "'>" + value.TXT_DESCRIPCION + "-" + value.TXT_TIPO + "</option>";
                        });
                        $("#cap9_p902").html(opciones_maquinarias);

                        
                    }
                    setTimeout(function(){ loading.modal("hide"); }, 2000);
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}


$("body").on("change", "#cap9_p902", function (e) {
    e.preventDefault();
    $(".cap9_p902").hide();
    $("#cap9_p902_888_889_especifique").val("");

    if($(this).val()=="888" || $(this).val()=="889"){
        $(".cap9_p902").show()
    }

});


$("body").on("change", "#cap9_p904", function (e) {
    e.preventDefault();
    $(".cap9_p904").hide();
    $("#cap9_p904_5_especifique").val("");

    if($(this).val()=="5"){
        $(".cap9_p904").show()
    }

});


</script>