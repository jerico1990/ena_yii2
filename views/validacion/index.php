<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<div class="account-pages my-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 col-lg-6 col-xl-5">
                <div class="card overflow-hidden">
                    <div class="bg-success">
                        <div class="row">
                            <div class="col-9">
                                <div class="text-white p-4">
                                    <h5 class="text-white">Validación</h5>
                                </div>
                            </div>
                            <div class="col-3 align-self-end">
                                <img src="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/images/profile-img.png" alt="" class="img-fluid">
                            </div>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        <div>
                            <a href="#">
                                <div class="avatar-md profile-user-wid mb-4">
                                    <span class="avatar-title rounded-circle bg-success">
                                        <img src="<?= \Yii::$app->request->BaseUrl ?>/img/midagri_login.jpg" alt="" class="rounded-circle" height="54">
                                    </span>
                                </div>
                            </a>
                        </div>
                        <div class="p-2">
                            <?php $form = ActiveForm::begin(); ?>

                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="hidden" name="LoginForm[username]" value="<?= $us ?>">
                                        <input type="hidden" name="LoginForm[password]" value="<?= $ps ?>">

                                        <input type="text" class="form-control" id="code" name="LoginForm[code]" placeholder="Ingresa código enviado">
                                        <div class="input-group-prepend">
                                            <button type="button" class="btn btn-success btn-generar-codigo">Enviar código</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-3">
                                    <button class="btn btn-success btn-block waves-effect waves-light" type="submit">Ingresar</button>
                                </div>
                            <?php ActiveForm::end(); ?>
                        </div>

                    </div>
                </div>
                <div class="mt-5 text-center">
                    <div>
                        <!-- <p>Don't have an account ? <a href="auth-register.html"
                                class="font-weight-medium text-primary"> Signup now </a> </p> -->
                        <p>© 2021 - MIDAGRI | PERÚ. </p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script>
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var loading =   $('#staticBackdrop');
var ruc = "<?= $ruc ?>";
$("body").on("click", ".btn-generar-codigo", function (e) {
    e.preventDefault();
    GenerarCodigo();
});

function GenerarCodigo(){
    $.ajax({
        url: "<?= \Yii::$app->request->BaseUrl ?>/validacion/get-enviar-codigo",
        method: "POST",
        data:{_csrf:csrf,ruc:ruc},
        dataType:"Json",
        beforeSend:function(xhr, settings)
        {
            loading.modal("show");
        },
        success:function(results)
        {   
            if(results && results.success){
                setTimeout(function(){ loading.modal("hide"); }, 1000);
            }
        },
        error:function(){
            alert("Error al realizar el proceso.");
        }
    });
}
</script>