

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Encuesta</h4>
                    <a class="btn btn-primary" href="<?= \Yii::$app->request->BaseUrl ?>/encuesta/index?geocodigo_fdo=<?= $geocodigo_fdo ?>">< Regresar</a>
                </div>
            </div>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Parcelas</li>
                </ol>
            </div>

        </div>
    </div>
</div>

<?php $form = ActiveForm::begin(['options' => ['id' => 'formCapituloVModuloG']]); ?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">517. ¿Qué cantidad de ... tuvo en enero 2020? Cantidad (No)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloG[cap5_modg_p517]" id="cap5_modg_p517" maxlength="12">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">518. ¿Qué cantidad de ...  tuvo en diciembre del 2020? Cantidad (No)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloG[cap5_modg_p518]" id="cap5_modg_p518" maxlength="12">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">519. ¿Qué cantidad de ... tendrá el 31  de diciembre del 2021? Cantidad (No)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloG[cap5_modg_p519]" id="cap5_modg_p519" maxlength="12">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">520. Durante el 2020, ¿Cual fue la carga de (Carga según línea genética durante todo el año). INCREMENTO DE STOCK</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">520a. Hy line? Cantidad (No)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloG[cap5_modg_p520_a]" id="cap5_modg_p520_a" maxlength="12">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">520b. Isa? Cantidad (No)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloG[cap5_modg_p520_b]" id="cap5_modg_p520_b" maxlength="12">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">520c. Lohman? Cantidad (No)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloG[cap5_modg_p520_c]" id="cap5_modg_p520_c" maxlength="12">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">520d. Babcok? Cantidad (No)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloG[cap5_modg_p520_d]" id="cap5_modg_p520_d" maxlength="12">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">520e. Bovans? Cantidad (No)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloG[cap5_modg_p520_e]" id="cap5_modg_p520_e" maxlength="12">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">520f. Hisex? Cantidad (No)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloG[cap5_modg_p520_f]" id="cap5_modg_p520_f" maxlength="12">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">520g. H&N? Cantidad (No)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloG[cap5_modg_p520_g]" id="cap5_modg_p520_g" maxlength="12">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">520h. Tetra? Cantidad (No)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloG[cap5_modg_p520_h]" id="cap5_modg_p520_h" maxlength="12">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">520i. Otras? Cantidad (No)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloG[cap5_modg_p520_i]" id="cap5_modg_p520_i" maxlength="12">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">521. ¿Cuanta es la producción de huevos fértiles o comerciales durante el 2020?</label>
                        </div>
                    </div>
                </div>



                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">521a. Total Huevos fértiles? Cantidad (No)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloG[cap5_modg_p521_a]" id="cap5_modg_p521_a" maxlength="12">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">521b. Total Huevos incubables? Cantidad (No)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloG[cap5_modg_p521_b]" id="cap5_modg_p521_b" maxlength="12">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">521c. Total Huevos comerciales? Cantidad (No)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloG[cap5_modg_p521_c]" id="cap5_modg_p521_c" maxlength="12">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">521d. Volúmen de Huevos comerciales? Cantidad (No)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloG[cap5_modg_p521_d]" id="cap5_modg_p521_d" maxlength="9">
                        </div>
                    </div>
                </div>



                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">522. DURANTE EL 2020 CUAL FUE LA REDUCCIÓN DE STOCK</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">522a. Saca</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">522a1. (Unidades)</label>
                            <input type="text" class="form-control v" name="CapituloVModuloG[cap5_modg_p522_a_1]" id="cap5_modg_p522_a_1" maxlength="12">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">522a2. Edad promedio? (Semanas)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloG[cap5_modg_p522_a_2]" id="cap5_modg_p522_a_2" maxlength="12">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">522a3. Volumen (kg) </label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloG[cap5_modg_p522_a_3]" id="cap5_modg_p522_a_3" maxlength="9">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">522a4. Peso promedio en pie? (Kg/animal)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloG[cap5_modg_p522_a_4]" id="cap5_modg_p522_a_4" maxlength="9">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">522a5. Precio al productor?  (S/ x kg)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloG[cap5_modg_p522_a_5]" id="cap5_modg_p522_a_5" maxlength="9">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">522b. ¿Cuál fue la mortalidad total de ... en el año 2020? Cantidad</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloG[cap5_modg_p522_b]" id="cap5_modg_p522_b" maxlength="12">
                        </div>
                    </div>
                </div>


                
                
                <hr>
                <button class="btn btn-success btn-grabar-capitulo-v-modulo-g">Grabar</button>

            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<?php ActiveForm::end(); ?>

<script>

var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var loading =   $('#staticBackdrop');
var id_encuesta = "<?= $id_encuesta ?>";
var id_avicola = "<?= $id_avicola ?>";

$("body").on("click", ".btn-grabar-capitulo-v-modulo-g", function (e) {

    e.preventDefault();



    var form = $("#formCapituloVModuloG");
    var formData = $("#formCapituloVModuloG").serializeArray();


    if (form.find(".has-error").length) {
        return false;
    }

    $.ajax({
        url:form.attr("action"),
        type: form.attr("method"),
        data: formData,
        dataType: "json",
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        beforeSend:function()
        {
            loading.modal("show");
        },
        success: function (results) {
            if(results.success){
                setTimeout(function(){ loading.modal("hide"); }, 2000);
            }
        },
    });
});
InformacionGeneral();
async function InformacionGeneral(){
    await   $.ajax({
                url: "<?= \Yii::$app->request->BaseUrl ?>/capitulov/get-modulo-g2",
                method: "POST",
                data:{_csrf:csrf,id_encuesta:id_encuesta,id_avicola:id_avicola},
                dataType:"Json",
                beforeSend:function(xhr, settings)
                {
                    loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        $("#cap5_modg_p517").val(results.cabecera.NUM_CANTIDAD_ENERO);
                        $("#cap5_modg_p518").val(results.cabecera.NUM_CANTIDAD_DIC);
                        $("#cap5_modg_p519").val(results.cabecera.NUM_CANTIDAD_JUL);
                        $("#cap5_modg_p520_a").val(results.cabecera.NUM_CANTIDAD_HYLINE);
                        $("#cap5_modg_p520_b").val(results.cabecera.NUM_CANTIDAD_ISA);
                        $("#cap5_modg_p520_c").val(results.cabecera.NUM_CANTIDAD_LOHMAN);
                        $("#cap5_modg_p520_d").val(results.cabecera.NUM_CANTIDAD_BABCOK);
                        $("#cap5_modg_p520_e").val(results.cabecera.NUM_CANTIDAD_BOVANS);
                        $("#cap5_modg_p520_f").val(results.cabecera.NUM_CANTIDAD_HISEX);
                        $("#cap5_modg_p520_g").val(results.cabecera.NUM_CANTIDAD_HYN);
                        $("#cap5_modg_p520_h").val(results.cabecera.NUM_CANTIDAD_TETRA);
                        $("#cap5_modg_p520_i").val(results.cabecera.NUM_CANTIDAD_OTRA);
                        $("#cap5_modg_p521_a").val(results.cabecera.NUM_HUEVO_FERTIL);
                        $("#cap5_modg_p521_b").val(results.cabecera.NUM_HUEVO_INCUBABLE);
                        $("#cap5_modg_p521_c").val(results.cabecera.NUM_HUEVO_COMERCIAL);
                        $("#cap5_modg_p521_d").val(results.cabecera.NUM_VOLUMEN_HUEVO_COMERCIAL);
                        $("#cap5_modg_p522_a_1").val(results.cabecera.NUM_SACA_STOCK);
                        $("#cap5_modg_p522_a_2").val(results.cabecera.NUM_SACA_EDAD);
                        $("#cap5_modg_p522_a_3").val(results.cabecera.NUM_SACA_VOLUMEN);
                        $("#cap5_modg_p522_a_4").val(results.cabecera.NUM_SACA_PESO_PIE);
                        $("#cap5_modg_p522_a_5").val(results.cabecera.NUM_SACA_PRECIO_PROD);
                        $("#cap5_modg_p522_b").val(results.cabecera.NUM_MORTALIDAD_TOTAL);
                    }
                    setTimeout(function(){ loading.modal("hide"); }, 2000);
                },
                error:function(){
                    alert("Error al realizar el proceso.");
                }
            });
}

</script>