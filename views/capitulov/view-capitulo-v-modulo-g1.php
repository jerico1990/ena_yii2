

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Encuesta</h4>
                </div>
            </div>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Parcelas</li>
                </ol>
            </div>

        </div>
    </div>
</div>

<?php $form = ActiveForm::begin(['options' => ['id' => 'formCapituloVModuloG']]); ?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">510. ¿Qué cantidad de ……….. tuvo en enero 2020? Cantidad (No)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloG[cap5_modg_p510]" id="cap5_modg_p510" maxlength="12">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">511. ¿Qué cantidad de ………... tuvo en diciembre del 2020? Cantidad (No)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloG[cap5_modg_p511]" id="cap5_modg_p511" maxlength="12">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">512. ¿Qué cantidad de ……….  Tendrá el 31 de diciembre del 2021? Cantidad (No)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloG[cap5_modg_p512]" id="cap5_modg_p512" maxlength="12">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">513. Durante el  2020 ¿Cuál fue la carga de ...(Carga según línea genética durante todo el año)</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">513a. Cobb? Cantidad (No)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloG[cap5_modg_p513_a]" id="cap5_modg_p513_a" maxlength="12">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">513b. Ross? Cantidad (No)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloG[cap5_modg_p513_b]" id="cap5_modg_p513_b" maxlength="12">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">513c. Hybrid? Cantidad (No)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloG[cap5_modg_p513_c]" id="cap5_modg_p513_c" maxlength="12">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">513d. Nicholas? Cantidad (No)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloG[cap5_modg_p513_d]" id="cap5_modg_p513_d" maxlength="12">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">513e. Hibro Nicholas? Cantidad (No)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloG[cap5_modg_p513_e]" id="cap5_modg_p513_e" maxlength="12">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">513f. Otras? Cantidad (No)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloG[cap5_modg_p513_f]" id="cap5_modg_p513_f" maxlength="12">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">514. ¿Cuanta es la producción de huevos fértiles durante el 2020?</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">514a. Total Cantidad (No)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloG[cap5_modg_p514_a]" id="cap5_modg_p514_a" maxlength="12">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">514b. Total huevos incubables Cantidad (No)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloG[cap5_modg_p514_b]" id="cap5_modg_p514_b" maxlength="12">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">515.  Durante el 2020 ¿Cuál fue la reducción de stock ... (Stock según línea genética durante todo el año)</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">515a. Saca</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">515a1. Cantidad (No)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloG[cap5_modg_p515_a_1]" id="cap5_modg_p515_a_1" maxlength="12">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">515a2. Edad Prom.? Días</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloG[cap5_modg_p515_a_2]" id="cap5_modg_p515_a_2" maxlength="12">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">515a3. Volumen (kg)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloG[cap5_modg_p515_a_3]" id="cap5_modg_p515_a_3" maxlength="9">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">515a4. Peso en pie? (Kg/animal)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloG[cap5_modg_p515_a_4]" id="cap5_modg_p515_a_4" maxlength="9">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">515a5. Precio al productor?  (S/ x kg)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloG[cap5_modg_p515_a_5]" id="cap5_modg_p515_a_5" maxlength="9">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">515b. Mortalidad total en el año 2020</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">515b1. Mortalidad levante año 2020? Cantidad</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloG[cap5_modg_p515_b_1]" id="cap5_modg_p515_b_1" maxlength="12">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">515b2. Mortalidad producción año 2020? Cantidad</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloG[cap5_modg_p515_b_2]" id="cap5_modg_p515_b_2" maxlength="12">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">515b3. Mortalidad total en el año 2020? Cantidad</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloG[cap5_modg_p515_b_3]" id="cap5_modg_p515_b_3" maxlength="12">
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<?php ActiveForm::end(); ?>

<script>
$('input,select,textarea').prop('disabled',true)
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var loading =   $('#staticBackdrop');
var id_encuesta = "<?= $id_encuesta ?>";
var id_avicola = "<?= $id_avicola ?>";

InformacionGeneral();
async function InformacionGeneral(){
    await   $.ajax({
                url: "<?= \Yii::$app->request->BaseUrl ?>/capitulov/get-modulo-g1",
                method: "POST",
                data:{_csrf:csrf,id_encuesta:id_encuesta,id_avicola:id_avicola},
                dataType:"Json",
                beforeSend:function(xhr, settings)
                {
                    loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        $("#cap5_modg_p510").val(results.cabecera.NUM_CANTIDAD_ENERO);
                        $("#cap5_modg_p511").val(results.cabecera.NUM_CANTIDAD_DIC);
                        $("#cap5_modg_p512").val(results.cabecera.NUM_CANTIDAD_JUL);
                        $("#cap5_modg_p513_a").val(results.cabecera.NUM_CANTIDAD_COBB);
                        $("#cap5_modg_p513_b").val(results.cabecera.NUM_CANTIDAD_ROSS);
                        $("#cap5_modg_p513_c").val(results.cabecera.NUM_CANTIDAD_HYBRID);
                        $("#cap5_modg_p513_d").val(results.cabecera.NUM_CANTIDAD_NICHOLAS);
                        $("#cap5_modg_p513_e").val(results.cabecera.NUM_CANTIDAD_HIBRONICHOLA);
                        $("#cap5_modg_p513_f").val(results.cabecera.NUM_CANTIDAD_OTRA);
                        $("#cap5_modg_p514_a").val(results.cabecera.NUM_HUEVO_FERTIL);
                        $("#cap5_modg_p514_b").val(results.cabecera.NUM_HUEVO_INCUBABLE);
                        $("#cap5_modg_p515_a_1").val(results.cabecera.NUM_SACA_STOCK);
                        $("#cap5_modg_p515_a_2").val(results.cabecera.NUM_SACA_EDAD);
                        $("#cap5_modg_p515_a_3").val(results.cabecera.NUM_SACA_VOLUMEN);
                        $("#cap5_modg_p515_a_4").val(results.cabecera.NUM_SACA_PESO_PIE);
                        $("#cap5_modg_p515_a_5").val(results.cabecera.NUM_SACA_PRECIO_PROD);
                        $("#cap5_modg_p515_b_1").val(results.cabecera.NUM_MORTALIDAD_LEVANTE);
                        $("#cap5_modg_p515_b_2").val(results.cabecera.NUM_MORTALIDAD_PRODUCCION);
                        $("#cap5_modg_p515_b_3").val(results.cabecera.NUM_MORTALIDAD_TOTAL);
                    }
                    setTimeout(function(){ loading.modal("hide"); }, 2000);
                },
                error:function(){
                    alert("Error al realizar el proceso.");
                }
            });
}

</script>