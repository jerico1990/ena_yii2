

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Encuesta</h4>
                    <a class="btn btn-primary" href="<?= \Yii::$app->request->BaseUrl ?>/encuesta/index?geocodigo_fdo=<?= $geocodigo_fdo ?>">< Regresar</a>
                </div>
            </div>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Parcelas</li>
                </ol>
            </div>

        </div>
    </div>
</div>

<?php $form = ActiveForm::begin(['options' => ['id' => 'formCapituloVModuloB']]); ?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">503. ¿Qué cantidad de …... tuvo en el mes de enero del 2020? Cantidad (No)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloB[cap5_modb_p503]" id="cap5_modb_p503" maxlength="12">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">504. DURANTE LOS MESES DE ENERO A DICIEMBRE 2020, ¿QUÉ CANTIDAD DE …………………………… :</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">INCREMENTO DE STOCK</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">504a. Nacieron? Cantidad (No)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloB[cap5_modb_p504_a]" id="cap5_modb_p504_a" maxlength="12">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">504b. Compró? Cantidad (No)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloB[cap5_modb_p504_b]" id="cap5_modb_p504_b" maxlength="12">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">504c. Recibió donación, regalo o trueque, pago en especie? Cantidad (No)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloB[cap5_modb_p504_c]" id="cap5_modb_p504_c" maxlength="12">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">REDUCCIÓN DE STOCK</label>
                        </div>
                    </div>
                </div>




                <div class="row " >
                    <div class="col-md-6" >
                        <div class="form-group">
                            <label for="">504d. Vendió beneficiado (carcasa)? Cantidad (N°)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloB[cap5_modb_p504_d_1]" id="cap5_modb_p504_d_1" maxlength="12">
                        </div>
                    </div>
                    <div class="col-md-6" >
                        <div class="form-group">
                            <label for="">504d. Vendió beneficiado (carcasa)? Precio/Kg (Soles)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloB[cap5_modb_p504_d_2]" id="cap5_modb_p504_d_2" maxlength="9">
                        </div>
                    </div>
                </div>

                <div class="row " >
                    <div class="col-md-4" >
                        <div class="form-group">
                            <label for="">504e. Vendió en pie? Cantidad (N°)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloB[cap5_modb_p504_e_1]" id="cap5_modb_p504_e_1" maxlength="12">
                        </div>
                    </div>
                    <div class="col-md-4" >
                        <div class="form-group">
                            <label for="">504e. Vendió en pie? Peso Promedio (Kg/animal)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloB[cap5_modb_p504_e_2]" id="cap5_modb_p504_e_2" maxlength="9">
                        </div>
                    </div>
                    <div class="col-md-4" >
                        <div class="form-group">
                            <label for="">504e. Vendió en pie? Precio (Soles/Kg)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloB[cap5_modb_p504_e_3]" id="cap5_modb_p504_e_3" maxlength="9">
                        </div>
                    </div>
                </div>

                <div class="row" >
                    <div class="col-md-12" >
                        <div class="form-group">
                            <label for="">504f. Consumió en el hogar? Cantidad (N°)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloB[cap5_modb_p504_f]" id="cap5_modb_p504_f" maxlength="12">
                        </div>
                    </div>
                </div>

                <div class="row" >
                    <div class="col-md-12" >
                        <div class="form-group">
                            <label for="">504g. Dió en trueque? Cantidad (N°)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloB[cap5_modb_p504_g]" id="cap5_modb_p504_g" maxlength="12">
                        </div>
                    </div>
                </div>

                <div class="row" >
                    <div class="col-md-12" >
                        <div class="form-group">
                            <label for="">504h. Murieron? Cantidad (N°)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloB[cap5_modb_p504_h]" id="cap5_modb_p504_h" maxlength="12">
                        </div>
                    </div>
                </div>

                <div class="row" >
                    <div class="col-md-12" >
                        <div class="form-group">
                            <label for="">504i. Se destinó a la elaboración de derivados? Cantidad (N°)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloB[cap5_modb_p504_i]" id="cap5_modb_p504_i" maxlength="12">
                        </div>
                    </div>
                </div>

                <div class="row" >
                    <div class="col-md-12" >
                        <div class="form-group">
                            <label for="">504j. Dió en donación, robados o como pago en especie? Cantidad (N°)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloB[cap5_modb_p504_j]" id="cap5_modb_p504_j" maxlength="12">
                        </div>
                    </div>
                </div>






                <div class="row">
                    <div class="col-md-12">
                        <label for="">505. ¿Donde realizó la venta de: …………..</label>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p505_1" value="1">
                                    <label class="custom-control-label" for="cap5_modb_p505_1">1. Donde cría sus animales?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p505_2" value="2">
                                    <label class="custom-control-label" for="cap5_modb_p505_2">2. Mercado local (feria local)?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p505_3" value="3">
                                    <label class="custom-control-label" for="cap5_modb_p505_3">3. Matadero local (camal local)?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p505_4" value="4">
                                    <label class="custom-control-label" for="cap5_modb_p505_4">4. Mercado regional (feria regional)?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p505_5" value="5">
                                    <label class="custom-control-label" for="cap5_modb_p505_5">5. Matadero regional (camal regional)? </label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p505_6" value="6">
                                    <label class="custom-control-label" for="cap5_modb_p505_6">6. Matadero de Lima (camal de Lima)?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p505_7" value="7">
                                    <label class="custom-control-label" for="cap5_modb_p505_7">7. Mercado exterior?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p505_8" value="8">
                                    <label class="custom-control-label" for="cap5_modb_p505_8">8. Agroindustria?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p505_9" value="9">
                                    <label class="custom-control-label" for="cap5_modb_p505_9">9. Mercado de Lima?  </label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p505_10" value="10">
                                    <label class="custom-control-label" for="cap5_modb_p505_10">10. BioFeria?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p505_11" value="11">
                                    <label class="custom-control-label" for="cap5_modb_p505_11">11. Otro:</label>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row cap5_modb_p505_11" style="display:none">
                    <div class="col-md-6" >
                        <div class="form-group">
                            <label for="">Especifique</label>
                            <input type="text" class="form-control" id="cap5_modb_p505_11_especifique" placeholder="Especifique" name="CapituloVModuloB[cap5_modb_p505_11_especifique]" maxlength="250">
                        </div>
                    </div>
                </div>


                <div class="row" >
                    <div class="col-md-12" >
                        <div class="form-group">
                            <label for="">506. ¿Qué cantidad de : …………………. tuvo  el 31 de diciembre  del 2020? Cantidad (N°)</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloB[cap5_modb_p506]" id="cap5_modb_p506" maxlength="12">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <label for="">507. ¿Cuáles fueron los factores causantes de la muerte de los animales?</label>
                        <div class="row">
                        <div class="col-md-12">
                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p507_1" value="1">
                                    <label class="custom-control-label" for="cap5_modb_p507_1">1. Pobreza de las pasturas</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p507_2" value="2">
                                    <label class="custom-control-label" for="cap5_modb_p507_2">2. No hay suficiente forraje,</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p507_3" value="3">
                                    <label class="custom-control-label" for="cap5_modb_p507_3">3. Acceso limitado a agua y energía,</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p507_4" value="4">
                                    <label class="custom-control-label" for="cap5_modb_p507_4">4. Falta de control sanitario,</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p507_5" value="5">
                                    <label class="custom-control-label" for="cap5_modb_p507_5">5. Abortos,</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p507_6" value="6">
                                    <label class="custom-control-label" for="cap5_modb_p507_6">6. Falta de infraestructura de protección,</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p507_7" value="7">
                                    <label class="custom-control-label" for="cap5_modb_p507_7">7. Falta de asistencia técnica, </label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p507_8" value="8">
                                    <label class="custom-control-label" for="cap5_modb_p507_8">8. Heladas,</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p507_9" value="9">
                                    <label class="custom-control-label" for="cap5_modb_p507_9">9. Estiaje,</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap5_modb_p507_10" value="10">
                                    <label class="custom-control-label" for="cap5_modb_p507_10">10. Otro:</label>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>

                <div class="row cap5_modb_p507_10" style="display:none">
                    <div class="col-md-6" >
                        <div class="form-group">
                            <label for="">Especifique</label>
                            <input type="text" class="form-control" id="cap5_modb_p507_10_especifique" placeholder="Especifique" name="CapituloVModuloB[cap5_modb_p507_10_especifique]" maxlength="250">
                        </div>
                    </div>
                </div>



                <div class="row" >
                    <div class="col-md-12" >
                        <div class="form-group">
                            <label for="">508. ¿Qué cantidad  tde …. tendrá en el mes de diciembre del 2021? Cantidad (N°)</label>
                            <input type="text" class="form-control" name="CapituloVModuloB[cap5_modb_p508]" id="cap5_modb_p508">
                        </div>
                    </div>
                </div>
                
                <hr>
                <button class="btn btn-success btn-grabar-capitulo-v-modulo-b">Grabar</button>

            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<?php ActiveForm::end(); ?>

<script>

var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var loading =   $('#staticBackdrop');
var id_encuesta = "<?= $id_encuesta ?>";
var id_especie_categoria = "<?= $id_especie_categoria ?>";

$("body").on("click", ".btn-grabar-capitulo-v-modulo-b", function (e) {

    e.preventDefault();

    var cap5_modb_p505 = "";
    var cap5_modb_p505_1   = ($("[id=\"cap5_modb_p505_1\"]:checked").val())?1:0;
    var cap5_modb_p505_2   = ($("[id=\"cap5_modb_p505_2\"]:checked").val())?2:0;
    var cap5_modb_p505_3   = ($("[id=\"cap5_modb_p505_3\"]:checked").val())?3:0;
    var cap5_modb_p505_4   = ($("[id=\"cap5_modb_p505_4\"]:checked").val())?4:0;
    var cap5_modb_p505_5   = ($("[id=\"cap5_modb_p505_5\"]:checked").val())?5:0;
    var cap5_modb_p505_6   = ($("[id=\"cap5_modb_p505_6\"]:checked").val())?6:0;
    var cap5_modb_p505_7   = ($("[id=\"cap5_modb_p505_7\"]:checked").val())?7:0;
    var cap5_modb_p505_8   = ($("[id=\"cap5_modb_p505_8\"]:checked").val())?8:0;
    var cap5_modb_p505_9   = ($("[id=\"cap5_modb_p505_9\"]:checked").val())?9:0;
    var cap5_modb_p505_10   = ($("[id=\"cap5_modb_p505_10\"]:checked").val())?10:0;
    var cap5_modb_p505_11   = ($("[id=\"cap5_modb_p505_11\"]:checked").val())?11:0;

    var cap5_modb_p505_lista = [cap5_modb_p505_1,cap5_modb_p505_2,cap5_modb_p505_3,cap5_modb_p505_4,cap5_modb_p505_5,cap5_modb_p505_6,cap5_modb_p505_7,cap5_modb_p505_8,cap5_modb_p505_9,cap5_modb_p505_10,cap5_modb_p505_11];
    var cap5_modb_p505_contador = 0;
    $.each(cap5_modb_p505_lista, function( index, value ) {
        if(value!=0){
            if(cap5_modb_p505_contador==0){
                cap5_modb_p505 = cap5_modb_p505 + value;
            }else{
                cap5_modb_p505 = cap5_modb_p505 + "," + value;
            }
            cap5_modb_p505_contador++;
        }
    });

    var cap5_modb_p507 = "";
    var cap5_modb_p507_1   = ($("[id=\"cap5_modb_p507_1\"]:checked").val())?1:0;
    var cap5_modb_p507_2   = ($("[id=\"cap5_modb_p507_2\"]:checked").val())?2:0;
    var cap5_modb_p507_3   = ($("[id=\"cap5_modb_p507_3\"]:checked").val())?3:0;
    var cap5_modb_p507_4   = ($("[id=\"cap5_modb_p507_4\"]:checked").val())?4:0;
    var cap5_modb_p507_5   = ($("[id=\"cap5_modb_p507_5\"]:checked").val())?5:0;
    var cap5_modb_p507_6   = ($("[id=\"cap5_modb_p507_6\"]:checked").val())?6:0;
    var cap5_modb_p507_7   = ($("[id=\"cap5_modb_p507_7\"]:checked").val())?7:0;
    var cap5_modb_p507_8   = ($("[id=\"cap5_modb_p507_8\"]:checked").val())?8:0;
    var cap5_modb_p507_9   = ($("[id=\"cap5_modb_p507_9\"]:checked").val())?9:0;
    var cap5_modb_p507_10   = ($("[id=\"cap5_modb_p507_10\"]:checked").val())?10:0;
    var cap5_modb_p507_11   = ($("[id=\"cap5_modb_p507_11\"]:checked").val())?11:0;

    var cap5_modb_p507_lista = [cap5_modb_p507_1,cap5_modb_p507_2,cap5_modb_p507_3,cap5_modb_p507_4,cap5_modb_p507_5,cap5_modb_p507_6,cap5_modb_p507_7,cap5_modb_p507_8,cap5_modb_p507_9,cap5_modb_p507_10,cap5_modb_p507_11];
    var cap5_modb_p507_contador = 0;
    $.each(cap5_modb_p507_lista, function( index, value ) {
        if(value!=0){
            if(cap5_modb_p507_contador==0){
                cap5_modb_p507 = cap5_modb_p507 + value;
            }else{
                cap5_modb_p507 = cap5_modb_p507 + "," + value;
            }
            cap5_modb_p507_contador++;
        }
    });



    var form = $("#formCapituloVModuloB");
    var formData = $("#formCapituloVModuloB").serializeArray();

    /* Seteando variables del capitulo IV Modulo A */
    formData.push({name: "CapituloVModuloB[cap5_modb_p505]", value: cap5_modb_p505});
    formData.push({name: "CapituloVModuloB[cap5_modb_p507]", value: cap5_modb_p507});


    if (form.find(".has-error").length) {
        return false;
    }

    $.ajax({
        url:form.attr("action"),
        type: form.attr("method"),
        data: formData,
        dataType: "json",
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        beforeSend:function()
        {
            loading.modal("show");
        },
        success: function (results) {
            if(results.success){
                setTimeout(function(){ loading.modal("hide"); }, 2000);
            }
        },
    });
});
InformacionGeneral();
async function InformacionGeneral(){
    await   $.ajax({
                url: "<?= \Yii::$app->request->BaseUrl ?>/capitulov/get-modulo-b",
                method: "POST",
                data:{_csrf:csrf,id_encuesta:id_encuesta,id_especie_categoria:id_especie_categoria},
                dataType:"Json",
                beforeSend:function(xhr, settings)
                {
                    loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        $("#cap5_modb_p503").val(results.cabecera.NUM_CANTIDAD_ENERO);
                        $("#cap5_modb_p504_a").val(results.cabecera.NUM_CANTIDAD_NACIERON);
                        $("#cap5_modb_p504_b").val(results.cabecera.NUM_CANTIDAD_COMPRO);
                        $("#cap5_modb_p504_c").val(results.cabecera.NUM_STOCK_DONACION);
                        $("#cap5_modb_p504_d_1").val(results.cabecera.NUM_CANTIDAD_VENDIO);
                        $("#cap5_modb_p504_d_2").val(results.cabecera.NUM_PRECIO_VENDIO);
                        $("#cap5_modb_p504_e_1").val(results.cabecera.NUM_CANTIDAD_PIE);
                        $("#cap5_modb_p504_e_2").val(results.cabecera.NUM_PESO_PROMEDIO_PIE);
                        $("#cap5_modb_p504_e_3").val(results.cabecera.NUM_PRECIO_PIE);
                        $("#cap5_modb_p504_f").val(results.cabecera.NUM_CANTIDAD_CONSUMO_HOGAR);
                        $("#cap5_modb_p504_g").val(results.cabecera.NUM_CANTIDAD_TRUEQUE);
                        $("#cap5_modb_p504_h").val(results.cabecera.NUM_CANTIDAD_MURIERON);
                        $("#cap5_modb_p504_i").val(results.cabecera.NUM_CANTIDAD_DERIVADO);
                        $("#cap5_modb_p504_j").val(results.cabecera.NUM_CANTIDAD_DONACION);
                        $("#cap5_modb_p506").val(results.cabecera.NUM_CANTIDAD_DIC_2020);
                        $("#cap5_modb_p508").val(results.cabecera.NUM_CANTIDAD_DIC_2021);

                        $.each(results.detalle_p505, function( index, value ) {
                            if(value.ID_LUGAR_VENTA){
                                $("#cap5_modb_p505_"+value.ID_LUGAR_VENTA).prop("checked", true);
                            }else{
                                $("#cap5_modb_p505_"+value.ID_LUGAR_VENTA).prop("checked", true);
                            }

                            if(value.ID_LUGAR_VENTA==11){
                                $("#cap5_modb_p505_11_especifique").val(value.TXT_OTRO);
                                $(".cap5_modb_p505_11").show()
                            }
                        });

                        $.each(results.detalle_p507, function( index, value ) {
                            if(value.ID_CAUSA_MUERTE){
                                $("#cap5_modb_p507_"+value.ID_CAUSA_MUERTE).prop("checked", true);
                            }else{
                                $("#cap5_modb_p507_"+value.ID_CAUSA_MUERTE).prop("checked", true);
                            }

                            if(value.ID_CAUSA_MUERTE==10){
                                $("#cap5_modb_p507_10_especifique").val(value.TXT_OTRO);
                                $(".cap5_modb_p507_10").show()
                            }
                        });

                    }
                    setTimeout(function(){ loading.modal("hide"); }, 2000);
                },
                error:function(){
                    alert("Error al realizar el proceso.");
                }
            });
}


$("body").on("change", "#cap5_modb_p505_11", function (e) {
    e.preventDefault();
    $(".cap5_modb_p505_11").hide();
    $("#cap5_modb_p505_11_especifique").val("");
    if($("[id=\"cap5_modb_p505_11\"]:checked").val()){
        $(".cap5_modb_p505_11").show()
    }
});

$("body").on("change", "#cap5_modb_p507_10", function (e) {
    e.preventDefault();
    $(".cap5_modb_p507_10").hide();
    $("#cap5_modb_p507_10_especifique").val("");
    if($("[id=\"cap5_modb_p507_10\"]:checked").val()){
        $(".cap5_modb_p507_10").show()
    }
});
</script>