

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Encuesta</h4>
                </div>
            </div>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Parcelas</li>
                </ol>
            </div>

        </div>
    </div>
</div>

<?php $form = ActiveForm::begin(['options' => ['id' => 'formCapituloVModuloH']]); ?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
            
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">525. ¿Cuánto fue la producción total de: ……….. ? </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">525a. Cantidad total</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloH[cap5_modh_p525_a]" id="cap5_modh_p525_a" maxlength="9">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">525b. Unidad de medida</label>
                            <select name="CapituloVModuloH[cap5_modh_p525_b]" id="cap5_modh_p525_b" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">525c. Equivalencia en kg/Lt</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloH[cap5_modh_p525_c]" id="cap5_modh_p525_c" maxlength="9">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">525d. Producción total en kg</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloH[cap5_modh_p525_d]" id="cap5_modh_p525_d" maxlength="9">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">526. ¿Cuántos animales (o colmenas) dieron esa producción?</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloH[cap5_modh_p526]" id="cap5_modh_p526" maxlength="12">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">526a. Rendimiento</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloH[cap5_modh_p526_a]" id="cap5_modh_p526_a" maxlength="12">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for=""> 527. ¿Cuánto se destinó a la venta? </label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">527a. Cantidad total</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloH[cap5_modh_p527_a]" id="cap5_modh_p527_a" maxlength="9">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">527b. Unidad de Medida</label>
                            <select name="CapituloVModuloH[cap5_modh_p527_b]" id="cap5_modh_p527_b" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">527c. Precio de Venta por kg/Lt</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloH[cap5_modh_p527_c]" id="cap5_modh_p527_c" maxlength="9">
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for=""> 528. De la producción de ... ¿Cuánto se destinó a consumo del hogar?</label>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">528a. Cantidad total</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloH[cap5_modh_p528_a]" id="cap5_modh_p528_a" maxlength="9">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">528b. Unidad de Medida</label>
                            <select name="CapituloVModuloH[cap5_modh_p528_b]" id="cap5_modh_p528_b" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">528c. Equivalencia en kg/Lt</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloH[cap5_modh_p528_c]" id="cap5_modh_p528_c" maxlength="9">
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for=""> 529. De la producción de ¿Cuánto se destinó a la produccion de derivados?</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">529a. Cantidad total</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloH[cap5_modh_p529_a]" id="cap5_modh_p529_a" maxlength="9">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">529b. Unidad de Medida</label>
                            <select name="CapituloVModuloH[cap5_modh_p529_b]" id="cap5_modh_p529_b" class="form-control">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">529c. Equivalencia en kg/Lt</label>
                            <input type="text" class="form-control numerico" name="CapituloVModuloH[cap5_modh_p529_c]" id="cap5_modh_p529_c" maxlength="9">
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<?php ActiveForm::end(); ?>

<script>
$('input,select,textarea').prop('disabled',true)
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var loading =   $('#staticBackdrop');
var id_encuesta = "<?= $id_encuesta ?>";
var id_producto = "<?= $id_producto ?>";

$("body").on("click", ".btn-grabar-capitulo-v-modulo-h", function (e) {

    e.preventDefault();



    var form = $("#formCapituloVModuloH");
    var formData = $("#formCapituloVModuloH").serializeArray();


    if (form.find(".has-error").length) {
        return false;
    }

    $.ajax({
        url:form.attr("action"),
        type: form.attr("method"),
        data: formData,
        dataType: "json",
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        beforeSend:function()
        {
            loading.modal("show");
        },
        success: function (results) {
            if(results.success){
                setTimeout(function(){ loading.modal("hide"); }, 2000);
            }
        },
    });
});
InformacionGeneral();
async function InformacionGeneral(){
    await   $.ajax({
                url: "<?= \Yii::$app->request->BaseUrl ?>/capitulov/get-modulo-h",
                method: "POST",
                data:{_csrf:csrf,id_encuesta:id_encuesta,id_producto:id_producto},
                dataType:"Json",
                beforeSend:function(xhr, settings)
                {
                    loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        console.log(results.cabecera.ID_UM_PRODUCCION);

                        UnidadesMedidasPesos(results.cabecera.ID_UM_PRODUCCION,results.cabecera.ID_UM_VENTA,results.cabecera.ID_UM_CONSUMO,results.cabecera.ID_UM_DERIVADOS)

                        $("#cap5_modh_p525_a").val(results.cabecera.NUM_CANTIDAD_PRODUCCION);
                        //$("#cap5_modh_p525_b").val(results.cabecera.ID_UM_PRODUCCION);
                        $("#cap5_modh_p525_c").val(results.cabecera.NUM_EQ_PRODUCCION);
                        $("#cap5_modh_p525_d").val(results.cabecera.NUM_CANTIDAD_PRODUCCION_TOTAL);
                        $("#cap5_modh_p526").val(results.cabecera.NUM_CANTIDAD_ANIMALES);
                        $("#cap5_modh_p526_a").val(results.cabecera.NUM_RENDIMIENTO);
                        $("#cap5_modh_p527_a").val(results.cabecera.NUM_PRODUCCION_VENTA);
                        //$("#cap5_modh_p527_b").val(results.cabecera.ID_UM_VENTA);
                        $("#cap5_modh_p527_c").val(results.cabecera.NUM_PRECIO_VENTA);
                        $("#cap5_modh_p528_a").val(results.cabecera.NUM_PRODUCCION_CONSUMO);
                        //$("#cap5_modh_p528_b").val(results.cabecera.ID_UM_CONSUMO);
                        $("#cap5_modh_p528_c").val(results.cabecera.NUM_EQ_CONSUMO);
                        $("#cap5_modh_p529_a").val(results.cabecera.NUM_PRODUCCION_DERIVADOS);
                        //$("#cap5_modh_p529_b").val(results.cabecera.ID_UM_DERIVADOS);
                        $("#cap5_modh_p529_c").val(results.cabecera.NUM_EQ_DERIVADOS);
                    }
                    setTimeout(function(){ loading.modal("hide"); }, 2000);
                },
                error:function(){
                    alert("Error al realizar el proceso.");
                }
            });
}




async function UnidadesMedidasPesos(p525,p527,p528,p529){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/unidad-medida/get-lista-unidades-medidas-pecuario',
                method: 'POST',
                data:{_csrf:csrf},
                dataType:'Json',
                beforeSend:function(){
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var opciones_unidades_medidas ="<option value>Seleccionar</option>";
                        $.each(results.unidades_medidas, function( index, value ) {
                            
                            opciones_unidades_medidas = opciones_unidades_medidas + "<option value='" + value.ID_UM + "'>" + value.TXT_DESCRIPCION + "</option>";
                        });
                        $("#cap5_modh_p525_b").html(opciones_unidades_medidas);
                        $("#cap5_modh_p527_b").html(opciones_unidades_medidas);
                        $("#cap5_modh_p528_b").html(opciones_unidades_medidas);
                        $("#cap5_modh_p529_b").html(opciones_unidades_medidas);
                        console.log(p525);
                        if(p525){
                            
                            $("#cap5_modh_p525_b").val(p525);
                        }

                        if(p527){
                            $("#cap5_modh_p527_b").val(p527);
                        }

                        if(p528){
                            $("#cap5_modh_p528_b").val(p528);
                        }

                        if(p529){
                            $("#cap5_modh_p529_b").val(p529);
                        }

                        
                        //loading.modal('hide');
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}


</script>