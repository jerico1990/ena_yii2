

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Encuesta</h4>
                    <a class="btn btn-primary" href="<?= \Yii::$app->request->BaseUrl ?>/encuesta/index?geocodigo_fdo=<?= $geocodigo_fdo ?>">< Regresar</a>
                </div>
            </div>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Parcelas</li>
                </ol>
            </div>

        </div>
    </div>
</div>
<!-- end page title -->
<?php $form = ActiveForm::begin(['options' => ['id' => 'formCapituloIVModuloA']]); ?>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">401a. N° Orden de lote</label>
                            <input type="text" class="form-control numerico" name="CapituloIVModuloA[cap4_moda_p401_a]" id="cap4_moda_p401_a" maxlength="5">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">401. ¿El cultivo que va a registrar es:</label>
                            <select class="form-control" name="CapituloIVModuloA[cap4_moda_p401]" id="cap4_moda_p401">
                                <option value="99999">Seleccionar</option>
                                <option value="32">1. Presente?</option>
                                <option value="33">2. Pasado?</option>
                                <option value="34">3. Futuro?</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">402. ¿Cuál es el nombre y variedad del cultivo?</label>
                            <select class="form-control" name="CapituloIVModuloA[cap4_moda_p402]" id="cap4_moda_p402">
                                <option value="99999">Seleccionar</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-4 cap4_moda_p402_9999" style="display:none">
                        <div class="form-group">
                            <label for="">Especifique</label>
                            <input type="text" class="form-control" id="cap4_moda_p402_9999_especifique" placeholder="Especifique" name="CapituloIVModuloA[cap4_moda_p402_9999_especifique]" maxlength="250">
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">403. ¿Cuál es la fecha de siembra o plantación en este lote?</label>
                            <input type="text" class="form-control numerico" name="CapituloIVModuloA[cap4_moda_p403_mes]" id="cap4_moda_p403_mes" maxlength="2">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">&nbsp;&nbsp</label> <br>
                            <input type="text" class="form-control numerico" name="CapituloIVModuloA[cap4_moda_p403_anio]" id="cap4_moda_p403_anio" maxlength="4">
                        </div>
                    </div>
                </div>
                <div class="row">
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">404. ¿El cultivo o plantación está/estuvo/estará:</label>
                            <select class="form-control" name="CapituloIVModuloA[cap4_moda_p404]" id="cap4_moda_p404">
                                <option value="99999">Seleccionar</option>
                                <option value="35">1. Solo?</option>
                                <option value="36">2. Asociado?</option>
                            </select>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">405. ¿Cuánta es el área sembrada o plantada?</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">405.a Cantidad</label>
                            <input type="text" class="form-control numerico" name="CapituloIVModuloA[cap4_moda_p405_a]" id="cap4_moda_p405_a" maxlength="9">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">405.b Unidad de medida</label>
                            <select class="form-control" name="CapituloIVModuloA[cap4_moda_p405_b]" id="cap4_moda_p405_b">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">405.c Equivalencia en metros cuadrados</label>
                            <input type="text" class="form-control numerico" name="CapituloIVModuloA[cap4_moda_p405_c]" id="cap4_moda_p405_c" maxlength="9">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">406. ¿Qué cantidad de plantones plantó/plantará en este lote?</label>
                            <input type="text" class="form-control numerico" name="CapituloIVModuloA[cap4_moda_p406]" id="cap4_moda_p406" maxlength="15">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <label for="">407. El origen de las semillas o plantones que usa es: </label>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p407_1" value="1">
                                    <label class="custom-control-label" for="cap4_moda_p407_1"> 1. ¿Propio?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p407_2" value="2">
                                    <label class="custom-control-label" for="cap4_moda_p407_2"> 2. ¿Familiar o amigo?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p407_3" value="3">
                                    <label class="custom-control-label" for="cap4_moda_p407_3"> 3. ¿Tienda agroinsumos?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p407_4" value="4">
                                    <label class="custom-control-label" for="cap4_moda_p407_4"> 4. ¿Empresa?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p407_5" value="5">
                                    <label class="custom-control-label" for="cap4_moda_p407_5">  5.  ¿Feria?</label>
                                </div>

                            </div>

                            <div class="col-md-6">
                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p407_6" value="6">
                                    <label class="custom-control-label" for="cap4_moda_p407_6"> 6. ¿Productor de semilla registrada?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p407_7" value="7">
                                    <label class="custom-control-label" for="cap4_moda_p407_7"> 7. ¿INIA?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p407_8" value="8">
                                    <label class="custom-control-label" for="cap4_moda_p407_8"> 8. ¿Universidad?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p407_9" value="9">
                                    <label class="custom-control-label" for="cap4_moda_p407_9"> 9.  ¿Importado?</label>
                                </div>
                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p407_10" value="10">
                                    <label class="custom-control-label" for="cap4_moda_p407_10"> 10. ¿Otro?:</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row cap4_moda_p407_10" style="display:none">
                    <div class="col-md-6" style="padding-left:0px">
                        <div class="form-group">
                            <label for="">Especifique</label>
                            <input type="text" class="form-control" id="cap4_moda_p407_10_especifique" placeholder="Especifique" name="CapituloIVModuloA[cap4_moda_p407_10_especifique]" maxlength="500">
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12">
                        <label for="">408. ¿Qué sistemas de riego utiliza?</label>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p408_1" value="1">
                                    <label class="custom-control-label" for="cap4_moda_p408_1">1. Gravedad</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p408_2" value="2">
                                    <label class="custom-control-label" for="cap4_moda_p408_2">2. Exudación</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p408_3" value="3">
                                    <label class="custom-control-label" for="cap4_moda_p408_3">3. Goteo</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p408_4" value="4">
                                    <label class="custom-control-label" for="cap4_moda_p408_4">4. Microaspersión</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p408_5" value="5">
                                    <label class="custom-control-label" for="cap4_moda_p408_5">5. Aspersión</label>
                                </div>

                            </div>

                            <div class="col-md-6">
                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p408_6" value="6">
                                    <label class="custom-control-label" for="cap4_moda_p408_6">6. Multicompuertas</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p408_7" value="7">
                                    <label class="custom-control-label" for="cap4_moda_p408_7">7. Pivot</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p408_8" value="8">
                                    <label class="custom-control-label" for="cap4_moda_p408_8">8. Manga</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p408_9" value="9">
                                    <label class="custom-control-label" for="cap4_moda_p408_9">9. Otro</label>
                                </div>
                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p408_10" value="10">
                                    <label class="custom-control-label" for="cap4_moda_p408_10">10. Sin riego (secano)</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row cap4_moda_p408_9" style="display:none">
                    <div class="col-md-6" style="padding-left:0px">
                        <div class="form-group">
                            <label for="">Especifique</label>
                            <input type="text" class="form-control" id="cap4_moda_p408_9_especifique" placeholder="Especifique" name="CapituloIVModuloA[cap4_moda_p408_9_especifique]" maxlength="500">
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">409. ¿Cuál es la finalidad de la plantación forestal?	</label>
                            <select class="form-control" name="CapituloIVModuloA[cap4_moda_p409]" id="cap4_moda_p409">
                                <option value="99999">Seleccionar</option>
                                <option value="37">1. ¿Producción?</option>
                                <option value="38">2. ¿Protección?</option>
                                <option value="39">3. ¿Rodal Semillero?</option>
                            </select>
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">410.a ¿Cuál es la fecha de inicio de cosecha?</label>
                            <input type="text" class="form-control numerico" name="CapituloIVModuloA[cap4_moda_p410_a_mes]" id="cap4_moda_p410_a_mes" maxlength="2">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for=""> &nbsp;&nbsp;</label> <br>
                            <input type="text" class="form-control numerico" name="CapituloIVModuloA[cap4_moda_p410_a_anio]" id="cap4_moda_p410_a_anio" maxlength="4">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">410.b ¿Cuál es la fecha final de cosecha?</label>
                            <input type="text" class="form-control numerico" name="CapituloIVModuloA[cap4_moda_p410_b_mes]" id="cap4_moda_p410_b_mes" maxlength="2">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for=""> &nbsp;&nbsp;</label> <br>
                            <input type="text" class="form-control numerico" name="CapituloIVModuloA[cap4_moda_p410_b_anio]" id="cap4_moda_p410_b_anio" maxlength="4">
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12">
                        <label for="">411. ¿En qué meses del 2020 se cosechan los cultivos permanentes?</label>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p411_1" value="1">
                                    <label class="custom-control-label" for="cap4_moda_p411_1">Enero</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p411_2" value="2">
                                    <label class="custom-control-label" for="cap4_moda_p411_2">Febrero</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p411_3" value="3">
                                    <label class="custom-control-label" for="cap4_moda_p411_3">Marzo</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p411_4" value="4">
                                    <label class="custom-control-label" for="cap4_moda_p411_4">Abril</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p411_5" value="5">
                                    <label class="custom-control-label" for="cap4_moda_p411_5">Mayo</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p411_6" value="6">
                                    <label class="custom-control-label" for="cap4_moda_p411_6">Junio</label>
                                </div>

                            </div>

                            <div class="col-md-6">
                            <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p411_7" value="7">
                                    <label class="custom-control-label" for="cap4_moda_p411_7">Julio</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p411_8" value="8">
                                    <label class="custom-control-label" for="cap4_moda_p411_8">Agosto</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p411_9" value="9">
                                    <label class="custom-control-label" for="cap4_moda_p411_9">Setiembre</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p411_10" value="10">
                                    <label class="custom-control-label" for="cap4_moda_p411_10">Octubre</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p411_11" value="11">
                                    <label class="custom-control-label" for="cap4_moda_p411_11">Noviembre</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p411_12" value="12">
                                    <label class="custom-control-label" for="cap4_moda_p411_12">Diciembre</label>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">412. ¿Cuánto es el área cosechada?</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">412.a Cantidad</label>
                            <input type="text" class="form-control numerico" name="CapituloIVModuloA[cap4_moda_p412_a]" id="cap4_moda_p412_a" maxlength="10">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">412.b Unidad de medida</label>
                            <select class="form-control" name="CapituloIVModuloA[cap4_moda_p412_b]" id="cap4_moda_p412_b">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">412.c Equivalencia en metros cuadrados</label>
                            <input type="text" class="form-control numerico" name="CapituloIVModuloA[cap4_moda_p412_c]" id="cap4_moda_p412_c" maxlength="10">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">413. ¿Cuànta es el área perdida en porcentaje?</label>
                            <input type="text" class="form-control numerico" name="CapituloIVModuloA[cap4_moda_p413]" id="cap4_moda_p413" maxlength="15">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">414. ¿Cuànta es el área afectada en porcentaje?</label>
                            <input type="text" class="form-control numerico" name="CapituloIVModuloA[cap4_moda_p414]" id="cap4_moda_p414" maxlength="15">
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12">
                        <label for="">415. ¿Qué fenomenos naturales afectaron la producción?</label>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p415_1" value="1">
                                    <label class="custom-control-label" for="cap4_moda_p415_1">1. Sequía</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p415_2" value="2">
                                    <label class="custom-control-label" for="cap4_moda_p415_2">2. Bajas temperaturas</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p415_3" value="3">
                                    <label class="custom-control-label" for="cap4_moda_p415_3">3. Helada</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p415_4" value="4">
                                    <label class="custom-control-label" for="cap4_moda_p415_4">4. Granizada</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p415_5" value="5">
                                    <label class="custom-control-label" for="cap4_moda_p415_5">5. Friaje</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p415_6" value="6">
                                    <label class="custom-control-label" for="cap4_moda_p415_6">6. Inundación</label>
                                </div>

                            </div>

                            <div class="col-md-6">
                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p415_7" value="7">
                                    <label class="custom-control-label" for="cap4_moda_p415_7">7. Huaicos</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p415_8" value="8">
                                    <label class="custom-control-label" for="cap4_moda_p415_8">8. Lluvias a destiempo</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p415_9" value="9">
                                    <label class="custom-control-label" for="cap4_moda_p415_9">9. Movimiento de tierras</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p415_10" value="10">
                                    <label class="custom-control-label" for="cap4_moda_p415_10">10. Incendios</label>
                                </div>
                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p415_11" value="11">
                                    <label class="custom-control-label" for="cap4_moda_p415_11">11. Plagas y enfermedades</label>
                                </div>
                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p415_12" value="12">
                                    <label class="custom-control-label" for="cap4_moda_p415_12">12. Ninguna</label>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <label for="">416. ¿Qué factores económicos afectaron la producción?</label>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p416_40" value="40">
                                    <label class="custom-control-label" for="cap4_moda_p416_40">1. ¿Escasez de mano de obra?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p416_41" value="41">
                                    <label class="custom-control-label" for="cap4_moda_p416_41">2. ¿Falta de maquinaria?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p416_42" value="42">
                                    <label class="custom-control-label" for="cap4_moda_p416_42">3. ¿Precios bajos?</label>
                                </div>

                            </div>

                            <div class="col-md-6">
                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p416_43" value="43">
                                    <label class="custom-control-label" for="cap4_moda_p416_43">4. ¿Por dificultades en la comercialización?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p416_44" value="44">
                                    <label class="custom-control-label" for="cap4_moda_p416_44">5. Ninguna</label>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <label for="">417. ¿Qué actividades de mejoramiento realizó en los pastos en el 2020?</label>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p417_45" value="45">
                                    <label class="custom-control-label" for="cap4_moda_p417_45">1. ¿Deshierbo?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p417_46" value="46">
                                    <label class="custom-control-label" for="cap4_moda_p417_46">2. ¿Fertilización?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p417_47" value="47">
                                    <label class="custom-control-label" for="cap4_moda_p417_47">3. ¿Abonamiento?</label>
                                </div>

                            </div>

                            <div class="col-md-6">
                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p417_48" value="48">
                                    <label class="custom-control-label" for="cap4_moda_p417_48">4. ¿Resiembra?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p417_49" value="49">
                                    <label class="custom-control-label" for="cap4_moda_p417_49">5. Ninguno</label>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">418. Principalmente la cosecha de los pastos sembrados fue:</label>
                            <select class="form-control" name="CapituloIVModuloA[cap4_moda_p418]" id="cap4_moda_p418">
                                <option value="99999">Seleccionar</option>
                                <option value="50">1. ¿Por corte?</option>
                                <option value="51">2.¿ Pastoreo?</option>
                            </select>
                        </div>
                    </div>
                    
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">419. ¿Cuál fue la producción de ….......... entre ………… y ………...?</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">419.a Cantidad</label>
                            <input type="text" class="form-control numerico" name="CapituloIVModuloA[cap4_moda_p419_a]" id="cap4_moda_p419_a" maxlength="10">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">419.b Unidad de medida</label>
                            <select class="form-control" name="CapituloIVModuloA[cap4_moda_p419_b]" id="cap4_moda_p419_b">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">419.c Equivalencia en (Kg)</label>
                            <input type="text" class="form-control numerico" name="CapituloIVModuloA[cap4_moda_p419_c]" id="cap4_moda_p419_c" maxlength="10">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">419.d ¿Cuál es la especificación del producto? </label>
                            <select class="form-control" name="CapituloIVModuloA[cap4_moda_p419_d]" id="cap4_moda_p419_d">
                                <option value>Seleccionar</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">419.e ¿Cuánta es  la producción en el 2020?</label>
                            <input type="text" class="form-control numerico" name="CapituloIVModuloA[cap4_moda_p419_e]" id="cap4_moda_p419_e" maxlength="10">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">420. Rendimiento (kg/ha)</label>
                            <input type="text" class="form-control numerico" name="CapituloIVModuloA[cap4_moda_p420]" id="cap4_moda_p420" maxlength="10">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">421. ¿Su producción fue por calidad?</label>
                            <select class="form-control" name="CapituloIVModuloA[cap4_moda_p421]" id="cap4_moda_p421">
                                <option value>Seleccionar</option>
                                <option value="1">Si</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                    </div>
                    
                </div>
                
                 <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">422. ¿Cuántas calidades obtuvo?</label>
                            <input type="text" class="form-control numerico" name="CapituloIVModuloA[cap4_moda_p422]" id="cap4_moda_p422" maxlength="10">
                        </div>
                    </div>
                </div>

                <div class="row">
                        <div class="col-md-12 mb-2" style="padding-left:0px">
                            <a class="btn btn-success disabled" target="_blank" href="#">Agregar</a>
                        </div>
                        <div class="col-md-12" style="padding-left:0px">
                        
                            <table id="lista-cap4-moda-calidad" class="table">
                                <thead>
                                    <th>425.a Cantidad</th>
                                    <th>425.b Unidad de Medida</th>
                                    <th>425.c Especificación del producto</th>
                                    <th>425.d Precio Unidad de Medida (S/ x kg)</th>
                                    <th>Acciones</th>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>



                <div class="row">
                    <div class="col-md-12">
                        <label for="">427. ¿Por qué siembra este cultivo?</label>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p427_1" value="1">
                                    <label class="custom-control-label" for="cap4_moda_p427_1">1. ¿Tradición?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p427_2" value="2">
                                    <label class="custom-control-label" for="cap4_moda_p427_2">2. ¿Siempre hay compradores?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p427_3" value="3">
                                    <label class="custom-control-label" for="cap4_moda_p427_3">3. ¿Buenos precios?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p427_4" value="4">
                                    <label class="custom-control-label" for="cap4_moda_p427_4">4. ¿Precios estables?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p427_5" value="5">
                                    <label class="custom-control-label" for="cap4_moda_p427_5">5. ¿Recomendación de la comunidad?</label>
                                </div>

                            </div>

                            <div class="col-md-6">
                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p427_6" value="6">
                                    <label class="custom-control-label" for="cap4_moda_p427_6">6. ¿Recomendación del proveedor agroinsumos?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p427_7" value="7">
                                    <label class="custom-control-label" for="cap4_moda_p427_7">7. ¿Autoridades agropecuarias?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p427_8" value="8">
                                    <label class="custom-control-label" for="cap4_moda_p427_8">8. ¿Proyecto de la Región?</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p427_9" value="9">
                                    <label class="custom-control-label" for="cap4_moda_p427_9">9. ¿Acceso a mecanismos de articulación comercial MIDAGRI</label>
                                </div>
                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_moda_p427_10" value="10">
                                    <label class="custom-control-label" for="cap4_moda_p427_10">10. ¿Otra?</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row cap4_moda_p427_10" style="display:none">
                    <div class="col-md-6" >
                        <div class="form-group">
                            <label for="">Especifique</label>
                            <input type="text" class="form-control" id="cap4_moda_p427_10_especifique" placeholder="Especifique" name="CapituloIVModuloA[cap4_moda_p427_10_especifique]" maxlength="500">
                        </div>
                    </div>
                </div>

                <hr>
                <button class="btn btn-success btn-grabar">Grabar</button>

            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<?php ActiveForm::end(); ?>

<div id="modal" class="fade modal" role="dialog"  tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        </div>
    </div>
</div>


<script>

var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var loading =   $('#staticBackdrop');



$('#lista-cap4-moda-calidad').DataTable();

Especificaciones();
async function Especificaciones(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/cultivo/get-lista-especificaciones',
                method: 'POST',
                data:{_csrf:csrf},
                dataType:'Json',
                beforeSend:function(){
                    loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var opciones_especificaciones ="<option value>Seleccionar</option>";
                        $.each(results.especificaciones, function( index, value ) {
                            opciones_especificaciones = opciones_especificaciones + "<option value='" + value.ID_ESPECIFICACION + "'>" + value.TXT_ESPECIFICACION + "</option>";
                        });
                        $("#cap4_moda_p419_d").html(opciones_especificaciones);
                        setTimeout(function(){ loading.modal("hide"); }, 2000);
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}


UnidadesMedidasAreas();
async function UnidadesMedidasAreas(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/unidad-medida/get-lista-unidades-medidas-areas',
                method: 'POST',
                data:{_csrf:csrf},
                dataType:'Json',
                beforeSend:function(){
                    loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var opciones_unidades_medidas ="<option value>Seleccionar</option>";
                        $.each(results.unidades_medidas, function( index, value ) {
                            
                            opciones_unidades_medidas = opciones_unidades_medidas + "<option value='" + value.ID_UM + "'>" + value.TXT_DESCRIPCION + "</option>";
                        });
                        $("#cap4_moda_p405_b").html(opciones_unidades_medidas);
                        $("#cap4_moda_p412_b").html(opciones_unidades_medidas);

                        //$("#cap4_moda_p419_b").html(opciones_unidades_medidas);
                        // $("#cap4_moda_p423_c").html(opciones_unidades_medidas);
                        // $("#cap4_moda_p425_b").html(opciones_unidades_medidas);
                        setTimeout(function(){ loading.modal("hide"); }, 2000);
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}
UnidadesMedidasPesos();
async function UnidadesMedidasPesos(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/unidad-medida/get-lista-unidades-medidas-pesos',
                method: 'POST',
                data:{_csrf:csrf},
                dataType:'Json',
                beforeSend:function(){
                    loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var opciones_unidades_medidas ="<option value>Seleccionar</option>";
                        $.each(results.unidades_medidas, function( index, value ) {
                            
                            opciones_unidades_medidas = opciones_unidades_medidas + "<option value='" + value.ID_UM + "'>" + value.TXT_DESCRIPCION + "</option>";
                        });

                        $("#cap4_moda_p419_b").html(opciones_unidades_medidas);
                        
                        setTimeout(function(){ loading.modal("hide"); }, 2000);
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}

Cultivos();
async function Cultivos(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/cultivo/get-lista-cultivos',
                method: 'POST',
                data:{_csrf:csrf},
                dataType:'Json',
                beforeSend:function(){
                    loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var opciones_cultivos ="<option value='99999'>Seleccionar</option>";
                        $.each(results.cultivos, function( index, value ) {
                            opciones_cultivos = opciones_cultivos + "<option value='" + value.ID_CULTIVO + "'>" + value.TXT_CULTIVO + "</option>";
                        });
                        $("#cap4_moda_p402").html(opciones_cultivos);
                        
                        setTimeout(function(){ loading.modal("hide"); }, 2000);
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}




$("body").on("click", ".btn-grabar", function (e) {

    e.preventDefault();

    var cap4_moda_p411_1   = ($("[id=\"cap4_moda_p411_1\"]:checked").val())?1:0;
    var cap4_moda_p411_2   = ($("[id=\"cap4_moda_p411_2\"]:checked").val())?1:0;
    var cap4_moda_p411_3   = ($("[id=\"cap4_moda_p411_3\"]:checked").val())?1:0;
    var cap4_moda_p411_4   = ($("[id=\"cap4_moda_p411_4\"]:checked").val())?1:0;
    var cap4_moda_p411_5   = ($("[id=\"cap4_moda_p411_5\"]:checked").val())?1:0;
    var cap4_moda_p411_6   = ($("[id=\"cap4_moda_p411_6\"]:checked").val())?1:0;
    var cap4_moda_p411_7   = ($("[id=\"cap4_moda_p411_7\"]:checked").val())?1:0;
    var cap4_moda_p411_8   = ($("[id=\"cap4_moda_p411_8\"]:checked").val())?1:0;
    var cap4_moda_p411_9   = ($("[id=\"cap4_moda_p411_9\"]:checked").val())?1:0;
    var cap4_moda_p411_10   = ($("[id=\"cap4_moda_p411_10\"]:checked").val())?1:0;
    var cap4_moda_p411_11   = ($("[id=\"cap4_moda_p411_11\"]:checked").val())?1:0;
    var cap4_moda_p411_12   = ($("[id=\"cap4_moda_p411_12\"]:checked").val())?1:0;

    var cap4_moda_p407 = "";
    var cap4_moda_p407_1   = ($("[id=\"cap4_moda_p407_1\"]:checked").val())?1:0;
    var cap4_moda_p407_2   = ($("[id=\"cap4_moda_p407_2\"]:checked").val())?2:0;
    var cap4_moda_p407_3   = ($("[id=\"cap4_moda_p407_3\"]:checked").val())?3:0;
    var cap4_moda_p407_4   = ($("[id=\"cap4_moda_p407_4\"]:checked").val())?4:0;
    var cap4_moda_p407_5   = ($("[id=\"cap4_moda_p407_5\"]:checked").val())?5:0;
    var cap4_moda_p407_6   = ($("[id=\"cap4_moda_p407_6\"]:checked").val())?6:0;
    var cap4_moda_p407_7   = ($("[id=\"cap4_moda_p407_7\"]:checked").val())?7:0;
    var cap4_moda_p407_8   = ($("[id=\"cap4_moda_p407_8\"]:checked").val())?8:0;
    var cap4_moda_p407_9   = ($("[id=\"cap4_moda_p407_9\"]:checked").val())?9:0;
    var cap4_moda_p407_10   = ($("[id=\"cap4_moda_p407_10\"]:checked").val())?10:0;

    var cap4_moda_p407_lista = [cap4_moda_p407_1,cap4_moda_p407_2,cap4_moda_p407_3,cap4_moda_p407_4,cap4_moda_p407_5,cap4_moda_p407_6,cap4_moda_p407_7,cap4_moda_p407_8,cap4_moda_p407_9,cap4_moda_p407_10];
    var cap4_moda_p407_contador = 0;
    $.each(cap4_moda_p407_lista, function( index, value ) {
        if(value!=0){
            if(cap4_moda_p407_contador==0){
                cap4_moda_p407 = cap4_moda_p407 + value;
            }else{
                cap4_moda_p407 = cap4_moda_p407 + "," + value;
            }
            cap4_moda_p407_contador++;
        }
    });

    var cap4_moda_p408 = "";
    var cap4_moda_p408_1   = ($("[id=\"cap4_moda_p408_1\"]:checked").val())?1:0;
    var cap4_moda_p408_2   = ($("[id=\"cap4_moda_p408_2\"]:checked").val())?2:0;
    var cap4_moda_p408_3   = ($("[id=\"cap4_moda_p408_3\"]:checked").val())?3:0;
    var cap4_moda_p408_4   = ($("[id=\"cap4_moda_p408_4\"]:checked").val())?4:0;
    var cap4_moda_p408_5   = ($("[id=\"cap4_moda_p408_5\"]:checked").val())?5:0;
    var cap4_moda_p408_6   = ($("[id=\"cap4_moda_p408_6\"]:checked").val())?6:0;
    var cap4_moda_p408_7   = ($("[id=\"cap4_moda_p408_7\"]:checked").val())?7:0;
    var cap4_moda_p408_8   = ($("[id=\"cap4_moda_p408_8\"]:checked").val())?8:0;
    var cap4_moda_p408_9   = ($("[id=\"cap4_moda_p408_9\"]:checked").val())?9:0;
    var cap4_moda_p408_10   = ($("[id=\"cap4_moda_p408_10\"]:checked").val())?10:0;

    var cap4_moda_p408_lista = [cap4_moda_p408_1,cap4_moda_p408_2,cap4_moda_p408_3,cap4_moda_p408_4,cap4_moda_p408_5,cap4_moda_p408_6,cap4_moda_p408_7,cap4_moda_p408_8,cap4_moda_p408_9,cap4_moda_p408_10];
    var cap4_moda_p408_contador = 0;
    $.each(cap4_moda_p408_lista, function( index, value ) {
        if(value!=0){
            if(cap4_moda_p408_contador==0){
                cap4_moda_p408 = cap4_moda_p408 + value;
            }else{
                cap4_moda_p408 = cap4_moda_p408 + "," + value;
            }
            cap4_moda_p408_contador++;
        }
    });


    var cap4_moda_p415 = "";
    var cap4_moda_p415_1   = ($("[id=\"cap4_moda_p415_1\"]:checked").val())?1:0;
    var cap4_moda_p415_2   = ($("[id=\"cap4_moda_p415_2\"]:checked").val())?2:0;
    var cap4_moda_p415_3   = ($("[id=\"cap4_moda_p415_3\"]:checked").val())?3:0;
    var cap4_moda_p415_4   = ($("[id=\"cap4_moda_p415_4\"]:checked").val())?4:0;
    var cap4_moda_p415_5   = ($("[id=\"cap4_moda_p415_5\"]:checked").val())?5:0;
    var cap4_moda_p415_6   = ($("[id=\"cap4_moda_p415_6\"]:checked").val())?6:0;
    var cap4_moda_p415_7   = ($("[id=\"cap4_moda_p415_7\"]:checked").val())?7:0;
    var cap4_moda_p415_8   = ($("[id=\"cap4_moda_p415_8\"]:checked").val())?8:0;
    var cap4_moda_p415_9   = ($("[id=\"cap4_moda_p415_9\"]:checked").val())?9:0;
    var cap4_moda_p415_10   = ($("[id=\"cap4_moda_p415_10\"]:checked").val())?10:0;
    var cap4_moda_p415_11   = ($("[id=\"cap4_moda_p415_11\"]:checked").val())?11:0;
    var cap4_moda_p415_12   = ($("[id=\"cap4_moda_p415_12\"]:checked").val())?12:0;

    var cap4_moda_p415_lista = [cap4_moda_p415_1,cap4_moda_p415_2,cap4_moda_p415_3,cap4_moda_p415_4,cap4_moda_p415_5,cap4_moda_p415_6,cap4_moda_p415_7,cap4_moda_p415_8,cap4_moda_p415_9,cap4_moda_p415_10,cap4_moda_p415_11,cap4_moda_p415_12];
    var cap4_moda_p415_contador = 0;
    $.each(cap4_moda_p415_lista, function( index, value ) {
        if(value!=0){
            if(cap4_moda_p415_contador==0){
                cap4_moda_p415 = cap4_moda_p415 + value;
            }else{
                cap4_moda_p415 = cap4_moda_p415 + "," + value;
            }
            cap4_moda_p415_contador++;
        }
    });

    var cap4_moda_p416 = "";
    var cap4_moda_p416_40   = ($("[id=\"cap4_moda_p416_40\"]:checked").val())?40:0;
    var cap4_moda_p416_41   = ($("[id=\"cap4_moda_p416_41\"]:checked").val())?41:0;
    var cap4_moda_p416_42   = ($("[id=\"cap4_moda_p416_42\"]:checked").val())?42:0;
    var cap4_moda_p416_43   = ($("[id=\"cap4_moda_p416_43\"]:checked").val())?43:0;
    var cap4_moda_p416_44   = ($("[id=\"cap4_moda_p416_44\"]:checked").val())?44:0;

    var cap4_moda_p416_lista = [cap4_moda_p416_40,cap4_moda_p416_41,cap4_moda_p416_42,cap4_moda_p416_43,cap4_moda_p416_44];
    var cap4_moda_p416_contador = 0;
    $.each(cap4_moda_p416_lista, function( index, value ) {
        if(value!=0){
            if(cap4_moda_p416_contador==0){
                cap4_moda_p416 = cap4_moda_p416 + value;
            }else{
                cap4_moda_p416 = cap4_moda_p416 + "," + value;
            }
            cap4_moda_p416_contador++;
        }
    });

    var cap4_moda_p417 = "";
    var cap4_moda_p417_45   = ($("[id=\"cap4_moda_p417_45\"]:checked").val())?45:0;
    var cap4_moda_p417_46   = ($("[id=\"cap4_moda_p417_46\"]:checked").val())?46:0;
    var cap4_moda_p417_47   = ($("[id=\"cap4_moda_p417_47\"]:checked").val())?47:0;
    var cap4_moda_p417_48   = ($("[id=\"cap4_moda_p417_48\"]:checked").val())?48:0;
    var cap4_moda_p417_49   = ($("[id=\"cap4_moda_p417_49\"]:checked").val())?49:0;

    var cap4_moda_p417_lista = [cap4_moda_p417_45,cap4_moda_p417_46,cap4_moda_p417_47,cap4_moda_p417_48,cap4_moda_p417_49];
    var cap4_moda_p417_contador = 0;
    $.each(cap4_moda_p417_lista, function( index, value ) {
        if(value!=0){
            if(cap4_moda_p417_contador==0){
                cap4_moda_p417 = cap4_moda_p417 + value;
            }else{
                cap4_moda_p417 = cap4_moda_p417 + "," + value;
            }
            cap4_moda_p417_contador++;
        }
    });

    var cap4_moda_p427 = "";
    var cap4_moda_p427_1   = ($("[id=\"cap4_moda_p427_1\"]:checked").val())?1:0;
    var cap4_moda_p427_2   = ($("[id=\"cap4_moda_p427_2\"]:checked").val())?2:0;
    var cap4_moda_p427_3   = ($("[id=\"cap4_moda_p427_3\"]:checked").val())?3:0;
    var cap4_moda_p427_4   = ($("[id=\"cap4_moda_p427_4\"]:checked").val())?4:0;
    var cap4_moda_p427_5   = ($("[id=\"cap4_moda_p427_5\"]:checked").val())?5:0;
    var cap4_moda_p427_6   = ($("[id=\"cap4_moda_p427_6\"]:checked").val())?6:0;
    var cap4_moda_p427_7   = ($("[id=\"cap4_moda_p427_7\"]:checked").val())?7:0;
    var cap4_moda_p427_8   = ($("[id=\"cap4_moda_p427_8\"]:checked").val())?8:0;
    var cap4_moda_p427_9   = ($("[id=\"cap4_moda_p427_9\"]:checked").val())?9:0;
    var cap4_moda_p427_10   = ($("[id=\"cap4_moda_p427_10\"]:checked").val())?10:0;

    var cap4_moda_p427_lista = [cap4_moda_p427_1,cap4_moda_p427_2,cap4_moda_p427_3,cap4_moda_p427_4,cap4_moda_p427_5,cap4_moda_p427_6,cap4_moda_p427_7,cap4_moda_p427_8,cap4_moda_p427_9,cap4_moda_p427_10];
    var cap4_moda_p427_contador = 0;
    $.each(cap4_moda_p427_lista, function( index, value ) {
        if(value!=0){
            if(cap4_moda_p427_contador==0){
                cap4_moda_p427 = cap4_moda_p427 + value;
            }else{
                cap4_moda_p427 = cap4_moda_p427 + "," + value;
            }
            cap4_moda_p427_contador++;
        }
    });



    var form = $("#formCapituloIVModuloA");
    var formData = $("#formCapituloIVModuloA").serializeArray();

    /* Seteando variables del capitulo IV Modulo A */
    formData.push({name: "CapituloIVModuloA[cap4_moda_p411_1]", value: cap4_moda_p411_1});
    formData.push({name: "CapituloIVModuloA[cap4_moda_p411_2]", value: cap4_moda_p411_2});
    formData.push({name: "CapituloIVModuloA[cap4_moda_p411_3]", value: cap4_moda_p411_3});
    formData.push({name: "CapituloIVModuloA[cap4_moda_p411_4]", value: cap4_moda_p411_4});
    formData.push({name: "CapituloIVModuloA[cap4_moda_p411_5]", value: cap4_moda_p411_5});
    formData.push({name: "CapituloIVModuloA[cap4_moda_p411_6]", value: cap4_moda_p411_6});
    formData.push({name: "CapituloIVModuloA[cap4_moda_p411_7]", value: cap4_moda_p411_7});
    formData.push({name: "CapituloIVModuloA[cap4_moda_p411_8]", value: cap4_moda_p411_8});
    formData.push({name: "CapituloIVModuloA[cap4_moda_p411_9]", value: cap4_moda_p411_9});
    formData.push({name: "CapituloIVModuloA[cap4_moda_p411_10]", value: cap4_moda_p411_10});
    formData.push({name: "CapituloIVModuloA[cap4_moda_p411_11]", value: cap4_moda_p411_11});
    formData.push({name: "CapituloIVModuloA[cap4_moda_p411_12]", value: cap4_moda_p411_12});

    formData.push({name: "CapituloIVModuloA[cap4_moda_p407]", value: cap4_moda_p407});
    formData.push({name: "CapituloIVModuloA[cap4_moda_p408]", value: cap4_moda_p408});
    formData.push({name: "CapituloIVModuloA[cap4_moda_p415]", value: cap4_moda_p415});
    formData.push({name: "CapituloIVModuloA[cap4_moda_p416]", value: cap4_moda_p416});
    formData.push({name: "CapituloIVModuloA[cap4_moda_p417]", value: cap4_moda_p417});
    //formData.push({name: "CapituloIVModuloA[cap4_moda_p422_calidad]", value: JSON.stringify(calidadListaJson)});
    formData.push({name: "CapituloIVModuloA[cap4_moda_p427]", value: cap4_moda_p427});

    

    

    if (form.find(".has-error").length) {
        return false;
    }

    $.ajax({
        url:form.attr("action"),
        type: form.attr("method"),
        data: formData,
        dataType: "json",
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        beforeSend:function()
        {
            loading.modal("show");
        },
        success: function (results) {
            if(results.success){
                window.location.href = "<?= \Yii::$app->request->BaseUrl ?>/capituloiv/update-capitulo-iv-modulo-a?id_cap_iv_a="+results.id_cap_iv_a+"&geocodigo_fdo=<?= $geocodigo_fdo ?>";
                //loading.modal("hide");
            }
        },
    });
});


$('body').on('click', '.btn-agregar-calidad', function (e) {
    e.preventDefault();
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/capituloiv/create-calidad');
    $('#modal').modal('show');
});


$("body").on("change", "#cap4_moda_p407_10", function (e) {
    e.preventDefault();
    $(".cap4_moda_p407_10").hide();
    $("#cap4_moda_p407_10_especifique").val("");
    if($("[id=\"cap4_moda_p407_10\"]:checked").val()){
        $(".cap4_moda_p407_10").show()
    }
});

$("body").on("change", "#cap4_moda_p408_9", function (e) {
    e.preventDefault();
    $(".cap4_moda_p408_9").hide();
    $("#cap4_moda_p408_9_especifique").val("");
    if($("[id=\"cap4_moda_p408_9\"]:checked").val()){
        $(".cap4_moda_p408_9").show()
    }
});

$("body").on("change", "#cap4_moda_p427_10", function (e) {
    e.preventDefault();
    $(".cap4_moda_p427_10").hide();
    $("#cap4_moda_p427_10_especifique").val("");
    if($("[id=\"cap4_moda_p427_10\"]:checked").val()){
        $(".cap4_moda_p427_10").show()
    }
});

$("body").on("change", "#cap4_moda_p402", function (e) {
    e.preventDefault();
    $(".cap4_moda_p402_9999").hide();
    $("#cap4_moda_p402_9999_especifique").val("");
    if($(this).val()=="00000009999"){
        $(".cap4_moda_p402_9999").show()
    }
});
</script>