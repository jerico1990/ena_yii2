

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Encuesta</h4>
                    <a class="btn btn-primary" href="<?= \Yii::$app->request->BaseUrl ?>/encuesta/index?geocodigo_fdo=<?= $geocodigo_fdo ?>">< Regresar</a>
                </div>
            </div>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Parcelas</li>
                </ol>
            </div>

        </div>
    </div>
</div>

<?php $form = ActiveForm::begin(['options' => ['id' => 'formCapituloIvModuloC']]); ?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">432.a N° Orden de lote</label>
                            <input type="text" class="form-control numerico" name="CapituloIVModuloC[cap4_modc_p432_a]" id="cap4_modc_p432_a" maxlength="12">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for=""> 432.b  ¿Cuáles son las especies que tuvo en el vivero, almácigo, semillero, huerto semillero, etc. Desde enero hasta 31 de diciembre de 2020?</label>
                            <select class="form-control" name="CapituloIVModuloC[cap4_modc_p432_b]" id="cap4_modc_p432_b">
                                <option value="99999">Seleccionar</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" >
                        <div class="form-group">
                            <label for="">433. ¿Cuántas plántulas o plantones de …..…..........  ha tenido durante el año 2020 en el vivero y/o semillero y/o huerto semillero o kilogramos de plántulas de almácigos?</label>
                        </div>
                    </div>
                </div>
                <div class="row " >
                    <div class="col-md-4" >
                        <div class="form-group">
                            <label for="">433a.  Cantidad </label>
                            <input type="text" class="form-control numerico" id="cap4_modc_p433_a" placeholder="Cantidad" name="CapituloIVModuloC[cap4_modc_p433_a]" maxlength="9">
                        </div>
                    </div>
                    <div class="col-md-4" >
                        <div class="form-group">
                            <label for="">433b. Unidad de medida </label>
                            <select class="form-control" name="CapituloIVModuloC[cap4_modc_p433_b]" id="cap4_modc_p433_b">
                                <option value="99999">Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4" >
                        <div class="form-group">
                            <label for="">433c. Equivalencia (kg) </label>
                            <input type="text" class="form-control numerico" id="cap4_modc_p433_c" placeholder="Equivalencia" name="CapituloIVModuloC[cap4_modc_p433_c]" maxlength="9">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">434. ¿Vendió plántulas o plantones del vivero y/o almácigo y/o semillero?</label>
                            <select name="CapituloIVModuloC[cap4_modc_p434]" id="cap4_modc_p434" class="form-control">
                                <option value>Seleccionar</option>
                                <option value="1">SI</option>
                                <option value="2">NO</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12" >
                        <div class="form-group">
                            <label for="">435. ¿Cuantas plántulas o plantones vendieron entre enero a diciembre del 2020?</label>
                        </div>
                    </div>
                </div>
                <div class="row " >
                    <div class="col-md-3" >
                        <div class="form-group">
                            <label for="">435a.  Cantidad </label>
                            <input type="text" class="form-control numerico" id="cap4_modc_p435_a" placeholder="Cantidad" name="CapituloIVModuloC[cap4_modc_p435_a]" maxlength="9">
                        </div>
                    </div>
                    <div class="col-md-3" >
                        <div class="form-group">
                            <label for="">435b. Unidad de medida </label>
                            <select class="form-control" name="CapituloIVModuloC[cap4_modc_p435_b]" id="cap4_modc_p435_b">
                                <option value="99999">Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3" >
                        <div class="form-group">
                            <label for="">435c. Equivalencia (kg) </label>
                            <input type="text" class="form-control numerico" id="cap4_modc_p435_c" placeholder="Equivalencia" name="CapituloIVModuloC[cap4_modc_p435_c]" maxlength="9">
                        </div>
                    </div>
                    <div class="col-md-3" >
                        <div class="form-group">
                            <label for="">435d. Número </label>
                            <input type="text" class="form-control numerico" id="cap4_modc_p435_d" placeholder="Número" name="CapituloIVModuloC[cap4_modc_p435_d]" maxlength="15">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12" >
                        <div class="form-group">
                            <label for="">436. ¿Cuál fue el valor por unidad de medida?</label>
                        </div>
                    </div>
                </div>
                <div class="row " >
                    <div class="col-md-6" >
                        <div class="form-group">
                            <label for="">436a. Soles por Unidad de medida ( kg )</label>

                            <input type="text" class="form-control numerico" id="cap4_modc_p436_a" placeholder="Cantidad" name="CapituloIVModuloC[cap4_modc_p436_a]" maxlength="5">
                        </div>
                    </div>
                    <div class="col-md-6" >
                        <div class="form-group">
                            <label for="">436b. Soles por plantón </label>
                            <input type="text" class="form-control numerico" id="cap4_modc_p436_b" placeholder="Soles por plantón" name="CapituloIVModuloC[cap4_modc_p436_b]" maxlength="5">
                        </div>
                    </div>
                   
                </div>
                
                
                <hr>
                <button type="button" class="btn btn-success btn-grabar-capitulo-iv-modulo-c">Grabar</button>

            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<?php ActiveForm::end(); ?>

<script>

var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var loading =   $('#staticBackdrop');

$("body").on("click", ".btn-grabar-capitulo-iv-modulo-c", function (e) {

    e.preventDefault();


    var form = $("#formCapituloIvModuloC");
    var formData = $("#formCapituloIvModuloC").serializeArray();



    if (form.find(".has-error").length) {
        return false;
    }

    $.ajax({
        url:form.attr("action"),
        type: form.attr("method"),
        data: formData,
        dataType: "json",
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        beforeSend:function()
        {
            loading.modal("show");
        },
        success: function (results) {
            if(results.success){
                window.location.href = "<?= \Yii::$app->request->BaseUrl ?>/capituloiv/update-capitulo-iv-modulo-c?num_orden_semillero=" + results.num_orden_semillero + "&id_encuesta=" + results.id_encuesta+"&geocodigo_fdo=<?= $geocodigo_fdo ?>";
                //loading.modal("hide");
            }
        },
    });
});

Cultivos();
async function Cultivos(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/cultivo/get-lista-cultivos',
                method: 'POST',
                data:{_csrf:csrf},
                dataType:'Json',
                beforeSend:function(){
                    loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var opciones_cultivos ="<option value='99999'>Seleccionar</option>";
                        $.each(results.cultivos, function( index, value ) {
                            opciones_cultivos = opciones_cultivos + "<option value='" + value.ID_CULTIVO + "'>" + value.TXT_CULTIVO + "</option>";
                        });
                        $("#cap4_modc_p432_b").html(opciones_cultivos);
                        
                        setTimeout(function(){ loading.modal("hide"); }, 2000);
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}
UnidadesMedidasPesos()
async function UnidadesMedidasPesos(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/unidad-medida/get-lista-unidades-medidas-pesos',
                method: 'POST',
                data:{_csrf:csrf},
                dataType:'Json',
                beforeSend:function(){
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var opciones_unidades_medidas ="<option value='99999'>Seleccionar</option>";
                        $.each(results.unidades_medidas, function( index, value ) {
                            
                            opciones_unidades_medidas = opciones_unidades_medidas + "<option value='" + value.ID_UM + "'>" + value.TXT_DESCRIPCION + "</option>";
                        });
                        $("#cap4_modc_p433_b").html(opciones_unidades_medidas);
                        $("#cap4_modc_p435_b").html(opciones_unidades_medidas);
                        
                        //loading.modal('hide');
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}
</script>