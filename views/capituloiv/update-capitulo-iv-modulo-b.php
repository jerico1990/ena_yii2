

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Encuesta</h4>
                    <a class="btn btn-primary" href="<?= \Yii::$app->request->BaseUrl ?>/encuesta/index?geocodigo_fdo=<?= $geocodigo_fdo ?>">< Regresar</a>
                </div>
            </div>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Parcelas</li>
                </ol>
            </div>

        </div>
    </div>
</div>

<?php $form = ActiveForm::begin(['options' => ['id' => 'formCapituloIvModuloB']]); ?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">428.a N° Orden de lote</label>
                            <input type="text" class="form-control" disabled value="<?= $cap_iv_modulo_b['NUM_ORDEN'] ?>">
                            <input type="hidden" class="form-control" name="CapituloIVModuloB[cap4_modb_p428_a]" id="cap4_modb_p428_a" value="<?= $cap_iv_modulo_b['NUM_ORDEN'] ?>">
                        </div>
                    </div>

                
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for=""> 428.b ¿Qué especies de árboles frutales, industriales y forestales tiene en esta parcela o chacra?</label>
                            <select class="form-control" name="CapituloIVModuloB[cap4_modb_p428_b]" id="cap4_modb_p428_b">
                                <option value="99999">Seleccionar</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row " >
                    <div class="col-md-6" >
                        <div class="form-group">
                            <label for="">429.  ¿Cuántas plantas tiene? </label>
                            <input type="text" class="form-control numerico" id="cap4_modb_p429" placeholder="¿Cuántas plantas tiene?" name="CapituloIVModuloB[cap4_modb_p429]" value="<?= $cap_iv_modulo_b['NUM_PLANTAS'] ?>" maxlength="12">
                        </div>
                    </div>
                    <div class="col-md-6" >
                        <div class="form-group">
                            <label for="">430. ¿Cuántas plantas están en produccion? </label>
                            <input type="text" class="form-control numerico" id="cap4_modb_p430" placeholder="¿Cuántas plantas están en produccion?" name="CapituloIVModuloB[cap4_modb_p430]" value="<?= $cap_iv_modulo_b['NUM_PLANTAS_PROD'] ?>" maxlength="12">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label for="">431.  ¿Cuáles fueron los meses de producción en el año 2020?</label>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_modb_p431_1" value="1" <?= (($cap_iv_modulo_b['FLG_ENE']=="1")?"checked":""); ?>>
                                    <label class="custom-control-label" for="cap4_modb_p431_1">Enero</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_modb_p431_2" value="2" <?= (($cap_iv_modulo_b['FLG_FEB']=="1")?"checked":""); ?>>
                                    <label class="custom-control-label" for="cap4_modb_p431_2">Febrero</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_modb_p431_3" value="3" <?= (($cap_iv_modulo_b['FLG_MAR']=="1")?"checked":""); ?>>
                                    <label class="custom-control-label" for="cap4_modb_p431_3">Marzo</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_modb_p431_4" value="4" <?= (($cap_iv_modulo_b['FLG_ABR']=="1")?"checked":""); ?>>
                                    <label class="custom-control-label" for="cap4_modb_p431_4">Abril</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_modb_p431_5" value="5" <?= (($cap_iv_modulo_b['FLG_MAY']=="1")?"checked":""); ?>>
                                    <label class="custom-control-label" for="cap4_modb_p431_5">Mayo</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_modb_p431_6" value="6" <?= (($cap_iv_modulo_b['FLG_JUN']=="1")?"checked":""); ?>>
                                    <label class="custom-control-label" for="cap4_modb_p431_6">Junio</label>
                                </div>

                            </div>

                            <div class="col-md-6">
                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_modb_p431_7" value="7" <?= (($cap_iv_modulo_b['FLG_JUL']=="1")?"checked":""); ?>>
                                    <label class="custom-control-label" for="cap4_modb_p431_7">Julio</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_modb_p431_8" value="8" <?= (($cap_iv_modulo_b['FLG_AGO']=="1")?"checked":""); ?>>
                                    <label class="custom-control-label" for="cap4_modb_p431_8">Agosto</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_modb_p431_9" value="9" <?= (($cap_iv_modulo_b['FLG_SET']=="1")?"checked":""); ?>>
                                    <label class="custom-control-label" for="cap4_modb_p431_9">Setiembre</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_modb_p431_10" value="10" <?= (($cap_iv_modulo_b['FLG_OCT']=="1")?"checked":""); ?>>
                                    <label class="custom-control-label" for="cap4_modb_p431_10">Octubre</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_modb_p431_11" value="11" <?= (($cap_iv_modulo_b['FLG_NOV']=="1")?"checked":""); ?>>
                                    <label class="custom-control-label" for="cap4_modb_p431_11">Noviembre</label>
                                </div>

                                <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                                    <input type="checkbox" class="custom-control-input" id="cap4_modb_p431_12" value="12" <?= (($cap_iv_modulo_b['FLG_DIC']=="1")?"checked":""); ?>>
                                    <label class="custom-control-label" for="cap4_modb_p431_12">Diciembre</label>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                
                <hr>
                <button class="btn btn-success btn-grabar-capitulo-iv-modulo-b">Grabar</button>

            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<?php ActiveForm::end(); ?>

<script>

var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var loading =   $('#staticBackdrop');

$("body").on("click", ".btn-grabar-capitulo-iv-modulo-b", function (e) {

    e.preventDefault();

    var cap4_modb_p431_1   = ($("[id=\"cap4_modb_p431_1\"]:checked").val())?1:0;
    var cap4_modb_p431_2   = ($("[id=\"cap4_modb_p431_2\"]:checked").val())?1:0;
    var cap4_modb_p431_3   = ($("[id=\"cap4_modb_p431_3\"]:checked").val())?1:0;
    var cap4_modb_p431_4   = ($("[id=\"cap4_modb_p431_4\"]:checked").val())?1:0;
    var cap4_modb_p431_5   = ($("[id=\"cap4_modb_p431_5\"]:checked").val())?1:0;
    var cap4_modb_p431_6   = ($("[id=\"cap4_modb_p431_6\"]:checked").val())?1:0;
    var cap4_modb_p431_7   = ($("[id=\"cap4_modb_p431_7\"]:checked").val())?1:0;
    var cap4_modb_p431_8   = ($("[id=\"cap4_modb_p431_8\"]:checked").val())?1:0;
    var cap4_modb_p431_9   = ($("[id=\"cap4_modb_p431_9\"]:checked").val())?1:0;
    var cap4_modb_p431_10   = ($("[id=\"cap4_modb_p431_10\"]:checked").val())?1:0;
    var cap4_modb_p431_11   = ($("[id=\"cap4_modb_p431_11\"]:checked").val())?1:0;
    var cap4_modb_p431_12   = ($("[id=\"cap4_modb_p431_12\"]:checked").val())?1:0;

    var form = $("#formCapituloIvModuloB");
    var formData = $("#formCapituloIvModuloB").serializeArray();

    /* Seteando variables del capitulo IV Modulo A */
    formData.push({name: "CapituloIVModuloB[cap4_modb_p431_1]", value: cap4_modb_p431_1});
    formData.push({name: "CapituloIVModuloB[cap4_modb_p431_2]", value: cap4_modb_p431_2});
    formData.push({name: "CapituloIVModuloB[cap4_modb_p431_3]", value: cap4_modb_p431_3});
    formData.push({name: "CapituloIVModuloB[cap4_modb_p431_4]", value: cap4_modb_p431_4});
    formData.push({name: "CapituloIVModuloB[cap4_modb_p431_5]", value: cap4_modb_p431_5});
    formData.push({name: "CapituloIVModuloB[cap4_modb_p431_6]", value: cap4_modb_p431_6});
    formData.push({name: "CapituloIVModuloB[cap4_modb_p431_7]", value: cap4_modb_p431_7});
    formData.push({name: "CapituloIVModuloB[cap4_modb_p431_8]", value: cap4_modb_p431_8});
    formData.push({name: "CapituloIVModuloB[cap4_modb_p431_9]", value: cap4_modb_p431_9});
    formData.push({name: "CapituloIVModuloB[cap4_modb_p431_10]", value: cap4_modb_p431_10});
    formData.push({name: "CapituloIVModuloB[cap4_modb_p431_11]", value: cap4_modb_p431_11});
    formData.push({name: "CapituloIVModuloB[cap4_modb_p431_12]", value: cap4_modb_p431_12});


    if (form.find(".has-error").length) {
        return false;
    }

    $.ajax({
        url:form.attr("action"),
        type: form.attr("method"),
        data: formData,
        dataType: "json",
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        beforeSend:function()
        {
            loading.modal("show");
        },
        success: function (results) {
            if(results.success){
                setTimeout(function(){ loading.modal("hide"); }, 2000);
            }
        },
    });
});

var cap4_modb_p428_b = "<?= $cap_iv_modulo_b['ID_ARBOLES_FRUTALES'] ?>";
Cultivos(cap4_modb_p428_b);
async function Cultivos(cultivo_id){
    console.log(cultivo_id);
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/cultivo/get-lista-cultivos',
                method: 'POST',
                data:{_csrf:csrf},
                dataType:'Json',
                beforeSend:function(){
                    loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var opciones_cultivos ="<option value='99999'>Seleccionar</option>";
                        $.each(results.cultivos, function( index, value ) {
                            opciones_cultivos = opciones_cultivos + "<option value='" + value.ID_CULTIVO + "'>" + value.TXT_CULTIVO + "</option>";
                        });
                        $("#cap4_modb_p428_b").html(opciones_cultivos);
                        if(cultivo_id){
                            cultivo_id = ("00000000000" + cultivo_id).substr(-11,11);
                            $("#cap4_modb_p428_b").val(cultivo_id);
                        }
                        setTimeout(function(){ loading.modal("hide"); }, 2000);
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}


</script>