<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(['options' => ['id' => 'formCalidad']]); ?>

    <div class="modal-header">
        <h4 class="modal-title">Registro de calidad</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">423. ¿Cuál fue la producción entre enero y diciembre de 2020 por calidades? </label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">423.a Nombre del tipo de calidad</label>
                    <input type="text" class="form-control" name="CapituloIVModuloA[cap4_moda_p423_a]" id="cap4_moda_p423_a" maxlength="250">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">423.b Cantidad cosechada por calidad</label>
                    <input type="text" class="form-control numerico" name="CapituloIVModuloA[cap4_moda_p423_b]" id="cap4_moda_p423_b" maxlength="12">
                </div>
            </div>
            
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">423.c Unidad de medida (Peso)</label>
                    <select class="form-control" name="CapituloIVModuloA[cap4_moda_p423_c]" id="cap4_moda_p423_c">
                        <option value>Seleccionar</option>
                    </select>
                </div>
            </div>
        </div>

        
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">423.d Equivalencia (Kg)</label>
                    <input type="text" class="form-control numerico" name="CapituloIVModuloA[cap4_moda_p423_d]" id="cap4_moda_p423_d" maxlength="12">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <label for="">424. ¿El destino de la producción fue: </label>
                <div class="row">
                    <div class="col-md-6">
                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="cap4_moda_p424_1" >
                            <label class="custom-control-label" for="cap4_moda_p424_1">1. ¿Venta directa al consumidor? </label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="cap4_moda_p424_2" >
                            <label class="custom-control-label" for="cap4_moda_p424_2">2. ¿Venta a intermediario? </label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="cap4_moda_p424_3" >
                            <label class="custom-control-label" for="cap4_moda_p424_3">3. ¿Venta a mayorista?</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="cap4_moda_p424_4" >
                            <label class="custom-control-label" for="cap4_moda_p424_4">4. ¿Venta a una bioFeria? </label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="cap4_moda_p424_5" >
                            <label class="custom-control-label" for="cap4_moda_p424_5">5. ¿Venta con valor agregado?  </label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="cap4_moda_p424_6" >
                            <label class="custom-control-label" for="cap4_moda_p424_6">6. ¿Autoconsumo? </label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="cap4_moda_p424_7" >
                            <label class="custom-control-label" for="cap4_moda_p424_7">7. ¿Semilla?</label>
                        </div>

                    </div>

                    <div class="col-md-6">
                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="cap4_moda_p424_8" >
                            <label class="custom-control-label" for="cap4_moda_p424_8">8. ¿Consumo de animales? </label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="cap4_moda_p424_9" >
                            <label class="custom-control-label" for="cap4_moda_p424_9">9. ¿Fertilización de suelos?</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="cap4_moda_p424_10" >
                            <label class="custom-control-label" for="cap4_moda_p424_10">10. ¿Intercambio o trueque?</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="cap4_moda_p424_11" >
                            <label class="custom-control-label" for="cap4_moda_p424_11">11. ¿Para la exportación?</label>
                        </div>
                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="cap4_moda_p424_12" >
                            <label class="custom-control-label" for="cap4_moda_p424_12">12. ¿Insumo de la agroindustria?</label>
                        </div>
                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="cap4_moda_p424_13" >
                            <label class="custom-control-label" for="cap4_moda_p424_13">13. ¿Otro?:</label>
                        </div>


                    </div>
                </div>
            </div>
        </div>

        <div class="row cap4_moda_p424_13" style="display:none">
            <div class="col-md-6" >
                <div class="form-group">
                    <label for="">Especifique</label>
                    <input type="text" class="form-control" id="cap4_moda_p424_13_especifique" placeholder="Especifique" name="CapituloIVModuloA[cap4_moda_p424_13_especifique]" maxlength="500">
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">425. ¿Cuánto de la producción se destinó para venta? </label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">425.a Cantidad</label>
                    <input type="text" class="form-control numerico" name="CapituloIVModuloA[cap4_moda_p425_a]" id="cap4_moda_p425_a" maxlength="12">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">425.b Unidad de medida</label>
                    <select class="form-control" name="CapituloIVModuloA[cap4_moda_p425_b]" id="cap4_moda_p425_b">
                        <option value>Seleccionar</option>
                    </select>
                </div>
            </div>
            
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">425.c Especificación del producto</label>
                    <select class="form-control" name="CapituloIVModuloA[cap4_moda_p425_c]" id="cap4_moda_p425_c">
                        <option value>Seleccionar</option>
                    </select>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">425.d Precio de unidad (S/ x kg) (S/ x t)</label>
                    <input type="text" class="form-control numerico" name="CapituloIVModuloA[cap4_moda_p425_d]" id="cap4_moda_p425_d" maxlength="12">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">425.d Moneda</label>
                    <select class="form-control" name="CapituloIVModuloA[cap4_moda_p425_d_moneda]" id="cap4_moda_p425_d_moneda">
                        <option value>Seleccionar</option>
                        <option value="Soles">Soles</option>
                        <option value="Dolar">Dolar</option>
                    </select>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <label for="">426a. ¿A quién vendió la producción: </label>
                <div class="row">
                    <div class="col-md-6">
                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="cap4_moda_p426_a_1" >
                            <label class="custom-control-label" for="cap4_moda_p426_a_1">1. Consumidor?</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="cap4_moda_p426_a_2" >
                            <label class="custom-control-label" for="cap4_moda_p426_a_2">2. Intermediario?</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="cap4_moda_p426_a_3" >
                            <label class="custom-control-label" for="cap4_moda_p426_a_3">3. Mayorista?</label>
                        </div>


                    </div>

                    <div class="col-md-6">
                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="cap4_moda_p426_a_4" >
                            <label class="custom-control-label" for="cap4_moda_p426_a_4">4. Bioferia?</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="cap4_moda_p426_a_5" >
                            <label class="custom-control-label" for="cap4_moda_p426_a_5">5. Empresa/agroindustria?</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="cap4_moda_p426_a_6" >
                            <label class="custom-control-label" for="cap4_moda_p426_a_6">6. Otro? </label>
                        </div>


                    </div>
                </div>
            </div>
        </div>

        <div class="row cap4_moda_p426_a_6" style="display:none">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Especifique</label>
                    <input type="text" class="form-control" id="cap4_moda_p426_a_6_especifique" placeholder="Especifique" name="CapituloIVModuloA[cap4_moda_p426_a_6_especifique]" maxlength="500">
                </div>
            </div>
        </div>



        <div class="row">
            <div class="col-md-12">
                <label for="">426b. ¿Cuál fue el punto de venta?</label>
                <div class="row">
                    <div class="col-md-6">
                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="cap4_moda_p426_b_1" >
                            <label class="custom-control-label" for="cap4_moda_p426_b_1">1. ¿En la parcela? (cosechado)</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="cap4_moda_p426_b_2" >
                            <label class="custom-control-label" for="cap4_moda_p426_b_2">2. ¿En el lote? (cultivo en pie)</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="cap4_moda_p426_b_3" >
                            <label class="custom-control-label" for="cap4_moda_p426_b_3">3. ¿En el mercado local/regional?</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="cap4_moda_p426_b_4" >
                            <label class="custom-control-label" for="cap4_moda_p426_b_4">4. ¿En el mercado de Lima?</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="cap4_moda_p426_b_5" >
                            <label class="custom-control-label" for="cap4_moda_p426_b_5">5.  ¿Venta en la industria?</label>
                        </div>


                    </div>

                    <div class="col-md-6">
                        
                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="cap4_moda_p426_b_6" >
                            <label class="custom-control-label" for="cap4_moda_p426_b_6">6. ¿En la cooperativa?</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="cap4_moda_p426_b_7" >
                            <label class="custom-control-label" for="cap4_moda_p426_b_7">7. ¿En la Bioferia?</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="cap4_moda_p426_b_8" >
                            <label class="custom-control-label" for="cap4_moda_p426_b_8">8. ¿En la asociación de productores?</label>
                        </div>

                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
                            <input type="checkbox" class="custom-control-input" id="cap4_moda_p426_b_9" >
                            <label class="custom-control-label" for="cap4_moda_p426_b_9">9. ¿Otro?:</label>
                        </div>


                    </div>
                </div>
            </div>
        </div>

        <div class="row cap4_moda_p426_b_9" style="display:none">
            <div class="col-md-6" >
                <div class="form-group">
                    <label for="">Especifique</label>
                    <input type="text" class="form-control" id="cap4_moda_p426_b_9_especifique" placeholder="Especifique" name="CapituloIVModuloA[cap4_moda_p426_b_9_especifique]" maxlength="500">
                </div>
            </div>
        </div>

        
    </div>
    <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary btn-grabar-calidad">Grabar</button>
    </div>

<?php ActiveForm::end(); ?>